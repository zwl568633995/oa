
drop DATABASE if exists db_recruit;

CREATE DATABASE db_recruit DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

use db_recruit;

drop table if exists t_dictionary;
create table t_dictionary
(
   id                   int not null auto_increment comment '主键id',
   type                 varchar(32) comment '类型',
   dict_name            varchar(32) comment '页面显示名',
   dict_value           varchar(32) comment '页面显示名对应的数据库值',
   order_index          integer(1),
   is_delete            integer(1) default 0 comment '是否删除(0:否;1:是)',
   adder                varchar(32) comment '添加人',
   add_time             timestamp default CURRENT_TIMESTAMP comment '添加时间',
   moder                varchar(32) comment '修改人',
   mod_time             timestamp default CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table t_dictionary comment '数据字典表';

INSERT INTO `t_dictionary` VALUES ('1', 'department', '001-总经办', '001', '1', '0', 'admin', '2018-06-27 14:29:26', null, '2018-06-27 14:29:26');
INSERT INTO `t_dictionary` VALUES ('2', 'department', '002-财务部', '002', '2', '0', 'admin', '2018-06-27 14:29:35', null, '2018-06-27 14:29:35');
INSERT INTO `t_dictionary` VALUES ('3', 'department', '003-人事行政部', '003', '3', '0', 'admin', '2018-06-27 14:29:44', null, '2018-06-27 14:29:44');
INSERT INTO `t_dictionary` VALUES ('4', 'department', '004-技术部', '004', '4', '0', 'admin', '2018-06-27 14:29:50', null, '2018-06-27 14:29:50');
INSERT INTO `t_dictionary` VALUES ('5', 'department', '005-采购部', '005', '5', '0', 'admin', '2018-06-27 14:29:56', null, '2018-06-27 14:29:56');
INSERT INTO `t_dictionary` VALUES ('6', 'department', '006-物流部', '006', '6', '0', 'admin', '2018-06-27 14:30:04', null, '2018-06-27 14:30:04');
INSERT INTO `t_dictionary` VALUES ('7', 'department', '007-客服部', '007', '7', '0', 'admin', '2018-06-27 14:30:09', null, '2018-06-27 14:30:09');
INSERT INTO `t_dictionary` VALUES ('8', 'department', '008-网站运营部', '008', '8', '0', 'admin', '2018-06-27 14:30:17', null, '2018-06-27 14:30:17');
INSERT INTO `t_dictionary` VALUES ('9', 'department', '009-天猫运营不', '009', '9', '0', 'admin', '2018-06-27 14:30:36', null, '2018-06-27 14:30:36');
INSERT INTO `t_dictionary` VALUES ('10', 'department', '010-批发部', '010', '10', '0', 'admin', '2018-06-27 14:30:43', null, '2018-06-27 14:30:43');
INSERT INTO `t_dictionary` VALUES ('11', 'department', '011-加工室', '011', '11', '0', 'admin', '2018-06-27 14:30:56', null, '2018-06-27 14:30:56');
INSERT INTO `t_dictionary` VALUES ('12', 'department', '012-框架眼镜事业部', '012', '12', '0', 'admin', '2018-06-27 14:31:08', null, '2018-06-27 14:31:08');
INSERT INTO `t_dictionary` VALUES ('13', 'department', '013-商品部', '013', '13', '0', 'admin', '2018-06-27 14:31:15', null, '2018-06-27 14:31:15');
INSERT INTO `t_dictionary` VALUES ('14', 'department', '014-TP部', '014', '14', '0', 'admin', '2018-06-27 14:31:30', null, '2018-06-27 14:31:30');
INSERT INTO `t_dictionary` VALUES ('15', 'jobStatus', '已创建', '已创建', '1', '0', 'admin', '2018-06-27 14:44:23', null, '2018-06-27 14:44:23');
INSERT INTO `t_dictionary` VALUES ('16', 'jobStatus', '已作废', '已作废', '2', '0', 'admin', '2018-06-27 14:44:12', null, '2018-06-27 14:44:12');
INSERT INTO `t_dictionary` VALUES ('17', 'jobStatus', '待审核', '待审核', '3', '0', 'admin', '2018-06-27 14:43:54', null, '2018-06-27 14:43:54');
INSERT INTO `t_dictionary` VALUES ('18', 'jobStatus', '返回修改', '返回修改', '4', '0', 'admin', '2018-06-27 14:41:11', null, '2018-06-27 14:41:11');
INSERT INTO `t_dictionary` VALUES ('19', 'jobStatus', '审核拒绝', '审核拒绝', '5', '0', 'admin', '2018-06-27 14:41:14', null, '2018-06-27 14:41:14');
INSERT INTO `t_dictionary` VALUES ('20', 'jobStatus', '招聘中', '招聘中', '6', '0', 'admin', '2018-06-27 14:41:18', null, '2018-06-27 14:41:18');
INSERT INTO `t_dictionary` VALUES ('21', 'jobStatus', '招聘暂停', '招聘暂停', '7', '0', 'admin', '2018-06-27 14:41:22', null, '2018-06-27 14:41:22');
INSERT INTO `t_dictionary` VALUES ('22', 'jobStatus', '招聘关闭', '招聘关闭', '8', '0', 'admin', '2018-06-27 14:41:27', null, '2018-06-27 14:41:27');
INSERT INTO `t_dictionary` VALUES ('23', 'jobStatus', '招聘完成', '招聘完成', '9', '0', 'admin', '2018-06-27 14:41:36', null, '2018-06-27 14:41:36');
INSERT INTO `t_dictionary` VALUES ('24', 'applicationReasons', '辞职补充', '辞职补充', '1', '0', 'admin', '2018-06-27 18:12:36', null, '2018-06-27 18:12:36');
INSERT INTO `t_dictionary` VALUES ('25', 'applicationReasons', '扩大编制', '扩大编制', '2', '0', 'admin', '2018-06-27 18:12:56', null, '2018-06-27 18:12:56');
INSERT INTO `t_dictionary` VALUES ('26', 'applicationReasons', '短期需求', '短期需求', '3', '0', 'admin', '2018-06-27 18:13:01', null, '2018-06-27 18:13:01');
INSERT INTO `t_dictionary` VALUES ('27', 'applicationReasons', '储备人才', '储备人才', '4', '0', 'admin', '2018-06-27 18:13:02', null, '2018-06-27 18:13:02');
INSERT INTO `t_dictionary` VALUES ('28', 'sex', '男', '男', '1', '0', 'admin', '2018-06-27 18:13:34', null, '2018-06-27 18:13:34');
INSERT INTO `t_dictionary` VALUES ('29', 'sex', '女', '女', '2', '0', 'admin', '2018-06-27 18:13:42', null, '2018-06-27 18:13:42');
INSERT INTO `t_dictionary` VALUES ('30', 'sex', '不限', '不限', '3', '0', 'admin', '2018-06-27 18:15:19', null, '2018-06-27 18:15:19');
INSERT INTO `t_dictionary` VALUES ('31', 'maritalStatus', '未婚', '未婚', '1', '0', 'admin', '2018-06-27 18:18:35', null, '2018-06-27 18:18:35');
INSERT INTO `t_dictionary` VALUES ('32', 'maritalStatus', '已婚', '已婚', '2', '0', 'admin', '2018-06-27 18:18:36', null, '2018-06-27 18:18:36');
INSERT INTO `t_dictionary` VALUES ('33', 'maritalStatus', '不限', '不限', '3', '0', 'admin', '2018-06-27 18:18:43', null, '2018-06-27 18:18:43');
INSERT INTO `t_dictionary` VALUES ('34', 'education', '硕士及以上', '1', '1', '0', 'admin', '2018-06-27 18:19:18', null, '2018-06-27 18:19:18');
INSERT INTO `t_dictionary` VALUES ('35', 'education', '硕士', '2', '2', '0', 'admin', '2018-06-27 18:19:22', null, '2018-06-27 18:19:22');
INSERT INTO `t_dictionary` VALUES ('36', 'education', '本科及以上', '3', '3', '0', 'admin', '2018-06-27 18:19:26', null, '2018-06-27 18:19:26');
INSERT INTO `t_dictionary` VALUES ('37', 'education', '本科', '4', '4', '0', 'admin', '2018-06-27 18:19:31', null, '2018-06-27 18:19:31');
INSERT INTO `t_dictionary` VALUES ('38', 'education', '大专及以上', '5', '5', '0', 'admin', '2018-06-27 18:19:36', null, '2018-06-27 18:19:36');
INSERT INTO `t_dictionary` VALUES ('39', 'education', '大专', '6', '6', '0', 'admin', '2018-06-27 18:19:40', null, '2018-06-27 18:19:40');
INSERT INTO `t_dictionary` VALUES ('40', 'education', '中专及以上', '7', '7', '0', 'admin', '2018-06-27 18:19:46', null, '2018-06-27 18:19:46');
INSERT INTO `t_dictionary` VALUES ('41', 'education', '中专', '8', '8', '0', 'admin', '2018-06-27 18:19:51', null, '2018-06-27 18:19:51');
INSERT INTO `t_dictionary` VALUES ('42', 'foreignLanguageLevel', '无要求', '无要求', '1', '0', 'admin', '2018-06-27 18:21:03', null, '2018-06-27 18:21:03');
INSERT INTO `t_dictionary` VALUES ('43', 'foreignLanguageLevel', '英语四级', '英语四级', '2', '0', 'admin', '2018-06-27 18:21:09', null, '2018-06-27 18:21:09');
INSERT INTO `t_dictionary` VALUES ('44', 'foreignLanguageLevel', '英语六级', '英语六级', '3', '0', 'admin', '2018-06-27 18:21:18', null, '2018-06-27 18:21:18');
INSERT INTO `t_dictionary` VALUES ('45', 'workAddress', '江苏省南京市秦淮区光华科技产业园A幢9楼', '1', '1', '0', 'admin', '2018-06-27 18:21:41', null, '2018-06-27 18:21:41');
INSERT INTO `t_dictionary` VALUES ('46', 'workAddress', '江苏省南京市秦淮区光华科技产业园A幢5楼', '2', '2', '0', 'admin', '2018-06-27 18:21:46', null, '2018-06-27 18:21:46');
INSERT INTO `t_dictionary` VALUES ('47', 'workAddress', '江苏省南京市秦淮区光华科技产业园A幢4楼', '3', '3', '0', 'admin', '2018-06-27 18:21:58', null, '2018-06-27 18:21:58');
INSERT INTO `t_dictionary` VALUES ('48', 'workAddress', '江苏省南京市江宁区湖熟镇波光路30号', '4', '4', '0', 'admin', '2018-06-27 18:21:59', null, '2018-06-27 18:21:59');
INSERT INTO `t_dictionary` VALUES ('49', 'resumeSource', '拉勾网', '拉勾网', '1', '0', null, '2018-07-05 17:24:10', null, '2018-07-05 17:24:10');
INSERT INTO `t_dictionary` VALUES ('50', 'resumeSource', 'BOSS直聘', 'BOSS直聘', '2', '0', null, '2018-07-05 17:24:20', null, '2018-07-05 17:24:20');
INSERT INTO `t_dictionary` VALUES ('51', 'resumeSource', '智联招聘', '智联招聘', '3', '0', null, '2018-07-05 17:24:30', null, '2018-07-05 17:24:30');
INSERT INTO `t_dictionary` VALUES ('52', 'resumeSource', '51job', '51job', '4', '0', null, '2018-07-05 17:24:39', null, '2018-07-05 17:24:39');
INSERT INTO `t_dictionary` VALUES ('53', 'resumeSource', '实习僧', '实习僧', '5', '0', null, '2018-07-05 17:24:45', null, '2018-07-05 17:24:45');
INSERT INTO `t_dictionary` VALUES ('54', 'resumeSource', '58同城', '58同城', '6', '0', null, '2018-07-05 17:24:53', null, '2018-07-05 17:24:53');
INSERT INTO `t_dictionary` VALUES ('55', 'resumeSource', '猎头推荐', '猎头推荐', '7', '0', null, '2018-07-05 17:24:58', null, '2018-07-05 17:24:58');
INSERT INTO `t_dictionary` VALUES ('56', 'resumeSource', '校园招聘', '校园招聘', '8', '0', null, '2018-07-05 17:24:59', null, '2018-07-05 17:24:59');
INSERT INTO `t_dictionary` VALUES ('57', 'resumeSource', '内推', '内推', '9', '0', null, '2018-07-05 17:25:08', null, '2018-07-05 17:25:08');

drop table if exists t_recruit_job_info;
create table t_recruit_job_info
(
   id                   int not null auto_increment comment '主键',
   job_code             varchar(16) comment '职位编号(ZP + yyyymmdd + 00)',
   job_status           varchar(16) comment '职位状态(已创建;已作废;待审核;返回修改;审核拒绝;招聘中;招聘暂停;招聘关闭;招聘完成)',
   application_Department varchar(4) comment '申请部门',
   job_name             varchar(64) comment '招聘职位',
   job_score            integer comment '职位评分',
   application_reasons  varchar(8) comment '申请原因(辞职补充;扩大编制; 短期需求;储备人才)',
   number_of_people     integer(2) comment '需求人数',
   apply_people         varchar(12) comment '申请人',
   apply_time           timestamp comment '申请时间',
   auditor              varchar(12) comment '审核人',
   audit_time           timestamp comment '审核时间',
   sex                  varchar(4) comment '性别(男;女;不限)',
   marital_status       varchar(4) comment '婚姻状况(未婚;已婚;不限)',
   age                  varchar(32) comment '年龄',
   education            integer comment '学历(1:硕士及以上;2:硕士;3:本科及以上;4:本科;5:大专及以上;6:大专;7:中专及以上;8中专)',
   major                varchar(32) comment '专业',
   foreign_language_level varchar(12) comment '外语水平(无要求;英语四级;英语六级)',
   salary_range         varchar(16) comment '薪资范围',
   expected_date        date comment '期望到岗日期',
   work_address         integer(2) comment '工作地点(1:江苏省南京市秦淮区光华科技产业园A幢9楼;2:江苏省南京市秦淮区光华科技产业园A幢5楼;3:江苏省南京市秦淮区光华科技产业园A幢4楼;4:江苏省南京市江宁区湖熟镇波光路30号)',
   qualification        varchar(1800) comment '任职资格',
   post_duties          varchar(1800) comment '岗位职责',
   first_interviewer    varchar(32) comment '初试面试官',
   second_interviewer   varchar(32) comment '复试面试官',
   audit_opinion        varchar(150) comment '审核意见',
   adder                varchar(32) comment '添加人',
   add_time             timestamp default CURRENT_TIMESTAMP comment '添加时间',
   moder                varchar(32) comment '修改人',
   mod_time             timestamp default CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table t_recruit_job_info comment '职位表';

