/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InitializationBean.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 上午11:54:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit;

import java.util.HashMap;
import java.util.Map;

/**
 * 业务系统初始化类<br> 
 *
 * @author zhixiang.meng
 */
public class InitializationBean {
    // 系统初始化
    public static Map<String, Object> initMap = new HashMap<String, Object>();
    // activiti 最新流程id和流程名对应关系 用来提高后续访问效率
    public static Map<String, Object> activitiMap = new HashMap<String, Object>();
}
