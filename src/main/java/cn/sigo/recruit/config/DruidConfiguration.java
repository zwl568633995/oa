package cn.sigo.recruit.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DruidConfiguration {
	
	@Bean
    public ServletRegistrationBean<StatViewServlet> statVeiwServlet(){
        // 注册servlet实体类
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<StatViewServlet>(new StatViewServlet(), "/druid/*");
        // 设备白名单
        bean.addInitParameter("allow","127.0.0.1");
        // 设置黑名单，如果allow 与 deny共同存在时，deny优先于allow
        bean.addInitParameter("deny","192.168.1.110");
        // 设置控制台管理用户
        bean.addInitParameter("loginUsername","druid");
        bean.addInitParameter("loginPassword","123456");
        // 是否可以重置数据
        bean.addInitParameter("resetEnable","false");
        return bean;
    }

    @Bean
    public FilterRegistrationBean<WebStatFilter> statFilter() {
        // 创建过滤器
        FilterRegistrationBean<WebStatFilter> bean = new FilterRegistrationBean<WebStatFilter>(new WebStatFilter());
        // 设置过滤器过滤路径
        bean.addUrlPatterns("/*");
        // 忽略过滤的形式
        bean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return bean;
    }

}
