/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: PropertiesListener.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午2:22:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.config;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class PropertiesListener implements ApplicationListener<ApplicationStartedEvent> {
    
    private String propertyFileName;
    
    public PropertiesListener(String propertyFileName) {
        this.propertyFileName = propertyFileName;
    }

    /* (non-Javadoc)
     * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
     */
    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        PropertiesListenerConfig.loadAllProperties(propertyFileName);
    }

}
