/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: PropertiesListenerConfig.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午2:23:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import cn.sigo.recruit.InitializationBean;

/**
 * 解析properties文件
 *
 * @author zhixiang.meng
 */
public class PropertiesListenerConfig {

    private static void processProperties(Properties props) throws BeansException {
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            try {
                // PropertiesLoaderUtils的默认编码是ISO-8859-1,在这里转码一下
                InitializationBean.initMap.put(keyStr,
                        new String(props.getProperty(keyStr).getBytes("ISO-8859-1"), "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (java.lang.Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void loadAllProperties(String propertyFileName) {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties(propertyFileName);
            processProperties(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
