package cn.sigo.recruit.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "sigo.role")
public class RoleConfig {
   private Integer roleId;
}
