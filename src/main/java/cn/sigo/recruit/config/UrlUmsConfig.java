package cn.sigo.recruit.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class UrlUmsConfig {

   @Value("${url.ums.saveUser}")
   private String saveUser;

   @Value("${url.ums.addSubTeamFromHr}")
   private String addSubTeamFromHr;

   @Value("${http://localhost:8080/team/editTeam}")
   private String editTeam;

}
