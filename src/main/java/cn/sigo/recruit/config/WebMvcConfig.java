package cn.sigo.recruit.config;

import cn.sigo.recruit.InitializationBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.io.File;

/**
 * @Auther: DELL
 * @Date: 2018/8/13 13:34
 * @Description:
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Value("${pdf.path}")
    private String pdfPath;

    @Value("${doc.path}")
    private String docPath;

    @Value("${html.path}")
    private String htmlPath;

    @Value("${image.path}")
    private String imagePath;

    @Value("${pdf.download}")
    private String pdfDownLoad;

    @Value("${doc.download}")
    private String docDownLoad;

    @Value("${html.download}")
    private String htmlDownLoad;

    @Value("${image.download}")
    private String imageDownLoad;

    @Value("${user.login.appkey}")
    private String erpAppKey;

    @Value("${user.login.appsecret}")
    private String erpAppSecret;

    @Value("${user.login.url}")
    private String erpUrl;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        //初始化erp连接信息
        InitializationBean.initMap.put("user.login.appkey",erpAppKey);
        InitializationBean.initMap.put("user.login.appsecret",erpAppSecret);
        InitializationBean.initMap.put("user.login.url",erpUrl);

        File pdf = new File(pdfPath);
        File doc = new File(docPath);
        File image = new File(imagePath);
        File html = new File(htmlPath);
        if (!pdf.exists()) {
            pdf.mkdirs();
        }
        if (!doc.exists()) {
            doc.mkdirs();
        }
        if (!image.exists()) {
            image.mkdirs();
        }
        if (!html.exists()) {
            html.mkdirs();
        }

        registry
                .addResourceHandler(pdfDownLoad+"**")
                .addResourceLocations("file:" + pdfPath);
        registry
                .addResourceHandler(docDownLoad+"**")
                .addResourceLocations("file:" + docPath);
        registry
                .addResourceHandler(htmlDownLoad+"**")
                .addResourceLocations("file:" + htmlPath);
        registry
                .addResourceHandler(imageDownLoad+"**")
                .addResourceLocations("file:" + imagePath);
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");


    }
}
