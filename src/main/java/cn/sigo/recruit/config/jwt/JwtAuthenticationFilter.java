/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JwtAuthenticationFilter.java
 * Author:   zhixiang.meng
 * Date:     2018年6月28日 下午5:04:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.config.jwt;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final PathMatcher pathMatcher = new AntPathMatcher();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        try {
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
            response.setHeader("Access-Control-Max-Age", "3600");
            response.setHeader("Access-Control-Allow-Headers",
                    "Content-Type, Accept, X-Requested-With, remember-me, Authorization, x-auth-token");

            if (isProtectedUrl(request) && !request.getMethod().equals("OPTIONS")) {
                request = JwtUtil.validateTokenAndAddUserIdToHeader(request);
            }
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
            return;

        }
        filterChain.doFilter(request, response);
    }

    private boolean isProtectedUrl(HttpServletRequest request) {
        return !(pathMatcher.match("/login", request.getServletPath())
                || pathMatcher.match("/index", request.getServletPath())
                || pathMatcher.match("/**/swagger-ui.html", request.getServletPath())
                || pathMatcher.match("/v2/api-docs", request.getServletPath())
                || pathMatcher.match("/swagger-resources/**", request.getServletPath())
                || pathMatcher.match("/webjars/**", request.getServletPath())
                || pathMatcher.match("/download/**", request.getServletPath())
        );
    }
}
