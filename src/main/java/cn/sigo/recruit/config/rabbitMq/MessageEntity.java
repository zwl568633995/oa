package cn.sigo.recruit.config.rabbitMq;

import java.io.Serializable;

/**
 * 消息实体
 */
@SuppressWarnings("serial")
public class MessageEntity implements Serializable {
    /**
     * 消息内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
