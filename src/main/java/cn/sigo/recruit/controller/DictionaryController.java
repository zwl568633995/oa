/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: DictionaryController.java
 * Author:   zhixiang.meng
 * Date:     2018年6月28日 上午10:00:52
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.service.DictionaryService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 数据字典相关操作<br>
 * 
 * 
 *
 * @author zhixiang.meng
 */
@RestController
@RequestMapping("/dictionary")
public class DictionaryController {

    private final static Logger LOGGER = LoggerFactory.getLogger(DictionaryController.class);
    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 
     * 新增数据字典信息
     *
     * @param request
     * @param dictionary
     * @return
     */
    @RequestMapping(value = "/add", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel addSave(HttpServletRequest request,@RequestBody Dictionary dictionary) {
        ResponseModel model = new ResponseModel();
        // 查询出用户名
        String userName = (String)request.getAttribute("userName");
        if(StringUtils.isNotBlank(userName)) {
            dictionary.setAdder(userName);
        }
        // 保存到数据库 并保存到redis缓存里面
        boolean result = dictionaryService.saveDictionary(dictionary, true);
        if(result) {
            model.setResultCode("0000");
        } else {
            model.setResultCode("0003");
            model.setResultMsg("异常！数据入库失败");
        }
        return model;
    }
}
