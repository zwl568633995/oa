package cn.sigo.recruit.controller;

import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.util.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Auther: DELL
 * @Date: 2018/8/13 12:01
 * @Description:
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @Value("${pdf.path}")
    private String pdfPath;

    @Value("${doc.path}")
    private String docPath;

    @Value("${html.path}")
    private String htmlPath;

    @Value("${image.path}")
    private String imagePath;

    @Value("${pdf.download}")
    private String pdfDownLoad;

    @Value("${doc.download}")
    private String docDownLoad;

    @Value("${html.download}")
    private String htmlDownLoad;

    @Value("${image.download}")
    private String imageDownLoad;

    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public ResponseModel upload(@RequestParam(value = "file",required = true) MultipartFile file,
                                @RequestParam(value = "type",required = true) String type
    ) throws IOException {
        ResponseModel model = new ResponseModel();
        String fileName = file.getOriginalFilename();
        if(fileName.endsWith(".docx")||fileName.endsWith(".doc")){
//            FileUtils.uploadFile(file.getBytes(),docPath,fileName);
//            model.setData(docDownLoad+fileName);
//            model.setResultCode("0000");
//            return model;
            model.setResultCode("0004");
            model.setResultMsg("暂不支持Word上传");
            return model;
        }
        if(fileName.endsWith(".pdf")){
            fileName = fileName.replace("_","");
            FileUtils.uploadFile(file.getBytes(),pdfPath,fileName);
            model.setData(pdfDownLoad+fileName);
            model.setResultCode("0000");
            return model;
        }
        if("html".equals(type)){
            FileUtils.uploadFile(file.getBytes(),htmlPath,fileName);
            model.setData(htmlDownLoad+fileName);
            model.setResultCode("0000");
            return model;
        }
        if("image".equals(type)){
            FileUtils.uploadFile(file.getBytes(),imagePath,fileName);
            model.setData(imageDownLoad+fileName);
            model.setResultCode("0000");
            return model;
        }

        model.setData("file upload type is incorrect");
        model.setResultCode("0004");
        return model;
    }

}
