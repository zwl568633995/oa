/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: GlobalExceptionHandler.java
 * Author:   zhixiang.meng
 * Date:     2018年08月06日 下午6:03:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sigo.recruit.model.ResponseModel;
/**
 * 校验框架，拦截参数
 *
 * @author zhixiang.meng
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 验证异常
     * @param req
     * @param e
     * @return
     * @throws MethodArgumentNotValidException
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseModel handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException e) throws MethodArgumentNotValidException {
    	ResponseModel r = new ResponseModel();
        BindingResult bindingResult = e.getBindingResult();
        String errorMesssage = "";

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errorMesssage += fieldError.getDefaultMessage() + "\n";
        }
        r.setResultCode("0004");
        r.setResultMsg(errorMesssage);
        logger.info("MethodArgumentNotValidException",e);
        return r;
    }

    /**
     * 全局异常
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseModel handleException(HttpServletRequest req, Exception e) throws Exception {
    	ResponseModel r = new ResponseModel();
        r.setResultCode("0004");
        r.setResultMsg(e.getMessage());
        logger.error(e.getMessage(),e);
        return r;
    }
}
