/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: IndexController.java
 * Author:   zhixiang.meng
 * Date:     2018年6月28日 下午6:03:36
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import cn.sigo.recruit.config.RoleConfig;
import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.controller.activiti.UserActivitiController;
import cn.sigo.recruit.model.ErpRole;
import cn.sigo.recruit.model.ResponseErpUserInfo;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.User;
import cn.sigo.recruit.util.EncryAdecryUtil;
import cn.sigo.recruit.util.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 系统默认的controller<br>
 *
 * @author zhixiang.meng
 */
@RestController
public class IndexController {

   @Autowired
   private UserActivitiController userActivitiController;

   @Autowired
   private RoleConfig roleConfig;

   /**
    * 登录接口
    *
    * @param user
    * @return
    * @throws Exception
    */
   @RequestMapping(value = "/login", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel userLogin(@RequestBody User user) throws Exception {
      ResponseModel model = new ResponseModel();
      if (null != user) {
         String encryPwd = EncryAdecryUtil.md5Password(user.getUserpassword());
         user.setUserpassword(encryPwd);
         String token = HttpClientUtils.getErpTocken();

         if (StringUtils.isNotBlank(token)) {
            ResponseErpUserInfo date = HttpClientUtils.getErpUserInfo(user, token);

            if (null != date) {
               date.setUserPassword(encryPwd);
               userActivitiController.saveActivitiUser(date);
               String backendRole = "";
               List<ErpRole> roles = date.getRackendRole();
               if (null == roles || roles.isEmpty()) {
                  backendRole = "recruit-普通员工";
               } else {
                  for (ErpRole role : roles) {
                     if (StringUtils.isNotBlank(role.getRoleName())
                       && role.getRoleName().indexOf("recruit") >= 0) {
                        backendRole += role.getRoleName() + ";";
                     }
                     //2018-11-6 判断用户角色为招聘入职人员,线上roleid = 189;
                     //测试环境roleid = 191
                     if (role.getRoleId() == roleConfig.getRoleId()) {
                        backendRole = "招聘/入职人员";
                        //响应一个标识
                        String jwtString = JwtUtil.generateToken(date.getUserName(), date.getRealName(), backendRole);
                        model.setData(jwtString);
                        model.setResultCode("0189");
                        model.setResultMsg("此用户为应聘/入职人员");
                        return model;
                     }
                  }
               }
               String jwtString = JwtUtil.generateToken(date.getUserName(), date.getRealName(), backendRole);
               model.setData(jwtString);
               model.setResultCode("0000");
               model.setResultMsg("");
            } else {
               model.setResultCode("0101");
               model.setResultMsg("用户不存在或密码错误");
            }
         } else {
            model.setResultCode("0001");
            model.setResultMsg("获取系统token异常");
         }
      }
      return model;
   }

   /**
    * 根据用户名去erp查询用户
    *
    * @param likeName
    * @return
    */
   @RequestMapping(value = "/queryLikeName/{likeName}", method = RequestMethod.POST)
   public ResponseModel userLogin(HttpServletResponse response, @PathVariable("likeName") String likeName) {
      ResponseModel model = new ResponseModel();
      if (StringUtils.isNotBlank(likeName)) {
         String token = HttpClientUtils.getErpTocken();
         if (StringUtils.isNotBlank(token)) {
            model.setResultCode("0000");
            model.setResultMsg("");
            model.setData(HttpClientUtils.getLikeForERP(likeName, token));
         } else {
            model.setResultCode("0002");
            model.setResultMsg("获取系统token异常");
         }
      } else {
         model.setResultCode("0000");
         model.setResultMsg("");
      }
      response.setCharacterEncoding("UTF-8");
      return model;
   }

   /**
    * 获取城市地址信息
    *
    * @param province：省份
    * @param city：城市
    * @return
    */
   @RequestMapping(value = "/getAddressInfo/{province}/{city}", method = RequestMethod.POST)
   public ResponseModel getAddressInfo(HttpServletResponse response, @PathVariable("province") String province,
                                       @PathVariable("city") String city) {
      ResponseModel model = new ResponseModel();
      if (StringUtils.isNotBlank(province) && province.equals("null")) {
         province = "";
      }
      if (StringUtils.isNotBlank(city) && city.equals("null")) {
         city = "";
      }
      String token = HttpClientUtils.getErpTocken();
      String jsonStr = HttpClientUtils.getAddressInfo(province, city, token);
      model.setData(jsonStr);
      model.setResultCode("0000");
      model.setResultMsg("");
      response.setCharacterEncoding("UTF-8");
      return model;
   }

}
