/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobInfoController.java
 * Author:   zhixiang.meng
 * Date:     2018年7月5日 下午5:43:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import cn.sigo.recruit.InitializationBean;
import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqAuditJobInfo;
import cn.sigo.recruit.model.req.ReqJobInfoModel;
import cn.sigo.recruit.service.DictionaryService;
import cn.sigo.recruit.service.JobApplicationProcessService;
import cn.sigo.recruit.service.JobInfoService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 职位信息操作相关信息
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@RestController
@RequestMapping("/job")
public class JobInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobInfoController.class);
    @Autowired
    private JobInfoService jobInfoService;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private JobApplicationProcessService jobApplicationProcessService;

    /**
     * 
     * 编辑保存招聘职位信息
     *
     * @param request
     * @param jobInfo:需保存的职位信息
     * @param jobId:传值可以是具体的职位id，如果是保存，传null
     * @return
     */
    @RequestMapping(value = "/{jobCode}/editJob", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel editJobInfo(HttpServletRequest request, @RequestBody @Valid RecruitJobInfo jobInfo,
            @PathVariable("jobCode") String jobCode) {
        ResponseModel model = new ResponseModel();
        // 获取erp的userId
        String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
        // 获取erp的用户名
        String applyPeople = (String) request.getAttribute(JwtUtil.REAL_NAME);
        if(StringUtils.isBlank(jobCode) || (StringUtils.isNotBlank(jobCode) && jobCode.equals("null"))) {
            jobInfo.setAdder(adder);
        } else {
            jobInfo.setModer(adder);
        }
        jobInfo.setApplyPeople(applyPeople);
        return jobInfoService.editJobInfo(jobInfo, jobCode);
    }

    /**
     * 
     * 查询职位列表
     *
     * @param request
     * @param reqModel
     * @return
     */
    @RequestMapping(value = "/queryJobInfos", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel queryJobInfo(HttpServletRequest request, @RequestBody ReqJobInfoModel reqModel) {
        //增加排序字段判断
        if(null!=reqModel.getField()&&null!=reqModel.getSort()){
            // 职位评分 排序
            if("jobScore".equals(reqModel.getField())){
                reqModel.setField("job_score");
            }
            // 期望到岗日期排序
            if("expectedDate".equals(reqModel.getField())){
                reqModel.setField("expected_date");
            }
        }
        ResponseModel model = new ResponseModel();
        // 当前系统登录用户
        String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
        String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
        // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
        if (StringUtils.isBlank(backendRole) || (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
                && backendRole.indexOf("recruit-系统管理员") < 0)) {
            reqModel.setBackendRole("recruit-系统管理员");
            reqModel.setUserId(userId);
        }
        // 查询职位
        PageInfo<RecruitJobInfo> jobs = jobInfoService.queryJobInfoList(reqModel);
        model.setData(jobs);
        model.setResultCode("0000");
        model.setResultMsg("");
        return model;
    }

    /**
     * 
     * 根据职位code查询职位信息
     *
     * @param request
     * @param reqModel
     * @return
     */
    @RequestMapping(value = "/{jobCode}/queryJobInfo", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel queryJobInfo(HttpServletRequest request, @PathVariable("jobCode") String jobCode) {
        ResponseModel model = new ResponseModel();
        // 查询职位
        RecruitJobInfo job = jobInfoService.queryByJobCode(jobCode);
        String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
        boolean isAutid = false;
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("job", job);
        if(null != job) {
            // 学历
            List<Dictionary> education = dictionaryService.queryDictionaryByType("education");
            for(Dictionary dic : education) {
                if(dic.getDictValue().equals(job.getEducation())) {
                    job.setEducation(dic.getDictName());
                    break;
                }
            }
            // 工作地址
            List<Dictionary> workAddress = dictionaryService.queryDictionaryByType("workAddress");
            for(Dictionary dic : workAddress) {
                if(dic.getDictValue().equals(job.getWorkAddress())) {
                    job.setWorkAddress(dic.getDictName());
                    break;
                }
            }
            // 判断是否流程已经启动，启动，查询任务
            if (StringUtils.isNotBlank(job.getProcId())) {
                if (StringUtils.isNotBlank(job.getAuditId()) && job.getAuditId().equals(userId)) {
                    isAutid = true;
                }
            }
        }
        resultMap.put("isAutid", isAutid);
        model.setResultCode("0000");
        model.setData(resultMap);
        model.setResultMsg("");
        return model;
    }

    /**
     * 
     * 查询职位列表 数据字典
     *
     * @param request
     * @param reqModel
     * @return
     */
    @RequestMapping(value = "/queryJobInfoDictionary", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel queryJobInfoDictionary(HttpServletRequest request) {
        ResponseModel model = new ResponseModel();
        // 查询数据字典 部门
        List<Dictionary> department = dictionaryService.queryDictionaryByType("department");
        // 查询数据字段 职位状态
        List<Dictionary> jobStatus = dictionaryService.queryDictionaryByType("jobStatus");
        // 查询数据自动 申请原因
        List<Dictionary> applicationReasons = dictionaryService.queryDictionaryByType("applicationReasons");
        String activitiId = (String) InitializationBean.activitiMap.get("申请招聘职位");
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("departments", department);
        data.put("jobStatuses", jobStatus);
        data.put("applicationReasonses", applicationReasons);
        data.put("procdefId", activitiId);
        model.setResultCode("0000");
        model.setData(data);
        model.setResultMsg("");
        return model;
    }

    /**
     * 
     * 编辑保存招聘职位信息 数据字典
     *
     * @return
     */
    @RequestMapping(value = "/editJobDictionary", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel editJobDictionary(HttpServletRequest request) {
        ResponseModel model = new ResponseModel();
        // 查询数据字典 部门
        List<Dictionary> department = dictionaryService.queryDictionaryByType("department");
        // 查询数据字段 职位状态
        List<Dictionary> jobStatus = dictionaryService.queryDictionaryByType("jobStatus");
        // 查询数据自动 申请原因
        List<Dictionary> applicationReasons = dictionaryService.queryDictionaryByType("applicationReasons");
        // 性别
        List<Dictionary> sex = dictionaryService.queryDictionaryByType("sex");
        // 婚姻状况
        List<Dictionary> maritalStatus = dictionaryService.queryDictionaryByType("maritalStatus");
        // 学历
        List<Dictionary> education = dictionaryService.queryDictionaryByType("education");
        // 外语
        List<Dictionary> foreignLanguageLevel = dictionaryService.queryDictionaryByType("foreignLanguageLevel");
        // 工作地址
        List<Dictionary> workAddress = dictionaryService.queryDictionaryByType("workAddress");
        //工作年限
        List<Dictionary> workYear = dictionaryService.queryDictionaryByType("workYear");

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("departments", department);
        data.put("jobStatuses", jobStatus);
        data.put("applicationReasonses", applicationReasons);
        data.put("sex", sex);
        data.put("maritalStatus", maritalStatus);
        data.put("education", education);
        data.put("foreignLanguageLevel", foreignLanguageLevel);
        data.put("workAddress", workAddress);
        data.put("workYear", workYear);
        model.setResultCode("0000");
        model.setData(data);
        model.setResultMsg("");
        return model;
    }

    /**
     * 
     * 审核职位
     *
     * @param request
     * @param jobCode
     * @param auditOpinion
     * @param auditResult
     * @return
     */
    @RequestMapping(value = "/{jobCode}/auditJob", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel auditJob(HttpServletRequest request, @PathVariable("jobCode") String jobCode,
    		@RequestBody ReqAuditJobInfo reqAuditJob) {
        ResponseModel model = new ResponseModel();
        RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(jobCode);
        if (null != jobInfo) {
            String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
            String jobStatus = "招聘中";
            String audit = "true";
            if (jobInfo.getAuditId().equals(userId)) {
                ReqAuditJobInfo auditJobInfo = new ReqAuditJobInfo();
                auditJobInfo.setJobCode(jobCode);
                auditJobInfo.setCandidateName(userId);
                auditJobInfo.setNumberOfPeople(Integer.toString(jobInfo.getNumberOfPeople()));
                auditJobInfo.setProcId(jobInfo.getProcId());
                if (reqAuditJob.getAuditResult().equals("拒绝招聘申请")) {
                    audit = "false";
                    jobStatus = "审核拒绝";
                } else if(reqAuditJob.getAuditResult().equals("返回修改")){
                    audit = "modify";
                    jobStatus = "返回修改";
                }
                // 设置审核意见
                if(StringUtils.isNotBlank(reqAuditJob.getAuditOpinion())) {
                    jobInfo.setAuditOpinion(reqAuditJob.getAuditOpinion());
                }
                // 设置审核结果
                jobInfo.setJobStatus(jobStatus);
                
                auditJobInfo.setAuditResult(audit);
                auditJobInfo.setAuditOpinion(reqAuditJob.getAuditOpinion());
                boolean result = jobApplicationProcessService.auditTask(auditJobInfo);
                // 修改职位信息表中的职位状态 和 保存审核意见
                boolean flag = jobInfoService.modifyJobInfo(jobInfo);
                if (result && flag) {
                    model.setResultCode("0000");
                    model.setResultMsg("");
                } else {
                    if (!flag) {
                        LOGGER.error("职位id：" + jobInfo.getId() + "， 修改职位状态：" + jobStatus + "， 异常");
                        model.setResultCode("0206");
                        model.setResultMsg("修改职位状态异常");
                    }
                    if (!result) {
                        LOGGER.error("职位id：" + jobInfo.getId() + "， 审核状态：" + audit + "， 异常");
                        model.setResultCode("0205");
                        model.setResultMsg("职位审核流程可能已结束");
                    }
                }
            } else {
                model.setResultCode("0204");
                model.setResultMsg("不能审核该职位");
            }
        } else {
            model.setResultCode("0203");
            model.setResultMsg("职位编号查不到职位信息");
        }
        return model;
    }

    /**
     * 
     * 修改职位的状态
     *
     * @param request
     * @param jobCode
     * @param jobStatus
     * @return
     */
    @RequestMapping(value = "/{jobCode}/{jobStatus}/modifyJobStatus", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel modifyJobStatus(HttpServletRequest request, @PathVariable("jobCode") String jobCode,
    		@PathVariable("jobStatus") String jobStatus) {
        ResponseModel model = new ResponseModel();
        // 获取当前用户的id
        String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
        // 根据jobCode查询职位信息
        RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(jobCode);
        if (null != jobInfo) {
            if (StringUtils.isNotBlank(jobInfo.getAdder()) && jobInfo.getAdder().equals(userId)) {
                // 当前职位状态
                String dbJobStatus = jobInfo.getJobStatus();
                boolean flag = false;
                if (dbJobStatus.equals("招聘中")) {
                    if (jobStatus.equals("暂停招聘")) {
                        jobStatus = "招聘暂停";
                        flag = true;
                    }
                    if (jobStatus.equals("关闭招聘")) {
                        jobStatus = "招聘关闭";
                        flag = true;
                    }
                }
                if (dbJobStatus.equals("招聘暂停")) {
                    if (jobStatus.equals("重启招聘")) {
                        jobStatus = "招聘中";
                        flag = true;
                    }
                    if (jobStatus.equals("关闭招聘")) {
                        jobStatus = "招聘关闭";
                        flag = true;
                    }
                }
                if (flag) {
                    boolean result = jobInfoService.modifyJobInfoByJobId(jobStatus, jobInfo.getId());
                    if (result) {
                        model.setResultCode("0000");
                        model.setResultMsg("");
                    } else {
                        model.setResultCode("0002");
                        model.setResultMsg("异常！数据入库失败");
                    }
                } else {
                    model.setResultCode("0207");
                    model.setResultMsg("该职位状态为：" + dbJobStatus + "，不能直接修改成：" + jobStatus);
                }
            } else {
                model.setResultCode("0206");
                model.setResultMsg("不能修改该职位的状态");
            }
        } else {
            model.setResultCode("0203");
            model.setResultMsg("职位编号查不到职位信息");
        }
        return model;
    }

}
