package cn.sigo.recruit.controller;

/**
 * @Auther: DELL
 * @Date: 2018/9/10 09:36
 * @Description: 组织架构
 */

import cn.sigo.recruit.model.OrganizationStructure;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqOrganization;
import cn.sigo.recruit.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/organization")
public class OrganizationController {

   @Autowired
   private OrganizationService organizationService;


//    /**
//     *
//     * 部门列表-（根据部门名称模糊查询 组织架构）
//     * @param department 非必传参数
//     * @return
//     */
//    @RequestMapping(value = "/queryDeptList", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
//    public ResponseModel queryDeptList(@RequestParam(value = "department",required = false)String department){
//        ResponseModel model = new ResponseModel();
//        model.setData(organizationService.queryDeptList(department));
//        return model;
//    }

   /**
    * 进入 组织架构 页面，初始化页面 的两个数据（组织结构树，部门列表）
    *
    * @param
    * @return
    */
   @RequestMapping(value = "/initOrganizationPage", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel initOrganizationPage() {
      ResponseModel model = new ResponseModel();
      model.setResultCode("0000");
      model.setData(organizationService.initOrganizationPage());
      return model;
   }

   /**
    * 根据父级部门ID-查询下级部门
    *
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/querySubDeptList", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel querySubDeptList(@RequestBody ReqOrganization reqOrganization) {
      ResponseModel model = new ResponseModel();
      model.setResultCode("0000");
      model.setData(organizationService.querySubDeptList(reqOrganization));
      return model;
   }

   /**
    * 添加下级部门
    *
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/addSubDept", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel addSubDept(@RequestBody OrganizationStructure organizationStructure) {
      ResponseModel model = new ResponseModel();
      //判断部门是否重复
      Map<String, Object> map = new HashMap<>();
      map.put("deptName", organizationStructure.getDeptName());
      map.put("superiorDeptId", organizationStructure.getSuperiorDeptId());
      if (organizationService.countByTeamName(map) > 0) {
         model.setResultCode("0004");
         model.setResultMsg("该部门名称重复");
         return model;
      }
      //添加部门是否成功
      Boolean result = organizationService.addSubDept(organizationStructure);
      if (!result) {
         model.setResultCode("0004");
         model.setResultMsg("添加下级部门-失败");
         return model;
      }

      model.setResultCode("0000");
      model.setResultMsg("添加下级部门-成功");
      //成功返回下级部门信息
      model.setData(organizationStructure);

      return model;
   }


   /**
    * 编辑部门
    *
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/updateDepartment", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel updateDepartment(@RequestBody OrganizationStructure organizationStructure) {
      ResponseModel model = new ResponseModel();
      Map<String, Object> map = new HashMap<>();
      map.put("deptName", organizationStructure.getDeptName());
      map.put("id", organizationStructure.getId());
      if (organizationService.countByTeamName(map) > 0) {
         model.setResultCode("0004");
         model.setResultMsg("该部门名称重复");
         return model;
      }

      //先做 停用 判断
      //当前部门下的在职人数>0时，不可停用部门，停用时弹出toast提示：“当前部门下在职人数大于0，不可停用”
      if (null != organizationStructure.getDeleted() && organizationStructure.getDeleted()) {
         if (organizationService.countDepartmentPeople(organizationStructure.getDeptName()) > 0) {
            model.setResultCode("0004");
            model.setResultMsg("当前部门下在职人数大于0，不可停用");
            return model;
         }
      }

      Boolean result = organizationService.updateDepartment(organizationStructure);
      if (!result) {
         model.setResultCode("0004");
         model.setResultMsg("编辑部门-失败");
         return model;
      }
      model.setResultCode("0000");
      model.setResultMsg("编辑部门-成功");
      return model;
   }

   /**
    * 根据 输入的 名字 模糊查询 部门负责人
    *
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/queryDeptLeaderInfo", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryDeptLeaderInfo(@RequestParam(value = "leaderName", required = true) String leaderName) {
      ResponseModel model = new ResponseModel();
      model.setResultCode("0000");
      model.setData(organizationService.queryDeptLeaderInfo(leaderName));
      return model;
   }

}
