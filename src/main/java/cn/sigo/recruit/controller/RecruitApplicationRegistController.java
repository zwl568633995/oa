/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationRegistController.java
 * Author:   zhixiang.meng
 * Date:     2018年7月17日 下午2:09:13
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.model.RecruitApplicationCertificate;
import cn.sigo.recruit.model.RecruitApplicationEducation;
import cn.sigo.recruit.model.RecruitApplicationFamilyMember;
import cn.sigo.recruit.model.RecruitApplicationRegist;
import cn.sigo.recruit.model.RecruitApplicationWork;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewPeople;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqApplicationRegist;
import cn.sigo.recruit.model.req.ReqRecruitApplicationRegist;
import cn.sigo.recruit.service.DictionaryService;
import cn.sigo.recruit.service.InterviewProcessService;
import cn.sigo.recruit.service.RecruitApplicationCertificateService;
import cn.sigo.recruit.service.RecruitApplicationEducationService;
import cn.sigo.recruit.service.RecruitApplicationFamilyMemberService;
import cn.sigo.recruit.service.RecruitApplicationRegistService;
import cn.sigo.recruit.service.RecruitApplicationWorkService;
import cn.sigo.recruit.service.RecruitBaseStatisticsService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;

/**
 * 应聘登记表
 *
 * @author zhixiang.meng
 */
@RestController
@RequestMapping("/recruitApplicationRegist")
public class RecruitApplicationRegistController {
   private final static Logger LOGGER = LoggerFactory.getLogger(RecruitApplicationRegistController.class);
   @Autowired
   private DictionaryService dictionaryService;
   @Autowired
   private RecruitApplicationRegistService recruitApplicationRegistService;
   @Autowired
   private RecruitInterviewManagementService recruitInterviewManagement;
   @Autowired
   private RecruitApplicationCertificateService recruitApplicationCertificateService;
   @Autowired
   private RecruitApplicationEducationService recruitApplicationEducationService;
   @Autowired
   private RecruitApplicationFamilyMemberService recruitApplicationFamilyMemberService;
   @Autowired
   private RecruitApplicationWorkService recruitApplicationWorkService;

   /**
    * 查询数据字段
    *
    * @return
    */
   @RequestMapping(value = "/queryDictionary", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryDictionary(HttpServletRequest request) {
      ResponseModel model = new ResponseModel();
      // 学历
      List<Dictionary> educationExperience = dictionaryService.queryDictionaryByType("educationExperience");
      // 查询数据字段 职位状态
      List<Dictionary> sex = dictionaryService.queryDictionaryByType("sex");
      // 查询数据字段 婚姻
      List<Dictionary> maritalStatus = dictionaryService.queryDictionaryByType("maritalStatus");
      //应聘来源
      List<Dictionary> resumeSource = dictionaryService.queryDictionaryByType("resumeSource");
      //学历性质
      List<Dictionary> educationNature = dictionaryService.queryDictionaryByType("educationNature");
      //政治面貌
      List<Dictionary> politicalOutlook = dictionaryService.queryDictionaryByType("politicalOutlook");
      //在岗状态
      List<Dictionary> jobState = dictionaryService.queryDictionaryByType("jobState");
      //预计到岗时间
      List<Dictionary> fastestDay = dictionaryService.queryDictionaryByType("fastestDay");
      //所获学位
      List<Dictionary> receivedDegree = dictionaryService.queryDictionaryByType("receivedDegree");

      Map<String, Object> map = new HashMap<String, Object>();
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 判断新增按钮是否显示
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
              && backendRole.indexOf("recruit-系统管理员") < 0) {
         map.put("isShowAddButton", false);
      } else {
         map.put("isShowAddButton", true);
      }

      map.put("educationExperience", educationExperience);
      map.put("sex", sex);
      map.put("maritalStatus", maritalStatus);
      map.put("resumeSource", resumeSource);
      map.put("educationNature", educationNature);
      map.put("politicalOutlook", politicalOutlook);
      map.put("jobState", jobState);
      map.put("fastestDay", fastestDay);
      map.put("receivedDegree", receivedDegree);
      model.setData(map);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 查询所有的待面试
    *
    * @param request
    * @param regist:请求列表参数
    * @return
    */
   @RequestMapping(value = "/queryBeInterviewes", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryBeInterviewes(HttpServletRequest request) {
      ResponseModel model = new ResponseModel();
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 普通用户不查询
      if (!(backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0)) {
         List<RecruitInterviewPeople> interviews = recruitInterviewManagement.queryInterviews();
         model.setData(interviews);
      }
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 应聘登记
    *
    * @param request
    * @param regist
    * @param certificates
    * @return
    */
   @RequestMapping(value = "/saveRecruitApplicationRegist", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel saveRecruitApplicationRegist(HttpServletRequest request,
                                                     @RequestBody @Valid ReqRecruitApplicationRegist reqRegist) {
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 普通用户不查询
      if (!(backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0)) {
         String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
         return recruitApplicationRegistService.saveRecruitApplicationRegist(reqRegist, adder);
      } else {
         ResponseModel model = new ResponseModel();
         model.setResultCode("0002");
         model.setResultMsg("对不起，您不能创建应聘登记表");
         return model;
      }
   }

   /**
    * 应聘登记
    *
    * @param request
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/queryRecruitApplicationRegist", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryRecruitApplicationRegist(HttpServletRequest request) {
      ResponseModel model = new ResponseModel();
      RecruitApplicationRegist regist = new RecruitApplicationRegist();
      regist.setApplicationName("mengzx");
      ReqRecruitApplicationRegist reqRegist = new ReqRecruitApplicationRegist();
      reqRegist.setRegist(regist);
      RecruitApplicationCertificate c = new RecruitApplicationCertificate();
      c.setCertificateName("pmp");
//        c.setCertificateDate(new Date());
      List<RecruitApplicationCertificate> cs = new ArrayList<RecruitApplicationCertificate>();
      cs.add(c);
      reqRegist.setCertificates(cs);
      model.setData(reqRegist);
      return model;
   }

   /**
    * 根据应聘表登记号查询详细信息
    *
    * @param applicationCode 应聘表登记号
    * @return
    */
   @RequestMapping(value = "/{applicationCode}/queryInfoByApplicationCode", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInfoByApplicationCode(@PathVariable(value = "applicationCode") String applicationCode) {
      ResponseModel model = new ResponseModel();
      RecruitApplicationRegist recruitApplicationRegist = recruitApplicationRegistService.queryApplicationRegistByApplicationCode(applicationCode);
      ReqRecruitApplicationRegist result = new ReqRecruitApplicationRegist();
      if (null != recruitApplicationRegist) {
         result.setRegist(recruitApplicationRegist);
         Integer applicationRegistId = recruitApplicationRegist.getId();
         // 查询证书
         List<RecruitApplicationCertificate> certificates = recruitApplicationCertificateService
           .queryCertificateByApplicationRegistId(applicationRegistId);
         result.setCertificates(certificates);
         // 查询教育经历
         List<RecruitApplicationEducation> educations = recruitApplicationEducationService
           .queryEducationByApplicationId(applicationRegistId);
         result.setEducations(educations);
         // 查询家庭成员
         List<RecruitApplicationFamilyMember> familyMembers = recruitApplicationFamilyMemberService
           .queryFamilyMemberByApplicationId(applicationRegistId);
         result.setFamilyMembers(familyMembers);
         // 查询工作经历
         List<RecruitApplicationWork> works = recruitApplicationWorkService
           .queryWorkByApplicationId(applicationRegistId);
         result.setApplicationWorks(works);
      }
      model.setData(result);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 根据身份证号查询应聘登记表
    *
    * @param idCardNo
    * @return
    */
   @RequestMapping(value = "/{idCardNo}/queryInfoByIdCardNo", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInfoByIdCardNo(@PathVariable("idCardNo") String idCardNo) {
      ResponseModel model = new ResponseModel();
      RecruitApplicationRegist recruitApplicationRegist = recruitApplicationRegistService.queryInfoByIdCardNo(idCardNo);

      ReqRecruitApplicationRegist result = new ReqRecruitApplicationRegist();
      if (null != recruitApplicationRegist) {
         result.setRegist(recruitApplicationRegist);
         Integer applicationRegistId = recruitApplicationRegist.getId();
         // 查询证书
         List<RecruitApplicationCertificate> certificates = recruitApplicationCertificateService
           .queryCertificateByApplicationRegistId(applicationRegistId);
         result.setCertificates(certificates);
         // 查询教育经历
         List<RecruitApplicationEducation> educations = recruitApplicationEducationService
           .queryEducationByApplicationId(applicationRegistId);
         result.setEducations(educations);
         // 查询家庭成员
         List<RecruitApplicationFamilyMember> familyMembers = recruitApplicationFamilyMemberService
           .queryFamilyMemberByApplicationId(applicationRegistId);
         result.setFamilyMembers(familyMembers);
         // 查询工作经历
         List<RecruitApplicationWork> works = recruitApplicationWorkService
           .queryWorkByApplicationId(applicationRegistId);
         result.setApplicationWorks(works);
      }else {
         model.setResultCode("0001");
         model.setResultMsg("暂未找到您的待入职信息，请联系人事");
         return model;
      }
      model.setData(result);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 应聘登记表 分页查询列表
    *
    * @param
    * @return
    */
   @RequestMapping(value = "queryApplicationRegistrationList", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryApplicationRegistrationList(HttpServletRequest request,
                                                         @RequestBody ReqApplicationRegist reqApplicationRegist) {
      ResponseModel model = new ResponseModel();
      // 当前系统登录用户
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      PageInfo<RecruitApplicationRegist> result = recruitApplicationRegistService.queryApplicationRegistrationList(reqApplicationRegist, userId, backendRole);
      model.setData(result);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }


}
