package cn.sigo.recruit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegister;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegisterPageQuery;
import cn.sigo.recruit.service.DictionaryService;
import cn.sigo.recruit.service.RecruitEntryRegisterService;

/**
 * 入职登记表
 *
 */
@RestController
@RequestMapping("/recruitEntryRegister")
public class RecruitEntryRegisterController {
    private final static Logger LOGGER = LoggerFactory.getLogger(RecruitEntryRegisterController.class);
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private RecruitEntryRegisterService recruitEntryRegisterService;
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;

    /**
     *
     * 入职登记保存
     * @param request
     * @param reqRecruitEntry
     * @param
     * @return
     */
    @RequestMapping(value = "/saveRecruitEntryRegister", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel saveRecruitEntryRegister(HttpServletRequest request,
                                                  @RequestBody @Valid  ReqRecruitEntryRegister reqRecruitEntry) {
        ResponseModel model = new ResponseModel();

        //判断 面试单是否为待面试状态
        RecruitInterviewManagement interview = recruitInterviewManagementService.queryInterviewManagentByCode(reqRecruitEntry.getBaseInfo().getInterviewCode());
        if(null==interview){
            model.setResultCode("0004");
            model.setResultMsg("该面试单号（interviewCode）不存在");
            return model;
        }

        if(!"待入职".equals(interview.getInterviewStatus())){
            model.setResultCode("0004");
            model.setResultMsg("面试单状态不是待入职状态");
            return model;
        }

        Map<String,Object> param = new HashMap<>();
        param.put("applicationCode",reqRecruitEntry.getBaseInfo().getApplicationCode());
        param.put("interviewCode",reqRecruitEntry.getBaseInfo().getInterviewCode());
        //判断 入职信息基本表中是否已经存在 该面试单号 或  应聘单号
        Boolean isExit = recruitEntryRegisterService.isExistInEntryRegisterBase(param);
        if(isExit){
            model.setResultCode("0004");
            model.setResultMsg("已存在 该面试单号 或  应聘单号");
            return model;
        }

        // 获取erp的userId
        String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
        Map<String,Object> result = recruitEntryRegisterService.saveRecruitEntryRegister(reqRecruitEntry,adder);
        model.setResultCode(result.get("resultCode").toString());
        model.setResultMsg(result.get("msg").toString());
        return model;
    }

    /**
     *
     * 根据入职登记表编号查询入职信息
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/{entryCode}/queryRecruitEntryRegister", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel queryRecruitEntryRegister(@PathVariable(value ="entryCode" )String entryCode){
        ResponseModel model = new ResponseModel();
        ReqRecruitEntryRegister result = recruitEntryRegisterService.queryRecruitEntryRegister(entryCode);
        model.setData(result);
        model.setResultCode("0000");
        model.setResultMsg("");
        return  model;
    }

    /**
     *
     * 入职登记表分页列表
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/pageQueryRecruitEntryRegister", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel pageQueryRecruitEntryRegister(HttpServletRequest request,@RequestBody ReqRecruitEntryRegisterPageQuery reqRecruitEntryRegisterPageQuery){
        ResponseModel model = new ResponseModel();
        // 当前系统登录用户
        String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
        String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
        PageInfo<ReqRecruitEntryRegisterPageQuery> result = recruitEntryRegisterService.pageQueryRecruitEntryRegister(reqRecruitEntryRegisterPageQuery,userId,backendRole);
        model.setData(result);
        model.setResultCode("0000");
        model.setResultMsg("");
        return  model;
    }

    /**
     *
     * 查询数据字段
     *
     * @return
     */
    @RequestMapping(value = "/queryDictionary", produces = { "application/json;charset=UTF-8" }, method=RequestMethod.POST)
    public ResponseModel queryDictionary() {
        ResponseModel model = new ResponseModel();
        // 性别
        List<Dictionary> sex = dictionaryService.queryDictionaryByType("sex");
        // 政治面貌
        List<Dictionary> politicalOutlook = dictionaryService.queryDictionaryByType("politicalOutlook");
        // 婚姻状况
        List<Dictionary> maritalStatus = dictionaryService.queryDictionaryByType("maritalStatus");
        // 户口性质
        List<Dictionary> householdNature = dictionaryService.queryDictionaryByType("householdNature");
        // 应聘来源
        List<Dictionary> resumeSource = dictionaryService.queryDictionaryByType("resumeSource");
        // 学历
        List<Dictionary> educationExperience = dictionaryService.queryDictionaryByType("educationExperience");
        // 学历性质
        List<Dictionary> educationNature = dictionaryService.queryDictionaryByType("educationNature");
        //所获学位
        List<Dictionary> receivedDegree = dictionaryService.queryDictionaryByType("receivedDegree");
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("sex",sex);
        resultMap.put("politicalOutlook",politicalOutlook);
        resultMap.put("maritalStatus",maritalStatus);
        resultMap.put("householdNature",householdNature);
        resultMap.put("resumeSource",resumeSource);
        resultMap.put("educationExperience",educationExperience);
        resultMap.put("educationNature",educationNature);
        resultMap.put("receivedDegree",receivedDegree);
        model.setData(resultMap);
        model.setResultCode("0000");
        model.setResultMsg("");
        return model;
    }

}
