/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitInterviewManagementController.java
 * Author:   zhixiang.meng
 * Date:     2018年7月12日 下午2:43:13
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller;

import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.req.ReqInterviewManagementModel;
import cn.sigo.recruit.model.req.ReqModifyInterviewTime;
import cn.sigo.recruit.service.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 面试管理
 *
 * @author zhixiang.meng
 */
@RestController
@RequestMapping("/recruitInterviewManagement")
public class RecruitInterviewManagementController {

   @Autowired
   private JobInfoService jobInfoService;
   @Autowired
   private DictionaryService dictionaryService;
   @Autowired
   private RecruitInterviewManagementService recruitInterviewManagementService;
   @Autowired
   private RecruitInterviewManagementResultService recruitInterviewManagementResultService;
   @Autowired
   private RecruitApplicationRegistService recruitApplicationRegistService;
   @Autowired
   private RecruitApplicationCertificateService recruitApplicationCertificateService;
   @Autowired
   private RecruitApplicationEducationService recruitApplicationEducationService;
   @Autowired
   private RecruitApplicationFamilyMemberService recruitApplicationFamilyMemberService;
   @Autowired
   private RecruitApplicationWorkService recruitApplicationWorkService;
   @Autowired
   private InterviewProcessService interviewProcessService;
   @Autowired
   private RecruitBaseStatisticsService recruitBaseStatisticsService;
   @Autowired
   private InterviewProgressTimeService interviewProgressTimeService;

   /**
    * 查询数据字段
    *
    * @return
    */
   @RequestMapping(value = "/queryDictionary", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryDictionary() {
      ResponseModel model = new ResponseModel();
      // 查询数据字典 部门
      List<Dictionary> department = dictionaryService.queryDictionaryByType("department");
      // 查询数据字段 职位状态
      List<Dictionary> resumeSource = dictionaryService.queryDictionaryByType("resumeSource");
      // 查询面试单状态
      List<Dictionary> interviewManagementStatus = dictionaryService
        .queryDictionaryByType("interviewManagementStatus");
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("department", department);
      map.put("resumeSource", resumeSource);
      map.put("interviewManagementStatus", interviewManagementStatus);
      model.setData(map);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 保存
    *
    * @param request
    * @param recruit
    * @return
    * @interviewCode: 面试单编号
    * @interviewStatus: 状态
    * @applyName: 应聘人姓名
    * @applyJobName: 应聘职位
    * @jobCode: 职位code
    * @applyDept: 应聘部门
    * @applyPhoneNumber: 应聘人手机号码
    * @resumeSource: 简历来源
    * @firstInterviewer: 初试面试官
    * @firstTime: 初试时间
    * @secondInterviewer: 初试时间
    * @secondTime: 复试时间
    * @offerAudit: offer审核人
    * @offerAuditTime: offer审核时间
    * @resumeUrl: 简历地址
    */
   @RequestMapping(value = "/{interviewerCode}/interviewer", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel saveInterviewer(HttpServletRequest request, @RequestBody RecruitInterviewManagement recruit,
                                        @PathVariable("interviewerCode") String interviewerCode) {
      ResponseModel model = new ResponseModel();
      // 判断用户角色，是否有权限操作
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0) {
         model.setResultCode("0304");
         model.setResultMsg("对不起！您没有权限新增面试！");
         return model;
      }
      String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
      String addName = (String) request.getAttribute(JwtUtil.REAL_NAME);
      recruit.setAdder(adder);
      recruit.setAddName(addName);
      recruit.setModer(adder);
      return recruitInterviewManagementService.saveInterviewManagement(recruit, interviewerCode);
   }

   /**
    * 根据部门和职位名查询 1、是否能创建面试人员 2、是否有面试官
    *
    * @return data 里面存储 map结构的对象，key为：firstInterviewer 和 secondInterviewer
    */
   @RequestMapping(value = "/{deptName}/{jobName}/queryInterviewer", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInterviewer(@PathVariable("deptName") String deptName,
                                         @PathVariable("jobName") String jobName) {
      ResponseModel model = new ResponseModel();
      RecruitJobInfo jobInfo = jobInfoService.queryJobInfoByDeptNameAndJobName(deptName, jobName);
      // 查询不出来，可以创建面试单
      if (null == jobInfo) {
         model.setResultCode("0000");
         model.setResultMsg("");
         model.setData(null);
      }
      // 查询出来，需要根据职位的状态判断是否可以创建面试单
      if (null != jobInfo) {
         String jobStatus = jobInfo.getJobStatus();
         if (StringUtils.isNotBlank(jobStatus)) {
            // 不能创建面试单
            if (jobStatus.equals("招聘完成") || jobStatus.equals("招聘关闭") || jobStatus.equals("招聘暂停")
              || jobStatus.equals("审核拒绝")) {
               model.setResultCode("0301");
               model.setResultMsg("当前职位不存在有效招聘申请单，不可创建");
               model.setData(null);
            } else {
               Map<String, String> resultData = new HashMap<String, String>();
               // 招聘中 需要将职位code 和 面试管理绑定
               if (jobStatus.equals("招聘中")) {
                  resultData.put("jobCode", jobInfo.getJobCode());
                  if (StringUtils.isNotBlank(jobInfo.getFirstInterviewer())) {
                     resultData.put("firstInterviewer", jobInfo.getFirstInterviewer());
                  }
                  if (StringUtils.isNotBlank(jobInfo.getSecondInterviewer())) {
                     resultData.put("secondInterviewer", jobInfo.getSecondInterviewer());
                  }
               }
               model.setResultCode("0000");
               model.setResultMsg("");
               model.setData(resultData);
            }
         }
      }
      return model;
   }

   /**
    * 根据面试单号，查询面试信息
    *
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/queryInterviewManagent", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInterviewManagentByCode(@PathVariable("interviewCode") String interviewCode) {
      ResponseModel model = new ResponseModel();
      if (StringUtils.isNotBlank(interviewCode)) {
         RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
           .queryInterviewManagentByCode(interviewCode);

         if (null != interviewManagement) {
            Integer interviewId = interviewManagement.getId();
            RecruitInterviewManagementResult interviewResult = recruitInterviewManagementResultService
              .queryResultForManagementId(interviewId);
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("interview", interviewManagement);
            if (null != interviewResult) {
               resultMap.put("interviewResult", interviewResult);
            }
            // 查询应聘登记表
            RecruitApplicationRegist applicationRegist = recruitApplicationRegistService
              .queryApplicationRegistByInterviewCode(interviewCode);
            if (null != applicationRegist) {
               resultMap.put("applicationRegist", applicationRegist);
               Integer applicationRegistId = applicationRegist.getId();
               // 查询证书
               List<RecruitApplicationCertificate> certificates = recruitApplicationCertificateService
                 .queryCertificateByApplicationRegistId(applicationRegistId);
               if (null != certificates && !certificates.isEmpty()) {
                  resultMap.put("certificates", certificates);
               }
               // 查询教育经历
               List<RecruitApplicationEducation> educations = recruitApplicationEducationService
                 .queryEducationByApplicationId(applicationRegistId);
               if (null != educations && !educations.isEmpty()) {
                  resultMap.put("educations", educations);
               }
               // 查询家庭成员
               List<RecruitApplicationFamilyMember> familyMenbers = recruitApplicationFamilyMemberService
                 .queryFamilyMemberByApplicationId(applicationRegistId);
               if (null != familyMenbers && !familyMenbers.isEmpty()) {
                  resultMap.put("familyMenbers", familyMenbers);
               }
               // 查询工作经历
               List<RecruitApplicationWork> works = recruitApplicationWorkService
                 .queryWorkByApplicationId(applicationRegistId);
               if (null != works && !works.isEmpty()) {
                  resultMap.put("works", works);
               }
            }
            model.setData(resultMap);
         }
      }
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 根据手机号查询面试单号
    *
    * @return
    */
   @RequestMapping(value = "/{phone}/queryInterviewManagentByPhone", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInterviewManagentByPhone(@PathVariable("phone") String phone) {
      ResponseModel model = new ResponseModel();

      if (StringUtils.isNotBlank(phone)) {
         RecruitInterviewManagement interviewManagement = recruitInterviewManagementService.queryInterviewManagentByPhone(phone);

         if (null != interviewManagement) {
            model.setResultCode("0000");
            model.setResultMsg("");
            model.setData(interviewManagement);
            return model;
         } else {
            model.setResultCode("0001");
            model.setResultMsg("暂未找到您的待面试信息，请联系人事");
            return model;
         }

      } else {
         model.setResultCode("0001");
         model.setResultMsg("手机号不能为空");
         return model;
      }

   }

   /**
    * 查询面试管理列表
    *
    * @param request
    * @param reqModel
    * @return
    */
   @RequestMapping(value = "/queryInterviewManagents", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInterviewManagents(HttpServletRequest request,
                                                @RequestBody ReqInterviewManagementModel reqModel) {
      ResponseModel model = new ResponseModel();
      // 当前系统登录用户
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      boolean isShowAdd = true;
      // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0) {
         isShowAdd = false;
      }
      // 面试管理：支持按初试时间、复试时间排序
      if (null != reqModel.getField() && null != reqModel.getSort()) {
         // 初试时间 排序
         if ("firstTime".equals(reqModel.getField())) {
            reqModel.setField("first_time");
         }
         // 复试时间 排序
         if ("secondTime".equals(reqModel.getField())) {
            reqModel.setField("second_time");
         }
      }
      PageInfo<RecruitInterviewManagement> interviewManagements = recruitInterviewManagementService
        .queryInterviewManagents(reqModel, userId, backendRole);
      Map<String, Object> dataMap = new HashMap<String, Object>();
      dataMap.put("isShowAdd", isShowAdd);
      dataMap.put("interviewManagements", interviewManagements);
      model.setData(dataMap);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 拒绝面试
    *
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/refusingInterview", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel refusingInterview(HttpServletRequest request,
                                          @PathVariable("interviewCode") String interviewCode) {
      ResponseModel model = new ResponseModel();
      // 当前系统登录用户
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0) {
         model.setResultCode("0304");
         model.setResultMsg("对不起！您没有权限新增面试！");
      } else {
         RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
           .queryInterviewManagentByCode(interviewCode);
         if (null != interviewManagement) {
            // 面试流程:拒绝面试
            interviewProcessService.isCommonInterview(userId, interviewManagement.getProcId(), "false");
            // 修改面试单记录
            recruitInterviewManagementService.modifyInterviewStatus(interviewCode, "初试爽约");
            // 更新统计表的基础信息
            recruitBaseStatisticsService.updateBaseStatistics(interviewCode, "noInterviewNumber");
            model.setResultCode("0000");
            model.setResultMsg("");
         } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
         }
      }
      return model;
   }

   /**
    * 拒绝复试
    *
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/refusingSencondInterview", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel refusingSencondInterview(HttpServletRequest request,
                                                 @PathVariable("interviewCode") String interviewCode) {
      ResponseModel model = new ResponseModel();
      // 当前系统登录用户
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0) {
         model.setResultCode("0304");
         model.setResultMsg("对不起！您没有权限新增面试！");
      } else {
         return recruitInterviewManagementResultService.refusingSencondInterview(interviewCode, userId);
      }
      return model;
   }

   /**
    * 【废弃】面试审核
    *
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/interviewReview", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel interviewReview(HttpServletRequest request,
                                        @PathVariable("interviewCode") String interviewCode,
                                        @RequestBody RecruitInterviewManagementResult interviewResult) {
      // 设置activiti面试官
      recruitInterviewManagementResultService.setInterviewer(interviewCode);
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      interviewResult.setAdder(userId);
      interviewResult.setModer(userId);
      return recruitInterviewManagementResultService.interviewReview(interviewCode, interviewResult);
   }

   /**
    * 初试审核
    *
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/interviewFirstReview", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel interviewFirstReview(HttpServletRequest request,
                                             @PathVariable("interviewCode") String interviewCode,
                                             @RequestBody RecruitInterviewManagementResult interviewResult) {
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      interviewResult.setAdder(userId);
      interviewResult.setModer(userId);
      return recruitInterviewManagementResultService.interviewFirstReview(interviewCode, interviewResult);
   }

   /**
    * 复试审核
    *
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/interviewSecondReview", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel interviewSecondReview(HttpServletRequest request,
                                              @PathVariable("interviewCode") String interviewCode,
                                              @RequestBody RecruitInterviewManagementResult interviewResult) {
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      interviewResult.setAdder(userId);
      interviewResult.setModer(userId);
      return recruitInterviewManagementResultService.interviewSecondReview(interviewCode, interviewResult);
   }

   /**
    * offer审核
    *
    * @param interviewCode:面试单
    * @param ：薪资
    * @param ：offer意见
    * @param request
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/offerAudit", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel offerAudit(HttpServletRequest request, @PathVariable("interviewCode") String interviewCode,
                                   @RequestBody RecruitInterviewManagementResult interviewResult) {
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      String salary = interviewResult.getOfferSalary();
      String offerOpinion = interviewResult.getOfferOpinion();
      String isInterviewPassed = interviewResult.getOperation();
      return recruitInterviewManagementResultService.offerAudit(interviewCode, salary, offerOpinion, userId,
        isInterviewPassed);
   }

   /**
    * 拒绝offer操作
    *
    * @param request
    * @param interviewCode
    * @return
    */
   @RequestMapping(value = "/{interviewCode}/noOffer", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel noOffer(HttpServletRequest request, @PathVariable("interviewCode") String interviewCode) {
      String userId = (String) request.getAttribute(JwtUtil.USER_NAME);
      return recruitInterviewManagementResultService.isSendOffer(interviewCode, "拒绝offer", userId);
   }

   /**
    * 模糊查询获取应聘职位
    *
    * @param job
    * @return
    */
   @RequestMapping(value = "/jobPosition", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel jobPosition(@RequestParam("job") String job) {
      ResponseModel model = new ResponseModel();
      if (null == job) {
         model.setResultCode("0004");
         model.setResultMsg("请求参数为空");
         return model;
      }
      List<String> result = jobInfoService.jobPosition(job);
      model.setResultCode("0000");
      model.setData(result);
      return model;
   }

   /**
    * 根据部门查询 所有 招聘中 的职位
    *
    * @param department(可为null)
    * @return
    */
   @RequestMapping(value = "/queryJobPositionByDepartment", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryJobPositionByDepartment(@RequestParam(value = "department", required = false) String
                                                       department) {
      ResponseModel model = new ResponseModel();
      List<Map<String, Object>> result = jobInfoService.queryJobPositionByDepartment(department);
      model.setResultCode("0000");
      model.setData(result);
      return model;
   }

   /**
    * 设置复试时间（面试单状态为：  待安排复试）
    */
   @RequestMapping(value = "/modifySecondInterviewTime", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel modifySecondInterviewTime(@RequestBody ReqModifyInterviewTime reqModifyInterviewTime) {
      ResponseModel model = new ResponseModel();
      if (StringUtils.isBlank(reqModifyInterviewTime.getInterviewCode())) {
         model.setResultCode("0004");
         model.setResultMsg("面试单号为空");
         return model;
      }
      if (StringUtils.isBlank(reqModifyInterviewTime.getSecondTime())) {
         model.setResultCode("0004");
         model.setResultMsg("复试时间为空");
         return model;
      }
      //设置复试时间
      RecruitInterviewManagement recruitInterviewManagement = recruitInterviewManagementService.queryInterviewManagentByCode(reqModifyInterviewTime.getInterviewCode());
      if ("待安排复试".equals(recruitInterviewManagement.getInterviewStatus())) {
         Boolean result = recruitInterviewManagementService.modifySecondInterviewTime(reqModifyInterviewTime);
         model.setResultCode(true == result ? "0000" : "0004");
         return model;
      } else {
         model.setResultCode("0004");
         model.setResultMsg("面试单状态不为 待安排复试");
         return model;
      }
   }

   /**
    * 修改 初试时间/复试时间
    */
   @RequestMapping(value = "/updateInterviewTime", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel updateInterviewTime(@RequestBody ReqModifyInterviewTime reqModifyInterviewTime) {
      ResponseModel model = new ResponseModel();
      if (StringUtils.isBlank(reqModifyInterviewTime.getInterviewCode())) {
         model.setResultCode("0004");
         model.setResultMsg("面试单号为空");
         return model;
      }
      if (StringUtils.isBlank(reqModifyInterviewTime.getFirstTime()) && StringUtils.isBlank(reqModifyInterviewTime.getSecondTime())) {
         model.setResultCode("0004");
         model.setResultMsg("面试时间为空");
         return model;
      }
      RecruitInterviewManagement recruitInterviewManagement = recruitInterviewManagementService.queryInterviewManagentByCode(reqModifyInterviewTime.getInterviewCode());
      if ("待应聘登记".equals(recruitInterviewManagement.getInterviewStatus()) || "待复试".equals(recruitInterviewManagement.getInterviewStatus())) {
         Boolean result = recruitInterviewManagementService.updateInterviewTime(reqModifyInterviewTime);
         model.setResultCode(true == result ? "0000" : "0004");
         model.setResultMsg("修改 面试时间 成功");
      } else {
         model.setResultCode("0004");
         model.setResultMsg("此面试单状态不可修改 面试时间");
      }
      return model;
   }

   /**
    * 获取面试单 进度
    */
   @RequestMapping(value = "/{interviewCode}/queryInterviewProgress", produces = {
     "application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryInterviewProgress(@PathVariable("interviewCode") String interviewCode) {
      ResponseModel model = new ResponseModel();
      model.setResultCode("0000");
      model.setData(interviewProgressTimeService.queryInterviewProgress(interviewCode));
      return model;
   }

}
