package cn.sigo.recruit.controller;

import cn.sigo.recruit.model.RecruitBaseStatistics;
import cn.sigo.recruit.model.ResStatisticalTransformation;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.service.RecruitBaseStatisticsService;
import cn.sigo.recruit.service.RecruitStatisticalTransformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * @Auther: DELL
 * @Date: 2018/8/16 09:13
 * @Description: 招聘统计
 */
@RestController
@RequestMapping("/statistic")
public class RecruitStatisticsController {

    @Autowired
    private RecruitBaseStatisticsService recruitBaseStatisticsService;
    @Autowired
    private RecruitStatisticalTransformationService recruitStatisticalTransformationService;

    /**
     *
     * 招聘统计 数据概览
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/dataOverview", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel dataOverview(@RequestParam(value = "yearMonth",required = true)String yearMonth){
        ResponseModel model = new ResponseModel();
        RecruitBaseStatistics result =  new RecruitBaseStatistics();
        result = recruitBaseStatisticsService.dataOverview(yearMonth);
        model.setData(result);
        model.setResultCode("0000");
        return  model;
    }

    /**
     *
     * 招聘统计 部门招聘详情
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/departmentRecruitInfo", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel departmentRecruitInfo(@RequestParam(value = "yearMonth",required = true)String yearMonth){
        ResponseModel model = new ResponseModel();
        List<RecruitBaseStatistics> result =  new ArrayList<>();
        result = recruitBaseStatisticsService.departmentRecruitInfo(yearMonth);
        model.setData(result);
        model.setResultCode("0000");
        return  model;
    }

    /**
     *
     * 招聘统计 职位招聘详情
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/positionRecruitInfo", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel positionRecruitInfo(@RequestParam(value = "yearMonth",required = true)String yearMonth,
                                             @RequestParam(value = "department",required = false)String department){
        ResponseModel model = new ResponseModel();
        List<RecruitBaseStatistics> result =  new ArrayList<>();
        result = recruitBaseStatisticsService.positionRecruitInfo(yearMonth,department);
        model.setData(result);
        model.setResultCode("0000");
        return  model;
    }

    /**
     *
     * 招聘统计 部门招聘转换分析
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/departmentRecruitTransformation", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel departmentRecruitTransformation(@RequestParam(value = "yearMonth",required = true)String yearMonth){
        ResponseModel model = new ResponseModel();
        List<ResStatisticalTransformation> result = recruitStatisticalTransformationService.departmentRecruitTransformation(yearMonth);
        model.setData(result);
        model.setResultCode("0000");
        return  model;
    }

    /**
     *
     * 招聘统计 职位招聘转换分析
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "/positionRecruitTransformation", produces = { "application/json;charset=UTF-8" }, method= RequestMethod.POST)
    public ResponseModel positionRecruitTransformation(@RequestParam(value = "yearMonth",required = true)String yearMonth,
                                                       @RequestParam(value = "department",required = false)String department){
        ResponseModel model = new ResponseModel();
        List<ResStatisticalTransformation> result = recruitStatisticalTransformationService.positionRecruitTransformation(yearMonth,department);
        model.setData(result);
        model.setResultCode("0000");
        return  model;
    }
}
