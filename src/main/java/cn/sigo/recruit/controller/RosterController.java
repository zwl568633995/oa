package cn.sigo.recruit.controller;

import cn.sigo.recruit.config.UrlUmsConfig;
import cn.sigo.recruit.config.jwt.JwtUtil;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.req.ReqRecruitIncumbencyHistory;
import cn.sigo.recruit.model.req.ReqRosterEmployee;
import cn.sigo.recruit.model.req.ReqRosterPageQuery;
import cn.sigo.recruit.model.req.ReqSelectRosterByReallyName;
import cn.sigo.recruit.service.DictionaryService;
import cn.sigo.recruit.service.OrganizationService;
import cn.sigo.recruit.service.RosterService;
import cn.sigo.recruit.util.CommonUtil;
import cn.sigo.recruit.util.ExportExcel;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: DELL
 * @Date: 2018/8/2 17:04
 * @Description: 花名冊
 */
@RestController
@RequestMapping("/roster")
public class RosterController {

   @Autowired
   private RosterService rosterService;
   @Autowired
   private ExportEntity exportEntity;
   @Autowired
   private DictionaryService dictionaryService;
   @Autowired
   private OrganizationService organizationService;
   @Autowired
   private UrlUmsConfig urlUmsConfig;

   /**
    * 花名冊 分页查询列表
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/pageQueryRosterList", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel pageQueryRosterList(HttpServletRequest request, @RequestBody ReqRosterPageQuery reqRosterPageQuery) {
      ResponseModel model = new ResponseModel();
      // 当前系统登录用户
      String userName = (String) request.getAttribute(JwtUtil.REAL_NAME);
      String backendRole = (String) request.getAttribute(JwtUtil.BACKEND_ROLE);
      PageInfo<ResponseRosterPageQuery> result = rosterService.pageQueryRosterList(reqRosterPageQuery, userName, backendRole);
      model.setData(result);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 花名冊 添加员工
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/saveRosterEmployee", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel saveRosterEmployee(HttpServletRequest request,
                                           @RequestBody @Valid ReqRosterEmployee reqRosterEmployee) {
      ResponseModel model = new ResponseModel();
      // 获取erp的userId
      String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
      Map<String, Object> result = rosterService.saveRosterEmployee(reqRosterEmployee, adder);
      model.setResultCode(result.get("resultCode").toString());
      model.setResultMsg(result.get("msg").toString());

      return model;
   }


   /**
    * 花名冊 更新员工
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/updateRosterEmployee", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel updateRosterEmployee(HttpServletRequest request,
                                             @RequestBody ReqRosterEmployee reqRosterEmployee) {
      ResponseModel model = new ResponseModel();
      // 获取erp的userId
      String adder = (String) request.getAttribute(JwtUtil.USER_NAME);
      Map<String, Object> result = rosterService.updateRosterEmployee(reqRosterEmployee, adder);
      model.setResultCode(result.get("resultCode").toString());
      model.setResultMsg(result.get("msg").toString());
      return model;
   }

   /**
    * 花名冊 查询单个员工信息
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/{incumbencyId}/queryEmployeeInfo", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryEmployeeInfo(@PathVariable("incumbencyId") String incumbencyId) {
      ResponseModel model = new ResponseModel();
      Map<String, Object> result = rosterService.queryEmployeeInfo(incumbencyId);
      model.setData(result);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 花名冊 导出
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/exportRosterList", method = RequestMethod.GET)
   public ResponseModel exportRosterList(HttpServletRequest req, HttpServletResponse response,
                                         @RequestParam(value = "entryRegisterName", required = false) String entryRegisterName,
                                         @RequestParam(value = "workNumber", required = false) String workNumber,
                                         @RequestParam(value = "jobName", required = false) String jobName,
                                         @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
                                         @RequestParam(value = "cardCode", required = false) String cardCode,
                                         @RequestParam(value = "deptName", required = false) String deptName,
                                         @RequestParam(value = "status", required = false) String status,
                                         @RequestParam(value = "contractWarning", required = false) Boolean contractWarning,
                                         @RequestParam(value = "timeOfEntryStart", required = false) String timeOfEntryStart,
                                         @RequestParam(value = "timeOfEntryEnd", required = false) String timeOfEntryEnd,
                                         @RequestParam(value = "startCreateTime", required = false) String startCreateTime,
                                         @RequestParam(value = "endCreateTime", required = false) String endCreateTime) {
      ResponseModel model = new ResponseModel();
      ReqRosterPageQuery reqRosterPageQuery = new ReqRosterPageQuery();
      reqRosterPageQuery.setEntryRegisterName(entryRegisterName);
      reqRosterPageQuery.setWorkNumber(workNumber);
      reqRosterPageQuery.setJobName(jobName);
      reqRosterPageQuery.setPhoneNumber(phoneNumber);
      reqRosterPageQuery.setCardCode(cardCode);
      reqRosterPageQuery.setDeptName(deptName);
      reqRosterPageQuery.setStatus(status);
      reqRosterPageQuery.setTimeOfEntryStart(timeOfEntryStart);
      reqRosterPageQuery.setTimeOfEntryEnd(timeOfEntryEnd);
      reqRosterPageQuery.setStartCreateTime(startCreateTime);
      reqRosterPageQuery.setEndCreateTime(endCreateTime);
      reqRosterPageQuery.setContractWarning(contractWarning);
      // 当前系统登录用户
      String userId = (String) req.getAttribute(JwtUtil.USER_NAME);
      String backendRole = (String) req.getAttribute(JwtUtil.BACKEND_ROLE);
      // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
      if (StringUtils.isBlank(backendRole) || (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0)) {
         reqRosterPageQuery.setCreatePerson(userId);
      }
      List<ExcelRosterList> result = rosterService.exportRosterList(reqRosterPageQuery);
      String exportRosterList = exportEntity.getRoster();
      if (CollectionUtils.isEmpty(result)) {
         model.setResultCode("0004");
         model.setResultMsg("您查询到的花名册名单为空");
         return model;
      }
      HSSFWorkbook hssfWorkbook = ExportExcel.exportExcel("花名册列表", exportRosterList, result, "yyyyMMdd");
      response.setCharacterEncoding("utf-8");
      if (null != hssfWorkbook) {
         try {
            // 清空response
            response.reset();
            response.addHeader("Content-Disposition", "attachment;filename=" + CommonUtil.getFileName("花名册列表-导出.xls", req));
            response.setContentType("application/octet-stream");
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            hssfWorkbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
      return null;
   }


   /**
    * 花名冊 人事动态 操作
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/employeeDynamicOperate", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel rosterEmployeeDynamicOperate(HttpServletRequest req, @RequestBody ReqRecruitIncumbencyHistory reqRecruitIncumbencyHistory) {
      ResponseModel model = new ResponseModel();
      RecruitIncumbencyHistory recruitIncumbencyHistory = new RecruitIncumbencyHistory();
      BeanUtils.copyProperties(reqRecruitIncumbencyHistory, recruitIncumbencyHistory);
      // 当前系统登录用户
      String adder = (String) req.getAttribute(JwtUtil.USER_NAME);
      recruitIncumbencyHistory.setAdder(adder);
      rosterService.saveIncumbencyHistory(recruitIncumbencyHistory);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 获取 二三级 部门树
    *
    * @param
    * @param
    * @param
    * @return
    */
   @RequestMapping(value = "/queryDeptTree", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryDeptTree() {
      ResponseModel model = new ResponseModel();
      model.setResultCode("0000");
      model.setData(organizationService.queryDeptTree());
      return model;
   }

   /**
    * 查询数据字段
    *
    * @return
    */
   @RequestMapping(value = "/queryDictionary", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public ResponseModel queryDictionary() {
      ResponseModel model = new ResponseModel();
      // 性别
      List<Dictionary> sex = dictionaryService.queryDictionaryByType("sex");
      // 婚姻状况
      List<Dictionary> maritalStatus = dictionaryService.queryDictionaryByType("maritalStatus");
      // 户口性质
      List<Dictionary> householdNature = dictionaryService.queryDictionaryByType("householdNature");
      // 政治面貌
      List<Dictionary> politicalOutlook = dictionaryService.queryDictionaryByType("politicalOutlook");
      // 学历
      List<Dictionary> educationExperience = dictionaryService.queryDictionaryByType("educationExperience");
      // 学历性质
      List<Dictionary> educationNature = dictionaryService.queryDictionaryByType("educationNature");
//        //部门  取自 新组织架构
//        // 查询数据字典 部门
//        List<Dictionary> department = dictionaryService.queryDictionaryByType("department");
      //用工性质
      List<Dictionary> jobNature = dictionaryService.queryDictionaryByType("jobNature");
      //合同主体
      List<Dictionary> contractSubject = dictionaryService.queryDictionaryByType("contractSubject");
      //合同类型
      List<Dictionary> contractType = dictionaryService.queryDictionaryByType("contractType");


      Map<String, Object> resultMap = new HashMap<String, Object>();
      resultMap.put("sex", sex);
      resultMap.put("politicalOutlook", politicalOutlook);
      resultMap.put("maritalStatus", maritalStatus);
      resultMap.put("householdNature", householdNature);
      resultMap.put("educationExperience", educationExperience);
      resultMap.put("educationNature", educationNature);
      resultMap.put("jobNature", jobNature);
      resultMap.put("contractSubject", contractSubject);
      resultMap.put("contractType", contractType);
      model.setData(resultMap);
      model.setResultCode("0000");
      model.setResultMsg("");
      return model;
   }

   /**
    * 根据真实姓名查找花名册用户
    * @param
    * @param reqSelectRosterByReallyName
    * @param result
    * @return
    */
   @RequestMapping(value = "/selectRosterByReallyName", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public RecruitIncumbencyUserBaseInfo selectRosterByReallyName(@RequestBody @Valid ReqSelectRosterByReallyName reqSelectRosterByReallyName, BindingResult result) {

//      if (result.hasErrors()) {
//         return null;
//      }

      RecruitIncumbencyUserBaseInfo recruitIncumbencyUserBaseInfo = rosterService.selectRosterByReallyName(reqSelectRosterByReallyName.getReallyName());

      return recruitIncumbencyUserBaseInfo;
   }

}

