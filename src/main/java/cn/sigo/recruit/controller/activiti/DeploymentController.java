/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: DeploymentController.java
 * Author:   zhixiang.meng
 * Date:     2018年7月4日 下午3:44:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller.activiti;

import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.sigo.recruit.model.ResponseModel;

/**
 * 流程定义 文件上传，查询等
 *
 * @author zhixiang.meng
 */
@RestController
@RequestMapping("/activitiDeployment")
public class DeploymentController {
    
    private static Logger LOGGER = LoggerFactory.getLogger(DeploymentController.class);
    
    @Autowired
    private RepositoryService repositoryService;
    
    /**
     * 
     * 查询流程定义
     *
     * @return
     */
    @RequestMapping(value = "/processList")
    public ResponseModel processList() {
        ResponseModel model = new ResponseModel();
        List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery().list();
        model.setData(processDefinitionList);
        model.setResultCode("0000");
        model.setResultMsg("");
        return model;
    }
    
    /**
     * 部署流程资源
     * @return
     */
    @RequestMapping(value = "/deploy")
    public ResponseModel deploy(@RequestParam(value = "file", required = true) MultipartFile file) {
        ResponseModel model = new ResponseModel();
        // 获取上传的文件名
        String fileName = file.getOriginalFilename();
        try {
            // 得到输入流（字节流）对象
            InputStream fileInputStream = file.getInputStream();
            // 文件的扩展名
            String extension = FilenameUtils.getExtension(fileName);
            // zip或者bar类型的文件用ZipInputStream方式部署
            DeploymentBuilder deployment = repositoryService.createDeployment();
            if (extension.equals("zip") || extension.equals("bar")) {
                ZipInputStream zip = new ZipInputStream(fileInputStream);
                deployment.addZipInputStream(zip);
            } else {
                // 其他类型的文件直接部署
                deployment.addInputStream(fileName, fileInputStream);
            }
            deployment.deploy();
            model.setResultCode("0000");
            model.setResultMsg("");
        } catch (Exception e) {
            LOGGER.error("error on deploy process, because of file input stream");
            model.setResultCode("0004");
            model.setResultMsg("流程定义部署失败");
        }
        return model;
    }

}
