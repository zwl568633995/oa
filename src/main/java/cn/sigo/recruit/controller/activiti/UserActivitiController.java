/*
  * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: UserActivitiController.java
 * Author:   zhixiang.meng
 * Date:     2018年7月3日 上午11:40:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.controller.activiti;

import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import cn.sigo.recruit.model.ResponseErpUserInfo;
import cn.sigo.recruit.model.ResponseModel;

/**
 * activiti user 表(act_id_user) 操作
 *
 * @author zhixiang.meng
 */
@RestController
public class UserActivitiController {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(UserActivitiController.class);
    
    // 注入activiti 身份service
    @Autowired
    private IdentityService identityService;
    
    /**
     * 
     * 保存用户信息到 act_id_user表
     *
     * @param user:activiti用户
     * @return
     */
    public ResponseModel saveActivitiUser(ResponseErpUserInfo info) {
        ResponseModel model = new ResponseModel();
        try {
            List<User> users = identityService.createUserQuery().userId(info.getUserName()).list();
            if(!(null != users && !users.isEmpty())) {
                User user = identityService.newUser(info.getUserName());
                user.setFirstName(info.getRealName());
                identityService.saveUser(user);
            } else {
                LOGGER.info("user name[" + info.getRealName() + "] already existing table act_id_user");
            }
        }catch(Exception e) {
            LOGGER.error("save activiti act_id_user error:" + e.getMessage());
        }
        model.setResultCode("0000");
        model.setResultMsg("");
        return model;
    }
    

}
