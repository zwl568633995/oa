package cn.sigo.recruit.feign;

import cn.sigo.recruit.model.response.ADResult;
import cn.sigo.recruit.model.response.ADUser;
import cn.sigo.recruit.model.response.RespADAddDept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(url = "${url.ad.url}", name = "adUrl")
public interface ADFeign {

   /**
    * AD 添加部门端口
    *
    * @param respADAddDept 添加部门需要的参数
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/dept/add", produces = {"application/json;charset=UTF-8"})
   public ADResult deptAdd(@RequestBody RespADAddDept respADAddDept);

   /**
    *新增用户信息
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/add",produces = { "application/json;charset=UTF-8" })
   public ADResult addUser(@RequestBody ADUser user);

}
