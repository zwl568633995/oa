package cn.sigo.recruit.feign;

import cn.sigo.recruit.model.response.SendNotification;
import cn.sigo.recruit.model.response.SendNotificationResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Component(value = "sendNotificationFeign")
@FeignClient(url = "${url.notification}", name = "notificationUrl")
public interface SendNotificationFeign {

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/message/notify", produces = {"application/json;charset=UTF-8"})
    public SendNotificationResult messageNotify(@RequestBody SendNotification sendNotification);
}
