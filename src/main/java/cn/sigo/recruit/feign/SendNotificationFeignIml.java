package cn.sigo.recruit.feign;

import cn.sigo.recruit.model.response.SendNotification;
import cn.sigo.recruit.model.response.SendNotificationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Service("sendNotificationFeign1")
public class SendNotificationFeignIml implements SendNotificationFeign{

    @Autowired
    SendNotificationFeign sendNotificationFeign;

    @Override
    public SendNotificationResult messageNotify(SendNotification sendNotification) {
        return sendNotificationFeign.messageNotify(sendNotification);
    }
}
