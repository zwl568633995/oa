package cn.sigo.recruit.feign;

import cn.sigo.recruit.model.response.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Component(value = "uMSFeign")
@FeignClient(url = "${url.ums.url}", name = "ums")
public interface UMSFeign {

   /**
    * UMS 修改部门端口
    *
    * @param teamEdit 修改部门需要的参数
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/team/editSubTeam", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSResponseModel editSubTeam(@RequestBody RespUmsEditTeam teamEdit);

   /**
    * UMS 修改用户接口
    *
    *  @param respUMSUpdateUser 修改用户需要的额参数
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/updateUser", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSResponseModel updateUser(@RequestBody  RespUMSUpdateUser respUMSUpdateUser);

   /**
    * UMS 添加用户接口
    * @param responseToUmsSaveUser
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/saveUser", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSResponseModel saveUser(@RequestBody ResponseToUmsSaveUser responseToUmsSaveUser);

   /**
    * UMS 通过花名册id查找用户
    * @param rosterId
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/selectUserByRosterId",produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSUser selectUserByRosterId(@RequestBody Integer rosterId);

   /**
    *  根据角色名 查找用户
    * @param umsRole
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/selectByRoleName", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSNewResponseModel selectByRoleName(@RequestBody UMSRole umsRole);


   /**
    * UMS 通过部门名称查找部门信息
    * @param respSelectTeamByDeptName
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/team/selectTeamByDeptName", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSTeam selectTeamByDeptName(@RequestBody RespUMSSelectTeamByDeptName respSelectTeamByDeptName);

   /**
    * 接收HR 传递过来的数据添加下级团队
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/team/addSubTeamFromHr", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSResponseModel addSubTeam(@RequestBody  RespUMSAddHRTeamToUMS respUMSAddHRTeamToUMS);

   /**
    * 设置用户团队关系
    * @param reqSetUserTeam
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/team/setUserTeam", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public  UMSResponseModel setUserTeam(@RequestBody RespUMSSetUserTeam reqSetUserTeam);

   /**
    * 通过真实姓名匹配花名册id
    * @param umsSelectByReallyName
    * @return
    */
   @Transactional(propagation = Propagation.REQUIRED)
   @RequestMapping(value = "/user/selectByReallyName", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
   public UMSResponseModel selectByReallyName(@RequestBody RespUMSSelectByReallyName umsSelectByReallyName);

}
