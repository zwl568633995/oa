package cn.sigo.recruit.feign;

import cn.sigo.recruit.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("uMSFeign1")
public class UMSFeignImpl implements UMSFeign {
    @Autowired
    private UMSFeign umsFeign;
    @Override
    public UMSResponseModel editSubTeam(RespUmsEditTeam teamEdit) {
        return umsFeign.editSubTeam(teamEdit);
    }

    @Override
    public UMSResponseModel updateUser(RespUMSUpdateUser respUMSUpdateUser) {
        return umsFeign.updateUser(respUMSUpdateUser);
    }

    @Override
    public UMSResponseModel saveUser(ResponseToUmsSaveUser responseToUmsSaveUser) {
        return umsFeign.saveUser(responseToUmsSaveUser);
    }

    @Override
    public UMSUser selectUserByRosterId(Integer rosterId) {
        return umsFeign.selectUserByRosterId(rosterId);
    }

    @Override
    public UMSNewResponseModel selectByRoleName(UMSRole umsRole) {
        return umsFeign.selectByRoleName(umsRole);
    }

    @Override
    public UMSTeam selectTeamByDeptName(RespUMSSelectTeamByDeptName respSelectTeamByDeptName) {
        return umsFeign.selectTeamByDeptName(respSelectTeamByDeptName);
    }

    @Override
    public UMSResponseModel addSubTeam(RespUMSAddHRTeamToUMS respUMSAddHRTeamToUMS) {
        return umsFeign.addSubTeam(respUMSAddHRTeamToUMS);
    }

    @Override
    public UMSResponseModel setUserTeam(RespUMSSetUserTeam reqSetUserTeam) {
        return umsFeign.setUserTeam(reqSetUserTeam);
    }

    @Override
    public UMSResponseModel selectByReallyName(RespUMSSelectByReallyName umsSelectByReallyName) {
        return umsFeign.selectByReallyName(umsSelectByReallyName);
    }
}
