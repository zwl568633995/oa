package cn.sigo.recruit.listener;

import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.mapper.RecruitInterviewManagementMapper;
import cn.sigo.recruit.model.Notification;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewManagementResult;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.service.RecruitInterviewManagementResultService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import cn.sigo.recruit.util.ApplicationContextUtil;
import cn.sigo.recruit.util.LogHelper;
import cn.sigo.recruit.util.PinyinUtil;
import org.activiti.engine.delegate.*;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 面试流程监听器
 */
@Component
public class InterviewProcessListener implements TaskListener, ExecutionListener {

    private static final long serialVersionUID = 1L;

    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private RecruitInterviewManagementService interviewManagementService;
    @Autowired
    private RecruitInterviewManagementResultService recruitInterviewManagementResultService;

    /**
     * 节点监听器
     *
     * @param delegateTask
     */
    private Expression message;

    public Expression getMessage() {
        return message;
    }

    public void setMessage(Expression message) {
        this.message = message;
    }

    @Override
    public void notify(DelegateTask delegateTask) {
        //System.out.println("init notify..............");
        if (null == notificationMapper) {
            notificationMapper = (NotificationMapper) ApplicationContextUtil.getBean("notificationMapper", NotificationMapper.class);
        }
        if (null == sendNotificationFeign) {
            sendNotificationFeign = (SendNotificationFeign) ApplicationContextUtil.getBean("sendNotificationFeign1", SendNotificationFeign.class);
        }
        if (null == umsFeign) {
            umsFeign = (UMSFeign) ApplicationContextUtil.getBean("uMSFeign1", UMSFeign.class);
        }
        if (null == interviewManagementService) {
            interviewManagementService = (RecruitInterviewManagementService) ApplicationContextUtil.getBean("recruitInterviewManagementService", RecruitInterviewManagementService.class);
        }
        if (null == recruitInterviewManagementResultService) {
            recruitInterviewManagementResultService = (RecruitInterviewManagementResultService) ApplicationContextUtil.getBean("recruitInterviewManagementResultService", RecruitInterviewManagementResultService.class);
        }

        //获取监听器配置的事件名称
        String eventName = delegateTask.getEventName();
        // 节点id :
        String userTaskId = delegateTask.getTaskDefinitionKey();

        //获取任务名称 : 是否面试,填写应聘登记表
        String taskName = delegateTask.getName();

        //获取监听器中 传递过来的 面试单号
        String interviewCode = (String) delegateTask.getVariable("interviewCode");
        //面试管理的Id
        String interviewerId = (String) delegateTask.getVariable("interviewerId");
        //面试管理单
        RecruitInterviewManagement interviewManagement = interviewManagementService.queryInterviewManagentByCode(interviewCode);

//        //应聘人姓名
//        String applyName = (String) delegateTask.getVariable("applyName");
//        //部门
//        String applyDept = (String) delegateTask.getVariable("applyDept");
//        //职位
//        String applyJobName = (String) delegateTask.getVariable("applyJobName");
//        //来源
//        String resumeSource = (String) delegateTask.getVariable("resumeSource");
//        //初试面试官
//        String firstInterviewer = (String) delegateTask.getVariable("firstInterviewer");
//        //初试时间
//        String firstTime = (String) delegateTask.getVariable("firstTime");
//        //初试反馈
//        String firsFeedback = (String) delegateTask.getVariable("firsFeedback");
//        //复试面试官
//        String secondInterviewer = (String) delegateTask.getVariable("secondInterviewer");
//        //复试时间
//        String secondTime = (String) delegateTask.getVariable("secondTime");
//        //复试反馈
//        String secondFeedback = (String) delegateTask.getVariable("secondFeedback");
//        //拟定薪酬
//        String feedbackSalary = (String) delegateTask.getVariable("feedbackSalary");
//        //offer薪酬
//        String offerSalary = (String) delegateTask.getVariable("offerSalary");

        //应聘人姓名
        String applyName = interviewManagement.getApplyName();
        //部门
        String applyDept = interviewManagement.getApplyDept();
        //职位
        String applyJobName = interviewManagement.getApplyJobName();
        //来源
        String resumeSource = interviewManagement.getResumeSource();
        //初试面试官
        String firstInterviewer = interviewManagement.getFirstInterviewer();
        //初试时间
        String firstTime = interviewManagement.getFirstTime();
        if (null != firstTime) {
            firstTime = firstTime.substring(0, 16);
        }
        //初试反馈
        String firsFeedback = (String) delegateTask.getVariable("firsFeedback");
        //复试面试官
        String secondInterviewer = interviewManagement.getSecondInterviewer();
        //复试时间

        String secondTime = interviewManagement.getSecondTime();
        if (null != secondTime) {
            secondTime = secondTime.substring(0, 16);
        }
        //复试反馈
        String secondFeedback = (String) delegateTask.getVariable("secondFeedback");
        //拟定薪酬
        String feedbackSalary = (String) delegateTask.getVariable("feedbackSalary");
        //offer薪酬
        String offerSalary = (String) delegateTask.getVariable("offerSalary");

        //offer审核人
        String offerAuditer = interviewManagement.getOfferAudit();
        //页面操作
        String operation = (String) delegateTask.getVariable("operation");


        RecruitInterviewManagementResult interviewManagementResult = recruitInterviewManagementResultService.queryResultForManagementId(interviewManagement.getId());
        if (null != interviewManagementResult) {
            firsFeedback = interviewManagementResult.getFirstRemarks();
            secondFeedback = interviewManagementResult.getSecondRemarks();
            feedbackSalary = interviewManagementResult.getFeedbackSalary();
            offerSalary = interviewManagementResult.getOfferSalary();
        }


        //复试面试官 没有数据的时候
        if (!StringUtils.isNotBlank(secondInterviewer)) {
            secondInterviewer = "xudou:无";
        }

        /**
         * 根据不同的 节点id 发送不同的 消息
         */
        String url = "https://exmail.qq.com";
        Notification notification = new Notification();
        Object[] params = null;
        //收件人集合
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();

        /**
         * 初试面试官 姓名
         */
        String firstInterviewerName = "无";
        firstInterviewerName = getFirstInterviewerName(firstInterviewer);
        String secondInterviewerName = "无";
        secondInterviewerName = getFirstInterviewerName(secondInterviewer);

        /**
         * 根据 不同的 节点 事件 选择不同的消息内容 和 收件人
         */
        if ("create".endsWith(eventName)) {

            //logHelper.LoggerSendINFO("任务监听器启动" + codeValue);
            // 是否面试 节点
            if ("confirmInterview".equals(userTaskId)) {
                //发送待初试通知
                notification = notificationMapper.selectByNode("提交面试单");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //模板中的占位符 : 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6url.
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, url};

                //收件人:初试面试官
                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

            }
//            else if ("interviewTask".equals(userTaskId)) {
//                /**
//                 * 开始 填写应聘登记表
//                 */
//                //获取要发送消息的模板 : 初试准备通知
//                notification = notificationMapper.selectByNode("候选人开始应聘登记");
//
//                url = MessageFormat.format(notification.getUrl(), interviewCode);
//
//                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6url.
//                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, url};
//
//                //选择收件人
//                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);
//            }
            else if ("usertask1".equals(userTaskId)) {
//                /**
//                 *  页面点击  复试待定
//                 */
//                if ("复试待定".equals(operation)) {
//                    notification = notificationMapper.selectByNode("复试待定");
//
//                    //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.url.
//                    params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, url};
//
//                    receiveAccountList = getRoleReceiveAccountList();
//                }

                /**
                 * 复试时间安排完成
                 */
                if ("复试时间安排完成".equals(operation)) {
                    //获取要发送消息的模板 : 复试时间安排完成
                    notification = notificationMapper.selectByNode("复试时间安排完成");
                    url = MessageFormat.format(notification.getUrl(), interviewCode);

                    //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.url.
                    params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, url};

                    //选择收件人 : 复试面试官
                    ReceiveAccount receiveAccount = new ReceiveAccount();
                    receiveAccount.setReceiveMsgAccount(secondInterviewer.split(":")[0] + "@sigo.cn");
                    receiveAccount.setReceiveMsgAccountName(secondInterviewer.split(":")[1]);
                    receiveAccountList.add(receiveAccount);
                }
            } else if ("entryTask".equals(userTaskId)) {
                /**
                 *  开始入职登记
                 */
                //获取要发送消息的模板 : 开始入职登记
                notification = notificationMapper.selectByNode("开始入职登记");
                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.offer薪资.12.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, offerSalary, url};

                //选择收件人 : 初，复试面试官及offer审核人

                if (!"无".equals(secondInterviewerName)) {
                    firstInterviewer = firstInterviewer + ";" + secondInterviewer;
                }
                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(offerAuditer.split(":")[0] + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(offerAuditer.split(":")[1]);
                receiveAccountList.add(receiveAccount);
            }

        } else if ("complete".endsWith(eventName)) {
            /**
             * 编辑事件
             */
            //填写 应聘登记表结束 发送 初试开始通知
            if ("interviewTask".equals(userTaskId)) {
                notification = notificationMapper.selectByNode("候选人应聘登记完成");
                //消息内容
                url = notification.getUrl();
                url = MessageFormat.format(url, interviewCode);
                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6url.
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, url};

                //选择收件人
                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);
            }
            //填写 offer 审核状态改为 同意
//            if ("autidOfferTask".equals(userTaskId)) {
//                notification = notificationMapper.selectByNode("同意发offer");
//
//                url = MessageFormat.format(notification.getUrl(), interviewCode);
//
//                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.offer薪资.12.url.
//                if (null == secondTime) {
//                    secondTime = "无";
//                }
//                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, offerSalary, url};
//
//                // 收件人 : 初，复试面试官及人事（角色=recruit-人事员工）
//                String firstAndSenced = firstInterviewer + ";" + secondInterviewer;
//                receiveAccountList = transFirstToReceiveAccount(firstAndSenced);
//
//                List<ReceiveAccount> roleReceiveAccountList = getRoleReceiveAccountList();
//
//                //合并两个集合
//                receiveAccountList.addAll(roleReceiveAccountList);
//            }

            //填写 应聘登记表结束 发送 初试开始通知
            if ("interviewTask2".equals(userTaskId)) {

//                notification = notificationMapper.selectByNode("初试不通过");
//                //消息内容
//                url = MessageFormat.format(notification.getUrl(), interviewCode);
//                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.url.
//                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, url};
//
//                //选择收件人
//                receiveAccountList = getRoleReceiveAccountList();
            }
        }

        /**
         *  发送消息
         */
        if (null != params) {
            sendNotification(notification, params, receiveAccountList);
        }

    }

    /**
     * 流程监听器
     *
     * @param execution
     */
    @Override
    public void notify(DelegateExecution execution) {
        //赋值我们需要的 Bean
        if (null == notificationMapper) {
            notificationMapper = (NotificationMapper) ApplicationContextUtil.getBean("notificationMapper", NotificationMapper.class);
        }
        if (null == sendNotificationFeign) {
            sendNotificationFeign = (SendNotificationFeign) ApplicationContextUtil.getBean("sendNotificationFeign1", SendNotificationFeign.class);
        }
        if (null == umsFeign) {
            umsFeign = (UMSFeign) ApplicationContextUtil.getBean("uMSFeign1", UMSFeign.class);
        }
        if (null == interviewManagementService) {
            interviewManagementService = (RecruitInterviewManagementService) ApplicationContextUtil.getBean("recruitInterviewManagementService", RecruitInterviewManagementService.class);
        }
        if (null == recruitInterviewManagementResultService) {
            recruitInterviewManagementResultService = (RecruitInterviewManagementResultService) ApplicationContextUtil.getBean("recruitInterviewManagementResultService", RecruitInterviewManagementResultService.class);
        }

        //线段的Id
        ExecutionEntityImpl executionEntity = (ExecutionEntityImpl) execution;
        String flowId = executionEntity.getActivityId();

        //事件名称
        String eventName = execution.getEventName();

        //获取监听器中 传递过来的 面试单号
        String interviewCode = (String) execution.getVariable("interviewCode");

        RecruitInterviewManagement interviewManagement = interviewManagementService.queryInterviewManagentByCode(interviewCode);
        //面试管理的Id
        String interviewerId = (String) execution.getVariable("interviewerId");

//        //应聘人姓名
//        String applyName = (String) execution.getVariable("applyName");
//        //部门
//        String applyDept = (String) execution.getVariable("applyDept");
//        //职位
//        String applyJobName = (String) execution.getVariable("applyJobName");
//        //来源
//        String resumeSource = (String) execution.getVariable("resumeSource");
//        //初试面试官
//        String firstInterviewer = (String) execution.getVariable("firstInterviewer");
//        //初试时间
//        String firstTime = (String) execution.getVariable("firstTime");
//        //初试反馈
//        String firsFeedback = (String) execution.getVariable("firsFeedback");
//        //复试面试官
//        String secondInterviewer = (String) execution.getVariable("secondInterviewer");
//        //复试时间
//        String secondTime = (String) execution.getVariable("secondTime");
//        //复试反馈
//        String secondFeedback = (String) execution.getVariable("secondFeedback");
//        //拟定薪酬
//        String feedbackSalary = (String) execution.getVariable("feedbackSalary");
//        //offer薪酬
//        String offerSalary = (String) execution.getVariable("offerSalary");

        //应聘人姓名
        String applyName = interviewManagement.getApplyName();
        //部门
        String applyDept = interviewManagement.getApplyDept();
        //职位
        String applyJobName = interviewManagement.getApplyJobName();
        //来源
        String resumeSource = interviewManagement.getResumeSource();
        //初试面试官
        String firstInterviewer = interviewManagement.getFirstInterviewer();
        //初试时间
        String firstTime = interviewManagement.getFirstTime();
        if (null != firstTime) {
            firstTime = firstTime.substring(0, 16);
        }
        //初试反馈
        String firsFeedback = (String) execution.getVariable("firsFeedback");
        //复试面试官
        String secondInterviewer = interviewManagement.getSecondInterviewer();
        //复试时间
        String secondTime = interviewManagement.getSecondTime();
        if (null != secondTime) {
            secondTime = secondTime.substring(0, 16);
        }
        //复试反馈
        String secondFeedback = (String) execution.getVariable("secondFeedback");
        //拟定薪酬
        String feedbackSalary = (String) execution.getVariable("feedbackSalary");
        //offer薪酬
        String offerSalary = (String) execution.getVariable("offerSalary");

        //offer审核人
        String offerAuditer = interviewManagement.getOfferAudit();
        //offer审核审核意见
        String offerOpinion = (String) execution.getVariable("offerOpinion");

        //页面操作
        String operation = (String) execution.getVariable("operation");

        //复试面试官 没有数据的时候
        if (StringUtils.isBlank(secondInterviewer)) {
            secondInterviewer = "xudou:无";
        }

        RecruitInterviewManagementResult interviewManagementResult = recruitInterviewManagementResultService.queryResultForManagementId(interviewManagement.getId());
        if (null != interviewManagementResult) {
            firsFeedback = interviewManagementResult.getFirstRemarks();
            secondFeedback = interviewManagementResult.getSecondRemarks();
            if (null == secondFeedback) {
                secondFeedback = "无";
            }
            feedbackSalary = interviewManagementResult.getFeedbackSalary();
        }


        //拆分初试面试官,复试面试官 格式 pinyin:汉字 -->  汉字
        String firstInterviewerName = "无";
        firstInterviewerName = getFirstInterviewerName(firstInterviewer);
        String secondInterviewerName = "无";
        secondInterviewerName = getFirstInterviewerName(secondInterviewer);
        /**
         * 根据不同的 节点id 发送不同的 消息
         */
        String url = "https://exmail.qq.com";
        Notification notification = new Notification();
        Object[] params = null;
        //收件人集合
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();


        if ("take".equals(eventName)) {
            /**
             * 初试不通过
             */
            if ("flow34".equals(flowId)) {
                notification = notificationMapper.selectByNode("初试不通过");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.url.
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, url};

                receiveAccountList = getRoleReceiveAccountList();
            }
            /**
             * 初试通过无需复试，直接offer审核
             */
            if ("flow35".equals(flowId)) {
                notification = notificationMapper.selectByNode("初试通过且不需要复试");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.拟定薪资.8.url.
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, feedbackSalary, url};

                // 收件人 : offer审核人
                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(offerAuditer.split(":")[0] + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(offerAuditer.split(":")[1]);
                receiveAccountList.add(receiveAccount);
            }
            /**
             * 初试通过且需要复试
             */
            if ("flow31".equals(flowId)) {
                notification = notificationMapper.selectByNode("初试通过且需要复试");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.url.
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, url};

                // 收件人 : 人事（角色=recruit-人事员工）
                receiveAccountList = getRoleReceiveAccountList();
            }
            /**
             *  复试爽约
             */
            if ("flow38".equals(flowId)) {
                notification = notificationMapper.selectByNode("复试爽约");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, url};

                // 收件人 : 复试面试官  一个的时候
                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(secondInterviewer.split(":")[0] + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(secondInterviewer.split(":")[1]);
                receiveAccountList.add(receiveAccount);
            }
            /**
             *  复试不通过
             */
            if ("flow26".equals(flowId)) {
                notification = notificationMapper.selectByNode("复试不通过");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, url};

                // 收件人 : 人事（角色=recruit-人事员工）
                receiveAccountList = getRoleReceiveAccountList();
            }
            /**
             *  复试通过
             */
            if ("flow24".equals(flowId)) {
                notification = notificationMapper.selectByNode("复试通过");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, url};

                // 收件人 : offer审批人
                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(offerAuditer.split(":")[0] + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(offerAuditer.split(":")[1]);
                receiveAccountList.add(receiveAccount);
            }
            /**
             *  同意发offer
             */
            if ("flow10".equals(flowId)) {
                notification = notificationMapper.selectByNode("同意发offer");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.offer薪资.12.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, offerSalary, url};

                // 收件人 : 初，复试面试官及人事（角色=recruit-人事员工）
                if (!"无".equals(secondInterviewerName)) {
                    firstInterviewer = firstInterviewer + ";" + secondInterviewer;
                }
                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

                List<ReceiveAccount> roleReceiveAccountList = getRoleReceiveAccountList();

                //合并两个集合
                receiveAccountList.addAll(roleReceiveAccountList);
            }
            /**
             * 面试人 同意offer
             */
            if ("flow12".equals(flowId)) {
                notification = notificationMapper.selectByNode("接受offer");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.offer薪资.12.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, offerSalary, url};

                // 收件人 : 初，复试面试官及人事（角色=recruit-人事员工）
                if (!"无".equals(secondInterviewerName)) {
                    firstInterviewer = firstInterviewer + ";" + secondInterviewer;
                }

                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

                List<ReceiveAccount> roleReceiveAccountList = getRoleReceiveAccountList();

                //合并两个集合
                receiveAccountList.addAll(roleReceiveAccountList);
            }
            /**
             *  拒绝发offer : offer审核不通过
             */
            if ("flow28".equals(flowId)) {
                notification = notificationMapper.selectByNode("拒绝发offer");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.拟定薪资.11.offer审核意见.12.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, feedbackSalary, offerOpinion, url};

                // 收件人 : 初，复试面试官及人事（角色=recruit-人事员工）
                if (!"无".equals(secondInterviewerName)) {
                    firstInterviewer = firstInterviewer + ";" + secondInterviewer;
                }

                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

                List<ReceiveAccount> roleReceiveAccountList = getRoleReceiveAccountList();

                //合并两个集合
                receiveAccountList.addAll(roleReceiveAccountList);
            }

            /**
             *  拒绝offer : 面试的人拒绝offer
             */
            if ("flow29".equals(flowId)) {
                notification = notificationMapper.selectByNode("拒绝offer");

                url = MessageFormat.format(notification.getUrl(), interviewCode);

                //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.10.url.
                if (null == secondTime) {
                    secondTime = "无";
                }
                params = new Object[]{applyName, applyDept, applyJobName, resumeSource, firstInterviewerName, firstTime, firsFeedback, secondInterviewerName, secondTime, secondFeedback, url};

                // 收件人 : 初，复试面试官及offer审核人
                if (!"无".equals(secondInterviewerName)) {
                    firstInterviewer = firstInterviewer + ";" + secondInterviewer;
                }

                receiveAccountList = transFirstToReceiveAccount(firstInterviewer);

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(offerAuditer.split(":")[0] + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(offerAuditer.split(":")[1]);
                receiveAccountList.add(receiveAccount);

            }
        }

        //发送消息
        if (null != params) {
            sendNotification(notification, params, receiveAccountList);
        }

    }

    /**
     * 获取 角色名为 recruit-人事员工 的 所有收件人
     *
     * @return
     */
    private List<ReceiveAccount> getRoleReceiveAccountList() {
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();

        UMSRole umsRole = new UMSRole();
        umsRole.setRoleName("recruit-人事员工");

        UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
        List<Map<String, String>> umsUserList = (List<Map<String, String>>) umsNewResponseModel.getData();

        for (Map<String, String> umsUser : umsUserList) {
            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(umsUser.get("userName")) + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(umsUser.get("reallyName"));
            receiveAccountList.add(receiveAccount);
        }

        return receiveAccountList;
    }

    /**
     * 获取 初试面试官的 姓名 pinyin: 汉字 --> 汉字
     *
     * @param firstInterviewer
     * @return
     */
    private String getFirstInterviewerName(String firstInterviewer) {
        String firstInterviewerName = "";
        if (StringUtils.isNotBlank(firstInterviewer)) {
            if (firstInterviewer.contains(";")) {

                String[] firstInterviewerss = firstInterviewer.split(";");
                for (String firstInterviewers : firstInterviewerss) {
                    // pinyin,name
                    String[] split = firstInterviewers.split(":");
                    firstInterviewerName = firstInterviewerName + split[1];
                }
            } else {
                //只有一个人的时候
                firstInterviewerName = firstInterviewer.split(":")[1];
            }
        }

        return firstInterviewerName;
    }

    /**
     * 发送 消息
     *
     * @return
     */
    private Map<String, Object> sendNotification(Notification notification, Object[] params, List<ReceiveAccount> receiveAccountList) {
        Map<String, Object> resultMap = new HashMap<>();
        //获取消息模板
        String content = notification.getContent();
        //填充参数
        content = MessageFormat.format(content, params);

        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setMsgContent(content);
        sendNotification.setReceiveAccountList(receiveAccountList);

        //调用 发送服务
        SendNotificationResult sendNotificationResult = sendNotificationFeign.messageNotify(sendNotification);

        if (null != sendNotificationResult && sendNotificationResult.getCode().equals("success")) {
            //消息发送成功
            resultMap.put("code", "success");
            return resultMap;
        }

        resultMap.put("msg", "消息发送失败");
        return resultMap;
    }

    /**
     * 根据传入的面试官数据 获取 邮件的收件人
     *
     * @param firstInterviewer
     * @return
     */
    private List<ReceiveAccount> transFirstToReceiveAccount(String firstInterviewer) {
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();

        if (StringUtils.isNotBlank(firstInterviewer)) {
            if (firstInterviewer.contains(";")) {
                //多个面试官
                String[] everyOneSplit = firstInterviewer.split(";");
                for (String firstName : everyOneSplit) {
                    String[] nameSplit = firstName.split(":");
                    //拼音
                    String receiveMsgAccount = nameSplit[0];
                    //汉字
                    String receiveMsgAccountName = nameSplit[1];
                    //收件人
                    ReceiveAccount receiveAccount = new ReceiveAccount();
                    receiveAccount.setReceiveMsgAccount(receiveMsgAccount + "@sigo.cn");
                    receiveAccount.setReceiveMsgAccountName(receiveMsgAccountName);
                    receiveAccountList.add(receiveAccount);
                }
                return receiveAccountList;
            }

            //一个面试官
            String[] firstInterviewerSplit = firstInterviewer.split(":");
            //拼音
            String receiveMsgAccount = firstInterviewerSplit[0];
            //汉字
            String receiveMsgAccountName = firstInterviewerSplit[1];
            //收件人
            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(receiveMsgAccount + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(receiveMsgAccountName);
            receiveAccountList.add(receiveAccount);

        }

        return receiveAccountList;
    }
}
