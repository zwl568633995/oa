package cn.sigo.recruit.listener;

import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.model.Notification;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.util.ApplicationContextUtil;
import cn.sigo.recruit.util.PinyinUtil;
import org.activiti.engine.delegate.*;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 职位申请流程监听器
 */
@Component
public class JobApplicationProcessListener implements ExecutionListener, TaskListener {
    private static final long serialVersionUID = 1L;

    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;

    private Expression message;

    public Expression getMessage() {
        return message;
    }

    public void setMessage(Expression message) {
        this.message = message;
    }

    //申请部门
    //申请职位
    //职位评分
    //申请原因
    //需求人数
    //申请人
    //暂停人
    //已入职人数
    //关闭人
    //审核人

    /**
     * * @param applicationDepartment  申请部门
     * * @param jobName   职位
     * * @param jobScore  职位评分
     * * @param applicationReasons    申请原因
     * * @param numberOfPeople    招聘人数
     * * @param applyPeople   申请人
     * * @param jobCode     职位id
     * <p>
     * * @param procId    流程id
     * * @param auditLeader   审核人
     * * @param applyUserId   谁启动的
     * * @param entryNumber 已经入职人数
     * @param execution
     */

    /**
     * 流程监听器
     *
     * @param execution
     */
    @Override
    public void notify(DelegateExecution execution) {
        //给模板 和 邮件发送对象赋值
        if (null == notificationMapper) {
            notificationMapper = (NotificationMapper) ApplicationContextUtil.getBean("notificationMapper", NotificationMapper.class);
        }
        if (null == sendNotificationFeign) {
            sendNotificationFeign = (SendNotificationFeign) ApplicationContextUtil.getBean("sendNotificationFeign1", SendNotificationFeign.class);
        }
        if (null == umsFeign) {
            umsFeign = (UMSFeign) ApplicationContextUtil.getBean("uMSFeign1", UMSFeign.class);
        }

        String applicationDepartment = (String) execution.getVariable("applicationDepartment");
        String jobName = (String) execution.getVariable("jobName");
        String jobScore = (String) execution.getVariable("jobScore");
        String applicationReasons = (String) execution.getVariable("applicationReasons");
        String applyPeople = (String) execution.getVariable("applyPeople");
        String jobCode = (String) execution.getVariable("jobCode");
        String numberOfPeople = (String) execution.getVariable("numberOfPeople");
        // wuyicheng
        String auditLeader = (String) execution.getVariable("auditLeader");
        String auditOpinion = (String) execution.getVariable("auditOpinion");
        String entryNumber = (String) execution.getVariable("entryNumber");
        String moder = (String) execution.getVariable("moder");

        //UMS 用户 转换成的集合
        List<Map<String, String>> hrs = new ArrayList<>();

        ExecutionEntity executionEntity = (ExecutionEntity) execution;

        //获取 事件名称
        String eventName = execution.getEventName();

        //收件人集合
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();

        if ("take".equals(eventName)) {
            Object[] params = null;
            String id = ((ExecutionEntity) execution).getActivityId();
            Notification notification = new Notification();

            if (String.valueOf(id).equals("flow3")) {
                notification = notificationMapper.selectByNode("同意招聘申请");
                String url = notification.getUrl() + jobCode;
                params = new Object[]{applicationDepartment, jobName, jobScore, applicationReasons, numberOfPeople, applyPeople, url};
                auditLeader = applyPeople;
                UMSRole umsRole = new UMSRole();
                umsRole.setRoleName("recruit-人事员工");
                UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
                hrs = (List<Map<String, String>>) umsNewResponseModel.getData();

                //添加 收件人
                for (Map<String, String> umsUser : hrs) {
                    ReceiveAccount receiveAccount = new ReceiveAccount();
                    receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(umsUser.get("userName")) + "@sigo.cn");
                    receiveAccount.setReceiveMsgAccountName(umsUser.get("reallyName"));
                    receiveAccountList.add(receiveAccount);
                }

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(applyPeople) + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(applyPeople);
                receiveAccountList.add(receiveAccount);

            }
            if (String.valueOf(id).equals("flow9")) {
                notification = notificationMapper.selectByNode("拒绝招聘申请");
                String url = notification.getUrl() + jobCode;
                params = new Object[]{applicationDepartment, jobName, jobScore, applicationReasons, numberOfPeople, applyPeople, auditOpinion, url};

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(applyPeople) + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(applyPeople);
                receiveAccountList.add(receiveAccount);
            }
            if (String.valueOf(id).equals("flow5")) {
                notification = notificationMapper.selectByNode("返回修改");
                String url = notification.getUrl() + jobCode;
                params = new Object[]{applicationDepartment, jobName, jobScore, applicationReasons, numberOfPeople, applyPeople, auditOpinion, url};

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(applyPeople) + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(applyPeople);
                receiveAccountList.add(receiveAccount);
            }

            if (String.valueOf(id).equals("flow7")) {
                notification = notificationMapper.selectByNode("重新提交招聘申请");
                String url = notification.getUrl() + jobCode;
                params = new Object[]{applicationDepartment, jobName, jobScore, applicationReasons, numberOfPeople, applyPeople, url};

                ReceiveAccount receiveAccount = new ReceiveAccount();
                receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(auditLeader) + "@sigo.cn");
                receiveAccount.setReceiveMsgAccountName(auditLeader);
                receiveAccountList.add(receiveAccount);
            }
            if (String.valueOf(id).equals("flow8")) {
                notification = notificationMapper.selectByNode("关闭招聘申请单");
                String url = notification.getUrl() + jobCode;
                params = new Object[]{applicationDepartment, jobName, numberOfPeople, entryNumber, moder, url};
                UMSRole umsRole = new UMSRole();
                umsRole.setRoleName("recruit-人事员工");
                UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
                hrs = (List<Map<String, String>>) umsNewResponseModel.getData();
                //添加 收件人
                for (Map<String, String> umsUser : hrs) {
                    ReceiveAccount receiveAccount = new ReceiveAccount();
                    receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(umsUser.get("userName")) + "@sigo.cn");
                    receiveAccount.setReceiveMsgAccountName(umsUser.get("reallyName"));
                    receiveAccountList.add(receiveAccount);
                }
            }

            //发送 邮件
            Boolean sendBoolean = transReceiveAccount(notification, params, receiveAccountList);
            if (sendBoolean) {
                //成功
                System.out.println("消息通知成功");
            } else {
                //失败
                System.out.println("消息通知失败");
            }

        }
    }

    /**
     * 任务监听器
     *
     * @param delegateTask
     */
    @Override
    public void notify(DelegateTask delegateTask) {
        if (null == notificationMapper) {
            notificationMapper = (NotificationMapper) ApplicationContextUtil.getBean("notificationMapper", NotificationMapper.class);
        }
        if (null == sendNotificationFeign) {
            sendNotificationFeign = (SendNotificationFeign) ApplicationContextUtil.getBean("sendNotificationFeign1", SendNotificationFeign.class);
        }

        String applicationDepartment = (String) delegateTask.getVariable("applicationDepartment");
        String jobName = (String) delegateTask.getVariable("jobName");
        String jobScore = (String) delegateTask.getVariable("jobScore");
        String applicationReasons = (String) delegateTask.getVariable("applicationReasons");
        String applyPeople = (String) delegateTask.getVariable("applyPeople");
        String jobCode = (String) delegateTask.getVariable("jobCode");
        String numberOfPeople = (String) delegateTask.getVariable("numberOfPeople");
        String auditLeader = (String) delegateTask.getVariable("auditLeader");
        String auditOpinion = (String) delegateTask.getVariable("auditOpinion");

        //收件人集合
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();

        //获取监听的userTask Id
        String id = delegateTask.getTaskDefinitionKey();
        if (String.valueOf(id).equals("auditJobId")) {
            Notification notification = notificationMapper.selectByNode("提交招聘申请单");
            String url = notification.getUrl() + jobCode;
            Object[] params = {applicationDepartment, jobName, jobScore, applicationReasons, numberOfPeople, applyPeople, url};

            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(auditLeader) + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(auditLeader);
            receiveAccountList.add(receiveAccount);

            Boolean sendBooleam = transReceiveAccount(notification, params, receiveAccountList);
            if (sendBooleam) {
                //成功
                System.out.println("消息通知成功");
            } else {
                //失败
                System.out.println("消息通知失败");
            }
        }


    }

    //转换收件人
    private Boolean transReceiveAccount(Notification notification, Object[] params, List<ReceiveAccount> receiveAccountList) {
        //邮件模板: 部门, 职位 评分 原因 需求人数 申请人 url
        String content = notification.getContent();

        //邮件内容
        String format = MessageFormat.format(content, params);

        //调用消息发送服务需要的参数
        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setMsgContent(format);
        sendNotification.setReceiveAccountList(receiveAccountList);

        try {
            SendNotificationResult adResult = sendNotificationFeign.messageNotify(sendNotification);
            if (null != adResult && adResult.getCode().equals("success")) {
                System.out.println("消息发送成功...");
//                    logHelper.LoggerSendINFO("消息发送成功...");
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println("消息发送失败...");
//                logHelper.LoggerSendERROR("send notification error");
            return false;
        }
    }

}
