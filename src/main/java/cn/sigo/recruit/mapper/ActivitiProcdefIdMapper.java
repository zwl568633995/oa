package cn.sigo.recruit.mapper;

import org.apache.ibatis.annotations.Param;

import cn.sigo.recruit.model.ActivitiProcdefId;

public interface ActivitiProcdefIdMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ActivitiProcdefId record);

    int insertSelective(ActivitiProcdefId record);

    ActivitiProcdefId selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ActivitiProcdefId record);

    int updateByPrimaryKey(ActivitiProcdefId record);
    
    /**
     * 
     * 查询关系表中的 流程key 是否存在
     *
     * @param procdeKey
     * @return
     */
    ActivitiProcdefId selectByProcdeKey(@Param("procdeKey")String procdeKey);
    
    /**
     * 
     * 根据流程key 更新流程id
     *
     * @param procdefKey
     * @param procdefId
     * @return
     */
    int updateActivitiProcdefId(@Param("procdeKey")String procdefKey, @Param("procdefId")String procdefId);
}