package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.DepartmentLeader;

import java.util.List;

public interface DepartmentLeaderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DepartmentLeader record);

    int insertSelective(DepartmentLeader record);

    DepartmentLeader selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DepartmentLeader record);

    int updateByPrimaryKey(DepartmentLeader record);

    /**
     *
     * 批量插入  部门 - 负责人 中间表
     * @param
     * @return
     */
    int batchInsertDeptLeader(List<DepartmentLeader> list);

    /**
     *
     * 根据部门Id 假删除 部门 - 负责人 中间表
     * @param
     * @return
     */
    int deleteByDeptId(Long deptId);
}