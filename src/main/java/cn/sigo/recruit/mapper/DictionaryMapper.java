package cn.sigo.recruit.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.sigo.recruit.model.Dictionary;

public interface DictionaryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Dictionary record);

    int insertSelective(Dictionary record);

    Dictionary selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Dictionary record);

    int updateByPrimaryKey(Dictionary record);
    /**
     * 
     * 查询数据字典类型 <br>
     *
     * @return
     */
    List<String> queryDictionaryType();
    
    /**
     * 
     * 根据缓存类型查询具体的数据字典值<br>
     *
     * @param type：数据字典类型
     * @return
     */
    List<Dictionary> queryDictionaryByType(@Param("type")String type);
}