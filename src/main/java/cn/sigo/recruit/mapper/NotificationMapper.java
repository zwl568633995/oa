package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.Notification;

public interface NotificationMapper {
    int deleteByPrimaryKey(Integer templateId);

    int insert(Notification record);

    int insertSelective(Notification record);

    Notification selectByPrimaryKey(Integer templateId);

    /**
     * 根据节点查询模板
     * @param node
     * @return
     */
    Notification selectByNode(String node);

    int updateByPrimaryKeySelective(Notification record);

    int updateByPrimaryKey(Notification record);
}