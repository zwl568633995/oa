package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.OrganizationStructure;
import cn.sigo.recruit.model.req.ReqOrganization;
import cn.sigo.recruit.model.response.ResponseTeamTree;

import java.util.List;
import java.util.Map;

public interface OrganizationStructureMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrganizationStructure record);

    int insertSelective(OrganizationStructure record);

    OrganizationStructure selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrganizationStructure record);

    int updateByPrimaryKey(OrganizationStructure record);
    /**
     *
     * 部门列表-（根据部门名称模糊查询 组织架构）
     * @param department 非必传参数
     * @return
     */
    List<OrganizationStructure> queryDeptList(String department);

    /**
     *
     * 根据父级部门ID-查询下级部门
     * @param
     * @param
     * @return
     */
    List<OrganizationStructure> querySubDeptList(ReqOrganization reqOrganization);

    Integer countByTeamName(Map<String,Object> map);

    /**
     *
     * 查询 初始化 组织架构树
     * @param
     * @param
     * @return
     */
    List<ResponseTeamTree> initOrganization();

    /**
     *
     * 查询 初始化 下级部门列表
     * @param
     * @param
     * @return
     */
    List<OrganizationStructure> initSubDeptList();

    /**
     *
     * 根据 输入的 部门负责人 模糊查询
     * @param
     * @param
     * @return
     */
    List<Map<String,Object>> queryDeptLeaderInfo(String leaderName);

    /**
     *
     * 当前部门下的在职人数
     * @param
     * @param
     * @return
     */
    Integer countDepartmentPeople(String deptName);

    /**
     * 查询所有部门信息
     */
    List<OrganizationStructure> selectAll();
}