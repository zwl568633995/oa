package cn.sigo.recruit.mapper;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationCertificate;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitApplicationCertificateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitApplicationCertificate record);

    int insertSelective(RecruitApplicationCertificate record);

    RecruitApplicationCertificate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitApplicationCertificate record);

    int updateByPrimaryKey(RecruitApplicationCertificate record);
    
    /**
     * 
     * 根据应聘id查询证书记录
     *
     * @param applicationId
     */
    List<RecruitApplicationCertificate> queryCertificateByApplicationRegistId(@Param("applicationId")Integer applicationId);
}