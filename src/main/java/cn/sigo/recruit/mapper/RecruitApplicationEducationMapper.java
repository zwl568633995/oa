package cn.sigo.recruit.mapper;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationEducation;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitApplicationEducationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitApplicationEducation record);

    int insertSelective(RecruitApplicationEducation record);

    RecruitApplicationEducation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitApplicationEducation record);

    int updateByPrimaryKey(RecruitApplicationEducation record);
    
    /**
     * 
     * 根据应聘id查询教育经历
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationEducation> queryEducationByApplicationId(@Param("applicationId")Integer applicationId);
}