package cn.sigo.recruit.mapper;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationFamilyMember;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitApplicationFamilyMemberMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitApplicationFamilyMember record);

    int insertSelective(RecruitApplicationFamilyMember record);

    RecruitApplicationFamilyMember selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitApplicationFamilyMember record);

    int updateByPrimaryKey(RecruitApplicationFamilyMember record);
    
    /**
     * 根据应聘id查询家庭成员
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationFamilyMember> queryFamilyMemberByApplicationId(@Param("applicationId")Integer applicationId);
}