package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitApplicationRegist;
import cn.sigo.recruit.model.req.ReqApplicationRegist;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

public interface RecruitApplicationRegistMapper {
   int deleteByPrimaryKey(Integer id);

   int insert(RecruitApplicationRegist record);

   int insertSelective(RecruitApplicationRegist record);

   RecruitApplicationRegist selectByPrimaryKey(Integer id);

   int updateByPrimaryKeySelective(RecruitApplicationRegist record);

   int updateByPrimaryKey(RecruitApplicationRegist record);

   /**
    * 查询系统当前最大的应聘编号
    *
    * @return
    */
   String queryMaxCode();

   /**
    * 根据面试单号查询应聘登记表记录
    *
    * @param interviewCode
    * @return
    */
   RecruitApplicationRegist queryApplicationRegistByInterviewCode(@Param("interviewCode") String interviewCode);

   /**
    * 根据应聘登记表好查询应聘登记表记录
    *
    * @param applicationCode
    * @return
    */
   RecruitApplicationRegist queryApplicationRegistByApplicationCode(String applicationCode);

   /**
    * 根据身份证号码查询应聘登记表记录
    * @param idCardNo
    * @return
    */
   RecruitApplicationRegist queryApplicationRegistByIdCardNo(String idCardNo);

   /**
    * 应聘登记表 分页查询列表
    *
    * @param
    * @return
    */
   List<RecruitApplicationRegist> queryApplicationRegistrationList(@Param("reqApplicationRegist") ReqApplicationRegist reqApplicationRegist);
}