package cn.sigo.recruit.mapper;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationWork;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitApplicationWorkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitApplicationWork record);

    int insertSelective(RecruitApplicationWork record);

    RecruitApplicationWork selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitApplicationWork record);

    int updateByPrimaryKey(RecruitApplicationWork record);
    
    /**
     * 
     * 根据应聘者id查询工作经历
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationWork> queryWorkByApplicationId(@Param("applicationId")Integer applicationId);
}