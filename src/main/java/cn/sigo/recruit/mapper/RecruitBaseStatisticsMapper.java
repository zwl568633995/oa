package cn.sigo.recruit.mapper;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitBaseStatistics;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitBaseStatisticsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitBaseStatistics record);

    int insertSelective(RecruitBaseStatistics record);

    RecruitBaseStatistics selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitBaseStatistics record);

    int updateByPrimaryKey(RecruitBaseStatistics record);
    
    /**
     * 
     * 查询基本信息统计
     *
     * @param param
     * @return 
     */
    RecruitBaseStatistics queryBaseStatistics(@Param("param")Map<String, Object> param);
    
    /**
     * 
     * 更新统计数量
     *
     * @param param: key:field 需要更新的字段
     * @param id：id
     * @return
     */
    int updateBaseStatistics(@Param("param")Map<String, Object> param);

    RecruitBaseStatistics dataOverview(@Param("yearMonth")String yearMonth);

    List<RecruitBaseStatistics> departmentRecruitInfo(@Param("yearMonth")String yearMonth);

    List<RecruitBaseStatistics> positionRecruitInfo(Map<String, Object> param);
}