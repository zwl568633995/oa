package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitEntryHealthy;
import org.apache.ibatis.annotations.Param;

public interface RecruitEntryHealthyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitEntryHealthy record);

    int insertSelective(RecruitEntryHealthy record);

    RecruitEntryHealthy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitEntryHealthy record);

    int updateByPrimaryKey(RecruitEntryHealthy record);

    RecruitEntryHealthy findHealthInfoByRegisterId(@Param(value = "registerId")Long registerId);
}