package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitEntryRegisterBaseInfo;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegisterPageQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RecruitEntryRegisterBaseInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitEntryRegisterBaseInfo record);

    int insertSelective(RecruitEntryRegisterBaseInfo record);

    RecruitEntryRegisterBaseInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitEntryRegisterBaseInfo record);

    int updateByPrimaryKey(RecruitEntryRegisterBaseInfo record);

    String queryMaxCode();

    RecruitEntryRegisterBaseInfo findBaseInfoByCode(@Param("entryCode")String entryCode);

    List<ReqRecruitEntryRegisterPageQuery> pageQueryRecruitEntryRegister(ReqRecruitEntryRegisterPageQuery reqRecruitEntryRegisterPageQuery);

    int updateJobInfoByInterviewCode(@Param("interviewCode")String interviewCode);

    int isExistInEntryRegisterBase(Map<String,Object> map);
}