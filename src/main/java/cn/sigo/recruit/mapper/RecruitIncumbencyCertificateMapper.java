package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyCertificate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecruitIncumbencyCertificateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyCertificate record);

    int insertSelective(RecruitIncumbencyCertificate record);

    RecruitIncumbencyCertificate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyCertificate record);

    int updateByPrimaryKey(RecruitIncumbencyCertificate record);

    List<RecruitIncumbencyCertificate> findCertificateInfoByRegisterId(@Param(value = "registerId")Long registerId);

    void saveBatchRecruitIncumbencyCertificate(List<RecruitIncumbencyCertificate> list);

    List<RecruitIncumbencyCertificate> selectByIncumbencyId(String incumbencyId);
}