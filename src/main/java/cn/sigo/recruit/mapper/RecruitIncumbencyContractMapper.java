package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyContract;

public interface RecruitIncumbencyContractMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyContract record);

    int insertSelective(RecruitIncumbencyContract record);

    RecruitIncumbencyContract selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyContract record);

    int updateByPrimaryKey(RecruitIncumbencyContract record);

    RecruitIncumbencyContract selectByIncumbencyId(String incumbencyId);
}