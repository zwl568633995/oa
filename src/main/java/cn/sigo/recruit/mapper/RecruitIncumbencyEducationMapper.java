package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyEducation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecruitIncumbencyEducationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyEducation record);

    int insertSelective(RecruitIncumbencyEducation record);

    RecruitIncumbencyEducation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyEducation record);

    int updateByPrimaryKey(RecruitIncumbencyEducation record);

    List<RecruitIncumbencyEducation> findEducationInfoByRegisterId(@Param(value = "registerId")Long registerId);

    void saveBatchRecruitIncumbencyEducation(List<RecruitIncumbencyEducation> list);

    List<RecruitIncumbencyEducation> selectByIncumbencyId(String incumbencyId);
}