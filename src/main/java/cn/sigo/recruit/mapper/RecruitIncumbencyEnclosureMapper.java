package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyEnclosure;

import java.util.List;

public interface RecruitIncumbencyEnclosureMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyEnclosure record);

    int insertSelective(RecruitIncumbencyEnclosure record);

    RecruitIncumbencyEnclosure selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyEnclosure record);

    int updateByPrimaryKey(RecruitIncumbencyEnclosure record);

    void saveBatchRecruitIncumbencyEnclosure(List<RecruitIncumbencyEnclosure> list);

    List<RecruitIncumbencyEnclosure> selectByIncumbencyId(String incumbencyId);
}