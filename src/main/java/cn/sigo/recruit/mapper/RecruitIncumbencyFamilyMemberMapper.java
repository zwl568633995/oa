package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyFamilyMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecruitIncumbencyFamilyMemberMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyFamilyMember record);

    int insertSelective(RecruitIncumbencyFamilyMember record);

    RecruitIncumbencyFamilyMember selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyFamilyMember record);

    int updateByPrimaryKey(RecruitIncumbencyFamilyMember record);

    List<RecruitIncumbencyFamilyMember> findFamilyInfoByRegisterId(@Param(value = "registerId")Long registerId);

    void saveBatchRecruitIncumbencyFamilyMember(List<RecruitIncumbencyFamilyMember> list);

    List<RecruitIncumbencyFamilyMember> selectByIncumbencyId(String incumbencyId);
}