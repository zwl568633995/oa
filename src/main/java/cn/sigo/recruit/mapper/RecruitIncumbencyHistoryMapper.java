package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyHistory;

import java.util.List;

public interface RecruitIncumbencyHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyHistory record);

    int insertSelective(RecruitIncumbencyHistory record);

    RecruitIncumbencyHistory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyHistory record);

    int updateByPrimaryKey(RecruitIncumbencyHistory record);

    List<RecruitIncumbencyHistory> selectByIncumbencyId(String incumbencyId);
}