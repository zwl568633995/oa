package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyInfo;

import java.util.List;

public interface RecruitIncumbencyInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyInfo record);

    int insertSelective(RecruitIncumbencyInfo record);

    RecruitIncumbencyInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyInfo record);

    int updateByPrimaryKey(RecruitIncumbencyInfo record);

    RecruitIncumbencyInfo selectByIncumbencyId(String incumbencyId);

    List<RecruitIncumbencyInfo> selectAll();
}