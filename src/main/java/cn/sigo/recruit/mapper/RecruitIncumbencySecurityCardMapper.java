package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencySecurityCard;

public interface RecruitIncumbencySecurityCardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencySecurityCard record);

    int insertSelective(RecruitIncumbencySecurityCard record);

    RecruitIncumbencySecurityCard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencySecurityCard record);

    int updateByPrimaryKey(RecruitIncumbencySecurityCard record);

    RecruitIncumbencySecurityCard selectByIncumbencyId(String incumbencyId);
}