package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.ExcelRosterList;
import cn.sigo.recruit.model.RecruitIncumbencyUserBaseInfo;
import cn.sigo.recruit.model.ResponseRosterPageQuery;
import cn.sigo.recruit.model.req.ReqRosterPageQuery;

import java.util.List;

public interface RecruitIncumbencyUserBaseInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyUserBaseInfo record);

    int insertSelective(RecruitIncumbencyUserBaseInfo record);

    RecruitIncumbencyUserBaseInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyUserBaseInfo record);

    int updateByPrimaryKey(RecruitIncumbencyUserBaseInfo record);

    List<ResponseRosterPageQuery> pageQueryRosterList(ReqRosterPageQuery reqRosterPageQuery);

    RecruitIncumbencyUserBaseInfo selectByIncumbencyId(String incumbencyId);

    List<ExcelRosterList> exportRosterList(ReqRosterPageQuery reqRosterPageQuery);

   List<RecruitIncumbencyUserBaseInfo> selectRosterByReallyName(String reallyName);

   List<RecruitIncumbencyUserBaseInfo> selectAll();
}