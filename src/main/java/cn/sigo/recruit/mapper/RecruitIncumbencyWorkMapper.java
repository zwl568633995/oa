package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitIncumbencyWork;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecruitIncumbencyWorkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitIncumbencyWork record);

    int insertSelective(RecruitIncumbencyWork record);

    RecruitIncumbencyWork selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitIncumbencyWork record);

    int updateByPrimaryKey(RecruitIncumbencyWork record);

    List<RecruitIncumbencyWork> findWorkInfoByRegisterId(@Param(value = "registerId")Long registerId);

    void saveBatchRecruitIncumbencyWork(List<RecruitIncumbencyWork> list);

    List<RecruitIncumbencyWork> selectByIncumbencyId(String incumbencyId);
}