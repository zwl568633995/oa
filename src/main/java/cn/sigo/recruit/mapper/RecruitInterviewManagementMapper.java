package cn.sigo.recruit.mapper;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewPeople;
import cn.sigo.recruit.model.req.ReqInterviewManagementModel;
import cn.sigo.recruit.model.req.ReqModifyInterviewTime;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitInterviewManagementMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitInterviewManagement record);

    int insertSelective(RecruitInterviewManagement record);

    RecruitInterviewManagement selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitInterviewManagement record);

    int updateByPrimaryKey(RecruitInterviewManagement record);
    
    /**
     * 查询最近面试code
     *
     * @return
     */
    String queryMaxCode();
    
    /**
     * 
     * 根据手机号码 查询面试单
     *
     * @param phone
     * @param id：可空，传入表示除这个面试单号以外
     * @return
     */
    RecruitInterviewManagement queryByPhoneIsNotCode(@Param("param")Map<String, Object> param);
    
    /**
     * 查询面试管理里面所有带面试的
     * 
     *
     * @return
     */
    List<RecruitInterviewPeople> queryInterviews(@Param("interviewStatus")String interviewStatus);
    
    /**
     * 根据面试单号，查询面试信息
     *
     * @param interviewCode
     * @return
     */
    RecruitInterviewManagement queryInterviewManagentByCode(@Param("interviewCode")String interviewCode);

    /**
     * 根据手机号查询面试信息为待应聘登记
     * @param phone
     * @return
     */
    RecruitInterviewManagement queryInterviewManagentByPhone(String phone);
    /**
     * 查询面试管理
     */
    List<RecruitInterviewManagement> queryInterviewManagents(@Param("reqModel")ReqInterviewManagementModel reqModel);
    
    /**
     * 
     * 根据面试单号，修改面试单状态
     *
     * @param interviewCode
     * @param status
     * @return
     */
    int modifyInterviewStatusByCode(@Param("param")Map<String, Object> param);
    
    /**
     * 
     * 将流程id 更新到数据库里面
     *
     * @param procId
     * @param interviewId
     * @return
     */
    int modifyInterviewProcId(@Param("param")Map<String, Object> param);
    
    
    /**
     * 
     * 修改offer审核时间
     *
     * @param interviewerCode：面试单
     * @return
     */
    int modifyOfferAuitTime(@Param("interviewerCode")String interviewerCode);

    /**
     * 设置复试时间（面试单状态为：  待安排复试）
     *
     */
    int modifySecondInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime);

    /**
     * 修改 初试时间/复试时间
     *
     */
    int updateInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime);
}