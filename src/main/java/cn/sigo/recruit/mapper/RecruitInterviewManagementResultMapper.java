package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitInterviewManagementResult;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitInterviewManagementResultMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitInterviewManagementResult record);

    int insertSelective(RecruitInterviewManagementResult record);

    RecruitInterviewManagementResult selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitInterviewManagementResult record);

    int updateByPrimaryKey(RecruitInterviewManagementResult record);
    
    /**
     * 
     * 根据面试管理id查询面试结果
     *
     * @param managementId
     * @return
     */
    RecruitInterviewManagementResult queryResultForManagementId(@Param("managementId")Integer managementId);

}