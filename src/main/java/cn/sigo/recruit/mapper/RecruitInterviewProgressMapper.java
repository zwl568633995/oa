package cn.sigo.recruit.mapper;

import cn.sigo.recruit.model.RecruitInterviewProgress;

public interface RecruitInterviewProgressMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RecruitInterviewProgress record);

    int insertSelective(RecruitInterviewProgress record);

    RecruitInterviewProgress selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RecruitInterviewProgress record);

    int updateByPrimaryKey(RecruitInterviewProgress record);

    int updateByInterviewCode(RecruitInterviewProgress record);

    /**
     * 获取面试单 进度
     *
     */
    RecruitInterviewProgress queryInterviewProgress(String interviewCode);
}