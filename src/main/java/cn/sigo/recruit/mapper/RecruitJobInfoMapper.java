package cn.sigo.recruit.mapper;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.req.ReqJobInfoModel;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitJobInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitJobInfo record);

    int insertSelective(RecruitJobInfo record);

    RecruitJobInfo selectByPrimaryKey(long id);

    int updateByPrimaryKeySelective(RecruitJobInfo record);

    int updateByPrimaryKey(RecruitJobInfo record);
    
    /**
     * 
     * 查询最近添加职位的jobcode
     *
     * @return
     */
    String queryMaxJobCode();
    
    /**
     * 
     * 根据要求查询职位列表信息
     *
     * @param reqModel
     * @return
     */
    List<RecruitJobInfo> queryJobInfoList(@Param("reqModel")ReqJobInfoModel reqModel);
    
    /**
     * 
     * 根据部门名和职位名 查询不是职位最终状态的总记录数
     *
     * @param deptName：部门名
     * @param jobName：职位名
     * @return
     */
    int countForDeptNameAndJobName(@Param("param")Map<String, Object> param);
    
    /**
     * 
     * 根据职位编码 查询职位信息
     *
     * @param jobCode
     * @return
     */
    RecruitJobInfo queryByJobCode(@Param("jobCode")String jobCode);
    
    /**
     * 
     * 根据部门名和职位名 查询职位
     *
     * @param deptName：部门名
     * @param jobName：职位名
     * @return
     */
    RecruitJobInfo queryJobInfoByDeptNameAndJobName(@Param("param")Map<String, Object> param);
    
    /**
     * 更新职位信息，将activiti流程实例启动的id 更新到对应的job表中
     * 
     * @param procId
     * @param jobId
     * @return
     */
    int updateProcIdForJob(@Param("param")Map<String, Object> param);

    /**
     * 模糊查询获取应聘职位
     *
     * @param job
     * @return
     */
    List<String> jobPosition(@Param("job")String job);

    /**
     * 根据部门查询 所有 招聘中 的职位
     *
     * @param department(可为null)
     * @return
     */
    List<Map<String,Object>> queryJobPositionByDepartment(@org.apache.ibatis.annotations.Param("department") String department);
}