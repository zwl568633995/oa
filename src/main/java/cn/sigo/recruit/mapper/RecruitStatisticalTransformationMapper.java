package cn.sigo.recruit.mapper;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitStatisticalTransformation;
import cn.sigo.recruit.model.ResStatisticalTransformation;
import io.lettuce.core.dynamic.annotation.Param;

public interface RecruitStatisticalTransformationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RecruitStatisticalTransformation record);

    int insertSelective(RecruitStatisticalTransformation record);

    RecruitStatisticalTransformation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RecruitStatisticalTransformation record);

    int updateByPrimaryKey(RecruitStatisticalTransformation record);
    
    /**
     * 
     * 查询统计信息转化
     *
     * @param param
     * @return 
     */
    RecruitStatisticalTransformation queryStatisticalTransformation(@Param("param")Map<String, Object> param);
    
    /**
     * 更新具体状态的数量
     *
     * @param param
     * @return
     */
    int updateBaseStatisticalTransformation(@Param("param")Map<String, Object> param);

    List<ResStatisticalTransformation> departmentRecruitTransformation(@Param("yearMonth") String yearMonth);

    List<ResStatisticalTransformation> positionRecruitTransformation(Map<String,Object> param);
}