package cn.sigo.recruit.model;

import java.util.Date;

public class ActivitiProcdefId {
    private Integer id;

    private String procdefKey;

    private String procdefId;
    
    private String procdefName;

    private String adder;

    private Date addTime;

    private String moder;

    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProcdefKey() {
        return procdefKey;
    }

    public void setProcdefKey(String procdefKey) {
        this.procdefKey = procdefKey == null ? null : procdefKey.trim();
    }

    public String getProcdefId() {
        return procdefId;
    }

    public void setProcdefId(String procdefId) {
        this.procdefId = procdefId == null ? null : procdefId.trim();
    }

    /**
     * @return the procdefName
     */
    public String getProcdefName() {
        return procdefName;
    }

    /**
     * @param procdefName the procdefName to set
     */
    public void setProcdefName(String procdefName) {
        this.procdefName = procdefName;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}