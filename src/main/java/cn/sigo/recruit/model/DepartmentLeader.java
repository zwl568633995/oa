package cn.sigo.recruit.model;

import java.util.Date;

/**
 *  部门 - 负责人 中间表
 */
public class DepartmentLeader {
    private Long id;
    /**
     *  部门负责人Id(花名册表： 主键Id)
     */
    private Integer deptLeaderId;
    /**
     *  部门Id(组织架构表：主键Id)
     */
    private Long deptId;

    private String adder;

    private Date addTime;

    private String moder;

    private Date modTime;
    /**
     *  数据状态：0：未删除； 1：删除
     */
    private Boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDeptLeaderId() {
        return deptLeaderId;
    }

    public void setDeptLeaderId(Integer deptLeaderId) {
        this.deptLeaderId = deptLeaderId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}