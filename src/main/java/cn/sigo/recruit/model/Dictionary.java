/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: Dictionary.java
 * Author:   zhixiang.meng
 * Date:     2018年6月27日 下午3:10:55
 * Description: 数据字典表 实体类
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 
 * 数据字典表<br> 
 *
 * @author zhixiang.meng
 */
public class Dictionary {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 类型
     */
    private String type;

    /**
     * 页面显示名
     */
    private String dictName;

    /**
     * 页面显示名对应的数据库值
     */
    private String dictValue;
    
    /**
     * 显示顺序
     */
    private Integer orderIndex;

    /**
     * 是否删除(0:否;1:是)
     */
    private Integer isDelete;

    /**
     * 添加人
     */
    private String adder;

    /**
     * 添加时间
     */
    private Date addTime;

    /**
     * 修改人
     */
    private String moder;

    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName == null ? null : dictName.trim();
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue == null ? null : dictValue.trim();
    }

    /**
     * @return the order
     */
    public Integer getOrderIndex() {
        return orderIndex;
    }

    /**
     * @param order the order to set
     */
    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}