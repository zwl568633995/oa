/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ERPUserInfo.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午5:07:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * erp 返回的user对象
 *
 * @author zhixiang.meng
 */
public class ERPUserInfo implements java.io.Serializable {
    
    /**
     */
    private static final long serialVersionUID = 1L;
    private String UserId;
    private String UserName;
    private String UserPassword;
    private String RealName;
    private String AddTime;
    private String Status;
    private String UserToken;
    private String Permission;
    private String DataPermission;
    /**
     * @return the userId
     */
    public String getUserId() {
        return UserId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        UserId = userId;
    }
    /**
     * @return the userName
     */
    public String getUserName() {
        return UserName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        UserName = userName;
    }
    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return UserPassword;
    }
    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String UserPassword) {
        this.UserPassword = UserPassword;
    }
    /**
     * @return the realName
     */
    public String getRealName() {
        return RealName;
    }
    /**
     * @param realName the realName to set
     */
    public void setRealName(String realName) {
        RealName = realName;
    }
    /**
     * @return the addTime
     */
    public String getAddTime() {
        return AddTime;
    }
    /**
     * @param addTime the addTime to set
     */
    public void setAddTime(String addTime) {
        AddTime = addTime;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return Status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        Status = status;
    }
    /**
     * @return the userToken
     */
    public String getUserToken() {
        return UserToken;
    }
    /**
     * @param userToken the userToken to set
     */
    public void setUserToken(String userToken) {
        UserToken = userToken;
    }
    /**
     * @return the permission
     */
    public String getPermission() {
        return Permission;
    }
    /**
     * @param permission the permission to set
     */
    public void setPermission(String permission) {
        Permission = permission;
    }
    /**
     * @return the dataPermission
     */
    public String getDataPermission() {
        return DataPermission;
    }
    /**
     * @param dataPermission the dataPermission to set
     */
    public void setDataPermission(String dataPermission) {
        DataPermission = dataPermission;
    }
}
