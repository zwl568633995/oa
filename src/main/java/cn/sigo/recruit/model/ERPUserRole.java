/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ERPUserRole.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午5:10:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * erp
 * 返回的用户角色
 *
 * @author zhixiang.meng
 */
public class ERPUserRole implements java.io.Serializable{
    
    /**
     */
    private static final long serialVersionUID = 1L;
    private int RoleID;
    private String RoleName;
    /**
     * @return the roleID
     */
    public int getRoleID() {
        return RoleID;
    }
    /**
     * @param roleID the roleID to set
     */
    public void setRoleID(int roleID) {
        RoleID = roleID;
    }
    /**
     * @return the roleName
     */
    public String getRoleName() {
        return RoleName;
    }
    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        RoleName = roleName;
    }
}
