/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ErpRole.java
 * Author:   zhixiang.meng
 * Date:     2018年7月23日 下午1:53:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SuppressWarnings("serial")
public class ErpRole implements java.io.Serializable{
    /**
     * 角色id
     */
    private int roleId;
    /**
     * 角色名
     */
    private String roleName;
    /**
     * @return the roleId
     */
    public int getRoleId() {
        return roleId;
    }
    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }
    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
