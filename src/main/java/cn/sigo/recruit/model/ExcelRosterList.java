package cn.sigo.recruit.model;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 17:10
 * @Description:
 */
public class ExcelRosterList {
    /**
     *工号
     */
    private String workNumber;
    /**
     *姓名
     */
    private String entryRegisterName;
    /**
     *英文名
     */
    private String entryRegisterNameEn;
    /**
     *花名
     */
    private String aliasName;
    /**
     *性别
     */
    private String sex;
    /**
     *身份证号码
     */
    private String cardCode;
    /**
     *身份证有效期
     */
    private String cardCodeTime;

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName;
    }

    public String getEntryRegisterNameEn() {
        return entryRegisterNameEn;
    }

    public void setEntryRegisterNameEn(String entryRegisterNameEn) {
        this.entryRegisterNameEn = entryRegisterNameEn;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCardCodeTime() {
        return cardCodeTime;
    }

    public void setCardCodeTime(String cardCodeTime) {
        this.cardCodeTime = cardCodeTime;
    }
}
