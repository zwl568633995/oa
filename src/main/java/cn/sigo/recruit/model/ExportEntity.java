package cn.sigo.recruit.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @Auther: DELL
 * @Date: 2018/8/7 13:20
 * @Description:
 */
@Component
@ConfigurationProperties(prefix="export")
@PropertySource("classpath:exportExcel.properties")
public class ExportEntity {
    private String roster;

    public String getRoster() {
        return roster;
    }

    public void setRoster(String roster) {
        this.roster = roster;
    }
}
