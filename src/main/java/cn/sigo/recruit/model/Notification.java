package cn.sigo.recruit.model;

import lombok.Data;

import java.util.Date;

@Data
public class Notification {
    private Integer templateId;

    private String templateType;

    private String titlePrefix;

    private String title;

    private String contentPrefix;

    private String content;

    private String url;

    private String urlName;

    private String receiveAccount;

    private String sendName;

    private String sendAccount;

    private String node;

    private Boolean isTiming;

    private String timingSendTime;

    private Integer status;

    private String projectName;

    private Integer adder;

    private Date addTime;

    private Integer moder;

    private Date modTime;

}