package cn.sigo.recruit.model;

import cn.sigo.recruit.model.response.ResponseDeptOver;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 *  组织架构
 */
public class OrganizationStructure {
    private Long id;
    /**
     *  部门名称
     */
    private String deptName;
    /**
     *  部门编码
     */
    private String deptCode;
    /**
     *团队类型
     */
    private String teamType;
    /**
     *  上级部门Id
     */
    @NotNull(message = "上级部门不能为空!")
    private Long superiorDeptId;

    /**
     *  负责部门人Id 集合
     */
    private List<Integer> deptLeaderIds;
    /**
     *  上级部门名称
     */
    private String superiorDeptName;
    /**
     *  负责人 集合
     */
    private List<ResponseDeptOver> deptOverList;


    private Integer isOrganizationalStructure;

    /**
     * 部门状态
     */
    private String teamStatus;

    /**
     *  状态 0：启用；1：停用
     */
    private Boolean deleted;

    /**
     *  在职人数
     */
    private Integer jobPeopleNum;

    private Integer deptLevel;

    private Integer orderIndex;
    /**
     *  添加人
     */
    private String adder;
    /**
     *  添加时间
     */
    private Date addTime;
    /**
     *  最新修改人
     */
    private String moder;
    /**
     *  最新修改时间
     */
    private Date modTime;

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public List<Integer> getDeptLeaderIds() {
        return deptLeaderIds;
    }

    public void setDeptLeaderIds(List<Integer> deptLeaderIds) {
        this.deptLeaderIds = deptLeaderIds;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getSuperiorDeptName() {
        return superiorDeptName;
    }

    public void setSuperiorDeptName(String superiorDeptName) {
        this.superiorDeptName = superiorDeptName;
    }

    public Integer getJobPeopleNum() {
        return jobPeopleNum;
    }

    public void setJobPeopleNum(Integer jobPeopleNum) {
        this.jobPeopleNum = jobPeopleNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType == null ? null : teamType.trim();
    }

    public Long getSuperiorDeptId() {
        return superiorDeptId;
    }

    public void setSuperiorDeptId(Long superiorDeptId) {
        this.superiorDeptId = superiorDeptId;
    }

    public List<ResponseDeptOver> getDeptOverList() {
        return deptOverList;
    }

    public void setDeptOverList(List<ResponseDeptOver> deptOverList) {
        this.deptOverList = deptOverList;
    }

    public Integer getIsOrganizationalStructure() {
        return isOrganizationalStructure;
    }

    public void setIsOrganizationalStructure(Integer isOrganizationalStructure) {
        this.isOrganizationalStructure = isOrganizationalStructure;
    }

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus == null ? null : teamStatus.trim();
    }

    public Integer getDeptLevel() {
        return deptLevel;
    }

    public void setDeptLevel(Integer deptLevel) {
        this.deptLevel = deptLevel;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    @Override
    public String toString() {
        return "OrganizationStructure{" +
          "id=" + id +
          ", deptName='" + deptName + '\'' +
          ", deptCode='" + deptCode + '\'' +
          ", teamType='" + teamType + '\'' +
          ", superiorDeptId=" + superiorDeptId +
          ", deptLeaderIds=" + deptLeaderIds +
          ", superiorDeptName='" + superiorDeptName + '\'' +
          ", deptOverList=" + deptOverList +
          ", isOrganizationalStructure=" + isOrganizationalStructure +
          ", teamStatus='" + teamStatus + '\'' +
          ", deleted=" + deleted +
          ", jobPeopleNum=" + jobPeopleNum +
          ", deptLevel=" + deptLevel +
          ", orderIndex=" + orderIndex +
          ", adder='" + adder + '\'' +
          ", addTime=" + addTime +
          ", moder='" + moder + '\'' +
          ", modTime=" + modTime +
          '}';
    }
}