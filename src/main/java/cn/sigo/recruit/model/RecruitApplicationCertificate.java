package cn.sigo.recruit.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
/**
 * 
 * 应聘者证书
 *
 * @author zhixiang.meng
 */
public class RecruitApplicationCertificate {
    /**
     * id
     */
    private Integer id;
    /**
     * 应聘人id
     */
    private Integer applicationId;
    /**
     * 证书名称
     */

    private String certificateName;
    /**
     * 证书日期
     */

    private String certificateDate;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName == null ? null : certificateName.trim();
    }

    public String getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(String certificateDate) {
        this.certificateDate = certificateDate;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}