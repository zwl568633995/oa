package cn.sigo.recruit.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
/**
 * 应聘者教育经历
 *
 * @author zhixiang.meng
 */
public class RecruitApplicationEducation {
    /**
     * id
     */
    private Integer id;
    /**
     * 应聘者id
     */
    private Integer applicationId;
    /**
     * 开始时间
     */
    @NotBlank(message = "请选择教育经历时间")
    private String startDate;
    /**
     * 结束时间
     */
    @NotBlank(message = "请选择教育经历时间")
    private String endDate;
    /**
     * 毕业学校
     */
    @NotBlank(message = "请选择教育经历毕业学校")
    private String graduateChool;
    /**
     * 所学专业
     */

    private String studyMajor;
    /**
     * 学历
     */
    @NotBlank(message = "请选择学历")
    private String education;
    /**
     * 学历性质
     */
    @NotBlank(message = "请选择学历性质")
    private String educationalBackground;
    /**
     * 所获学位
     */
    @NotBlank(message = "请填写所获学位")
    private String receivedDegree;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGraduateChool() {
        return graduateChool;
    }

    public void setGraduateChool(String graduateChool) {
        this.graduateChool = graduateChool == null ? null : graduateChool.trim();
    }

    public String getStudyMajor() {
        return studyMajor;
    }

    public void setStudyMajor(String studyMajor) {
        this.studyMajor = studyMajor == null ? null : studyMajor.trim();
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education == null ? null : education.trim();
    }

    public String getEducationalBackground() {
        return educationalBackground;
    }

    public void setEducationalBackground(String educationalBackground) {
        this.educationalBackground = educationalBackground == null ? null : educationalBackground.trim();
    }

    public String getReceivedDegree() {
        return receivedDegree;
    }

    public void setReceivedDegree(String receivedDegree) {
        this.receivedDegree = receivedDegree == null ? null : receivedDegree.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}