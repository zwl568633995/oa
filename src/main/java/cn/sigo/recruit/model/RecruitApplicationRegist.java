package cn.sigo.recruit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 应聘登记
 *
 * @author zhixiang.meng
 */
public class RecruitApplicationRegist {
    /**
     * id
     */
    private Integer id;
    /**
     * 应聘code
     */
    private String applicationCode;
    /**
     * 姓名
     */
    @NotBlank(message = "请填写姓名")
    private String applicationName;
    /**
     * 英文名
     */
    private String applicationEhName;
    /**
     * 面试单号
     */
    @NotBlank(message = "请填写面试单号")
    private String interviewCode;
    /**
     * 性别
     */
    @NotBlank(message = "请选择性别")
    private String sex;
    /**
     * 出生日期
     */
//    @IsDate
    @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2}",message = "请输入正确的年月日(yyyy-MM-dd)")
    private String birthday;
    /**
     * 身份证号码
     */
    @NotBlank(message = "请填写身份证号码")
    private String idCardNo;
    /**
     * 民族
     */

    private String nation;
    /**
     * 政治面貌
     */

    private String politicaOutlook;
    /**
     * 婚姻状况
     */
    @NotBlank(message = "请选择婚姻状况")
    private String maritalStatus;
    /**
     * 已育子女数
     */
    @NotNull(message = "请填写已育子女数")
    private Integer childrenNumber;
    /**
     * 最高学历
     */
    private String maxEducation;
    /**
     * 手机号码
     */
    @NotBlank(message = "请填写手机号码")
    @Pattern(regexp = "1[0-9][0-9]\\d{8}",message = "请输入11位的手机号，以1开始")
    private String phoneNumber;
    /**
     * 邮箱
     */
    @NotBlank(message = "请填写邮箱")
    @Pattern(regexp = "^(.+)@(.+)$",message = "邮箱的格式不正确")
    private String email;
    /**
     * 居住地址
     */
    @NotBlank(message = "请填写居住地址")
    private String residentialAddress;
    /**
     * 户籍地址
     */

    private String permanentAddress;
    /**
     * 紧急联系人姓名
     */
    @NotBlank(message = "请填写紧急联系人姓名")
    private String emergencyContactName;
    /**
     * 紧急联系人手机号码
     */
    @NotBlank(message = "请填写紧急联系人手机号码")
    @Pattern(regexp = "1[0-9][0-9]\\d{8}",message = "请输入11位的手机号，以1开始")
    private String emergencyContactPhone;
    /**
     * 健康状况
     */
    @NotBlank(message = "请填写健康状况")
    private String health;
    /**
     * 是否有传染病或慢性病史(是，否)
     */
    @NotBlank(message = "请选择是否有传染病或慢性病史")
    private String isMedicalHistory;
    /**
     * 病史说明
     */
    private String medicalHistoryDesc;
    /**
     * 当前是否怀孕(是否)
     */

    private String isPregnant;
    /**
     * 当前是否在产假期、哺乳期
     */

    private String isVacation;
    /**
     * 是否有在本公司工作经历
     */

    private String isOurCompanyWorking;
    /**
     * 本公司工作经历说明
     */
    private String ourCompanyDesc;
    /**
     * 是否有亲友在本公司工作
     */
    @NotBlank(message = "请选择是否有亲友在本公司工作")
    private String isRelatives;
    /**
     * 亲友姓名
     */
    private String relativesName;
    /**
     * 应聘来源
     */
    @NotBlank(message = "请选择应聘来源")
    private String applicationSource;
    /**
     * 最快到岗日
     */
    @NotBlank(message = "请选择最快到岗日")
    private String fastestDay;
    /**
     * 目前税前薪资
     */

    private Integer nowSalary;
    /**
     * 期望税前薪资
     */

    private Integer expectedSalary;
    /**
     * 是否与其他公司存在劳动关系或竞业禁止条款
     */
    @NotBlank(message = "请选择是否与其他公司存在劳动关系或竞业禁止条款")
    private String isLaborDispute;
    /**
     * 是否接受过行政或者刑事处分，或与原公司存在劳动/财务纠纷，或因违章违纪遭辞退/开除等情况
     */
    @NotBlank(message = "是否接受过行政或者刑事处分，或与原公司存在劳动/财务纠纷，或因违章违纪遭辞退/开除等情况")
    private String isIllegality;
    /**
     * 在岗状态
     * */
    @NotBlank(message = "请选择在岗状态")
    private  String jobState;

    public String getJobState() {
        return jobState;
    }

    public void setJobState(String jobState) {
        this.jobState = jobState;
    }

    /**
     * 是否已入职
     */
    private Boolean entry;
    /**
     * 职位名
     */
    private String jobName;
    /**
     * 职位部门
     */
    private String jobDept;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;


    public Boolean getEntry() {
        return entry;
    }

    public void setEntry(Boolean entry) {
        this.entry = entry;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName == null ? null : applicationName.trim();
    }

    public String getApplicationEhName() {
        return applicationEhName;
    }

    public void setApplicationEhName(String applicationEhName) {
        this.applicationEhName = applicationEhName == null ? null : applicationEhName.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo == null ? null : idCardNo.trim();
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation == null ? null : nation.trim();
    }

    public String getPoliticaOutlook() {
        return politicaOutlook;
    }

    public void setPoliticaOutlook(String politicaOutlook) {
        this.politicaOutlook = politicaOutlook == null ? null : politicaOutlook.trim();
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
    }

    public Integer getChildrenNumber() {
        return childrenNumber;
    }

    public void setChildrenNumber(Integer childrenNumber) {
        this.childrenNumber = childrenNumber;
    }

    public String getMaxEducation() {
        return maxEducation;
    }

    public void setMaxEducation(String maxEducation) {
        this.maxEducation = maxEducation == null ? null : maxEducation.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress == null ? null : residentialAddress.trim();
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress == null ? null : permanentAddress.trim();
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName == null ? null : emergencyContactName.trim();
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone == null ? null : emergencyContactPhone.trim();
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health == null ? null : health.trim();
    }

    public String getIsMedicalHistory() {
        return isMedicalHistory;
    }

    public void setIsMedicalHistory(String isMedicalHistory) {
        this.isMedicalHistory = isMedicalHistory == null ? null : isMedicalHistory.trim();
    }

    public String getMedicalHistoryDesc() {
        return medicalHistoryDesc;
    }

    public void setMedicalHistoryDesc(String medicalHistoryDesc) {
        this.medicalHistoryDesc = medicalHistoryDesc == null ? null : medicalHistoryDesc.trim();
    }

    public String getIsPregnant() {
        return isPregnant;
    }

    public void setIsPregnant(String isPregnant) {
        this.isPregnant = isPregnant == null ? null : isPregnant.trim();
    }

    public String getIsVacation() {
        return isVacation;
    }

    public void setIsVacation(String isVacation) {
        this.isVacation = isVacation == null ? null : isVacation.trim();
    }

    public String getIsOurCompanyWorking() {
        return isOurCompanyWorking;
    }

    public void setIsOurCompanyWorking(String isOurCompanyWorking) {
        this.isOurCompanyWorking = isOurCompanyWorking == null ? null : isOurCompanyWorking.trim();
    }

    public String getOurCompanyDesc() {
        return ourCompanyDesc;
    }

    public void setOurCompanyDesc(String ourCompanyDesc) {
        this.ourCompanyDesc = ourCompanyDesc == null ? null : ourCompanyDesc.trim();
    }

    public String getIsRelatives() {
        return isRelatives;
    }

    public void setIsRelatives(String isRelatives) {
        this.isRelatives = isRelatives == null ? null : isRelatives.trim();
    }

    public String getRelativesName() {
        return relativesName;
    }

    public void setRelativesName(String relativesName) {
        this.relativesName = relativesName == null ? null : relativesName.trim();
    }

    public String getApplicationSource() {
        return applicationSource;
    }

    public void setApplicationSource(String applicationSource) {
        this.applicationSource = applicationSource == null ? null : applicationSource.trim();
    }

    public String getFastestDay() {
        return fastestDay;
    }

    public void setFastestDay(String fastestDay) {
        this.fastestDay = fastestDay;
    }

    public Integer getNowSalary() {
        return nowSalary;
    }

    public void setNowSalary(Integer nowSalary) {
        this.nowSalary = nowSalary;
    }

    public Integer getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(Integer expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public String getIsLaborDispute() {
        return isLaborDispute;
    }

    public void setIsLaborDispute(String isLaborDispute) {
        this.isLaborDispute = isLaborDispute == null ? null : isLaborDispute.trim();
    }

    public String getIsIllegality() {
        return isIllegality;
    }

    public void setIsIllegality(String isIllegality) {
        this.isIllegality = isIllegality == null ? null : isIllegality.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }

    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * @return the jobDept
     */
    public String getJobDept() {
        return jobDept;
    }

    /**
     * @param jobDept the jobDept to set
     */
    public void setJobDept(String jobDept) {
        this.jobDept = jobDept;
    }
}