package cn.sigo.recruit.model;

import java.util.Date;
/**
 * 入职登记表--健康状况
 *
 * @author zhixiang.meng
 */
public class RecruitEntryHealthy {
    /**
     * id
     */
    private Integer id;
    /**
     * 入职登记表id
     */
    private Integer entryRegisterId;
    /**
     * 是否有工伤
     */
    private String isJobInjury;
    /**
     * 工伤说明
     */
    private String jobInjuryExplain;
    /**
     * 是否从事危险工作
     */
    private String isDangerWork;
    /**
     * 是否有传染病
     */
    private String isInfectiousDiseases;
    /**
     * 病史说明
     */
    private String medicalHistory;
    /**
     * 最近接受过医学治疗
     */
    private String recentlyMedicalHistory;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntryRegisterId() {
        return entryRegisterId;
    }

    public void setEntryRegisterId(Integer entryRegisterId) {
        this.entryRegisterId = entryRegisterId;
    }

    public String getIsJobInjury() {
        return isJobInjury;
    }

    public void setIsJobInjury(String isJobInjury) {
        this.isJobInjury = isJobInjury == null ? null : isJobInjury.trim();
    }

    public String getJobInjuryExplain() {
        return jobInjuryExplain;
    }

    public void setJobInjuryExplain(String jobInjuryExplain) {
        this.jobInjuryExplain = jobInjuryExplain == null ? null : jobInjuryExplain.trim();
    }

    public String getIsDangerWork() {
        return isDangerWork;
    }

    public void setIsDangerWork(String isDangerWork) {
        this.isDangerWork = isDangerWork == null ? null : isDangerWork.trim();
    }

    public String getIsInfectiousDiseases() {
        return isInfectiousDiseases;
    }

    public void setIsInfectiousDiseases(String isInfectiousDiseases) {
        this.isInfectiousDiseases = isInfectiousDiseases == null ? null : isInfectiousDiseases.trim();
    }

    public String getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory == null ? null : medicalHistory.trim();
    }

    public String getRecentlyMedicalHistory() {
        return recentlyMedicalHistory;
    }

    public void setRecentlyMedicalHistory(String recentlyMedicalHistory) {
        this.recentlyMedicalHistory = recentlyMedicalHistory == null ? null : recentlyMedicalHistory.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}