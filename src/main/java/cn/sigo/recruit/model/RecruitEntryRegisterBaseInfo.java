package cn.sigo.recruit.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 
 * 入职登记表基本信息 实体bean
 *
 * @author zhixiang.meng
 */
public class RecruitEntryRegisterBaseInfo {
    /**
     * id
     */
    private Integer id;
    /**
     * 入职登记表编号
     */
    private String entryCode;
    /**
     * 应聘登记表编号
     */
    @NotBlank(message = "请填写应聘登记表编号")
    private String applicationCode;
    /**
     * 面试登记表编号
     */
    @NotBlank(message = "请填写面试登记表编号")
    private String interviewCode;
    /**
     * 姓名
     */
    @NotBlank(message = "请填写姓名")
    private String entryRegisterName;
    /**
     * 英文名
     */
    private String entryRegisterNameEn;
    /**
     * 性别
     */
    @NotBlank(message = "请选择性别")
    private String sex;
    /**
     * 职位
     */
    private String jobName;
    /**
     * 身份证号码
     */
    @NotBlank(message = "请填写身份证号码")
    private String cardCode;
    /**
     * 出生年月
     */
    @NotBlank(message = "请输入出生年月")
    @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2}",message = "请输入正确的年月日(yyyy-MM-dd)")
    private String dateOfBirth;
    /**
     * 民族
     */

    private String nation;
    /**
     * 籍贯
     */
    @NotBlank(message = "请选择籍贯")
    private String nativePlace;
    /**
     * 政治面貌
     */

    private String politicalOutlook;
    /**
     * 婚姻状况
     */
    @NotBlank(message = "请选择婚姻状况")
    private String maritalStatus;
    /**
     * 已育子女数
     */
    @NotNull(message = "请填写已育子女数")
    private Integer numberOfChildren;
    /**
     * 手机号码
     */
    @NotBlank(message = "请填写手机号码")
    @Pattern(regexp = "1[0-9][0-9]\\d{8}",message = "请输入11位的手机号，以1开始")
    private String phoneNumber;
    /**
     * 邮箱
     */
    @NotBlank(message = "请填写邮箱")
    @Pattern(regexp = "^(.+)@(.+)$",message = "邮箱的格式不正确")
    private String email;
    /**
     * 居住地址
     */
    @NotBlank(message = "请填写居住地址")
    private String residentialAddress;
    /**
     * 籍贯地址
     */

    private String nativePlaceAddress;
    /**
     * 户口性质
     */
    @NotBlank(message = "请选择户口性质")
    private String householdRegistration;
    /**
     * 最高学历
     */
    private String highestEducation;
    /**
     * 应聘简历来源
     */
    @NotBlank(message = "请选择应聘来源")
    private String resumeSource;
    /**
     * 是否有在公司工作的经历
     */

    private String isMyCompanyWorking;
    /**
     * 是否与前用人单位约定了保密协议与竞业限制条款
     */
    @NotBlank(message = "请选择是否与前用人单位约定了保密协议与竞业限制条款")
    private String isLimit;
    /**
     * 是否与前用人单位有未尽的法律事宜
     */
    @NotBlank(message = "是否与前用人单位有未尽的法律事宜")
    private String isLegalRestrictions;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;
    /**
     * 紧急联系人姓名
     */
    private String emergencyContactName;
    /**
     * 紧急联系人手机号码
     */
    @Pattern(regexp = "1[0-9][0-9]\\d{8}",message = "请输入11位的手机号，以1开始")
    private String emergencyContactPhone;
    /**
     * 紧急联系人-关系
     */
    private String emergencyContactRelation;
    private String emergencyContactAddress;

    public String getEmergencyContactAddress() {
        return emergencyContactAddress;
    }

    public void setEmergencyContactAddress(String emergencyContactAddress) {
        this.emergencyContactAddress = emergencyContactAddress;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }

    public String getEntryCode() {
        return entryCode;
    }

    public void setEntryCode(String entryCode) {
        this.entryCode = entryCode;
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getEmergencyContactRelation() {
        return emergencyContactRelation;
    }

    public void setEmergencyContactRelation(String emergencyContactRelation) {
        this.emergencyContactRelation = emergencyContactRelation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName == null ? null : entryRegisterName.trim();
    }

    public String getEntryRegisterNameEn() {
        return entryRegisterNameEn;
    }

    public void setEntryRegisterNameEn(String entryRegisterNameEn) {
        this.entryRegisterNameEn = entryRegisterNameEn == null ? null : entryRegisterNameEn.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode == null ? null : cardCode.trim();
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation == null ? null : nation.trim();
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace == null ? null : nativePlace.trim();
    }

    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook == null ? null : politicalOutlook.trim();
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
    }

    public Integer getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(Integer numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress == null ? null : residentialAddress.trim();
    }

    public String getNativePlaceAddress() {
        return nativePlaceAddress;
    }

    public void setNativePlaceAddress(String nativePlaceAddress) {
        this.nativePlaceAddress = nativePlaceAddress == null ? null : nativePlaceAddress.trim();
    }

    public String getHouseholdRegistration() {
        return householdRegistration;
    }

    public void setHouseholdRegistration(String householdRegistration) {
        this.householdRegistration = householdRegistration == null ? null : householdRegistration.trim();
    }

    public String getHighestEducation() {
        return highestEducation;
    }

    public void setHighestEducation(String highestEducation) {
        this.highestEducation = highestEducation == null ? null : highestEducation.trim();
    }

    public String getResumeSource() {
        return resumeSource;
    }

    public void setResumeSource(String resumeSource) {
        this.resumeSource = resumeSource == null ? null : resumeSource.trim();
    }

    public String getIsMyCompanyWorking() {
        return isMyCompanyWorking;
    }

    public void setIsMyCompanyWorking(String isMyCompanyWorking) {
        this.isMyCompanyWorking = isMyCompanyWorking == null ? null : isMyCompanyWorking.trim();
    }

    public String getIsLimit() {
        return isLimit;
    }

    public void setIsLimit(String isLimit) {
        this.isLimit = isLimit == null ? null : isLimit.trim();
    }

    public String getIsLegalRestrictions() {
        return isLegalRestrictions;
    }

    public void setIsLegalRestrictions(String isLegalRestrictions) {
        this.isLegalRestrictions = isLegalRestrictions == null ? null : isLegalRestrictions.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}