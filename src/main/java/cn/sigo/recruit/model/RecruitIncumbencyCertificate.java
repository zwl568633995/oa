package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 在职员工证书 实体类
 *
 * @author zhixiang.meng
 */
public class RecruitIncumbencyCertificate {
    /**
     * id
     */
    private Integer id;
    /**
     * 入职登记表id
     */
    private Integer entryRegisterId;
    /**
     * 在职员工id
     */
    private Integer incumbencyId;
    /**
     * 证书名称
     */
    private String certificateName;
    /**
     * 证书日期
     */
    private String certificateDate;
    /**
     * 证书到期日
     */
    private String certificateTermOfValidity;
    /**
     * 证书编号
     */
    private String certificateCode;
    /**
     * 认证机构
     */
    private String certificateBody;
    /**
     * 证书级别
     */
    private String certificationLevel;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntryRegisterId() {
        return entryRegisterId;
    }

    public void setEntryRegisterId(Integer entryRegisterId) {
        this.entryRegisterId = entryRegisterId;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName == null ? null : certificateName.trim();
    }

    public String getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(String certificateDate) {
        this.certificateDate = certificateDate;
    }

    public String getCertificateTermOfValidity() {
        return certificateTermOfValidity;
    }

    public void setCertificateTermOfValidity(String certificateTermOfValidity) {
        this.certificateTermOfValidity = certificateTermOfValidity;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public void setCertificateCode(String certificateCode) {
        this.certificateCode = certificateCode == null ? null : certificateCode.trim();
    }

    public String getCertificateBody() {
        return certificateBody;
    }

    public void setCertificateBody(String certificateBody) {
        this.certificateBody = certificateBody == null ? null : certificateBody.trim();
    }

    public String getCertificationLevel() {
        return certificationLevel;
    }

    public void setCertificationLevel(String certificationLevel) {
        this.certificationLevel = certificationLevel == null ? null : certificationLevel.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}