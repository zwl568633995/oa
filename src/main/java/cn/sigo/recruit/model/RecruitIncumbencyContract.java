package cn.sigo.recruit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 在职合同表
 */
public class RecruitIncumbencyContract {
    private Integer id;
    /**
     * 在职信息id
     */
    private Integer incumbencyId;
    /**
     *合同主体
     */
    @NotBlank(message = "请选择合同信息合同主体")
    private String contractSubject;
    /**
     *合同类型
     */
    @NotBlank(message = "请选择合同信息合同类型")
    private String contractType;
    /**
     *合同起始日期
     */
    @NotBlank(message = "请填写合同信息合同起始日期")
    private String contractDateStart;
    /**
     *合同终止日期
     */
    @NotBlank(message = "请填写合同信息合同终止日期")
    private String contractDateEnd;
    /**
     *合同签订日期
     */
    @NotBlank(message = "请填写合同信息合同签订日期")
    private String contractAwardDate;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getContractSubject() {
        return contractSubject;
    }

    public void setContractSubject(String contractSubject) {
        this.contractSubject = contractSubject == null ? null : contractSubject.trim();
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType == null ? null : contractType.trim();
    }

    public String getContractDateStart() {
        return contractDateStart;
    }

    public void setContractDateStart(String contractDateStart) {
        this.contractDateStart = contractDateStart;
    }

    public String getContractDateEnd() {
        return contractDateEnd;
    }

    public void setContractDateEnd(String contractDateEnd) {
        this.contractDateEnd = contractDateEnd;
    }

    public String getContractAwardDate() {
        return contractAwardDate;
    }

    public void setContractAwardDate(String contractAwardDate) {
        this.contractAwardDate = contractAwardDate;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}