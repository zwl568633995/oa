package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 在职员工附件表
 */
public class RecruitIncumbencyEnclosure {
    private Integer id;
    /**
     *在职信息id
     */
    private Integer incumbencyId;
    /**
     *路径
     */
    private String filePath;
    /**
     *类型
     */
    private String type;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}