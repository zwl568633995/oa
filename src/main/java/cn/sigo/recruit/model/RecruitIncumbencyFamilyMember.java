package cn.sigo.recruit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
/**
 * 在职员工家庭成员 实体类
 *
 * @author zhixiang.meng
 */
public class RecruitIncumbencyFamilyMember {
    /**
     * id
     */
    private Integer id;
    /**
     * 入职登记表id
     */
    private Integer entryRegisterId;
    /**
     * 在职员工id
     */
    private Integer incumbencyId;
    /**
     * 姓名
     */
    @NotBlank(message = "请填写家庭成员姓名")
    private String familyName;
    /**
     * 关系
     */
    @NotBlank(message = "请填写家庭成员关系")
    private String relationship;
    /**
     * 出生年月
     */

    private String dateOfBirth;
    /**
     * 工作单位
     */

    private String companyName;
    /**
     * 工作职责
     */

    private String operatingDuty;
    /**
     * 联系电话
     */
    
    private String phone;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntryRegisterId() {
        return entryRegisterId;
    }

    public void setEntryRegisterId(Integer entryRegisterId) {
        this.entryRegisterId = entryRegisterId;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName == null ? null : familyName.trim();
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship == null ? null : relationship.trim();
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getOperatingDuty() {
        return operatingDuty;
    }

    public void setOperatingDuty(String operatingDuty) {
        this.operatingDuty = operatingDuty == null ? null : operatingDuty.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}