package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 在职员工历史记录表
 */
public class RecruitIncumbencyHistory {
    private Integer id;
    /**
     *在职信息id
     */
    private Integer incumbencyId;
    /**
     *操作人
     */
    private String operator;
    /**
     *操作时间
     */
    private Date operationTime;
    /**
     *操作类型
     */
    private String operationType;
    /**
     *操作详情
     */
    private String operationDesc;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType == null ? null : operationType.trim();
    }

    public String getOperationDesc() {
        return operationDesc;
    }

    public void setOperationDesc(String operationDesc) {
        this.operationDesc = operationDesc == null ? null : operationDesc.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}