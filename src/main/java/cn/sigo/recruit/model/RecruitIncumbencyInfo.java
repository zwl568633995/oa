package cn.sigo.recruit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 在职信息表
 */
public class
RecruitIncumbencyInfo {
    private Integer id;
    /**
     * 在职信息id
     */
    private Integer incumbencyId;
    /**
     * 工号
     */
   @NotBlank(message = "请填写在职信息工号")
    private String workNumber;
    /**
     * 状态
     */
    private String status;
    /**
     * 部门Id
     */
    @NotNull(message = "请填写部门Id")
    private Long deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 职位
     */
    @NotBlank(message = "请填写在职信息职位")
    private String jobName;
    /**
     * 用工性质
     */
    @NotBlank(message = "请选择在职信息用工性质")
    private String workingNature;
    /**
     * 入职时间
     */
    @NotBlank(message = "请选择在职信息入职时间")
    private String timeOfEntry;
    /**
     * 试用时间-开始
     */
    @NotBlank(message = "请选择在职信息试用期开始时间")
    private String trialTimeStart;
    /**
     *试用时间-结束
     */
   @NotBlank(message = "请选择在职信息试用期结束时间")
    private String trialTimeEnd;
    /**
     *试用薪资
     */
    @NotBlank(message = "请填写在职信息试用薪资")
    private String probationOfSalary;
    /**
     *转正时间
     */
    private String correctionTime;
    /**
     *转正薪资
     */
    private String correctionOfSalary;
    /**
     *离职时间
     */
    private Date departureTime;
    /**
     *离职原因
     */
    private String reasonsForLeaving;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber == null ? null : workNumber.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public String getWorkingNature() {
        return workingNature;
    }

    public void setWorkingNature(String workingNature) {
        this.workingNature = workingNature == null ? null : workingNature.trim();
    }

    public String getProbationOfSalary() {
        return probationOfSalary;
    }

    public void setProbationOfSalary(String probationOfSalary) {
        this.probationOfSalary = probationOfSalary == null ? null : probationOfSalary.trim();
    }

    public String getTimeOfEntry() {
        return timeOfEntry;
    }

    public void setTimeOfEntry(String timeOfEntry) {
        this.timeOfEntry = timeOfEntry;
    }

    public String getTrialTimeStart() {
        return trialTimeStart;
    }

    public void setTrialTimeStart(String trialTimeStart) {
        this.trialTimeStart = trialTimeStart;
    }

    public String getTrialTimeEnd() {
        return trialTimeEnd;
    }

    public void setTrialTimeEnd(String trialTimeEnd) {
        this.trialTimeEnd = trialTimeEnd;
    }

    public String getCorrectionTime() {
        return correctionTime;
    }

    public void setCorrectionTime(String correctionTime) {
        this.correctionTime = correctionTime;
    }

    public String getCorrectionOfSalary() {
        return correctionOfSalary;
    }

    public void setCorrectionOfSalary(String correctionOfSalary) {
        this.correctionOfSalary = correctionOfSalary == null ? null : correctionOfSalary.trim();
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getReasonsForLeaving() {
        return reasonsForLeaving;
    }

    public void setReasonsForLeaving(String reasonsForLeaving) {
        this.reasonsForLeaving = reasonsForLeaving == null ? null : reasonsForLeaving.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}