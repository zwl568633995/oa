package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 在职员工工资卡社保信息
 */
public class RecruitIncumbencySecurityCard {
    private Integer id;
    /**
     *在职信息id
     */
    private Integer incumbencyId;
    /**
     *劳动保障号
     */
    private String laborSecurityNumber;
    /**
     *公积金账号
     */
    private String providentFundAccount;
    /**
     *工资卡号
     */
    private String bankCode;
    /**
     *开户行
     */
    private String bankName;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getLaborSecurityNumber() {
        return laborSecurityNumber;
    }

    public void setLaborSecurityNumber(String laborSecurityNumber) {
        this.laborSecurityNumber = laborSecurityNumber == null ? null : laborSecurityNumber.trim();
    }

    public String getProvidentFundAccount() {
        return providentFundAccount;
    }

    public void setProvidentFundAccount(String providentFundAccount) {
        this.providentFundAccount = providentFundAccount == null ? null : providentFundAccount.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}