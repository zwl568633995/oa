package cn.sigo.recruit.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 在职员工基本信息表
 */
@Data
public class RecruitIncumbencyUserBaseInfo {
    private Integer id;
    /**
     *姓名
     */
    @NotBlank(message = "请填写姓名")
    private String entryRegisterName;
    /**
     *花名
     */
    private String aliasName;
    /**
     *英文名
     */
    private String entryRegisterNameEn;
    /**
     *性别
     */
    @NotBlank(message = "请填写性别")
    private String sex;
    /**
     *身份证号码
     */
    private String cardCode;
    /**
     *身份证有效期
     */
    private String cardCodeTime;
    /**
     *出生年月
     */
    @Pattern(regexp = "[0-9]{4}-[0-9]{2}-[0-9]{2}",message = "请输入正确的年月日(yyyy-MM-dd)")
    private String dateOfBirth;
    /**
     *生日
     */
    private String birthday;
    /**
     *民族
     */
    private String nation;
    /**
     *籍贯
     */
    private String nativePlace;
    /**
     *政治面貌
     */
    private String politicalOutlook;
    /**
     *婚姻状况
     */
    private String maritalStatus;
    /**
     *手机号码
     */
    private String phoneNumber;
    /**
     *个人邮箱
     */
    private String email;
    /**
     *公司邮箱
     */
    private String companyEmail;
    /**
     *居住地址
     */
    private String residentialAddress;
    /**
     *籍贯地址
     */
    private String nativePlaceAddress;
    /**
     *户口性质
     */
    private String householdRegistration;
    /**
     *文化程度
     */
    private String degreeOfEducation;
    /**
     *最高学历
     */
    private String highestEducation;
    /**
     *备注
     */
    private String remarks;
    /**
     *紧急联系人姓名
     */
    private String emergencyContactName;
    /**
     *紧急联系人手机号码
     */
    private String emergencyContactPhone;
    /**
     *紧急联系人-关系
     */
    private String emergencyContactRelation;
    /**
     *紧急联系人-联系地址
     */
    private String emergencyContactAddress;
    /**
     *添加人
     */
    private String adder;
    /**
     *添加时间
     */
    private Date addTime;
    /**
     *修改人
     */
    private String moder;
    /**
     *修改时间
     */
    private Date modTime;
    /**
     *工号
     */
    private String workNumber;
    /**
     *状态
     */
    private String status;
    /**
     *合同到期时间
     */
    private String contractDateEnd;

    public String getContractDateEnd() {
        return contractDateEnd;
    }
//
//    public void setContractDateEnd(String contractDateEnd) {
//        this.contractDateEnd = contractDateEnd;
//    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName == null ? null : entryRegisterName.trim();
    }

    public String getEntryRegisterNameEn() {
        return entryRegisterNameEn;
    }

    public void setEntryRegisterNameEn(String entryRegisterNameEn) {
        this.entryRegisterNameEn = entryRegisterNameEn == null ? null : entryRegisterNameEn.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode == null ? null : cardCode.trim();
    }

    public String getCardCodeTime() {
        return cardCodeTime;
    }

    public void setCardCodeTime(String cardCodeTime) {
        this.cardCodeTime = cardCodeTime;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation == null ? null : nation.trim();
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace == null ? null : nativePlace.trim();
    }

    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook == null ? null : politicalOutlook.trim();
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail == null ? null : companyEmail.trim();
    }

    public String getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(String residentialAddress) {
        this.residentialAddress = residentialAddress == null ? null : residentialAddress.trim();
    }

    public String getNativePlaceAddress() {
        return nativePlaceAddress;
    }

    public void setNativePlaceAddress(String nativePlaceAddress) {
        this.nativePlaceAddress = nativePlaceAddress == null ? null : nativePlaceAddress.trim();
    }

    public String getHouseholdRegistration() {
        return householdRegistration;
    }

    public void setHouseholdRegistration(String householdRegistration) {
        this.householdRegistration = householdRegistration == null ? null : householdRegistration.trim();
    }

    public String getDegreeOfEducation() {
        return degreeOfEducation;
    }

    public void setDegreeOfEducation(String degreeOfEducation) {
        this.degreeOfEducation = degreeOfEducation == null ? null : degreeOfEducation.trim();
    }

    public String getHighestEducation() {
        return highestEducation;
    }

    public void setHighestEducation(String highestEducation) {
        this.highestEducation = highestEducation == null ? null : highestEducation.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName == null ? null : emergencyContactName.trim();
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone == null ? null : emergencyContactPhone.trim();
    }

    public String getEmergencyContactRelation() {
        return emergencyContactRelation;
    }

    public void setEmergencyContactRelation(String emergencyContactRelation) {
        this.emergencyContactRelation = emergencyContactRelation == null ? null : emergencyContactRelation.trim();
    }

    public String getEmergencyContactAddress() {
        return emergencyContactAddress;
    }

    public void setEmergencyContactAddress(String emergencyContactAddress) {
        this.emergencyContactAddress = emergencyContactAddress == null ? null : emergencyContactAddress.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber == null ? null : workNumber.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}