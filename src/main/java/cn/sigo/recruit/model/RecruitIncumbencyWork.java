package cn.sigo.recruit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
/**
 * 在职员工工作经历 实体类
 *
 * @author zhixiang.meng
 */
public class RecruitIncumbencyWork {
    /**
     * id
     */
    private Integer id;
    /**
     * 入职登记表id
     */
    private Integer entryRegisterId;
    /**
     * 在职员工id
     */
    private Integer incumbencyId;
    /**
     * 开始时间
     */
    @NotBlank(message = "请选择工作经历开始时间")
    private String startDate;
    /**
     * 结束时间
     */
    @NotBlank(message = "请选择工作经历结束时间")
    private String endDate;
    /**
     * 公司名称
     */
    @NotBlank(message = "请填写工作经历公司名称")
    private String companyName;
    /**
     * 职位
     */
    @NotBlank(message = "请填写工作经历职位")
    private String position;
    /**
     * 证明人
     */
    @NotBlank(message = "请填写工作经历证明人")
    private String witness;
    /**
     * 证明人联系电话
     */
    @NotBlank(message = "请填写工作经历证明人联系电话")
    private String witnessPhone;
    /**
     * 离职原因
     */
    @NotBlank(message = "请填写工作经历离职原因")
    private String reasonsLeaving;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntryRegisterId() {
        return entryRegisterId;
    }

    public void setEntryRegisterId(Integer entryRegisterId) {
        this.entryRegisterId = entryRegisterId;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getWitness() {
        return witness;
    }

    public void setWitness(String witness) {
        this.witness = witness == null ? null : witness.trim();
    }

    public String getWitnessPhone() {
        return witnessPhone;
    }

    public void setWitnessPhone(String witnessPhone) {
        this.witnessPhone = witnessPhone == null ? null : witnessPhone.trim();
    }

    public String getReasonsLeaving() {
        return reasonsLeaving;
    }

    public void setReasonsLeaving(String reasonsLeaving) {
        this.reasonsLeaving = reasonsLeaving == null ? null : reasonsLeaving.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}