package cn.sigo.recruit.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;



/**
 * 
 * 面试管理表
 *
 * @author zhixiang.meng
 */
public class RecruitInterviewManagement {
    /**
     * id
     */
    private Integer id;
    /**
     * 面试单编号
     */
    private String interviewCode;
    /**
     * 状态
     */
    private String interviewStatus;
    /**
     * 应聘人姓名
     */
    @NotBlank(message = "请填写应聘人姓名")
    private String applyName;
    /**
     * 应聘职位
     */
    @NotBlank(message = "请填写应聘职位")
    private String applyJobName;
    /**
     * 职位code
     */
    private String jobCode;
    /**
     * 应聘部门
     */
    @NotBlank(message = "请选择部门")
    private String applyDept;
    /**
     * 应聘人手机号码
     */
    @NotBlank(message = "请填写应聘人手机号码")
    @Pattern(regexp = "1[0-9][0-9]\\d{8}",message = "请输入11位的手机号，以1开始")
    private String applyPhoneNumber;
    /**
     * 简历来源
     */
    @NotBlank(message = "请选择应聘来源")
    private String resumeSource;
    /**
     * 初试面试官
     */
    @NotBlank(message = "请选择初试面试官")
    private String firstInterviewer;
    /**
     * 初试时间 
     */
    @NotBlank(message = "请选择初试时间")
    private String firstTime;
    /**
     * 复试面试官
     */
    private String secondInterviewer;
    /**
     * 复试时间 
     */
    private String secondTime;
    /**
     * offer审核人
     */
    @NotBlank(message = "请选择offer审核人")
    private String offerAudit;
    /**
     * offer审核时间
     */
    private String offerAuditTime;
    /**
     * 简历地址
     */
    private String resumeUrl;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加人
     */
    private String addName;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;
    /**
     * 流程id
     */
    private String activitiProcdId;
    /**
     * 启动流程实例id
     */
    private String procId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode == null ? null : interviewCode.trim();
    }

    public String getInterviewStatus() {
        return interviewStatus;
    }

    public void setInterviewStatus(String interviewStatus) {
        this.interviewStatus = interviewStatus == null ? null : interviewStatus.trim();
    }

    public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getApplyName() {
        return applyName;
    }

    public void setApplyName(String applyName) {
        this.applyName = applyName == null ? null : applyName.trim();
    }

    public String getApplyJobName() {
        return applyJobName;
    }

    public void setApplyJobName(String applyJobName) {
        this.applyJobName = applyJobName == null ? null : applyJobName.trim();
    }

    public String getApplyDept() {
        return applyDept;
    }

    public void setApplyDept(String applyDept) {
        this.applyDept = applyDept == null ? null : applyDept.trim();
    }

    public String getApplyPhoneNumber() {
        return applyPhoneNumber;
    }

    public void setApplyPhoneNumber(String applyPhoneNumber) {
        this.applyPhoneNumber = applyPhoneNumber == null ? null : applyPhoneNumber.trim();
    }

    public String getResumeSource() {
        return resumeSource;
    }

    public void setResumeSource(String resumeSource) {
        this.resumeSource = resumeSource == null ? null : resumeSource.trim();
    }

    public String getFirstInterviewer() {
        return firstInterviewer;
    }

    public void setFirstInterviewer(String firstInterviewer) {
        this.firstInterviewer = firstInterviewer == null ? null : firstInterviewer.trim();
    }

    public String getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(String firstTime) {
        this.firstTime = firstTime;
    }

    public String getSecondTime() {
        return secondTime;
    }

    public void setSecondTime(String secondTime) {
        this.secondTime = secondTime;
    }

    public String getSecondInterviewer() {
        return secondInterviewer;
    }

    public void setSecondInterviewer(String secondInterviewer) {
        this.secondInterviewer = secondInterviewer == null ? null : secondInterviewer.trim();
    }

    public String getOfferAudit() {
        return offerAudit;
    }

    public void setOfferAudit(String offerAudit) {
        this.offerAudit = offerAudit == null ? null : offerAudit.trim();
    }

    public String getOfferAuditTime() {
        return offerAuditTime;
    }

    public void setOfferAuditTime(String offerAuditTime) {
        this.offerAuditTime = offerAuditTime;
    }

    public String getResumeUrl() {
        return resumeUrl;
    }

    public void setResumeUrl(String resumeUrl) {
        this.resumeUrl = resumeUrl == null ? null : resumeUrl.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    /**
     * @return the addName
     */
    public String getAddName() {
        return addName;
    }

    /**
     * @param addName the addName to set
     */
    public void setAddName(String addName) {
        this.addName = addName;
    }

    /**
     * @return the activitiProcdId
     */
    public String getActivitiProcdId() {
        return activitiProcdId;
    }

    /**
     * @param activitiProcdId the activitiProcdId to set
     */
    public void setActivitiProcdId(String activitiProcdId) {
        this.activitiProcdId = activitiProcdId;
    }

    /**
     * @return the procId
     */
    public String getProcId() {
        return procId;
    }

    /**
     * @param procId the procId to set
     */
    public void setProcId(String procId) {
        this.procId = procId;
    }
}