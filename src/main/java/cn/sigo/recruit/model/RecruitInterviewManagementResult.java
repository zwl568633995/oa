package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 
 * 应聘结果表
 *
 * @author zhixiang.meng
 */
public class RecruitInterviewManagementResult {
    /**
     * 
     */
    private Integer id;
    /**
     * 面试管理表id
     */
    private Integer interviewManagementId;
    /**
     * 应聘记录id
     */
    private Integer applicationRegistId;
    /**
     * 初面反馈
     */
    private String firstFeedback;
    /**
     * 初面备注
     */
    private String firstRemarks;
    /**
     * 复试面试官
     */
    private String secondInterviewer;
    /**
     * 复试时间 
     */
    private String secondTime;
    /**
     * 复试反馈
     */
    private String secondFeedback;
    /**
     * 复试备注
     */
    private String secondRemarks;
    /**
     * 面试期望薪资
     */
    private String feedbackSalary;
    /**
     * offer工资
     */
    private String offerSalary;
    /**
     * offer意见
     */
    private String offerOpinion;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人  
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;
    /**
     * 页面 操作
     */
    private String operation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInterviewManagementId() {
        return interviewManagementId;
    }

    public void setInterviewManagementId(Integer interviewManagementId) {
        this.interviewManagementId = interviewManagementId;
    }

    public Integer getApplicationRegistId() {
        return applicationRegistId;
    }

    public void setApplicationRegistId(Integer applicationRegistId) {
        this.applicationRegistId = applicationRegistId;
    }

    public String getFirstFeedback() {
        return firstFeedback;
    }

    public void setFirstFeedback(String firstFeedback) {
        this.firstFeedback = firstFeedback == null ? null : firstFeedback.trim();
    }

    public String getFirstRemarks() {
        return firstRemarks;
    }

    public void setFirstRemarks(String firstRemarks) {
        this.firstRemarks = firstRemarks == null ? null : firstRemarks.trim();
    }

    public String getSecondFeedback() {
        return secondFeedback;
    }

    public void setSecondFeedback(String secondFeedback) {
        this.secondFeedback = secondFeedback == null ? null : secondFeedback.trim();
    }

    public String getSecondRemarks() {
        return secondRemarks;
    }

    public void setSecondRemarks(String secondRemarks) {
        this.secondRemarks = secondRemarks == null ? null : secondRemarks.trim();
    }

    public String getOfferSalary() {
        return offerSalary;
    }

    public void setOfferSalary(String offerSalary) {
        this.offerSalary = offerSalary == null ? null : offerSalary.trim();
    }

    public String getOfferOpinion() {
        return offerOpinion;
    }

    public void setOfferOpinion(String offerOpinion) {
        this.offerOpinion = offerOpinion == null ? null : offerOpinion.trim();
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * @return the feedbackSalary
     */
    public String getFeedbackSalary() {
        return feedbackSalary;
    }

    /**
     * @param feedbackSalary the feedbackSalary to set
     */
    public void setFeedbackSalary(String feedbackSalary) {
        this.feedbackSalary = feedbackSalary;
    }

    /**
     * @return the secondInterviewer
     */
    public String getSecondInterviewer() {
        return secondInterviewer;
    }

    /**
     * @param secondInterviewer the secondInterviewer to set
     */
    public void setSecondInterviewer(String secondInterviewer) {
        this.secondInterviewer = secondInterviewer;
    }

    /**
     * @return the secondTime
     */
    public String getSecondTime() {
        return secondTime;
    }

    /**
     * @param secondTime the secondTime to set
     */
    public void setSecondTime(String secondTime) {
        this.secondTime = secondTime;
    }
}