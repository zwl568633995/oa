package cn.sigo.recruit.model;


/**
 *
 * 应聘人员
 *
 */
public class RecruitInterviewPeople {
    /**
     * 面试单编号
     */
    private String interviewCode;
    /**
     * 应聘人姓名
     */
    private String applyName;
    /**
     * 应聘职位
     */
    private String applyJobName;
    /**
     * 应聘部门
     */
    private String applyDept;
    /**
     * 应聘人手机号码
     */
    private String applyPhoneNumber;
    /**
     * 简历来源
     */
    private String resumeSource;

    public String getApplyDept() {
        return applyDept;
    }

    public void setApplyDept(String applyDept) {
        this.applyDept = applyDept;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }

    public String getApplyName() {
        return applyName;
    }

    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }

    public String getApplyJobName() {
        return applyJobName;
    }

    public void setApplyJobName(String applyJobName) {
        this.applyJobName = applyJobName;
    }

    public String getApplyPhoneNumber() {
        return applyPhoneNumber;
    }

    public void setApplyPhoneNumber(String applyPhoneNumber) {
        this.applyPhoneNumber = applyPhoneNumber;
    }

    public String getResumeSource() {
        return resumeSource;
    }

    public void setResumeSource(String resumeSource) {
        this.resumeSource = resumeSource;
    }
}
