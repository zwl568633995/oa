package cn.sigo.recruit.model;

import java.util.Date;

/**
 * 面试进度
 */
public class RecruitInterviewProgress {
    private Long id;
    /**
     * 面试单号
     */
    private String interviewCode;
    /**
     * 安排初试
     */
    private Date firstInterview;
    /**
     * 应聘登记
     */
    private Date applyRegister;
    /**
     * 面试
     */
    private Date interview;
    /**
     * offer审核
     */
    private Date offerAudit;
    /**
     * 入职
     */
    private Date entry;

    private String adder;

    private Date addTime;

    private String moder;

    private Date modTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode == null ? null : interviewCode.trim();
    }

    public Date getFirstInterview() {
        return firstInterview;
    }

    public void setFirstInterview(Date firstInterview) {
        this.firstInterview = firstInterview;
    }

    public Date getApplyRegister() {
        return applyRegister;
    }

    public void setApplyRegister(Date applyRegister) {
        this.applyRegister = applyRegister;
    }

    public Date getInterview() {
        return interview;
    }

    public void setInterview(Date interview) {
        this.interview = interview;
    }

    public Date getOfferAudit() {
        return offerAudit;
    }

    public void setOfferAudit(Date offerAudit) {
        this.offerAudit = offerAudit;
    }

    public Date getEntry() {
        return entry;
    }

    public void setEntry(Date entry) {
        this.entry = entry;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}