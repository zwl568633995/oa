package cn.sigo.recruit.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RecruitJobInfo {
    /**
     * 主键
     */
    private Long id;
    /**
     * 职位code
     */
    private String jobCode;
    /**
     * 职位状态
     */
    private String jobStatus;
    /**
     * 申请部门
     */
    @NotBlank(message = "请选择申请部门")
    private String applicationDepartment;
    /**
     * 招聘职位
     */
    @NotBlank(message = "请填写申请招聘职位")
    private String jobName;
    /**
     * 职位评分
     */
    @NotNull(message = "请选择职位评分")
    private Integer jobScore;
    /**
     * 申请原因
     */
    @NotBlank(message = "请选择申请原因")
    private String applicationReasons;
    /**
     * 需求人数
     */
    @NotNull(message = "请填写需求人数")
    private Integer numberOfPeople;
    /**
     * 入职人数
     */
    private Integer entryNumber;
    /**
     * 申请人
     */
    private String applyPeople;
    /**
     * 申请时间
     */
    private Date applyTime;
    /**
     * 审核人
     */
    @NotBlank(message = "请选择审核人")
    private String auditor;
    /**
     * 审核人id
     */
    private String auditId;
    /**
     * 审核时间
     */
    private Date auditTime;
    /**
     * 性别
     */
//    @NotBlank(message = "请选择性别要求")
    private String sex;
    /**
     * 婚姻状况
     */
//    @NotBlank(message = "请选择婚姻状况要求")
    private String maritalStatus;
    /**
     * 工作年限
     */
//    @NotBlank(message = "请选择工作年限")
    private String workYear;
    /**
     * 年龄
     */
//    @NotBlank(message = "请填写年龄要求")
    private String age;
    /**
     * 学历
     */
    @NotBlank(message = "请选择学历要求")
    private String education;
    /**
     * 专业
     */
  //  @NotBlank(message = "请填写专业要求")
    private String major;
    /**
     * 外语水平
     */
//    @NotBlank(message = "请选择外语水平要求")
    private String foreignLanguageLevel;
    /**
     * 薪资范围
     */
    @NotBlank(message = "请填写薪资范围")
    private String salaryRange;
    /**
     * 期望到岗日期
     */
    @NotNull(message = "请选择期望到岗日期")
    private Date expectedDate;
    /**
     * 工作地点
     */
    @NotBlank(message = "请选择工作地点")
    private String workAddress;
    /**
     * 任职资格
     */
    @NotBlank(message = "请填写任职资格")
    private String qualification;
    /**
     * 岗位职责
     */
    @NotBlank(message = "请填写岗位职责")
    private String postDuties;
    /**
     * 初试面试官
     */
    @NotBlank(message = "请选择初试面试官")
    private String firstInterviewer;
    /**
     * 复试面试官
     */
    private String secondInterviewer;
    /**
     * 审核意见
     */
    private String auditOpinion;
    /**
     * 流程id
     */
    private String activitiProcdId;
    /**
     * 启动流程实例id
     */
    private String procId;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public String getWorkYear() {
        return workYear;
    }

    public void setWorkYear(String workYear) {
        this.workYear = workYear;
    }

    /**
     * 
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode == null ? null : jobCode.trim();
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus == null ? null : jobStatus.trim();
    }

    public String getApplicationDepartment() {
        return applicationDepartment;
    }

    public void setApplicationDepartment(String applicationDepartment) {
        this.applicationDepartment = applicationDepartment == null ? null : applicationDepartment.trim();
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public Integer getJobScore() {
        return jobScore;
    }

    public void setJobScore(Integer jobScore) {
        this.jobScore = jobScore;
    }

    public String getApplicationReasons() {
        return applicationReasons;
    }

    public void setApplicationReasons(String applicationReasons) {
        this.applicationReasons = applicationReasons == null ? null : applicationReasons.trim();
    }

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    /**
     * @return the entryNumber
     */
    public Integer getEntryNumber() {
        return entryNumber;
    }

    /**
     * @param entryNumber the entryNumber to set
     */
    public void setEntryNumber(Integer entryNumber) {
        this.entryNumber = entryNumber;
    }

    public String getApplyPeople() {
        return applyPeople;
    }

    public void setApplyPeople(String applyPeople) {
        this.applyPeople = applyPeople == null ? null : applyPeople.trim();
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor == null ? null : auditor.trim();
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age == null ? null : age.trim();
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getForeignLanguageLevel() {
        return foreignLanguageLevel;
    }

    public void setForeignLanguageLevel(String foreignLanguageLevel) {
        this.foreignLanguageLevel = foreignLanguageLevel == null ? null : foreignLanguageLevel.trim();
    }

    public String getSalaryRange() {
        return salaryRange;
    }

    public void setSalaryRange(String salaryRange) {
        this.salaryRange = salaryRange == null ? null : salaryRange.trim();
    }

    public Date getExpectedDate() {
        return expectedDate;
    }

    public void setExpectedDate(Date expectedDate) {
        this.expectedDate = expectedDate;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification == null ? null : qualification.trim();
    }

    public String getPostDuties() {
        return postDuties;
    }

    public void setPostDuties(String postDuties) {
        this.postDuties = postDuties == null ? null : postDuties.trim();
    }

    public String getFirstInterviewer() {
        return firstInterviewer;
    }

    public void setFirstInterviewer(String firstInterviewer) {
        this.firstInterviewer = firstInterviewer == null ? null : firstInterviewer.trim();
    }

    public String getSecondInterviewer() {
        return secondInterviewer;
    }

    public void setSecondInterviewer(String secondInterviewer) {
        this.secondInterviewer = secondInterviewer == null ? null : secondInterviewer.trim();
    }

    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion == null ? null : auditOpinion.trim();
    }

    /**
     * @return the activitiProcdId
     */
    public String getActivitiProcdId() {
        return activitiProcdId;
    }

    /**
     * @param activitiProcdId the activitiProcdId to set
     */
    public void setActivitiProcdId(String activitiProcdId) {
        this.activitiProcdId = activitiProcdId;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    /**
     * @return the procdId
     */
    public String getProcId() {
        return procId;
    }

    /**
     * @param procdId the procdId to set
     */
    public void setProcId(String procId) {
        this.procId = procId;
    }

    /**
     * @return the auditId
     */
    public String getAuditId() {
        return auditId;
    }

    /**
     * @param auditId the auditId to set
     */
    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }
}