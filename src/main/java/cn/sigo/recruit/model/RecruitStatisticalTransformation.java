package cn.sigo.recruit.model;

import java.util.Date;

public class RecruitStatisticalTransformation {
    /**
     * id
     */
    private Integer id;
    /**
     * 部门名
     */
    private String deptName;
    /**
     * 招聘职位
     */
    private String jobName;
    /**
     * 职位评分
     */
    private Integer jobScore;
    /**
     * 统计月份
     */
    private String statisticsMonth;
    /**
     * 候选人数
     */
    private Integer numberOfVacancies;
    /**
     * 面试通过人数
     */
    private Integer interviewNumber;
    /**
     * 发offer人数
     */
    private Integer numberOfOffer;
    /**
     * 已入职人数
     */
    private Integer numberOfRecruits;
    /**
     * 添加人
     */
    private String adder;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改人
     */
    private String moder;
    /**
     * 修改时间
     */
    private Date modTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public Integer getJobScore() {
        return jobScore;
    }

    public void setJobScore(Integer jobScore) {
        this.jobScore = jobScore;
    }

    public String getStatisticsMonth() {
        return statisticsMonth;
    }

    public void setStatisticsMonth(String statisticsMonth) {
        this.statisticsMonth = statisticsMonth == null ? null : statisticsMonth.trim();
    }

    public Integer getNumberOfVacancies() {
        return numberOfVacancies;
    }

    public void setNumberOfVacancies(Integer numberOfVacancies) {
        this.numberOfVacancies = numberOfVacancies;
    }

    public Integer getInterviewNumber() {
        return interviewNumber;
    }

    public void setInterviewNumber(Integer interviewNumber) {
        this.interviewNumber = interviewNumber;
    }

    public Integer getNumberOfOffer() {
        return numberOfOffer;
    }

    public void setNumberOfOffer(Integer numberOfOffer) {
        this.numberOfOffer = numberOfOffer;
    }

    public Integer getNumberOfRecruits() {
        return numberOfRecruits;
    }

    public void setNumberOfRecruits(Integer numberOfRecruits) {
        this.numberOfRecruits = numberOfRecruits;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder == null ? null : adder.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder == null ? null : moder.trim();
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}