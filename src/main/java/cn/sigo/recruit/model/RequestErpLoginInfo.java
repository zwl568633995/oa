/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ErpapiLoginInfo.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 上午11:50:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * erp登录接口信息bean<br> 
 *
 * @author zhixiang.meng
 */
public class RequestErpLoginInfo {
    private String appkey;
    private String accesstoken;
    private String action;
    private Object data;
    /**
     * @return the appkey
     */
    public String getAppkey() {
        return appkey;
    }
    /**
     * @param appkey the appkey to set
     */
    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }
    /**
     * @return the accesstoken
     */
    public String getAccesstoken() {
        return accesstoken;
    }
    /**
     * @param accesstoken the accesstoken to set
     */
    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the data
     */
    public Object getData() {
        return data;
    }
    /**
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
