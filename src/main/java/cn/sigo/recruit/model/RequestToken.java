/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RequestToken.java
 * Author:   zhixiang.meng
 * Date:     2018年7月4日 上午11:21:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * 请求erptoken
 *
 * @author zhixiang.meng
 */
public class RequestToken {
    private String appkey;
    private String appsecret;
    private String action;
    /**
     * @return the appkey
     */
    public String getAppkey() {
        return appkey;
    }
    /**
     * @param appkey the appkey to set
     */
    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }
    /**
     * @return the appsecret
     */
    public String getAppsecret() {
        return appsecret;
    }
    /**
     * @param appsecret the appsecret to set
     */
    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
}
