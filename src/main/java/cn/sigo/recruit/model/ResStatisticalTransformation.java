package cn.sigo.recruit.model;

/**
 * @Auther: DELL
 * @Date: 2018/8/16 10:23
 * @Description: 招聘统计转换率 返回前台实体
 */
public class ResStatisticalTransformation {
    /**
     * id
     */
    private Integer id;
    /**
     * 部门名
     */
    private String deptName;
    /**
     * 招聘职位
     */
    private String jobName;
    /**
     * 职位评分
     */
    private Integer jobScore;
    /**
     * 候选人-->面试通过
     */
    private Integer passInterviewRate;
    /**
     * 面试通过-->发offer
     */
    private Integer offerRate;
    /**
     * 发offer-->入职
     */
    private Integer offerRecruitRate;
    /**
     * 候选人-->入职
     */
    private Integer candidateRecruitRate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Integer getJobScore() {
        return jobScore;
    }

    public void setJobScore(Integer jobScore) {
        this.jobScore = jobScore;
    }

    public Integer getPassInterviewRate() {
        return passInterviewRate;
    }

    public void setPassInterviewRate(Integer passInterviewRate) {
        this.passInterviewRate = passInterviewRate;
    }

    public Integer getOfferRate() {
        return offerRate;
    }

    public void setOfferRate(Integer offerRate) {
        this.offerRate = offerRate;
    }

    public Integer getOfferRecruitRate() {
        return offerRecruitRate;
    }

    public void setOfferRecruitRate(Integer offerRecruitRate) {
        this.offerRecruitRate = offerRecruitRate;
    }

    public Integer getCandidateRecruitRate() {
        return candidateRecruitRate;
    }

    public void setCandidateRecruitRate(Integer candidateRecruitRate) {
        this.candidateRecruitRate = candidateRecruitRate;
    }
}
