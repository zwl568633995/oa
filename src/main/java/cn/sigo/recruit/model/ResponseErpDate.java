/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ResponseErpDate.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午5:06:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

/**
 * 调用erp登录接口返回值
 *
 * @author zhixiang.meng
 */
public class ResponseErpDate implements java.io.Serializable{
    
    /**
     */
    private static final long serialVersionUID = 1L;
    private int error_code;
    private String error_message;
    private Object data;
    /**
     * @return the error_code
     */
    public int getError_code() {
        return error_code;
    }
    /**
     * @param error_code the error_code to set
     */
    public void setError_code(int error_code) {
        this.error_code = error_code;
    }
    /**
     * @return the error_message
     */
    public String getError_message() {
        return error_message;
    }
    /**
     * @param error_message the error_message to set
     */
    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
    /**
     * @return the data
     */
    public Object getData() {
        return data;
    }
    /**
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
