/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ResponseErpUserInfo.java
 * Author:   zhixiang.meng
 * Date:     2018年7月23日 下午1:53:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

import java.util.List;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SuppressWarnings("serial")
public class ResponseErpUserInfo implements java.io.Serializable{
    private int userId;
    private String userName;
    private String userPassword;
    private String realName;
    private String addTime;
    private String status;
    private String userToken;
    private String permission;
    private String dataPermission;
    private List<ErpRole> rackendRole;
    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }
    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }
    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }
    /**
     * @param realName the realName to set
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }
    /**
     * @return the addTime
     */
    public String getAddTime() {
        return addTime;
    }
    /**
     * @param addTime the addTime to set
     */
    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return the userToken
     */
    public String getUserToken() {
        return userToken;
    }
    /**
     * @param userToken the userToken to set
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
    /**
     * @return the permission
     */
    public String getPermission() {
        return permission;
    }
    /**
     * @param permission the permission to set
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }
    /**
     * @return the dataPermission
     */
    public String getDataPermission() {
        return dataPermission;
    }
    /**
     * @param dataPermission the dataPermission to set
     */
    public void setDataPermission(String dataPermission) {
        this.dataPermission = dataPermission;
    }
    /**
     * @return the rackendRole
     */
    public List<ErpRole> getRackendRole() {
        return rackendRole;
    }
    /**
     * @param rackendRole the rackendRole to set
     */
    public void setRackendRole(List<ErpRole> rackendRole) {
        this.rackendRole = rackendRole;
    }
}
