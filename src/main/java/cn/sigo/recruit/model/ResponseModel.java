/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ResponseModel.java
 * Author:   zhixiang.meng
 * Date:     2018年6月28日 上午10:07:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model;

import java.io.Serializable;

/**
 * 返回页面参数<br> 
 *
 * @author zhixiang.meng
 */
public class ResponseModel implements Serializable {
    
    /**
     */
    private static final long serialVersionUID = 1L;
    /**
     * 返回code
     * 0000：表示成功
     */
    private String resultCode;
    /**
     * 返回消息，成功为null；失败有具体消息
     */
    private String resultMsg;
    /**
     * 返回的object类
     */
    private Object data;
    /**
     * @return the resultCode
     */
    public String getResultCode() {
        return resultCode;
    }
    /**
     * @param resultCode the resultCode to set
     */
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
    /**
     * @return the resultMsg
     */
    public String getResultMsg() {
        return resultMsg;
    }
    /**
     * @param resultMsg the resultMsg to set
     */
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
    /**
     * @return the date
     */
    public Object getData() {
        return data;
    }
    /**
     * @param date the date to set
     */
    public void setData(Object data) {
        this.data = data;
    }
}
