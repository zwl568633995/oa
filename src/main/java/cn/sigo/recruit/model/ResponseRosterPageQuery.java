package cn.sigo.recruit.model;

import java.util.Date;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 10:08
 * @Description: 花名册分页返回参数
 */
public class ResponseRosterPageQuery {
    private Integer id;
    /**
     *姓名
     */
    private String entryRegisterName;
    /**
     * 在职信息 id
     */
    private Integer incumbencyId;
    /**
     *工号
     */
    private String workNumber;
    /**
     * 职位
     */
    private String jobName;
    /**
     * 部门
     */
    private String deptName;
    /**
     * 用工性质
     */
    private String workingNature;
    /**
     * 入职时间
     */
    private Date timeOfEntry;
    /**
     * 在司工龄
     */
    private String workYear;
    /**
     *手机号码
     */
    private String phoneNumber;
    /**
     *身份证号码
     */
    private String cardCode;
    /**
     *状态
     */
    private String status;
    /**
     *创建时间
     */
    private Date addTime;
    /**
     *合同预警（天数）
     */
    private Integer contractWarning;

    /**
     *合同到期时间
     */
    private Date contractDateEnd;

    public Date getContractDateEnd() {
        return contractDateEnd;
    }

    public void setContractDateEnd(Date contractDateEnd) {
        this.contractDateEnd = contractDateEnd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getWorkingNature() {
        return workingNature;
    }

    public void setWorkingNature(String workingNature) {
        this.workingNature = workingNature;
    }

    public Date getTimeOfEntry() {
        return timeOfEntry;
    }

    public void setTimeOfEntry(Date timeOfEntry) {
        this.timeOfEntry = timeOfEntry;
    }

    public String getWorkYear() {
        return workYear;
    }

    public void setWorkYear(String workYear) {
        this.workYear = workYear;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getContractWarning() {
        return contractWarning;
    }

    public void setContractWarning(Integer contractWarning) {
        this.contractWarning = contractWarning;
    }
}
