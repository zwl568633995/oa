/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ReqApplicationRegist.java
 * Author:   zhixiang.meng
 * Date:     2018年7月30日 下午4:59:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model.req;

/**
 * 应聘登记表列表 请求参数
 *
 * @author zhixiang.meng
 */
@SuppressWarnings("serial")
public class ReqApplicationRegist implements java.io.Serializable {
    /**
     * 编号
     */
    private String codeNumber;
    /**
     * 面试单编号
     */
    private String interviewCode;
    /**
     * 应聘code
     */
    private String applicationCode;

    /**
     * 应聘人名
     */
    private String applicationName;
    /**
     * 应聘手机号码
     */
    private String phone;
    /**
     * 应聘部门
     */
    private String applicationDept;
    /**
     * 应聘职位
     */
    private String jobName;
    /**
     * 简历来源
     */
    private String applicationSource;
    /**
     * 创建人
     */
    private String createPeople;
    /**
     * 创建开始时间
     */
    private String startCreateTime;
    /**
     * 创建结束时间
     */
    private String endCreateTime;
    /**
     * 查询第几页
     */
    private Integer pageNum = 1;
    /**
     * 每页显示的数量
     */
    private Integer pageSize = 30;

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople;
    }

    /**
     * @return the codeNumber
     */
    public String getCodeNumber() {
        return codeNumber;
    }
    /**
     * @param codeNumber the codeNumber to set
     */
    public void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }
    /**
     * @return the interviewCode
     */
    public String getInterviewCode() {
        return interviewCode;
    }
    /**
     * @param interviewCode the interviewCode to set
     */
    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }
    /**
     * @return the applicationCode
     */
    public String getApplicationCode() {
        return applicationCode;
    }
    /**
     * @param applicationCode the applicationCode to set
     */
    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }
    /**
     * @return the applicationName
     */
    public String getApplicationName() {
        return applicationName;
    }
    /**
     * @param applicationName the applicationName to set
     */
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }
    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * @return the applicationDept
     */
    public String getApplicationDept() {
        return applicationDept;
    }
    /**
     * @param applicationDept the applicationDept to set
     */
    public void setApplicationDept(String applicationDept) {
        this.applicationDept = applicationDept;
    }
    /**
     * @return the applicationSource
     */
    public String getApplicationSource() {
        return applicationSource;
    }
    /**
     * @param applicationSource the applicationSource to set
     */
    public void setApplicationSource(String applicationSource) {
        this.applicationSource = applicationSource;
    }
    /**
     * @return the startCreateTime
     */
    public String getStartCreateTime() {
        return startCreateTime;
    }
    /**
     * @param startCreateTime the startCreateTime to set
     */
    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }
    /**
     * @return the endCreateTime
     */
    public String getEndCreateTime() {
        return endCreateTime;
    }
    /**
     * @param endCreateTime the endCreateTime to set
     */
    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }
    /**
     * @return the pageNum
     */
    public Integer getPageNum() {
        return pageNum;
    }
    /**
     * @param pageNum the pageNum to set
     */
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }
    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
}
