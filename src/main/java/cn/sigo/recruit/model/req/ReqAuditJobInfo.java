/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ReqAuditJobInfo.java
 * Author:   zhixiang.meng
 * Date:     2018年7月19日 上午10:11:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model.req;

/**
 * 职位审核请求参数
 *
 * @author zhixiang.meng
 */
public class ReqAuditJobInfo {
    /**
     * 流程实例id
     */
    private String procId;
    /**
     * 职位编码
     */
    private String jobCode;
    /**
     * 招聘人数
     */
    private String numberOfPeople;
    /**
     * 审核意见
     */
    private String auditOpinion;
    /**
     * 审核结果
     */
    private String auditResult;
    /**
     * 任务用户者
     */
    private String candidateName;
    /**
     * 是否重新提交
     */
    private String isCommit;
    
    /**
     * @return the procId
     */
    public String getProcId() {
        return procId;
    }
    /**
     * @param procId the procId to set
     */
    public void setProcId(String procId) {
        this.procId = procId;
    }
    /**
     * @return the jobCode
     */
    public String getJobCode() {
        return jobCode;
    }
    /**
     * @param jobCode the jobCode to set
     */
    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }
    /**
     * @return the numberOfPeople
     */
    public String getNumberOfPeople() {
        return numberOfPeople;
    }
    /**
     * @param numberOfPeople the numberOfPeople to set
     */
    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }
    /**
     * @return the auditOpinion
     */
    public String getAuditOpinion() {
        return auditOpinion;
    }
    /**
     * @param auditOpinion the auditOpinion to set
     */
    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion;
    }
    /**
     * @return the auditResult
     */
    public String getAuditResult() {
        return auditResult;
    }
    /**
     * @param auditResult the auditResult to set
     */
    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }
    /**
     * @return the candidateName
     */
    public String getCandidateName() {
        return candidateName;
    }
    /**
     * @param candidateName the candidateName to set
     */
    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }
    /**
     * @return the isCommit
     */
    public String getIsCommit() {
        return isCommit;
    }
    /**
     * @param isCommit the isCommit to set
     */
    public void setIsCommit(String isCommit) {
        this.isCommit = isCommit;
    }
}
