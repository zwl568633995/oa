/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ReqInterviewManagementModel.java
 * Author:   zhixiang.meng
 * Date:     2018年7月20日 下午2:44:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model.req;

import java.util.List;

/**
 * 面试单查询
 *
 * @author zhixiang.meng
 */
@SuppressWarnings("serial")
public class ReqInterviewManagementModel  implements java.io.Serializable {
    /**
     * 编号
     */
    private String codeNumber;
    /**
     * 职位编号
     */
    private String jobCode;
    /**
     * 面试单编号
     */
    private String interviewCode;
    /**
     * 应聘人姓名
     */
    private String interviewName;
    /**
     * 应聘职位
     */
    private String interviewJobName;
    /**
     * 应聘人手机号码
     */
    private String interviewPhone;
    /**
     * 面试官
     */
    private String interviewer;
    /**
     * offer审核人
     */
    private String offerAuditer;
    /**
     * 职位招聘部门
     */
    private String jobDept;
    /**
     * 职位来源
     */
    private String resumeSource;
    /**
     * 职位状态
     */
    private List<String> interviewStatuses;
    /**
     * 创建时间  -- 开始
     */
    private String startCreateTime;
    /**
     * 创建时间  -- 结束
     */
    private String endCreateTime;
    /**
     * 面试时间 -- 开始
     */
    private String startInterviewTime;
    /**
     * 面试时间 -- 结束
     */
    private String endInterviewTime;
    /**
     * offer审核时间 -- 开始
     */
    private String startAuditOfferTime;
    /**
     * offer审核时间 -- 结束
     */
    private String endAuditOfferTime;
    /**
     * 查询第几页
     */
    private Integer pageNum = 1;
    /**
     * 每页显示的数量
     */
    private Integer pageSize = 30;
    /**
     * 
     */
    private String userId;
    /**
     * 排序字段
     */
    private String field;
    /**
     * 排序(desc/asc)
     */
    private String sort;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * @return the jobCode
     */
    public String getJobCode() {
        return jobCode;
    }
    /**
     * @param jobCode the jobCode to set
     */
    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }
    /**
     * @return the interviewCode
     */
    public String getInterviewCode() {
        return interviewCode;
    }
    /**
     * @param interviewCode the interviewCode to set
     */
    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }
    /**
     * @return the interviewName
     */
    public String getInterviewName() {
        return interviewName;
    }
    /**
     * @param interviewName the interviewName to set
     */
    public void setInterviewName(String interviewName) {
        this.interviewName = interviewName;
    }
    /**
     * @return the interviewJobName
     */
    public String getInterviewJobName() {
        return interviewJobName;
    }
    /**
     * @param interviewJobName the interviewJobName to set
     */
    public void setInterviewJobName(String interviewJobName) {
        this.interviewJobName = interviewJobName;
    }
    /**
     * @return the interviewPhone
     */
    public String getInterviewPhone() {
        return interviewPhone;
    }
    /**
     * @param interviewPhone the interviewPhone to set
     */
    public void setInterviewPhone(String interviewPhone) {
        this.interviewPhone = interviewPhone;
    }
    /**
     * @return the interviewer
     */
    public String getInterviewer() {
        return interviewer;
    }
    /**
     * @param interviewer the interviewer to set
     */
    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }
    /**
     * @return the offerAuditer
     */
    public String getOfferAuditer() {
        return offerAuditer;
    }
    /**
     * @param offerAuditer the offerAuditer to set
     */
    public void setOfferAuditer(String offerAuditer) {
        this.offerAuditer = offerAuditer;
    }
    /**
     * @return the jobDept
     */
    public String getJobDept() {
        return jobDept;
    }
    /**
     * @param jobDept the jobDept to set
     */
    public void setJobDept(String jobDept) {
        this.jobDept = jobDept;
    }
    /**
     * @return the resumeSource
     */
    public String getResumeSource() {
        return resumeSource;
    }
    /**
     * @param resumeSource the resumeSource to set
     */
    public void setResumeSource(String resumeSource) {
        this.resumeSource = resumeSource;
    }
    /**
     * @return the interviewStatuses
     */
    public List<String> getInterviewStatuses() {
        return interviewStatuses;
    }
    /**
     * @param interviewStatuses the interviewStatuses to set
     */
    public void setInterviewStatuses(List<String> interviewStatuses) {
        this.interviewStatuses = interviewStatuses;
    }
    /**
     * @return the startCreateTime
     */
    public String getStartCreateTime() {
        return startCreateTime;
    }
    /**
     * @param startCreateTime the startCreateTime to set
     */
    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }
    /**
     * @return the endCreateTime
     */
    public String getEndCreateTime() {
        return endCreateTime;
    }
    /**
     * @param endCreateTime the endCreateTime to set
     */
    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }
    /**
     * @return the startInterviewTime
     */
    public String getStartInterviewTime() {
        return startInterviewTime;
    }
    /**
     * @param startInterviewTime the startInterviewTime to set
     */
    public void setStartInterviewTime(String startInterviewTime) {
        this.startInterviewTime = startInterviewTime;
    }
    /**
     * @return the endInterviewTime
     */
    public String getEndInterviewTime() {
        return endInterviewTime;
    }
    /**
     * @param endInterviewTime the endInterviewTime to set
     */
    public void setEndInterviewTime(String endInterviewTime) {
        this.endInterviewTime = endInterviewTime;
    }
    /**
     * @return the startAuditOfferTime
     */
    public String getStartAuditOfferTime() {
        return startAuditOfferTime;
    }
    /**
     * @param startAuditOfferTime the startAuditOfferTime to set
     */
    public void setStartAuditOfferTime(String startAuditOfferTime) {
        this.startAuditOfferTime = startAuditOfferTime;
    }
    /**
     * @return the endAuditOfferTime
     */
    public String getEndAuditOfferTime() {
        return endAuditOfferTime;
    }
    /**
     * @param endAuditOfferTime the endAuditOfferTime to set
     */
    public void setEndAuditOfferTime(String endAuditOfferTime) {
        this.endAuditOfferTime = endAuditOfferTime;
    }
    /**
     * @return the pageNum
     */
    public Integer getPageNum() {
        return pageNum;
    }
    /**
     * @param pageNum the pageNum to set
     */
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }
    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    /**
     * @return the codeNumber
     */
    public String getCodeNumber() {
        return codeNumber;
    }
    /**
     * @param codeNumber the codeNumber to set
     */
    public void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }
    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
