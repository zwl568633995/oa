/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ReqJobInfoModel.java
 * Author:   zhixiang.meng
 * Date:     2018年7月10日 下午5:51:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model.req;

import java.util.List;

/**
 * 职位招聘请求的参数
 *
 * @author zhixiang.meng
 */
@SuppressWarnings("serial")
public class ReqJobInfoModel implements java.io.Serializable{
    /**
     * 职位编号
     */
    private String jobCode;
    /**
     * 招聘职位name
     */
    private String jobName;
    /**
     * 申请人
     */
    private String applyPeople;
    /**
     * 审核人
     */
    private String auditor;
    /**
     * 申请部门
     */
    private String applicationDepartment;
    /**
     * 申请原因
     */
    private String applicationReasons;
    /**
     * 状态
     */
    private List<String> jobStatuses;
    /**
     * 申请开始时间
     */
    private String applyStartTime;
    /**
     * 申请结束时间
     */
    private String applyEndTime;
    /**
     * 审核开始时间
     */
    private String auditStartTime;
    /**
     * 审核结束时间
     */
    private String auditEndTime;
    /**
     * 查询第几页
     */
    private Integer pageNum = 1;
    /**
     * 每页显示的数量
     */
    private Integer pageSize = 30;
    /**
     * 角色
     */
    private String backendRole;
    /**
     * 当前登陆者id
     */
    private String userId;

    /**
     * 排序字段
     */
    private String field;
    /**
     * 排序(desc/asc)
     */
    private String sort;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * @return the jobCode
     */
    public String getJobCode() {
        return jobCode;
    }
    /**
     * @param jobCode the jobCode to set
     */
    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }
    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }
    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    /**
     * @return the applyPeople
     */
    public String getApplyPeople() {
        return applyPeople;
    }
    /**
     * @param applyPeople the applyPeople to set
     */
    public void setApplyPeople(String applyPeople) {
        this.applyPeople = applyPeople;
    }
    /**
     * @return the auditor
     */
    public String getAuditor() {
        return auditor;
    }
    /**
     * @param auditor the auditor to set
     */
    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }
    /**
     * @return the applicationDepartment
     */
    public String getApplicationDepartment() {
        return applicationDepartment;
    }
    /**
     * @param applicationDepartment the applicationDepartment to set
     */
    public void setApplicationDepartment(String applicationDepartment) {
        this.applicationDepartment = applicationDepartment;
    }
    /**
     * @return the applicationReasons
     */
    public String getApplicationReasons() {
        return applicationReasons;
    }
    /**
     * @param applicationReasons the applicationReasons to set
     */
    public void setApplicationReasons(String applicationReasons) {
        this.applicationReasons = applicationReasons;
    }

    /**
     * @return the jobStatuses
     */
    public List<String> getJobStatuses() {
        return jobStatuses;
    }
    /**
     * @param jobStatuses the jobStatuses to set
     */
    public void setJobStatuses(List<String> jobStatuses) {
        this.jobStatuses = jobStatuses;
    }
    /**
     * @return the applyStartTime
     */
    public String getApplyStartTime() {
        return applyStartTime;
    }
    /**
     * @param applyStartTime the applyStartTime to set
     */
    public void setApplyStartTime(String applyStartTime) {
        this.applyStartTime = applyStartTime;
    }
    /**
     * @return the applyEndTime
     */
    public String getApplyEndTime() {
        return applyEndTime;
    }
    /**
     * @param applyEndTime the applyEndTime to set
     */
    public void setApplyEndTime(String applyEndTime) {
        this.applyEndTime = applyEndTime;
    }
    /**
     * @return the auditStartTime
     */
    public String getAuditStartTime() {
        return auditStartTime;
    }
    /**
     * @param auditStartTime the auditStartTime to set
     */
    public void setAuditStartTime(String auditStartTime) {
        this.auditStartTime = auditStartTime;
    }
    /**
     * @return the auditEndTime
     */
    public String getAuditEndTime() {
        return auditEndTime;
    }
    /**
     * @param auditEndTime the auditEndTime to set
     */
    public void setAuditEndTime(String auditEndTime) {
        this.auditEndTime = auditEndTime;
    }
    /**
     * @return the pageNum
     */
    public Integer getPageNum() {
        return pageNum;
    }
    /**
     * @param pageNum the pageNum to set
     */
    public void setPageNum(Integer pageNum) {
        this.pageNum = (null == pageNum ? 1 : pageNum);
    }
    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }
    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = (null == pageSize ? 30 : pageSize);
    }
    /**
     * @return the backendRole
     */
    public String getBackendRole() {
        return backendRole;
    }
    /**
     * @param backendRole the backendRole to set
     */
    public void setBackendRole(String backendRole) {
        this.backendRole = backendRole;
    }
    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
