package cn.sigo.recruit.model.req;



/**
 * @Auther: DELL
 * @Date: 2018/9/27 10:39
 * @Description:
 */
public class ReqModifyInterviewTime {
    /**
     * 面试单编号
     */
    private String interviewCode;
    /**
     * 初试时间
     */
    private String firstTime;
    /**
     * 复试时间
     */
    private String secondTime;

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }

    public String getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(String firstTime) {
        this.firstTime = firstTime;
    }

    public String getSecondTime() {
        return secondTime;
    }

    public void setSecondTime(String secondTime) {
        this.secondTime = secondTime;
    }
}
