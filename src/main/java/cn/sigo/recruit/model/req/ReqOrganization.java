package cn.sigo.recruit.model.req;

/**
 * @Auther: DELL
 * @Date: 2018/9/10 11:04
 * @Description:
 */
public class ReqOrganization {
    private String parentId;

    private String department;

    private Boolean status;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
