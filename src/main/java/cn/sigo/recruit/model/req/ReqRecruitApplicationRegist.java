/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ReqRecruitApplicationRegist.java
 * Author:   zhixiang.meng
 * Date:     2018年7月17日 下午2:30:58
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.model.req;

import cn.sigo.recruit.model.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 应聘登录记表注册
 *
 * @author zhixiang.meng
 */
@SuppressWarnings("serial")
public class ReqRecruitApplicationRegist implements java.io.Serializable{
    @NotNull(message = "请补全基本信息")
    private @Valid RecruitApplicationRegist regist;
    @NotEmpty(message = "请添加教育经历")
    private @Valid List<RecruitApplicationEducation> educations;
    private List<RecruitApplicationCertificate> certificates;
    private List<RecruitApplicationFamilyMember> familyMembers;
    private List<RecruitApplicationWork> applicationWorks;


    public RecruitApplicationRegist getRegist() {
        return regist;
    }
    public void setRegist(RecruitApplicationRegist regist) {
        this.regist = regist;
    }
    public List<RecruitApplicationCertificate> getCertificates() {
        return certificates;
    }
    public void setCertificates(List<RecruitApplicationCertificate> certificate) {
        certificates = certificate;
    }
    public List<RecruitApplicationEducation> getEducations() {
        return educations;
    }
    public void setEducations(List<RecruitApplicationEducation> educations) {
        this.educations = educations;
    }
    public List<RecruitApplicationFamilyMember> getFamilyMembers() {
        return familyMembers;
    }
    public void setFamilyMembers(List<RecruitApplicationFamilyMember> familyMembers) {
        this.familyMembers = familyMembers;
    }
    public List<RecruitApplicationWork> getApplicationWorks() {
        return applicationWorks;
    }
    public void setApplicationWorks(List<RecruitApplicationWork> applicationWorks) {
        this.applicationWorks = applicationWorks;
    }
}
