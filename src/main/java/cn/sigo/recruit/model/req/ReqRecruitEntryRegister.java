package cn.sigo.recruit.model.req;

import cn.sigo.recruit.model.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 入职登录记表保存实体
 *
 */
public class ReqRecruitEntryRegister {
    @NotNull(message = "请补全基本信息")
    private @Valid RecruitEntryRegisterBaseInfo baseInfo;
    private RecruitEntryHealthy healthy;
    private List<RecruitIncumbencyCertificate> certificates;
    @NotEmpty(message = "请添加教育经历")
    private @Valid List<RecruitIncumbencyEducation> educations;
    private List<RecruitIncumbencyFamilyMember> familyMembers;
    private @Valid List<RecruitIncumbencyWork> works;

    public RecruitEntryRegisterBaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(RecruitEntryRegisterBaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public RecruitEntryHealthy getHealthy() {
        return healthy;
    }

    public void setHealthy(RecruitEntryHealthy healthy) {
        this.healthy = healthy;
    }

    public List<RecruitIncumbencyCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<RecruitIncumbencyCertificate> certificates) {
        this.certificates = certificates;
    }

    public List<RecruitIncumbencyEducation> getEducations() {
        return educations;
    }

    public void setEducations(List<RecruitIncumbencyEducation> educations) {
        this.educations = educations;
    }

    public List<RecruitIncumbencyFamilyMember> getFamilyMembers() {
        return familyMembers;
    }

    public void setFamilyMembers(List<RecruitIncumbencyFamilyMember> familyMembers) {
        this.familyMembers = familyMembers;
    }

    public List<RecruitIncumbencyWork> getWorks() {
        return works;
    }

    public void setWorks(List<RecruitIncumbencyWork> works) {
        this.works = works;
    }
}
