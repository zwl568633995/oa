package cn.sigo.recruit.model.req;

import java.util.Date;

/**
 *
 * 入职登记表分页查询 请求参数接口
 *
 */
public class ReqRecruitEntryRegisterPageQuery {
    /**
     * 入职登记表/应聘登记表/面试单编号
     */
    private String codeNumber;
    /**
     * 姓名
     */
    private String entryRegisterName;
    /**
     * 手机号码
     */
    private String phoneNumber;
    /**
     * 职位
     */
    private String jobName;
    /**
     * 部门
     */
    private String jobDept;
    /**
     * 入职登记表编号
     */
    private String entryCode;
    /**
     * 应聘登记表编号
     */
    private String applicationCode;
    /**
     * 面试登记表编号
     */
    private String interviewCode;
    /**
     * 应聘简历来源
     */
    private String resumeSource;
    /**
     * 创建人
     */
    private String createPeople;
    /**
     * 创建时间  -- 开始
     */
    private String startCreateTime;
    /**
     * 创建时间  -- 结束
     */
    private String endCreateTime;
    /**
     * 创建时间
     */
    private Date addTime;
    /**
     * 查询第几页
     */
    private Integer pageNum = 1;
    /**
     * 每页显示的数量
     */
    private Integer pageSize = 20;

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDept() {
        return jobDept;
    }

    public void setJobDept(String jobDept) {
        this.jobDept = jobDept;
    }

    public String getCodeNumber() {
        return codeNumber;
    }

    public void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEntryCode() {
        return entryCode;
    }

    public void setEntryCode(String entryCode) {
        this.entryCode = entryCode;
    }

    public String getInterviewCode() {
        return interviewCode;
    }

    public void setInterviewCode(String interviewCode) {
        this.interviewCode = interviewCode;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getResumeSource() {
        return resumeSource;
    }

    public void setResumeSource(String resumeSource) {
        this.resumeSource = resumeSource;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
