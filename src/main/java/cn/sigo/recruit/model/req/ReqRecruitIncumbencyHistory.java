package cn.sigo.recruit.model.req;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 11:55
 * @Description: 人事动态 操作
 */
public class ReqRecruitIncumbencyHistory {
    /**
     *在职信息id
     */
    private Integer incumbencyId;
    /**
     *操作类型
     */
    private String operationType;
    /**
     *操作详情
     */
    private String operationDesc;

    public Integer getIncumbencyId() {
        return incumbencyId;
    }

    public void setIncumbencyId(Integer incumbencyId) {
        this.incumbencyId = incumbencyId;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationDesc() {
        return operationDesc;
    }

    public void setOperationDesc(String operationDesc) {
        this.operationDesc = operationDesc;
    }
}
