package cn.sigo.recruit.model.req;

import cn.sigo.recruit.model.*;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 13:53
 * @Description:
 */
@Data
public class ReqRosterEmployee {
   @NotNull(message = "请补全基本信息")
   private @Valid RecruitIncumbencyUserBaseInfo userBaseInfo;

   private List<RecruitIncumbencyCertificate> certificates;

   @NotEmpty(message = "请添加教育经历")
   private @Valid List<RecruitIncumbencyEducation> educations;

   @NotEmpty(message = "请添加家庭成员")
   private @Valid List<RecruitIncumbencyFamilyMember> familyMembers;

   @NotEmpty(message = "请添加工作经历")
   private @Valid List<RecruitIncumbencyWork> works;

   @NotNull(message = "请补全在职信息")
   private @Valid RecruitIncumbencyInfo info;

   @NotNull(message = "请补全合同信息")
   private @Valid RecruitIncumbencyContract contract;

   private RecruitIncumbencySecurityCard card;

   private List<RecruitIncumbencyEnclosure> attach;

   public RecruitIncumbencyContract getContract() {
      return contract;
   }

   public void setContract(RecruitIncumbencyContract contract) {
      this.contract = contract;
   }

   public RecruitIncumbencyUserBaseInfo getUserBaseInfo() {
      return userBaseInfo;
   }

   public void setUserBaseInfo(RecruitIncumbencyUserBaseInfo userBaseInfo) {
      this.userBaseInfo = userBaseInfo;
   }

   public List<RecruitIncumbencyCertificate> getCertificates() {
      return certificates;
   }

   public void setCertificates(List<RecruitIncumbencyCertificate> certificates) {
      this.certificates = certificates;
   }

   public List<RecruitIncumbencyEducation> getEducations() {
      return educations;
   }

   public void setEducations(List<RecruitIncumbencyEducation> educations) {
      this.educations = educations;
   }

   public List<RecruitIncumbencyFamilyMember> getFamilyMembers() {
      return familyMembers;
   }

   public void setFamilyMembers(List<RecruitIncumbencyFamilyMember> familyMembers) {
      this.familyMembers = familyMembers;
   }

   public List<RecruitIncumbencyWork> getWorks() {
      return works;
   }

   public void setWorks(List<RecruitIncumbencyWork> works) {
      this.works = works;
   }

   public RecruitIncumbencyInfo getInfo() {
      return info;
   }

   public void setInfo(RecruitIncumbencyInfo info) {
      this.info = info;
   }

   public RecruitIncumbencySecurityCard getCard() {
      return card;
   }

   public void setCard(RecruitIncumbencySecurityCard card) {
      this.card = card;
   }

   public List<RecruitIncumbencyEnclosure> getAttach() {
      return attach;
   }

   public void setAttach(List<RecruitIncumbencyEnclosure> attach) {
      this.attach = attach;
   }
}
