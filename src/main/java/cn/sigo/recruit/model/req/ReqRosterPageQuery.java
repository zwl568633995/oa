package cn.sigo.recruit.model.req;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 09:52
 * @Description: 花名册分页查询参数
 */
public class ReqRosterPageQuery {
    /**
     *姓名
     */
    private String entryRegisterName;
    /**
     *工号
     */
    private String workNumber;
    /**
     * 职位
     */
    private String jobName;
    /**
     *手机号码
     */
    private String phoneNumber;
    /**
     *身份证号码
     */
    private String cardCode;
    /**
     * 部门
     */
    private String deptName;
    /**
     *状态
     */
    private String status;
    /**
     *合同预警（是/否）
     */
    private Boolean contractWarning;
    /**
     *合同预警-时间
     */
    private String contractWarningTime;
    /**
     * 入职时间-开始
     */
    private String timeOfEntryStart;
    /**
     * 入职时间-结束
     */
    private String timeOfEntryEnd;
    /**
     * 创建时间  -- 开始
     */
    private String startCreateTime;
    /**
     * 创建时间  -- 结束
     */
    private String endCreateTime;
    /**
     * 创建人
     */
    private String createPerson;
    /**
     * 查询第几页
     */
    private Integer pageNum = 1;

    public String getContractWarningTime() {
        return contractWarningTime;
    }

    public void setContractWarningTime(String contractWarningTime) {
        this.contractWarningTime = contractWarningTime;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * 每页显示的数量
     */

    private Integer pageSize = 10;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getEntryRegisterName() {
        return entryRegisterName;
    }

    public void setEntryRegisterName(String entryRegisterName) {
        this.entryRegisterName = entryRegisterName;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getContractWarning() {
        return contractWarning;
    }

    public void setContractWarning(Boolean contractWarning) {
        this.contractWarning = contractWarning;
    }

    public String getTimeOfEntryStart() {
        return timeOfEntryStart;
    }

    public void setTimeOfEntryStart(String timeOfEntryStart) {
        this.timeOfEntryStart = timeOfEntryStart;
    }

    public String getTimeOfEntryEnd() {
        return timeOfEntryEnd;
    }

    public void setTimeOfEntryEnd(String timeOfEntryEnd) {
        this.timeOfEntryEnd = timeOfEntryEnd;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }
}
