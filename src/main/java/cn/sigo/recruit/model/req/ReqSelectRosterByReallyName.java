package cn.sigo.recruit.model.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ReqSelectRosterByReallyName {

   @NotBlank
   private String reallyName;
}
