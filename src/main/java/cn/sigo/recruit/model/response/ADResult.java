package cn.sigo.recruit.model.response;

import lombok.Data;

import java.util.Map;

@Data
public class ADResult {

   private String code;
   private String message;
   private Map data;

}
