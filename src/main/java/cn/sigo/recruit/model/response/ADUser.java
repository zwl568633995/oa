package cn.sigo.recruit.model.response;

public class ADUser {
    /**
     * 用户表
     *   userName:姓名全拼
     *   deptPath:层级
     *   email:用户邮箱地址
     *   givenName:用户 名的拼音
     *   surname:用户 姓的拼音
     *   realName:用户中文名
     *   position:用户职位
     *   mobile:用户手机号
     *   password:用户密码
     *   department:用户部门
     */
    private String userName;
    private String deptPath;
    private String email;
    private String givenName;
    private String surname;
    private String realName;
    private String position;
    private String password;
    private String mobile;
    private String department;

    @Override
    public String toString() {
        return "ADUser{" +
            "userName='" + userName + '\'' +
            ", deptPath='" + deptPath + '\'' +
            ", email='" + email + '\'' +
            ", givenName='" + givenName + '\'' +
            ", surname='" + surname + '\'' +
            ", realName='" + realName + '\'' +
            ", position='" + position + '\'' +
            ", password='" + password + '\'' +
            ", mobile='" + mobile + '\'' +
            ", department='" + department + '\'' +
            '}';
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeptPath() {
        return deptPath;
    }

    public void setDeptPath(String deptPath) {
        this.deptPath = deptPath;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
