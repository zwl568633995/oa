package cn.sigo.recruit.model.response;

import lombok.Data;


/**
 * 邮件收件人信息
 */
@Data
public class ReceiveAccount {
    /**
     * 收件人邮箱
     */

    private String receiveMsgAccount;

    /**
     * 收件人名称
     */
    private String receiveMsgAccountName;
}
