package cn.sigo.recruit.model.response;

import lombok.Data;

@Data
public class RespADAddDept {
   private String name;
   private String path;
}
