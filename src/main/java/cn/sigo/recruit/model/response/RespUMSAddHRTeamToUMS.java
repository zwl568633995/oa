package cn.sigo.recruit.model.response;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class RespUMSAddHRTeamToUMS {

   /**
    * 团队id
    */
   @NotNull(message = "部门id不能为空")
   private Long id;
   /**
    * 团队名称
    */
   @NotBlank(message = "团队名称不能为空")
   private String deptName;
   /**
    * 团队类型
    */
   private String teamType;
   /**
    * 上级团队id
    */
   @NotNull(message = "上级团队id不能为空")
   private long superiorDeptId;
   /**
    * 是否是组织架构
    */
   private Integer isOrganizationalStructure;

   /**
    * 部门状态
    */
   private String teamStatus;

   /**
    * 添加人
    */
   private String adder;

   /**
    * 添加时间
    */
   private Date addTime;

   /**
    * 最新修改人
    */
   private String moder;
   /**
    * 最新修改时间
    */
   private Date modTime;

}
