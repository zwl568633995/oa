package cn.sigo.recruit.model.response;

import lombok.Data;

@Data
public class RespUMSSelectByReallyName {
   private String reallyName;
}
