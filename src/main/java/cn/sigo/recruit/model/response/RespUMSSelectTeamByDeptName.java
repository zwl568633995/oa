package cn.sigo.recruit.model.response;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RespUMSSelectTeamByDeptName {

   @NotBlank
   private String deptName;
}
