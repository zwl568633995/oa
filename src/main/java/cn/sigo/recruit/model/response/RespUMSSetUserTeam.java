package cn.sigo.recruit.model.response;

import lombok.Data;

import java.util.List;

@Data
public class RespUMSSetUserTeam {

   /**
    * 用户ad
    */
   private Integer userId;

   /**
    * 团队id集合
    */
   private List<Integer> teamIds;
}
