package cn.sigo.recruit.model.response;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RespUMSUpdateUser {

   @NotNull(message = "用户不能为空!")
   private Long id;
   private Boolean deleted;
   private String email;
   private String phone;
   private String userName;
   private String reallyName;

   private List<Long> roleIds;
   private List<Long> teamIds;
}
