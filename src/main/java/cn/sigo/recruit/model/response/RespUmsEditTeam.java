package cn.sigo.recruit.model.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RespUmsEditTeam {

   private Long id;
   /**
    * 团队名
    */
   private String deptName;
   /**
    * 团队类型
    */
   private String teamType;
   /**
    * 上级团队id
    */
   private Long superiorDeptId;

   /**
    * 仓库id 集合
    */
   private List<Long> warehouseIds;
   /**
    * 团队负责人
    */
   private String deptOver;
   /**
    * 是否是组织架构(0:否;1:是)
    */
   private Integer isOrganizationalStructure;
   /**
    *  状态 0：启用；1：停用
    */
   private Boolean deleted;

   private String teamStatus;

   /**
    * 显示顺序
    */
   private Integer orderIndex;

   private String adder;

   private Date addTime;

   private String moder;

   private Date modTime;
}
