package cn.sigo.recruit.model.response;

/**
 * @Auther: DELL
 * @Date: 2018/9/13 14:20
 * @Description:
 */
public class ResponseDeptOver {
    private String deptOverName;

    public String getDeptOverName() {
        return deptOverName;
    }

    public void setDeptOverName(String deptOverName) {
        this.deptOverName = deptOverName;
    }
}
