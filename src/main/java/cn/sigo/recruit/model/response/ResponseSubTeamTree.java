package cn.sigo.recruit.model.response;

/**
 * @Auther: DELL
 * @Date: 2018/9/12 14:41
 * @Description:
 */
public class ResponseSubTeamTree {
    private Long id;

    private String teamName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
