package cn.sigo.recruit.model.response;

import java.util.List;

/**
 * @Auther: DELL
 * @Date: 2018/9/12 14:39
 * @Description:
 */
public class ResponseTeamTree {
    private Long parentId;

    private String parentName;

    private Long id;

    private String teamType;

    private String teamName;

    private List<ResponseSubTeamTree> subTeamTree;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<ResponseSubTeamTree> getSubTeamTree() {
        return subTeamTree;
    }

    public void setSubTeamTree(List<ResponseSubTeamTree> subTeamTree) {
        this.subTeamTree = subTeamTree;
    }
}
