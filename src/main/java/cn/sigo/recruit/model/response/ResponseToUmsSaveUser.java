package cn.sigo.recruit.model.response;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class ResponseToUmsSaveUser {

   private Long id;

   /**
    * 花名册id
    */
   @NotNull(message = "花名册id不能为空")
   private Integer rosterId;
   /**
    *  用户名
    */
   @NotBlank(message = "用户名不能为空")
   private String userName;
   /**
    *  密码
    */
   @NotBlank(message = "密码不能为空")
   private String password;
   /**
    *  真实姓名
    */
   private String reallyName;
   /**
    *  手机号码
    */

   /**
    * 用户类型
    */
   private String userType;

   @NotBlank(message = "手机号码不能为空")
   private String phone;
   /**
    *  邮箱
    */
   @NotBlank(message = "邮箱不能为空")
   private String email;
   /**
    *  状态 0：启用；1：停用
    */
   private Boolean deleted;

   private String status;

   /**
    * 职位
    */
   private String position;
   /**
    *  团队Ids
    */
   private List<Long> teamIds;
   /**
    *  角色Ids
    */
   private List<Long> roleIds;
   /**
    *  角色名称
    */
   private String roleNames;
   /**
    *  添加人
    */
   private String adder;
   /**
    *  添加时间
    */
   private Date addTime;
   /**
    *  修改人
    */
   private String moder;
   /**
    *  修改时间
    */
   private Date modTime;
}
