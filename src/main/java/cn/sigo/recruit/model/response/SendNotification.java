package cn.sigo.recruit.model.response;

import lombok.Data;

import java.util.List;

/**
 * 调用发送消息服务需要的参数
 */
@Data
public class SendNotification {

    /**
     * 消息标题
     */
    private String msgTitle;

    /**
     * 消息内容
     */
    private String msgContent;

    /**
     * 收件人集合
     */
    List<ReceiveAccount> receiveAccountList;

}
