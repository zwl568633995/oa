package cn.sigo.recruit.model.response;

import lombok.Data;

@Data
public class SendNotificationResult {

    private String code;
    private String message;
    private Object data;
}
