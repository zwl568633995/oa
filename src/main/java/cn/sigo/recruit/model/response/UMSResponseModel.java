package cn.sigo.recruit.model.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class UMSResponseModel implements Serializable {

   private static final long serialVersionUID = 1L;

   /**
    * 返回code
    * 0000：表示成功
    */
   private String resultCode;
   /**
    * 返回消息，成功为null；失败有具体消息
    */
   private String resultMsg;
   /**
    * 返回的object类
    */
   private Object data;
}
