package cn.sigo.recruit.model.response;

import java.util.Date;
import java.util.List;

/**
 *  UMS 角色实体
 */
public class UMSRole {

    private Long id;
    /**
     * 角色名
     */
    private String roleName;
    /**
     * 角色组id
     */
    private Long roleGroupId;
    /**
     * 角色组id 集合
     */
    private List<Long> roleGroupIds;
    /**
     * 角色组名
     */
    private String roleGroupName;
    /**
     * 所属团队id
     */
    private Long teamId;
    /**
     * 所属团队名
     */
    private String teamName;
    /**
     *  状态 0：启用；1：停用
     */
    private Boolean deleted;

    /**
     * 状态
     */
    private String roleStatus;
    /**
     * 团队类型
     */
    private String teamType;

    private String adder;

    private Date addTime;

    private String moder;

    private Date modTime;

    @Override
    public String toString() {
        return "UMSRole{" +
            "id=" + id +
            ", roleName='" + roleName + '\'' +
            ", roleGroupId=" + roleGroupId +
            ", roleGroupIds=" + roleGroupIds +
            ", roleGroupName='" + roleGroupName + '\'' +
            ", teamId=" + teamId +
            ", teamName='" + teamName + '\'' +
            ", deleted=" + deleted +
            ", roleStatus='" + roleStatus + '\'' +
            ", teamType='" + teamType + '\'' +
            ", adder='" + adder + '\'' +
            ", addTime=" + addTime +
            ", moder='" + moder + '\'' +
            ", modTime=" + modTime +
            '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getRoleGroupId() {
        return roleGroupId;
    }

    public void setRoleGroupId(Long roleGroupId) {
        this.roleGroupId = roleGroupId;
    }

    public List<Long> getRoleGroupIds() {
        return roleGroupIds;
    }

    public void setRoleGroupIds(List<Long> roleGroupIds) {
        this.roleGroupIds = roleGroupIds;
    }

    public String getRoleGroupName() {
        return roleGroupName;
    }

    public void setRoleGroupName(String roleGroupName) {
        this.roleGroupName = roleGroupName;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(String roleStatus) {
        this.roleStatus = roleStatus;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getModer() {
        return moder;
    }

    public void setModer(String moder) {
        this.moder = moder;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }
}
