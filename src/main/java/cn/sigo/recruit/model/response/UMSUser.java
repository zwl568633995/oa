package cn.sigo.recruit.model.response;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

public class UMSUser {

   private Long id;

   /**
    * 花名册id
    */
   private Integer rosterId;

   /**
    * 用户名
    */
   @NotBlank(message = "用户名不能为空")
   private String userName;
   /**
    * 密码
    */
   @NotBlank(message = "密码不能为空")
   private String password;
   /**
    * 真实姓名
    */
   private String reallyName;

   /**
    * 用户类型
    */
   @NotBlank(message = "用户类型不能为空")
   private String userType;
   /**
    * 手机号码
    */
   @NotBlank(message = "手机号码不能为空")
   private String phone;
   /**
    * 邮箱
    */
   @NotBlank(message = "邮箱不能为空")
   private String email;
   /**
    * 状态 0：启用；1：停用
    */
   private Boolean deleted;

   private String status;

   /**
    * 职位
    */
   private String position;
   /**
    * 团队Ids
    */
   private List<Long> teamIds;
   /**
    * 角色Ids
    */
   private List<Long> roleIds;
   /**
    * 角色名称
    */
   private String roleNames;
   /**
    * 添加人
    */
   private String adder;
   /**
    * 添加时间
    */
   private Date addTime;
   /**
    * 修改人
    */
   private String moder;
   /**
    * 修改时间
    */
   private Date modTime;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Integer getRosterId() {
      return rosterId;
   }

   public void setRosterId(Integer rosterId) {
      this.rosterId = rosterId;
   }

   public String getUserName() {
      return userName;
   }

   public void setUserName(String userName) {
      this.userName = userName;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getReallyName() {
      return reallyName;
   }

   public void setReallyName(String reallyName) {
      this.reallyName = reallyName;
   }

   public String getUserType() {
      return userType;
   }

   public void setUserType(String userType) {
      this.userType = userType;
   }

   public String getPhone() {
      return phone;
   }

   public void setPhone(String phone) {
      this.phone = phone;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public Boolean getDeleted() {
      return deleted;
   }

   public void setDeleted(Boolean deleted) {
      this.deleted = deleted;
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getPosition() {
      return position;
   }

   public void setPosition(String position) {
      this.position = position;
   }

   public List<Long> getTeamIds() {
      return teamIds;
   }

   public void setTeamIds(List<Long> teamIds) {
      this.teamIds = teamIds;
   }

   public List<Long> getRoleIds() {
      return roleIds;
   }

   public void setRoleIds(List<Long> roleIds) {
      this.roleIds = roleIds;
   }

   public String getRoleNames() {
      return roleNames;
   }

   public void setRoleNames(String roleNames) {
      this.roleNames = roleNames;
   }

   public String getAdder() {
      return adder;
   }

   public void setAdder(String adder) {
      this.adder = adder;
   }

   public Date getAddTime() {
      return addTime;
   }

   public void setAddTime(Date addTime) {
      this.addTime = addTime;
   }

   public String getModer() {
      return moder;
   }

   public void setModer(String moder) {
      this.moder = moder;
   }

   public Date getModTime() {
      return modTime;
   }

   public void setModTime(Date modTime) {
      this.modTime = modTime;
   }
}
