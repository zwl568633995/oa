/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiProcdefIdService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月10日 上午11:35:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import cn.sigo.recruit.model.ActivitiProcdefId;

/**
 * activiti 流程部署文件
 * 流程文件更新的时候，需要将最新的activiti流程id更新到
 * t_activiti_procdef_id表中
 *
 * @author zhixiang.meng
 */
public interface ActivitiProcdefIdService {

    /**
     * 保存
     *
     * @param procedef
     * @return
     */
    boolean savenActivitiProcdefId(ActivitiProcdefId procedef);
    
    /**
     * 
     * 更新流程id
     *
     * @param id
     * @param ProcdefId
     * @return
     */
    boolean updateActivitiProcdefId(String procdefKey, String procdefId);
    
    /**
     * 
     * 根据流程key，判断是否为null
     *
     * @param procdef
     * @return
     */
    boolean isExist(String procdeKey);
    
    /**
     * 
     * 根据流程key，判断是否不为null
     *
     * @param procdef
     * @return
     */
    boolean isNotExist(String procdeKey);
}
