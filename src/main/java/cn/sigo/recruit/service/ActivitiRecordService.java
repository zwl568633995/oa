/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiRecordService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月9日 下午1:49:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.Date;

import cn.sigo.recruit.model.ActivitiRecord;

/**
 * activiti 流程文件记录
 *
 * @author zhixiang.meng
 */
public interface ActivitiRecordService {
    /**
     * 
     * 根据流程文件名，查询流程文件
     *
     * @param activitiName:流程文件名
     * @return
     */
    ActivitiRecord queryByForActivitiName(String activitiName);
    /**
     * 保存流程文件
     * 流程文件存在不添加，不存在添加
     *
     * @param record
     * @param isQuery:true:表示查询；false：表示不查询
     * @return
     */
    boolean saveActivitiRecordService(ActivitiRecord record, boolean isQuery);
    /**
     * 
     * 更新流程文件的更新时间
     *
     * @param acitivitiId：流程文件id
     * @return
     */
    boolean updateActivitiRecordService(Integer acitivitiId, Date lastModifiedDate);

}
