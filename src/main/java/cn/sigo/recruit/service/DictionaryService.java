/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: DictionaryService.java
 * Author:   zhixiang.meng
 * Date:     2018年6月27日 下午3:04:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.Dictionary;

/**
 * 数据字典表操作<br> 
 * 获取相关数据
 *  1、系统重启的时候，重新数据库中加载数据到缓存中
 *  2、每次上来先判断是否存在：存在直接取出;不存在从数据库中查询，并存入缓存
 * @author zhixiang.meng
 */
public interface DictionaryService {
    
    /**
     * 
     * 保存数据字典数据 <br>
     *
     * @param dictionary：数据字典
     * @return true:保存成功；false：保存失败
     */
    boolean saveDictionary(Dictionary dictionary);
    
    /**
     * 
     * 保存数据字典数据 <br>
     *
     * @param dictionary：数据字典
     * @param isRedisSave:是否保存redis，true：保存，false：不保存
     * @return true:保存成功；false：保存失败
     */
    boolean saveDictionary(Dictionary dictionary, boolean isRedisSave);
    
    /**
     * 
     * 查询需要缓存的类型值，返回List类型<br>
     *
     * @return
     */
    List<String> queryDictionaryType();
    
    /**
     * 
     * 删除系统初始化参数<br>
     *
     * @param dictionaryTypes
     */
    void removeRedis(List<String> dictionaryTypes);
    
    /**
     * 
     * 根据缓存类型查询具体的数据字典值 <br>
     *
     * @param type：数据字典类型
     * @return
     */
    List<Dictionary> queryDictionaryByType(String type);
    
    /**
     * 
     * 将数据字典里面的所有数据按类型把保存到缓存里面 <br>
     *
     * @param dictionaryTypes
     */
    void addAllDictionaryInRedis(List<String> dictionaryTypes);
}
