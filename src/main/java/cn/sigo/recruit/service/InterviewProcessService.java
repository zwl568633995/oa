/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InterviewProcessService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月25日 下午2:15:00
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.Set;

import org.activiti.engine.task.Task;

import cn.sigo.recruit.model.RecruitInterviewManagement;

/**
 * activiti流程
 *
 * @author zhixiang.meng
 */
public interface InterviewProcessService {

    /**
     * 面试流程启动
     *
     * @param firstInterviewer  初试面试官
     * @param firstTime         初试时间
     * @param secondInterviewer 复试面试官
     * @param secondTime        复试时间
     * @param applyJobName      申请职位
     * @param applyName         申请人
     * @param applyDept         申请部门
     * @param resumeSource      职位评分
     * @param procId            流程id
     * @param applyUserId       这个流程启动人
     * @param interviewCode     面试单号
     * @param offerAuditer      offer审核人
     * @param interviewerId     面试单id
     * @param firsFeedback      初试反馈
     * @param secondFeedback    复试反馈
     * @param offerSalary       offer 薪资
     * @param offerOpinion      offer审核意见
     * @param feedbackSalary    面试期望薪资
     * @return
     */
    String startInterviewProcess(String firstInterviewer, String firstTime, String secondInterviewer,
                                 String secondTime, String applyJobName, String applyName,
                                 String applyDept, String resumeSource, String procId,
                                 String applyUserId, String interviewCode, String offerAuditer,
                                 String interviewerId, String firsFeedback, String secondFeedback,
                                 String offerSalary, String offerOpinion, String feedbackSalary);

    /**
     * 面试流程启动
     *
     * @param recruit：面试流程单
     * @return
     */
    String startInterviewProcess(RecruitInterviewManagement recruit);

    /**
     * 拒绝面试
     *
     * @param userId：用户id
     * @param procId：启动流程id
     * @param isComeInterview：当前任务的状态
     * @return
     */
    boolean isCommonInterview(String userId, String procId, String isComeInterview);

    /**
     * 复试面试
     *
     * @param userId：用户id
     * @param procId：启动流程id
     * @param isComeInterview：当前任务的状态
     * @return
     */
    boolean isSecondCommonInterview(String userId, String procId, String isSecondComeInterview,String operation);

    /**
     * 根据启动流程实例的id，查询任务
     *
     * @param procId：启动流程实例id
     * @return
     */
    Task queryTask(String procId);

    /**
     * 根据启动流程实例的id，查询任务
     *
     * @param procId：启动流程实例id
     * @return
     */
    Task queryTask(String procId, String userId);

    /**
     * 完成应聘登记表的填写任务
     *
     * @param userId：用户id
     * @param procId：启动流程id
     * @param applicationCode：应聘登记code
     * @return
     */
    boolean fillRegistration(String userId, String procId, String applicationCode);

    /**
     * 面试流程
     *
     * @param userId：用户id
     * @param procId：流程启动id
     * @param isInterviewPassed:面试结果
     * @return
     */
    boolean procInterviewReview(String userId, String procId, String isInterviewPassed,String operation);

    /**
     * 复试面试流程
     *
     * @param userId：用户id
     * @param procId：流程启动id
     * @param isSencondInterviewPassed:面试结果
     * @return
     */
    boolean procSecondInterviewReview(String userId, String procId, String isSencondInterviewPassed);

    /**
     * offer审核流程
     *
     * @param procId：流程启动id
     * @param userId：用户id
     * @param isOfferPassed:面试结果
     * @return
     */
    boolean offerAudit(String procId, String userId, String isOfferPassed,String offerOpinion,String offerSalary);

    /**
     * 是否发送offer
     *
     * @param procId
     * @param isSendOffer
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    boolean sendOffer(String procId, String isSendOffer, String userId,String interviewCode);

    /**
     * activiti流程
     *
     * @param procId
     * @param entryCode
     * @param userId
     * @return
     */
    boolean employeeEntry(String procId, String entryCode, String userId);

    /**
     * 设置
     *
     * @param interviewers
     * @return
     */
    boolean setInterviewers(Set<String> interviewers, String procId);
}
