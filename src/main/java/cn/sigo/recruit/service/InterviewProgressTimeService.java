package cn.sigo.recruit.service;

import cn.sigo.recruit.model.RecruitInterviewProgress;

/**
 * @Auther: DELL
 * @Date: 2018/9/27 11:45
 * @Description:
 */
public interface InterviewProgressTimeService {

    /**
     *
     * 更新面试进度 时间
     * @param
     * @return
     */
    Boolean updateInterviewProgress(RecruitInterviewProgress recruitInterviewProgress);

    /**
     *
     * 面试进度 保存
     * @param
     * @return
     */
    Boolean insertInterviewProgress(RecruitInterviewProgress recruitInterviewProgress);

    /**
     * 获取面试单 进度
     *
     */
    RecruitInterviewProgress queryInterviewProgress(String interviewCode);
}
