/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobApplicationProcessService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月13日 上午10:58:54
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.task.Task;

import cn.sigo.recruit.model.req.ReqAuditJobInfo;

/**
 * 职位申请流程 业务
 *
 * @author zhixiang.meng
 */
public interface JobApplicationProcessService {

    /**
     * 申请职位审核，启动
     *
     * @param applicationDepartment 申请部门
     * @param jobName               职位
     * @param jobScore              职位评分
     * @param applicationReasons    申请原因
     * @param applyPeople           申请人
     * @param jobCode               职位id
     * @param numberOfPeople        招聘人数
     * @param procId                流程id
     * @param auditLeader           审核人
     * @param applyUserId           谁启动的
     * @return 返回流程启动的id
     */
    String startJobActiviti(String applicationDepartment, String jobName, Integer jobScore,
                            String applicationReasons, String applyPeople, String jobCode,
                            String numberOfPeople, String procId, String auditLeader,
                            String applyUserId, String auditOpinion);

    /**
     * 根据启动流程实例的id 和 任务处理人，查询任务
     *
     * @param procId：启动流程实例id
     * @param candidateName：任务处理人
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    Task queryTask(String procId, String candidateName);

    /**
     * 查询任务提交的蚕食
     *
     * @param taskId：任务id
     * @return
     */
    TaskFormData queryTaskFormData(String taskId);

    /**
     * 任务节点审核
     *
     * @param auditJobInfo
     */
    boolean auditTask(ReqAuditJobInfo auditJobInfo);

    /**
     * 返回修改
     *
     * @param auditJobInfo
     * @return
     */
    boolean modifyJobInfo(ReqAuditJobInfo auditJobInfo);

}
