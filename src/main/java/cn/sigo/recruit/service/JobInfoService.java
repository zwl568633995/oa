/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobInfoService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月5日 下午5:52:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqJobInfoModel;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;

/**
 * 
 *
 * @author zhixiang.meng
 */
public interface JobInfoService {
    
    /**
     * 查询最近添加职位的jobcode
     *
     * @return
     */
    String queryMaxJobCode();
    
    /**
     * 
     * 根据要求查询职位列表信息
     *
     * @param reqModel：查询条件
     * @return
     */
    PageInfo<RecruitJobInfo> queryJobInfoList(ReqJobInfoModel reqModel);
    
    /**
     * 
     * 根据职位名和部门名查询职位基本信息
     *
     * @param deptName
     * @param jobName
     * @return
     */
    RecruitJobInfo queryJobInfoByDeptNameAndJobName(String deptName, String jobName);
    
    /**
     * 
     * 根据部门和职位名判断 非职位最终状态是否存在
     *
     * @param deptName：部门名
     * @param jobName：职位名
     * @return
     */
    boolean isExist(String deptName, String jobName);
    
    /**
     * 
     * 根据部门和职位名判断 非职位最终状态是否存在
     *
     * @param deptName：部门名
     * @param jobName：职位名
     * @return
     */
    boolean isExist(String deptName, String jobName, Long jobId);
    
    /**
     * 保存新增的职位信息
     *
     * @param jobInfo
     * @return long:返回新增的主键id
     */
    long saveJobInfo(RecruitJobInfo jobInfo);
    
    /**
     * 
     * 修改职位
     *
     * @param jobInfo
     * @return
     */
    boolean modifyJobInfo(RecruitJobInfo jobInfo);
    
    /**
     * 
     * 根据职位id 更新职位状态
     *
     * @param jobStatus：职位状态
     * @param jobId：职位id
     * @return
     */
    boolean modifyJobInfoByJobId(String jobStatus, Long jobId);
    
    /**
     * 
     * 根据职位id查询职位
     *
     * @param jobId
     * @return
     */
    RecruitJobInfo queryByPrimaryKey(long jobId);
    
    /**
     * 
     * 根据职位编码查询
     *
     * @param jobCode
     * @return
     */
    RecruitJobInfo queryByJobCode(String jobCode);
    
    /**
     * 更新职位信息，将activiti流程实例启动的id 更新到对应的job表中
     * 
     * @param procId
     * @param jobId
     * @return
     */
    boolean updateProcIdForJob(String procId, Long jobId);

    /**
     * 模糊查询获取应聘职位
     *
     * @param job
     * @return
     */
    List<String> jobPosition(String job);
    /**
     * 根据部门查询 所有 招聘中 的职位
     *
     * @param department(可为null)
     * @return
     */
    List<Map<String,Object>> queryJobPositionByDepartment(String department);
    
    /**
     * 
     * 编辑职位信息
     *
     * @param jobInfo
     * @param jobCode
     * @return
     */
    ResponseModel editJobInfo(RecruitJobInfo jobInfo, String jobCode);

}
