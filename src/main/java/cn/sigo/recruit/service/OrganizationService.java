package cn.sigo.recruit.service;

import cn.sigo.recruit.model.OrganizationStructure;
import cn.sigo.recruit.model.req.ReqOrganization;
import cn.sigo.recruit.model.response.ResponseTeamTree;

import java.util.List;
import java.util.Map;

/**
 * @Auther: DELL
 * @Date: 2018/9/10 10:22
 * @Description:
 */
public interface OrganizationService {

    /**
     *
     * 部门列表-（根据部门名称模糊查询 组织架构）
     * @param department 非必传参数
     * @return
     */
    List<OrganizationStructure> queryDeptList(String department);

    /**
     *
     * 根据父级部门ID-查询下级部门
     * @param
     * @param
     * @return
     */
    List<OrganizationStructure> querySubDeptList(ReqOrganization reqOrganization);

    /**
     *
     * 添加下级部门
     * @param
     * @param
     * @return
     */
    Boolean addSubDept(OrganizationStructure organizationStructure);

    Integer countByTeamName(Map<String,Object> map);

    /**
     *
     * 编辑部门
     * @param
     * @param
     * @return
     */
    Boolean updateDepartment(OrganizationStructure organizationStructure);

    /**
     *
     * 进入 组织架构 页面，初始化页面 的两个数据（组织结构树，部门列表）
     * @param
     * @return
     */
    Map<String,Object> initOrganizationPage();


    /**
     *
     * 根据 输入的 部门负责人 模糊查询
     * @param
     * @param
     * @return
     */
    List<Map<String,Object>> queryDeptLeaderInfo(String leaderName);

    /**
     *
     * 当前部门下的在职人数
     * @param
     * @param
     * @return
     */
    Integer countDepartmentPeople(String deptName);

    /**
     *
     * 获取 二三级 部门树
     * @param
     * @param
     * @param
     * @return
     */
    List<ResponseTeamTree> queryDeptTree();

    /**
     * 查询所有部门
     */
    List<OrganizationStructure> selectAll();
}
