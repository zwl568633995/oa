/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationCertificateService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:19:01
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationCertificate;

/**
 * 应聘者 证书信息
 * @author zhixiang.meng
 */
public interface RecruitApplicationCertificateService {

    /**
     * 
     * 保存应聘者证书信息
     *
     * @param certificate
     * @return
     */
    boolean saveRecruitApplicationCertificate(RecruitApplicationCertificate certificate);
    
    /**
     * 
     * 批量保存
     *
     * @param certificates
     * @return
     */
    boolean saveRecruitApplicationCertificates(List<RecruitApplicationCertificate> certificates, Integer applicationId, String adder);
    
    /**
     * 
     * 根据应聘id查询证书记录
     *
     * @param applicationId
     */
    List<RecruitApplicationCertificate> queryCertificateByApplicationRegistId(Integer applicationId);
}
