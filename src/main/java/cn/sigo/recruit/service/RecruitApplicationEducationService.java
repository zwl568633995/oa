/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationEducationService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:34:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationEducation;

/**
 * 保存教育经历
 *
 * @author zhixiang.meng
 */
public interface RecruitApplicationEducationService {
    /**
     * 
     * 保存教育经历
     *
     * @param education
     * @return
     */
    boolean saveRecruitApplicationEducation(RecruitApplicationEducation education);
    
    /**
     * 批量保存教育经历
     *
     * @param educations
     * @param applicationId
     * @return
     */
    boolean saveRecruitApplicationEducations(List<RecruitApplicationEducation> educations, Integer applicationId, String adder);
    
    /**
     * 
     * 根据应聘id查询教育经历
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationEducation> queryEducationByApplicationId(Integer applicationId);

}
