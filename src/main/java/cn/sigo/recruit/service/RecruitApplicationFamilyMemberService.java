/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationFamilyMemberService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:41:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationFamilyMember;

/**
 * 保存家庭成员
 * 
 * @author zhixiang.meng
 */
public interface RecruitApplicationFamilyMemberService {

    /**
     * 
     * 保存家庭成员
     *
     * @param familyMenber
     * @return
     */
    boolean saveRecruitApplicationFamilyMember(RecruitApplicationFamilyMember familyMenber);

    /**
     * 批量保存
     * 
     * @param familyMenbers
     * @param applicationId
     * @return
     */
    boolean saveRecruitApplicationFamilyMembers(List<RecruitApplicationFamilyMember> familyMenbers, Integer applicationId,
            String adder);
    
    /**
     * 根据应聘id查询家庭成员
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationFamilyMember> queryFamilyMemberByApplicationId(Integer applicationId);
}
