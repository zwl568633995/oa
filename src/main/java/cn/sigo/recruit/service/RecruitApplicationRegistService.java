/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationRegistService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月17日 上午10:42:00
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.model.RecruitApplicationRegist;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqApplicationRegist;
import cn.sigo.recruit.model.req.ReqRecruitApplicationRegist;

/**
 * 应聘人员基本信息
 *
 * @author zhixiang.meng
 */
public interface RecruitApplicationRegistService {
    
    /**
     * 
     * 查询系统当前最大的应聘编号
     *
     * @return
     */
    String queryMaxCode();
    
    /**
     * 
     * 保存应聘人员基本信息
     *
     * @param reqRegist
     * @param userId
     * @return 返回应聘人员id
     */
    ResponseModel saveRecruitApplicationRegist(ReqRecruitApplicationRegist reqRegist, String userId);

    /**
     * 
     * 保存应聘人员基本信息
     *
     * @param regist
     * @return 返回应聘人员id
     */
    RecruitApplicationRegist saveRecruitApplicationRegist(RecruitApplicationRegist regist);
    
    /**
     * 
     * 根据面试单号查询应聘登记表记录
     *
     * @param interviewCode
     * @return
     */
    RecruitApplicationRegist queryApplicationRegistByInterviewCode(String interviewCode);

    /**
     *
     * 根据应聘登记表号查询应聘登记表记录
     *
     * @param applicationCode
     * @return
     */
    RecruitApplicationRegist queryApplicationRegistByApplicationCode(String applicationCode);


    /**
     * 根据身份证号码查询应聘登记表记录
     * @param idCardNo
     * @return
     */
    RecruitApplicationRegist queryInfoByIdCardNo(String idCardNo);

    /**
     *
     * 应聘登记表 分页查询列表
     * @param
     * @return
     */
    PageInfo<RecruitApplicationRegist> queryApplicationRegistrationList(ReqApplicationRegist reqApplicationRegist, String userId, String backendRole);
}
