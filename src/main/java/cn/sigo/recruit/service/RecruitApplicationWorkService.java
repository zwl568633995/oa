/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationWorkService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:47:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.RecruitApplicationWork;

/**
 * 工作经历
 *
 * @author zhixiang.meng
 */
public interface RecruitApplicationWorkService {

    /**
     * 保存工作经历
     * @param work
     * @return
     */
    boolean saveRecruitApplicationWork(RecruitApplicationWork work);
    
    /**
     * 批量保存工作经历
     * @param work
     * @param applicationId
     * @return
     */
    boolean saveRecruitApplicationWorks(List<RecruitApplicationWork> works, Integer applicationId, String adder);
    
    /**
     * 
     * 根据应聘者id查询工作经历
     *
     * @param applicationId
     * @return
     */
    List<RecruitApplicationWork> queryWorkByApplicationId(Integer applicationId);
}
