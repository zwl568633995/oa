/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitBaseStatisticsService.java
 * Author:   zhixiang.meng
 * Date:     2018年8月13日 下午4:00:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitBaseStatistics;
import cn.sigo.recruit.model.RecruitInterviewManagement;

/**
 * 基础统计
 * @author zhixiang.meng
 */
public interface RecruitBaseStatisticsService {
    
    /**
     * 
     * 保存或者更新候选人数
     *
     * @param jobCode
     * @return
     */
    boolean saveOrUpdateBaseStatistics(String jobCode);
    
    /**
     * 
     * 查询基本信息统计
     *
     * @param param
     * @return 
     */
    RecruitBaseStatistics queryBaseStatistics(Map<String, Object> param);
    
    /**
     * 
     * 保存基础统计信息
     *
     * @param baseStatistics
     * @return
     */
    Integer saveBaseStatistics(RecruitBaseStatistics baseStatistics);
    
    /**
     * 
     * 更新统计数量
     *
     * @param param: key:field 需要更新的字段
     * @param id：id
     * @return
     */
    boolean updateBaseStatistics(Map<String, Object> param);
    
    /**
     * 
     * 面试过程中更新
     *
     * @param interviewCode
     * @param status
     * @return
     */
    boolean updateBaseStatistics(String interviewCode, String status);

    /**
     *
     * 招聘统计 数据概览
     *
     * @param yearMonth
     * @return
     */
    RecruitBaseStatistics dataOverview(String yearMonth);

    /**
     *
     * 招聘统计 部门招聘详情
     *
     * @param yearMonth
     * @return
     */
    List<RecruitBaseStatistics> departmentRecruitInfo(String yearMonth);

    /**
     *
     * 招聘统计 职位招聘详情
     *
     * @param yearMonth
     * @return
     */
    List<RecruitBaseStatistics> positionRecruitInfo(String yearMonth,String department);

}
