package cn.sigo.recruit.service;

import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegister;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegisterPageQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface RecruitEntryRegisterService {
    RecruitEntryRegisterBaseInfo saveRecruitEntryRegisterBaseInfo(RecruitEntryRegisterBaseInfo recruitEntryRegisterBaseInfo);

    Boolean saveRecruitEntryHealthy(RecruitEntryHealthy recruitEntryHealthy,Long applicationId,
                                    String adder);

    Boolean saveRecruitIncumbencyCertificates(List<RecruitIncumbencyCertificate> recruitIncumbencyCertificates,Long applicationId,
                                             String adder);

    Boolean saveRecruitIncumbencyEducations(List<RecruitIncumbencyEducation> recruitIncumbencyEducations,Long applicationId,
                                           String adder);

    Boolean saveRecruitIncumbencyFamilyMembers(List<RecruitIncumbencyFamilyMember> recruitIncumbencyFamilyMembers,Long applicationId,
                                              String adder);

    Boolean saveRecruitIncumbencyWorks(List<RecruitIncumbencyWork> recruitIncumbencyWorks,Long applicationId,
                                      String adder);

    ReqRecruitEntryRegister queryRecruitEntryRegister(String entryCode);

    PageInfo<ReqRecruitEntryRegisterPageQuery> pageQueryRecruitEntryRegister (ReqRecruitEntryRegisterPageQuery reqRecruitEntryRegisterPageQuery, String userId, String backendRole);

    Map<String,Object> saveRecruitEntryRegister(ReqRecruitEntryRegister reqRecruitEntry, String adder);


    //判断 入职信息基本表中是否已经存在 该面试单号 或  应聘单号
    Boolean isExistInEntryRegisterBase(Map<String,Object> map);
}
