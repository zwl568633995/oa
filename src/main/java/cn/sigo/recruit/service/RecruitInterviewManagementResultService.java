/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InterviewManagementResultService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月16日 下午4:42:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import cn.sigo.recruit.model.RecruitInterviewManagementResult;
import cn.sigo.recruit.model.ResponseModel;

/**
 * 面试结果
 *
 * @author zhixiang.meng
 */
public interface RecruitInterviewManagementResultService {
    
    /**
     * 
     * 根据面试id 查询面试结果
     *
     * @param managementId
     * @return
     */
    RecruitInterviewManagementResult queryResultForManagementId(Integer managementId);
    
    /**
     * 
     * 保存面试结果
     *
     * @param interviewResult
     * @return
     */
    boolean saveInterviewManagementResult(RecruitInterviewManagementResult interviewResult);
    
    /**
     * 
     * 修改面试结果
     *
     * @param interviewResult
     * @return
     */
    boolean modifyInterviewManagementResult(RecruitInterviewManagementResult interviewResult);
    
    /**
     * 
     * 面试
     *
     * @param interviewCode
     * @param interviewResult
     * @return
     * @throws Exception 用户没有权限操作当前任务，需要执行数据库的事务
     */
    ResponseModel interviewReview(String interviewCode, RecruitInterviewManagementResult interviewResult);
    
    /**
     * 
     * offer 审核
     *
     * @param interviewCode：面试单id
     * @param salary：薪资
     * @param offerOpinion：offer建议
     * @param userId：审核人id
     * @param isInterviewPassed:审核结果
     * @return
     */
    ResponseModel offerAudit(String interviewCode, String salary, String offerOpinion, String userId, String isInterviewPassed);
    
    /**
     * 是否同意offer
     *
     * @param interviewCode
     * @param isSendOffer
     * @return
     */
    ResponseModel isSendOffer(String interviewCode, String isSendOffer, String userId);
    
    /**
     * 
     * 入职接口
     *
     * @param interviewCode
     * @param userId
     * @param entryCode
     * @return
     */
    boolean employeeEntry(String interviewCode, String userId, String entryCode);
    
    /**
     * 
     * 设置工作流里面任务的处理人
     *
     * @param interviewCode
     */
    void setInterviewer(String interviewCode);
    
    /**
     * 
     * 初试面试
     *
     * @param interviewCode
     * @param interviewResult
     * @return
     * @throws Exception 用户没有权限操作当前任务，需要执行数据库的事务
     */
    ResponseModel interviewFirstReview(String interviewCode, RecruitInterviewManagementResult interviewResult);
    
    /**
     * 
     * 复试
     *
     * @param interviewCode
     * @param interviewResult
     * @return
     * @throws Exception 用户没有权限操作当前任务，需要执行数据库的事务
     */
    ResponseModel interviewSecondReview(String interviewCode, RecruitInterviewManagementResult interviewResult);
    
    /**
     * 
     * 不来复试
     *
     * @param interviewCode：面试单号
     */
    ResponseModel refusingSencondInterview(String interviewCode, String userId);
}
