/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitInterviewManagementService.java
 * Author:   zhixiang.meng
 * Date:     2018年7月12日 下午2:05:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;

import cn.sigo.recruit.model.req.ReqModifyInterviewTime;
import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewPeople;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqInterviewManagementModel;

/**
 * 面试管理
 *
 * @author zhixiang.meng
 */
public interface RecruitInterviewManagementService {
    
    /**
     * 查询最近面试code
     *
     * @return
     */
    String queryMaxCode();
    
    /**
     * 
     * 根据手机号码 查询面试单
     *
     * @param phone
     * @param id：可空，传入表示除这个面试单号以外
     * @return
     */
    RecruitInterviewManagement queryByPhone(String phone, String code);

    /**
     * 根据手机号码查询面试单
     * @param phone
     * @return
     */
    RecruitInterviewManagement queryInterviewManagentByPhone(String phone);
    
    /**
     * 
     * 保存面试
     *
     * @param recruitInterview
     * @return
     */
    RecruitInterviewManagement saveRecruitInterview(RecruitInterviewManagement recruitInterview);
    
    /**
     * 
     * 修改面试单
     *
     * @param recruitInterview
     * @return
     */
    boolean modifyRecruitInterview(RecruitInterviewManagement recruitInterview);
    
    /**
     * 查询面试管理里面所有带面试的
     * 
     *
     * @return
     */
    List<RecruitInterviewPeople> queryInterviews();
    
    /**
     * 根据面试单号，查询面试信息
     *
     * @param interviewCode
     * @return
     */
    RecruitInterviewManagement queryInterviewManagentByCode(String interviewCode);
    
    /**
     * 
     * 查询面试管理
     *
     * @param reqModel
     * @param userId:登录用户的id，如：mengzhixiang
     * @param backendRole：角色
     * @return
     */
    PageInfo<RecruitInterviewManagement> queryInterviewManagents(ReqInterviewManagementModel reqModel, String userId, String backendRole);
    
    /**
     * 
     * 根据面试单号，修改面试单状态
     *
     * @param interviewCode
     * @param status
     * @return
     */
    boolean modifyInterviewStatus(String interviewCode, String status);
    
    /**
     * 
     * 根据面试单号，修改面试单状态
     *
     * @param interviewCode:单号
     * @param status：状态
     * @param secondInterviewer：复试人
     * @return
     */
    boolean modifyInterviewStatus(String interviewCode, String status, String secondInterviewer);
    
    /**
     * 
     * 将流程id 更新到数据库里面
     *
     * @param procId
     * @param interviewId
     * @return
     */
    boolean modifyInterviewProcId(String procId, Integer interviewId);
    
    /**
     * 
     * 保存面试单
     *
     * @param recruit
     * @return
     */
    ResponseModel saveInterviewManagement(RecruitInterviewManagement recruit, String interviewerCode);
    
    /**
     * 
     * 修改offer审核时间
     *
     * @param interviewerCode：面试单
     * @return
     */
    boolean modifyOfferAuitTime(String interviewerCode);

    /**
     * 设置复试时间（面试单状态为：  待安排复试）
     *
     */
    Boolean modifySecondInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime);

    /**
     * 修改 初试时间/复试时间
     *
     */
    Boolean updateInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime);
}
