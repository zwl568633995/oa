/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: StatisticalTransformationService.java
 * Author:   zhixiang.meng
 * Date:     2018年8月14日 上午9:24:31
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service;

import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.RecruitStatisticalTransformation;
import cn.sigo.recruit.model.ResStatisticalTransformation;

/**
 * 统计转化
 *
 * @author zhixiang.meng
 */
public interface RecruitStatisticalTransformationService {
    
    /**
     * 
     * 保存或者更新候选人数
     *
     * @param jobCode
     * @return
     */
    boolean saveOrUpdateStatisticalTransformation(String jobCode);
    
    /**
     * 
     * 查询统计信息转化
     *
     * @param param
     * @return 
     */
    RecruitStatisticalTransformation queryStatisticalTransformation(Map<String, Object> param);
    
    /**
     * 
     * 保存统计信息转化
     *
     * @param baseStatistics
     * @return
     */
    boolean saveBaseStatisticalTransformation(RecruitStatisticalTransformation statisticalTransformation);
    
    /**
     * 
     * 更新具体状态的数量
     *
     * @param param
     * @return
     */
    boolean updateBaseStatisticalTransformation(Map<String, Object> param);
    
    /**
     * 
     * 根据面试单跟新
     *
     * @param interviewCode
     * @param status
     * @return
     */
    boolean updateBaseStatisticalTransformation(String interviewCode, String status);

    /**
     *
     * 招聘统计 部门招聘转换分析
     * @param
     * @param
     * @param
     * @return
     */
    List<ResStatisticalTransformation> departmentRecruitTransformation(String yearMonth);

    /**
     *
     * 招聘统计 职位招聘转换分析
     * @param
     * @param
     * @param
     * @return
     */
    List<ResStatisticalTransformation> positionRecruitTransformation(String yearMonth,String department);
}
