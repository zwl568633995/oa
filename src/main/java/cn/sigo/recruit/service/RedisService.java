package cn.sigo.recruit.service;

import java.util.List;
import java.util.Set;

/**
 * redis 缓存 对象级别的操作
 * 
 * 目前已经完成 单个对象 和 列表(set get push range) 保持 删除 和 判断是否存在
 * @author mengzx
 * @version 1.0
 *
 */
public interface RedisService {

    /**
     * 写入缓存
     * @param key
     * @param value
     * @return
     */
    boolean set(final String key, Object value);

    /**
     * 写入缓存设置时效时间
     * @param key
     * @param value
     * @return
     */
    boolean set(final String key, Object value, Long expireTime);

    /**
     * 批量删除对应的value
     * @param keys
     */
    void remove(final String... keys);

    /**
     * 批量删除key
     * @param pattern
     */
    void removePattern(final String pattern);

    /**
     * 删除对应的value
     * @param key
     */
    void remove(final String key);

    /**
     * 判断缓存中是否有对应的value
     * @param key
     * @return
     */
    boolean exists(final String key);

    /**
     * 读取缓存
     * @param key
     * @return
     */
    Object get(final String key, Class clazz);

    /**
     * 哈希 添加
     * @param key
     * @param hashKey
     * @param value
     */
    void hmSet(String key, Object hashKey, Object value);

    /**
     * 哈希获取数据
     * @param key
     * @param hashKey
     * @return
     */
    Object hmGet(String key, Object hashKey);

    /**
     * 列表添加
     * @param k
     * @param v
     */
    void push(String k,List<Object> objs);

    /**
     * 列表获取
     * @param k
     * @param l
     * @param l1
     * @return
     */
    List<Object> range(String k, long l, long l1, Class clazz);

    /**
     * 集合添加(仅支持基本类型 和 string类型)
     * @param key
     * @param value
     */
    void add(String key,Object value);

    /**
     * 集合获取(仅支持基本类型 和 string类型)
     * @param key
     * @return
     */
    Set<Object> setMembers(String key);

    /**
     * 有序集合添加(仅支持基本类型 和 string类型)
     * @param key
     * @param value
     * @param scoure
     */
    void zAdd(String key,Object value,double scoure);

    /**
     * 有序集合获取(仅支持基本类型 和 string类型)
     * @param key
     * @param scoure
     * @param scoure1
     * @return
     */
    Set<Object> rangeByScore(String key,double scoure,double scoure1);
}
