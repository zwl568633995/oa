package cn.sigo.recruit.service;

import cn.sigo.recruit.model.ExcelRosterList;
import cn.sigo.recruit.model.RecruitIncumbencyHistory;
import cn.sigo.recruit.model.RecruitIncumbencyUserBaseInfo;
import cn.sigo.recruit.model.ResponseRosterPageQuery;
import cn.sigo.recruit.model.req.ReqRosterEmployee;
import cn.sigo.recruit.model.req.ReqRosterPageQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 09:28
 * @Description: 花名册
 */
public interface RosterService {
    PageInfo<ResponseRosterPageQuery> pageQueryRosterList(ReqRosterPageQuery reqRosterPageQuery, String userName, String backendRole);

    Integer saveIncumbencyHistory(RecruitIncumbencyHistory recruitIncumbencyHistory);

    Map<String,Object> saveRosterEmployee(ReqRosterEmployee reqRosterEmployee, String adder);

    Map<String,Object> queryEmployeeInfo(String incumbencyId);

    List<ExcelRosterList> exportRosterList(ReqRosterPageQuery reqRosterPageQuery);

    Map<String,Object> updateRosterEmployee(ReqRosterEmployee reqRosterEmployee, String adder);

   RecruitIncumbencyUserBaseInfo selectRosterByReallyName(String reallyName);

}
