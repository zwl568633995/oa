package cn.sigo.recruit.service;

import cn.sigo.recruit.model.User;

import java.util.List;

public interface UserService {
    int addUser(User user);

    List<User> findAllUser(int pageNum, int pageSize);
    
    /**
     * 
     * 根据用户名 密码查询用户 <br>
     *
     * @param userName：用户名
     * @param userPwd：密码
     * @return
     */
    User queryUserForInfo(String userName, String userPwd);
}
