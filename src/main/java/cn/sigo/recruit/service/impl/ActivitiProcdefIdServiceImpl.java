/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiProcdefIdServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月10日 上午11:47:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.ActivitiProcdefIdMapper;
import cn.sigo.recruit.model.ActivitiProcdefId;
import cn.sigo.recruit.service.ActivitiProcdefIdService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service("activitiProcdefIdService")
public class ActivitiProcdefIdServiceImpl implements ActivitiProcdefIdService {

    @Autowired
    private ActivitiProcdefIdMapper activitiProcdefIdServiceMapper;

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.ActivitiProcdefIdService#savenActivitiProcdefId(cn.sigo.recruit.model.ActivitiProcdefId)
     */
    @Override
    public boolean savenActivitiProcdefId(ActivitiProcdefId procedef) {
        procedef.setAddTime(DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS));
        procedef.setAdder("admin");
        return activitiProcdefIdServiceMapper.insert(procedef) > 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiProcdefIdService#updateActivitiProcdefId(java.lang.Integer,
     * java.lang.String)
     */
    @Override
    public boolean updateActivitiProcdefId(String procdefKey, String procdefId) {
        
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiProcdefIdService#isExist(java.lang.String)
     */
    @Override
    public boolean isExist(String procdeKey) {
        return activitiProcdefIdServiceMapper.selectByProcdeKey(procdeKey) == null;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiProcdefIdService#isNotExist(java.lang.String)
     */
    @Override
    public boolean isNotExist(String procdeKey) {
        return !isExist(procdeKey);
    }

}
