/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiRecordServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月9日 下午1:52:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.ActivitiRecordMapper;
import cn.sigo.recruit.model.ActivitiRecord;
import cn.sigo.recruit.service.ActivitiRecordService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 流程文件实现
 *
 * @author zhixiang.meng
 */
@Service("activitiRecordService")
public class ActivitiRecordServiceImpl implements ActivitiRecordService {
    
    @Autowired
    private ActivitiRecordMapper activitiRecordMapper;

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiRecordService#queryByForActivitiName(java.lang.String)
     */
    @Override
    public ActivitiRecord queryByForActivitiName(String activitiName) {
        return activitiRecordMapper.queryByForActivitiName(activitiName);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiRecordService#saveActivitiRecordService(cn.sigo.recruit.model.ActivitiRecord)
     */
    @Override
    public boolean saveActivitiRecordService(ActivitiRecord record, boolean isQuery) {
        ActivitiRecord queryRecord = null;
        if(isQuery) {
            queryRecord = this.queryByForActivitiName(record.getActivitiName());
        }
        if(null == queryRecord) {
            // 设置发布时间
            Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
            record.setAddTime(date);
            record.setAdder("admin");
            record.setIsDelete(0);
            int result = activitiRecordMapper.insert(record);
            return result > 0;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.ActivitiRecordService#updateActivitiRecordService(java.lang.Integer)
     */
    @Override
    public boolean updateActivitiRecordService(Integer acitivitiId, Date lastModifiedDate) {
        ActivitiRecord record = new ActivitiRecord();
        record.setUdpateTime(lastModifiedDate);
        record.setModer("admin");
        record.setId(acitivitiId);
        return activitiRecordMapper.updateByPrimaryKeySelective(record) > 0;
    }

}
