/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: DictionaryServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年6月27日 下午3:10:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.DictionaryMapper;
import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.service.DictionaryService;
import cn.sigo.recruit.service.RedisService;

/**
 * 系统初始化相关配置信息<br> 
 *
 * @author zhixiang.meng
 */
@Service("dictionaryService")
public class DictionaryServiceImpl implements DictionaryService {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(DictionaryService.class);
    @Autowired
    private DictionaryMapper dictionaryMapper;
    @Autowired
    private RedisService redisService;
    

    /* (non-Javadoc)
     * @see com.demo.service.DictionaryService#queryType()
     */
    @Override
    public List<String> queryDictionaryType() {
        return dictionaryMapper.queryDictionaryType();
    }


    /* (non-Javadoc)
     * @see com.demo.service.DictionaryService#removeRedis(java.util.List)
     */
    @Override
    public void removeRedis(List<String> dictionaryTypes) {
        if(!dictionaryTypes.isEmpty()) {
            for(String dictionaryType : dictionaryTypes) {
                redisService.remove(dictionaryType);
            }
        }
    }


    /* (non-Javadoc)
     * @see com.demo.service.DictionaryService#queryDictionaryByType(java.lang.String)
     */
    @Override
    public List<Dictionary> queryDictionaryByType(String type) {
        // 字符串不为null 且redis缓存中存在对应的key值
        if(StringUtils.isNotBlank(type)) {
            // 存在 从redis缓存中取值
            List<Dictionary> dictionary = (List<Dictionary>)(List)redisService.range(type, 0, -1, Dictionary.class);
            if(null == dictionary || dictionary.isEmpty()) {
                // 判断为null 从 db里面取
                dictionary = dictionaryMapper.queryDictionaryByType(type);
                if(null != dictionary && !dictionary.isEmpty()) {
                    redisService.push(type, (List<Object>)(List)dictionary);
                }
            }
            return dictionary;
        }
        return null;
    }


    /* (non-Javadoc)
     * @see com.demo.service.DictionaryService#addAllDictionaryInRedis(java.util.List)
     */
    @Override
    public void addAllDictionaryInRedis(List<String> dictionaryTypes) {
        if(!dictionaryTypes.isEmpty()) {
            for(String dictionaryType : dictionaryTypes) {
                queryDictionaryByType(dictionaryType);
            }
        }
    }


    /* (non-Javadoc)
     * @see com.demo.service.DictionaryService#saveDictionary(com.demo.model.Dictionary)
     */
    @Override
    public boolean saveDictionary(Dictionary dictionary) {
        return saveDictionary(dictionary, false);
    }


    /* (non-Javadoc)
     * @see com.recruit.service.DictionaryService#saveDictionary(com.recruit.model.Dictionary, boolean)
     */
    @Override
    public boolean saveDictionary(Dictionary dictionary, boolean isRedisSave) {
        if(null != dictionary) {
            int result = dictionaryMapper.insert(dictionary);
            // 保存到redis 缓存中
            if(isRedisSave) {
                // 判断redis 中是否已经存在
                if(redisService.exists(dictionary.getType())) {
                    List<Dictionary> dicRedises = new ArrayList<Dictionary>();
                    // 首先当单个对象来取
                    //Object o = redisService.get(dictionary.getType(), Dictionary.class);

                    Dictionary dicRedis = (Dictionary)redisService.get(dictionary.getType(), Dictionary.class);
                    if(null == dicRedis) {
                        // 说明redis里面存储的是list对象
                        dicRedises =  (List<Dictionary>)(List)redisService.range(dictionary.getType(), 0, -1, dictionary.getClass());
                    } else {
                        dicRedises.add(dicRedis);
                    }
                    dicRedises.add(dictionary);
                    redisService.push(dictionary.getType(), (List<Object>)(List)dicRedises);
                } else {
                    // 缓存中没有直接加入
                    redisService.set(dictionary.getType(), dictionary);
                }
            }
            return result > 0;
        }
        return false;
    }

}
