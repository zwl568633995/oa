/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InterviewProcessServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月25日 下午2:22:17
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.mapper.RecruitInterviewManagementResultMapper;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewManagementResult;
import cn.sigo.recruit.service.InterviewProcessService;
import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service("interviewProcessService")
public class InterviewProcessServiceImpl implements InterviewProcessService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterviewProcessService.class);

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private FormService formService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RecruitInterviewManagementResultMapper interviewManagementResultMapper;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#startInterviewProcess(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String startInterviewProcess(String firstInterviewer, String firstTime, String secondInterviewer,
                                        String secondTime, String applyJobName, String applyName,
                                        String applyDept, String resumeSource, String procId,
                                        String applyUserId, String interviewCode, String offerAuditer,
                                        String interviewerId, String firsFeedback, String secondFeedback,
                                        String offerSalary, String offerOpinion, String feedbackSalary) {
        if (StringUtils.isNotBlank(procId) && StringUtils.isNotBlank(applyUserId)
            && StringUtils.isNotBlank(interviewCode) && StringUtils.isNotBlank(firstInterviewer)) {
            // 启动流程
            ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(procId).singleResult();
            Map<String, String> param = new HashMap<String, String>();
            param.put("firstInterviewer", firstInterviewer);
            param.put("firstTime", firstTime);
            param.put("secondInterviewer", secondInterviewer);
            param.put("secondTime", secondTime);
            param.put("applyJobName", applyJobName);
            param.put("applyName", applyName);
            param.put("applyDept", applyDept);
            param.put("resumeSource", resumeSource);
            param.put("procId", procId);
            param.put("applyUserId", applyUserId);
            param.put("interviewCode", interviewCode);
            param.put("offerAuditer", offerAuditer);
            param.put("interviewerId", interviewerId);
            param.put("firsFeedback", firsFeedback);
            param.put("secondFeedback", secondFeedback);
            param.put("offerSalary", offerSalary);
            param.put("offerOpinion", offerOpinion);
            param.put("feedbackSalary", feedbackSalary);
            ProcessInstance pi = formService.submitStartFormData(processDefinition.getId(), param);
            LOGGER.info("职位申请：" + interviewCode + " , 已提交，过程实例id： " + pi.getId());
            return pi.getId();
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#queryTask(java.lang.String)
     */
    @Override
    public Task queryTask(String procId) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procId).list();
        return tasks.get(0);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#queryTask(java.lang.String)
     */
    @Override
    public Task queryTask(String procId, String userId) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procId).taskCandidateOrAssigned(userId)
            .list();
        if (!tasks.isEmpty()) {
            return tasks.get(0);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#startInterviewProcess(cn.sigo.recruit.model.
     * RecruitInterviewManagement)
     */
    @Override
    public String startInterviewProcess(RecruitInterviewManagement recruit) {
        String firstInterviewAuditer = "";
        String secondInterviewAuditer = "";
        String offerAuditer = "";
        if (StringUtils.isNotBlank(recruit.getOfferAudit())) {
            offerAuditer = recruit.getOfferAudit().split(":")[0];
        }
        // 没有复试
        if (StringUtils.isNotBlank(recruit.getFirstInterviewer())
            && StringUtils.isBlank(recruit.getSecondInterviewer())) {
            if (recruit.getFirstInterviewer().indexOf(";") > 0) {
                String[] interviewes = recruit.getFirstInterviewer().split(";");
                firstInterviewAuditer = interviewes[0].split(":")[0];
                if (interviewes.length > 1) {
                    secondInterviewAuditer = interviewes[1].split(":")[0];
                }
            } else {
                firstInterviewAuditer = recruit.getFirstInterviewer().split(":")[0];
            }
        }
        // 有复试
        if (StringUtils.isNotBlank(recruit.getSecondInterviewer())) {
            if (recruit.getSecondInterviewer().indexOf(";") > 0) {
                String[] interviewes = recruit.getSecondInterviewer().split(";");
                firstInterviewAuditer = interviewes[0].split(":")[0];
                if (interviewes.length > 1) {
                    secondInterviewAuditer = interviewes[1].split(":")[0];
                }
            } else {
                firstInterviewAuditer = recruit.getSecondInterviewer().split(":")[0];
            }
        }

        //添加 初始化 流程 参数
        Integer interviewerId = recruit.getId();
        String firstFeedback = "无";
        String secondFeedback = "无";
        String offerSalary = "无";
        String offerOpinion = "无";
        String feedbackSalary = "无";


        RecruitInterviewManagementResult interviewManagementResult = interviewManagementResultMapper.queryResultForManagementId(interviewerId);
        if (null != interviewManagementResult) {
            //初试反馈
            firstFeedback = interviewManagementResult.getFirstRemarks();
            //复试反馈
            secondFeedback = interviewManagementResult.getSecondRemarks();
            //offer 薪资
            offerSalary = interviewManagementResult.getOfferSalary();
            //offer 审核意见
            offerOpinion = interviewManagementResult.getOfferOpinion();
            //面试期望薪资 --> 拟定薪资
            feedbackSalary = interviewManagementResult.getFeedbackSalary();
        }
        //初试面试官 复试面试官 格式问题: wuyicheng : 武宜城
        return this.startInterviewProcess(
            recruit.getFirstInterviewer(), recruit.getFirstTime(), recruit.getSecondInterviewer(),
            recruit.getSecondTime(), recruit.getApplyJobName(), recruit.getApplyName(),
            recruit.getApplyDept(), recruit.getResumeSource(), recruit.getActivitiProcdId(),
            recruit.getAdder(), recruit.getInterviewCode(), offerAuditer,
            recruit.getId().toString(), firstFeedback, secondFeedback,
            offerSalary, offerOpinion, feedbackSalary);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#refusingInterview(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isCommonInterview(String userId, String procId, String isComeInterview) {
        Task task = this.queryTask(procId);
        if (null != task) {
            // 绑定任务
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("isComeInterview", isComeInterview);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#refusingInterview(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isSecondCommonInterview(String userId, String procId, String isSecondComeInterview,String operation) {
        Task task = this.queryTask(procId);
        if (null != task) {
            // 绑定任务
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("isSecondComeInterview", isSecondComeInterview);
            param.put("operation", operation);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#fillRegistration(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean fillRegistration(String userId, String procId, String applicationCode) {
        Task task = this.queryTask(procId);
        if (null != task) {
            // 绑定任务
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("applicationCode", applicationCode);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#procInterviewReview(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean procInterviewReview(String userId, String procId, String isInterviewPassed,String operation) {
        Task task = this.queryTask(procId);
        if (null != task) {
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("isInterviewPassed", isInterviewPassed);
            param.put("operation", operation);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#procSecondInterviewReview(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public boolean procSecondInterviewReview(String userId, String procId, String isSencondInterviewPassed) {
        Task task = this.queryTask(procId);
        if (null != task) {
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("isSencondInterviewPassed", isSencondInterviewPassed);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#offerAudit(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean offerAudit(String procId, String userId, String isOfferPassed,String offerOpinion,String offerSalary) {
        Task task = this.queryTask(procId, userId);
        if (null != task) {
            Map<String, String> param = new HashMap<String, String>();
            param.put("isOfferPassed", isOfferPassed);
            param.put("offerOpinion", offerOpinion);
            param.put("offerSalary", offerSalary);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#sendOffer(java.lang.String, java.lang.String)
     */
    @Override
    public boolean sendOffer(String procId, String isSendOffer, String userId,String interviewCode) {
        Task task = this.queryTask(procId);
        if (null != task) {
            // 绑定任务
            // taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("isSendOffer", isSendOffer);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#employeeEntry(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean employeeEntry(String procId, String entryCode, String userId) {
        Task task = this.queryTask(procId);
        if (null != task) {
            // 绑定任务
            taskService.claim(task.getId(), userId);
            Map<String, String> param = new HashMap<String, String>();
            param.put("entryCode", entryCode);
            formService.submitTaskFormData(task.getId(), param);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewProcessService#setInterviewers(java.util.Set)
     */
    @Override
    public boolean setInterviewers(Set<String> interviewers, String procId) {
        Task task = this.queryTask(procId);
        if (null != task) {
            for (String str : interviewers) {
                taskService.addCandidateUser(task.getId(), str);
            }
        }
        return true;
    }

}
