package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.mapper.RecruitInterviewProgressMapper;
import cn.sigo.recruit.model.RecruitInterviewProgress;
import cn.sigo.recruit.service.InterviewProgressTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @Auther: DELL
 * @Date: 2018/9/27 11:45
 * @Description:
 */
@Service("interviewProgressTimeService")
public class InterviewProgressTimeServiceImpl implements InterviewProgressTimeService {
    @Autowired
    private RecruitInterviewProgressMapper recruitInterviewProgressMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateInterviewProgress(RecruitInterviewProgress recruitInterviewProgress) {
        Boolean result = null;
        try{
            result = recruitInterviewProgressMapper.updateByInterviewCode(recruitInterviewProgress)>0;
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            return false;
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertInterviewProgress(RecruitInterviewProgress recruitInterviewProgress) {
        Boolean result = null;
        try{
            result = recruitInterviewProgressMapper.insert(recruitInterviewProgress)>0;
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            return false;
        }
        return result;
    }

    @Override
    public RecruitInterviewProgress queryInterviewProgress(String interviewCode) {
        return recruitInterviewProgressMapper.queryInterviewProgress(interviewCode);
    }
}
