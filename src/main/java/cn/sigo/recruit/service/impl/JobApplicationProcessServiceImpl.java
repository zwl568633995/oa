/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobApplicationProcessServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月16日 上午9:48:44
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.model.req.ReqAuditJobInfo;
import cn.sigo.recruit.service.JobApplicationProcessService;

/**
 * 实现类
 *
 * @author zhixiang.meng
 */
@Service("jobApplicationProcessService")
public class JobApplicationProcessServiceImpl implements JobApplicationProcessService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobApplicationProcessService.class);

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private FormService formService;
    @Autowired
    private TaskService taskService;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#startJobActiviti(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String startJobActiviti(String applicationDepartment, String jobName, Integer jobScore,
                                   String applicationReasons, String applyPeople, String jobCode,
                                   String numberOfPeople, String procId, String auditLeader,
                                   String applyUserId,String auditOpinion) {
        if (StringUtils.isNotBlank(jobCode) && StringUtils.isNotBlank(numberOfPeople) && StringUtils.isNotBlank(procId)
            && StringUtils.isNotBlank(auditLeader) && StringUtils.isNotBlank(applyUserId)) {
            // 启动流程
            ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(procId).singleResult();
            Map<String, String> param = new HashMap<String, String>();
            param.put("applicationDepartment", applicationDepartment);
            param.put("jobName", jobName);
            param.put("jobScore", jobScore.toString());
            param.put("applicationReasons", applicationReasons);
            param.put("applyPeople", applyPeople);
            param.put("jobCode", jobCode);
            param.put("auditOpinion",auditOpinion);
            param.put("numberOfPeople", numberOfPeople);
            param.put("procId", procId);
            param.put("auditLeader", auditLeader);
            param.put("applyUserId", applyUserId);
            ProcessInstance pi = formService.submitStartFormData(processDefinition.getId(), param);
            LOGGER.info("职位申请：" + jobCode + " , 已提交，过程实例id： " + pi.getId());
            return pi.getId();
        }
        return "";
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#queryTask(java.lang.String)
     */
    @Override
    public Task queryTask(String procId, String candidateName) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(procId)
            .taskCandidateOrAssigned(candidateName).list();
        return tasks.get(0);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#queryTaskFormData(java.lang.String)
     */
    @Override
    public TaskFormData queryTaskFormData(String taskId) {
        if (StringUtils.isNotBlank(taskId))
            return formService.getTaskFormData(taskId);
        return null;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobApplicationProcessService#auditTask(cn.sigo.recruit.model.req.ReqAuditJobInfo)
     */
    @Override
    public boolean auditTask(ReqAuditJobInfo auditJobInfo) {
        // 查询task
        Task task = this.queryTask(auditJobInfo.getProcId(), auditJobInfo.getCandidateName());
        if (null != task) {
            Map<String, String> properties = new HashMap<String, String>();
            properties.put("auditOpinion", auditJobInfo.getAuditOpinion());
            properties.put("auditResult", auditJobInfo.getAuditResult());
            if (StringUtils.isNotBlank(auditJobInfo.getIsCommit())) {
                properties.put("isCommit", auditJobInfo.getIsCommit());
            }
            formService.submitTaskFormData(task.getId(), properties);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.JobApplicationProcessService#modifyJobInfo(cn.sigo.recruit.model.req.ReqAuditJobInfo)
     */
    @Override
    public boolean modifyJobInfo(ReqAuditJobInfo auditJobInfo) {
        // 查询task
        Task task = this.queryTask(auditJobInfo.getProcId(), auditJobInfo.getCandidateName());
        if (null != task) {
            Map<String, String> properties = new HashMap<String, String>();
            properties.put("jobCode", auditJobInfo.getJobCode());
            properties.put("numberOfPeople", auditJobInfo.getNumberOfPeople());
            if (StringUtils.isNotBlank(auditJobInfo.getIsCommit())) {
                properties.put("isCommit", auditJobInfo.getIsCommit());
            }
            formService.submitTaskFormData(task.getId(), properties);
            return true;
        }
        return false;
    }
}
