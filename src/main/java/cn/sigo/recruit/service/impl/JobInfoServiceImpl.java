/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobInfoServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月5日 下午6:06:57
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.InitializationBean;
import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.mapper.RecruitJobInfoMapper;
import cn.sigo.recruit.model.Notification;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqAuditJobInfo;
import cn.sigo.recruit.model.req.ReqJobInfoModel;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.service.JobApplicationProcessService;
import cn.sigo.recruit.service.JobInfoService;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.GenerateCode;
import cn.sigo.recruit.util.LogHelper;
import cn.sigo.recruit.util.PinyinUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * 职位信息处理业务层
 *
 * @author zhixiang.meng
 */
@Service("jobInfoService")
public class JobInfoServiceImpl implements JobInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobInfoService.class);

    @Autowired
    private RecruitJobInfoMapper recruitJobInfoMapper;
    @Autowired
    private JobApplicationProcessService jobApplicationProcessService;
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private LogHelper logHelper;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#queryMaxJobCode()
     */
    @Override
    public String queryMaxJobCode() {
        return recruitJobInfoMapper.queryMaxJobCode();
    }


    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#queryJobInfoList(cn.sigo.recruit.model.req.ReqJobInfoModel)
     */
    @Override
    public PageInfo<RecruitJobInfo> queryJobInfoList(ReqJobInfoModel reqModel) {
        String orderBy = null;
        // 支持按职位评分、期望到岗日期排序
        if (StringUtils.isNotBlank(reqModel.getField()) && StringUtils.isNotBlank(reqModel.getSort())) {
            orderBy = reqModel.getField() + " " + reqModel.getSort();
        }
        // 保持默认值
        if (StringUtils.isBlank(reqModel.getField()) || StringUtils.isBlank(reqModel.getSort())) {
            orderBy = "apply_time DESC";
        }
        PageHelper.startPage(reqModel.getPageNum(), reqModel.getPageSize(), orderBy);
        List<RecruitJobInfo> jobInfo = recruitJobInfoMapper.queryJobInfoList(reqModel);
        return new PageInfo<RecruitJobInfo>(jobInfo);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#queryByJobCode(long)
     */
    @Override
    public RecruitJobInfo queryByJobCode(String jobCode) {
        if (StringUtils.isNotBlank(jobCode)) {
            return recruitJobInfoMapper.queryByJobCode(jobCode);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#saveJobInfo(cn.sigo.recruit.service.RecruitJobInfo)
     */
    @Override
    public long saveJobInfo(RecruitJobInfo jobInfo) {
        synchronized (this) {
            String maxJobCode = queryMaxJobCode();
            String nextJobCode = GenerateCode.queryNextMaxCode(maxJobCode, "ZP");
            jobInfo.setJobCode(nextJobCode);
            jobInfo.setEntryNumber(0);
            // 设置添加时间
            Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
            jobInfo.setApplyTime(date);
            jobInfo.setAddTime(date);
            int result = recruitJobInfoMapper.insertSelective(jobInfo);
            if (result > 0) {
                return jobInfo.getId();
            }
            return result;
        }
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#modifyJobInfo(cn.sigo.recruit.model.RecruitJobInfo)
     */
    @Override
    public boolean modifyJobInfo(RecruitJobInfo jobInfo) {
        // 设置修改时间
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        jobInfo.setModTime(date);
        if (jobInfo.getJobStatus().equals("招聘中") || jobInfo.getJobStatus().equals("拒绝招聘申请") || jobInfo.getJobStatus().equals("返回修改")) {
            jobInfo.setAuditTime(date);
        }
        return recruitJobInfoMapper.updateByPrimaryKeySelective(jobInfo) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#queryByPrimaryKey(long)
     */
    @Override
    public RecruitJobInfo queryByPrimaryKey(long jobId) {
        return recruitJobInfoMapper.selectByPrimaryKey(jobId);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#modifyJobInfoByJobId(java.lang.String, java.lang.Long)
     */
    @Override
    public boolean modifyJobInfoByJobId(String jobStatus, Long jobId) {
        if (StringUtils.isNotBlank(jobStatus) && null != jobId) {
            RecruitJobInfo jobInfo = new RecruitJobInfo();
            jobInfo.setId(jobId);
            jobInfo.setJobStatus(jobStatus);
            int result = recruitJobInfoMapper.updateByPrimaryKeySelective(jobInfo);
            RecruitJobInfo recruitJobInfo = recruitJobInfoMapper.selectByPrimaryKey(jobId);
            sendEmail(jobStatus, recruitJobInfo);
            return result > 0;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#queryJobInfoByDeptNameAndJobName(java.lang.String, java.lang.String)
     */
    @Override
    public RecruitJobInfo queryJobInfoByDeptNameAndJobName(String deptName, String jobName) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("applicationDepartment", deptName);
        param.put("jobName", jobName);
        return recruitJobInfoMapper.queryJobInfoByDeptNameAndJobName(param);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#isExist(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isExist(String deptName, String jobName) {
        return isExist(deptName, jobName, null);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#isExist(java.lang.String, java.lang.String, java.lang.Integer)
     */
    @Override
    public boolean isExist(String deptName, String jobName, Long jobId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("applicationDepartment", deptName);
        param.put("jobName", jobName);
        if (null != jobId) {
            param.put("jobId", jobId);
        }
        int count = recruitJobInfoMapper.countForDeptNameAndJobName(param);
        return count == 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#updateProcIdForJob(java.lang.String, java.lang.Long)
     */
    @Override
    public boolean updateProcIdForJob(String procId, Long jobId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("procId", procId);
        param.put("jobId", jobId);
        int result = recruitJobInfoMapper.updateProcIdForJob(param);
        return result > 0;
    }

    @Override
    public List<String> jobPosition(String job) {
        return recruitJobInfoMapper.jobPosition(job);
    }

    @Override
    public List<Map<String, Object>> queryJobPositionByDepartment(String department) {
        return recruitJobInfoMapper.queryJobPositionByDepartment(department);
    }


    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.JobInfoService#editJobInfo(cn.sigo.recruit.model.RecruitJobInfo, java.lang.String)
     */
    @Override
    @Transactional
    public ResponseModel editJobInfo(RecruitJobInfo jobInfo, String jobCode) {
        ResponseModel model = new ResponseModel();
        Long dbJobId = null;
        // 设置职位状态
        String jobStatus = jobInfo.getJobStatus();
        if (StringUtils.isNotBlank(jobStatus) && jobStatus.equals("保存")) {
            jobStatus = "已创建";
        } else if (StringUtils.isNotBlank(jobStatus) && jobStatus.equals("提交")) {
            jobStatus = "待审核";
        } else if (StringUtils.isNotBlank(jobStatus) && jobStatus.equals("作废")) {
            jobStatus = "已作废";
        }
        jobInfo.setJobStatus(jobStatus);
        String activitiId = (String) InitializationBean.activitiMap.get("添加招聘申请");
        jobInfo.setActivitiProcdId(activitiId);
        if (StringUtils.isBlank(jobCode) || (StringUtils.isNotBlank(jobCode) && jobCode.equals("null"))) {
            if (StringUtils.isNotBlank(jobInfo.getApplicationDepartment())
                && StringUtils.isNotBlank(jobInfo.getJobName())) {
                boolean count = this.isExist(jobInfo.getApplicationDepartment(), jobInfo.getJobName());
                if (count) {
                    dbJobId = this.saveJobInfo(jobInfo);
                    if (dbJobId > 0) {
                        model.setResultCode("0000");
                        model.setResultMsg("");
                    } else {
                        model.setResultCode("0002");
                        model.setResultMsg("异常！数据入库失败");
                        return model;
                    }
                } else {
                    model.setResultCode("0201");
                    model.setResultMsg("职位名重复，职位已存在");
                    return model;
                }
            }
        } else {
            // 查询职位
            RecruitJobInfo job = this.queryByJobCode(jobCode);
            dbJobId = job.getId();
            jobInfo.setId(dbJobId);
            jobInfo.setAdder(job.getAdder());
            // 获取erp的userId
            String moder = (String) jobInfo.getModer();
            if (!moder.equals(job.getAdder())) {
                model.setResultCode("0208");
                model.setResultMsg("您没有权限修改该职位！");
                return model;
            }
            jobInfo.setModer(moder);
            boolean count = this.isExist(jobInfo.getApplicationDepartment(), jobInfo.getJobName(),
                jobInfo.getId());
            if (count) {
                boolean result = this.modifyJobInfo(jobInfo);
                if (result) {
                    model.setResultCode("0000");
                    model.setResultMsg("");
                } else {
                    model.setResultCode("0002");
                    model.setResultMsg("异常！数据入库失败");
                    return model;
                }
            } else {
                model.setResultCode("0201");
                model.setResultMsg("职位名重复，职位已存在");
                return model;
            }
        }
        // activiti流程
        if (jobInfo.getJobStatus().equals("待审核") || jobInfo.getJobStatus().equals("作废")) {
            // 判断用户是否是重新提交审核，流程实例id 为空的时候，说明 是新提交审核
            if (StringUtils.isNotBlank(jobInfo.getProcId())) {
                ReqAuditJobInfo auditJobInfo = new ReqAuditJobInfo();
                auditJobInfo.setJobCode(jobInfo.getJobCode());
                // 返回修改，任务的拥护者是提交这个申请的人
                auditJobInfo.setCandidateName(jobInfo.getAdder());
                auditJobInfo.setNumberOfPeople(Integer.toString(jobInfo.getNumberOfPeople()));
                auditJobInfo.setProcId(jobInfo.getProcId());
                // 修改职位信息重新提交流程
                if (jobInfo.getJobStatus().equals("作废")) {
                    auditJobInfo.setIsCommit("false");
                } else {
                    auditJobInfo.setIsCommit("true");
                }
                // 提交任务给工作流走
                jobApplicationProcessService.modifyJobInfo(auditJobInfo);
            } else {

                if (StringUtils.isBlank(jobCode) || "null".equals(jobCode)) {
                    jobCode = jobInfo.getJobCode();
                }
                // 启动activiti流程
                String procId = jobApplicationProcessService.startJobActiviti(
                    jobInfo.getApplicationDepartment(), jobInfo.getJobName(), jobInfo.getJobScore(),
                    jobInfo.getApplicationReasons(), jobInfo.getApplyPeople(), jobCode,
                    Integer.toString(jobInfo.getNumberOfPeople()), jobInfo.getActivitiProcdId(), jobInfo.getAuditId(),
                    jobInfo.getAdder(), jobInfo.getAuditOpinion());

                // 流程启动实例id保存到jobinfo里面
                boolean flag = this.updateProcIdForJob(procId, dbJobId);
                if (flag) {
                    LOGGER.info("职位id：" + dbJobId + "，提交审核的流程实例id为：" + procId);
                    model.setResultCode("0000");
                    model.setResultMsg("");
                } else {
                    LOGGER.error("职位id：" + dbJobId + "，提交审核的流程实例id为：" + procId + "。保存数据库异常。。。。");
                    model.setResultCode("0202");
                    model.setResultMsg("启动流程实例id更新到数据库异常");
                }
            }
        }
        return model;
    }

    public void sendEmail(String jobStatus, RecruitJobInfo recruitJobInfo) {
        String jobName = recruitJobInfo.getJobName();
        String jobCode = recruitJobInfo.getJobCode();
        String applicationDepartment = recruitJobInfo.getApplicationDepartment();
        Integer numberOfPeople = recruitJobInfo.getNumberOfPeople();
        Integer entryNumber = recruitJobInfo.getEntryNumber();
        String moder = recruitJobInfo.getApplyPeople();
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();
        String status = "";
        if (jobStatus.equals("招聘暂停")) {
            status = "暂停";
        }
        if (jobStatus.equals("招聘关闭")) {
            status = "关闭";
        }
        if (jobStatus.equals("招聘中")) {
            return;
        }
        Notification notification = notificationMapper.selectByNode(status + "招聘申请单");
        String url = notification.getUrl() + jobCode;
        Object[] params = {applicationDepartment, jobName, numberOfPeople, entryNumber, moder, url};
        UMSRole umsRole = new UMSRole();
        umsRole.setRoleName("recruit-人事员工");
        UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
        List<Map<String, String>> hrs = (List<Map<String, String>>) umsNewResponseModel.getData();
        //添加 收件人
        for (Map<String, String> umsUser : hrs) {
            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(PinyinUtil.getPingYin(umsUser.get("userName")) + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(umsUser.get("reallyName"));
            receiveAccountList.add(receiveAccount);
        }
        String content = notification.getContent();
        logHelper.LoggerSendINFO("开始拼接");
        //邮件内容
        String format = MessageFormat.format(content, params);

        //调用消息发送服务需要的参数
        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setMsgContent(format);
        sendNotification.setReceiveAccountList(receiveAccountList);

        try {
            SendNotificationResult adResult = sendNotificationFeign.messageNotify(sendNotification);
            if (null != adResult && adResult.getCode().equals("success")) {
                System.out.println("消息发送成功...");
                    logHelper.LoggerSendINFO("消息发送成功...");

            }

        } catch (Exception e) {
            System.out.println("消息发送失败...");
                logHelper.LoggerSendERROR("send notification error"+e.toString());

        }
    }

}
