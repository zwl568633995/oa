package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.config.UrlUmsConfig;
import cn.sigo.recruit.feign.ADFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.DepartmentLeaderMapper;
import cn.sigo.recruit.mapper.OrganizationStructureMapper;
import cn.sigo.recruit.model.DepartmentLeader;
import cn.sigo.recruit.model.OrganizationStructure;
import cn.sigo.recruit.model.req.ReqOrganization;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.service.OrganizationService;
import cn.sigo.recruit.util.HttpClientUtils;
import cn.sigo.recruit.util.LogHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import cn.sigo.recruit.feign.UMSFeign;

/**
 * @Auther: DELL
 * @Date: 2018/9/10 10:23
 * @Description:
 */
@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {
    @Autowired
    private OrganizationStructureMapper organizationStructureMapper;
    @Autowired
    private DepartmentLeaderMapper departmentLeaderMapper;
    @Autowired
    private UrlUmsConfig urlUmsConfig;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private ADFeign adFeign;

    @Override
    public List<OrganizationStructure> queryDeptList(String department) {
        return organizationStructureMapper.queryDeptList(department);
    }

    @Override
    public List<OrganizationStructure> querySubDeptList(ReqOrganization reqOrganization) {
        return organizationStructureMapper.querySubDeptList(reqOrganization);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addSubDept(OrganizationStructure organizationStructure) {
        try {
            //插入组织结构表
            organizationStructureMapper.insert(organizationStructure);
            //插入组织结构表后 返回的主键Id
            Long deptId = organizationStructure.getId();

            //判断部门负责人是否为空
            if (!CollectionUtils.isEmpty(organizationStructure.getDeptLeaderIds())) {
                //批量插入 部门 - 负责人 中间表
                batchInsertDeptLeader(organizationStructure, deptId);
            }

            //添加下级部门成功,调用UMS接口,UMS添加部门,转换数据格式
            ResponseToUmsSaveSubTeam responseToUmsSaveSubTeam = transOrganizationStructureToUMSSubTeam(organizationStructure);
            String umsSaveSubTeam = HttpClientUtils.getUmsSaveSubTeam(responseToUmsSaveSubTeam, urlUmsConfig.getAddSubTeamFromHr());
            if (!umsSaveSubTeam.contains("添加-下级团队-成功")) {
                logHelper.LoggerSendERROR("UMS添加下级团队失败,请手动添加!");
                return false;
            }

            //添加下级部门成功,调用AD接口,AD添加部门,转换数据格式
            RespADAddDept respADAddDept = new RespADAddDept();
            if (StringUtils.isNotBlank(organizationStructure.getDeptName())) {
                //设置本级部门
                respADAddDept.setName(organizationStructure.getDeptName());

                //设置所属层级
//                if (StringUtils.isNotBlank(organizationStructure.getSuperiorDeptName())) {
//                    respADAddDept.setPath(organizationStructure.getSuperiorDeptName());
//                } else {
//                    //上级部门名称为空,通过上级部门id查找上级部门名称
//                    OrganizationStructure superOrganizationStructure = organizationStructureMapper.selectByPrimaryKey(organizationStructure.getSuperiorDeptId());
//                    if (null != superOrganizationStructure) {
//                        respADAddDept.setPath(superOrganizationStructure.getDeptName() + "," + organizationStructure.getDeptName());
//                    }
//                }
                //根据上级部门id判断
                String path = "";
                while (true) {

                    if (0 == organizationStructure.getSuperiorDeptId()) {
                        //上级部门id为 0 的时候,层级结束
                        break;
                    }
                    //根据上级部门id 获取 部门名称
                    organizationStructure = organizationStructureMapper.selectByPrimaryKey(organizationStructure.getSuperiorDeptId());
                    //上级部门名称
                    path = ",ou=" + organizationStructure.getDeptName();
                }
                if (path.length() > 0) {
                    path = path.substring(1);
                }

                respADAddDept.setPath(path);

                logHelper.LoggerSendINFO("调用AD添加部门接口");
                ADResult deptAddResult = adFeign.deptAdd(respADAddDept);
                //判断AD修改用户接口返回信息
                if (null != deptAddResult && deptAddResult.getCode().equals("success")) {
                    logHelper.LoggerSendINFO("调用AD添加部门接口成功");
                } else {
                    logHelper.LoggerSendERROR("调用AD添加部门接口失败!!");
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                    return false;
                }
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            logHelper.LoggerSendERROR("添加部门失败" + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 把数据转换成UMS添加子团队需要的格式
     *
     * @param organizationStructure
     * @return
     */
    public ResponseToUmsSaveSubTeam transOrganizationStructureToUMSSubTeam(OrganizationStructure organizationStructure) {
        ResponseToUmsSaveSubTeam responseToUmsSaveSubTeam = new ResponseToUmsSaveSubTeam();
        //添加人
        responseToUmsSaveSubTeam.setAdder(organizationStructure.getAdder());
        //添加时间
        responseToUmsSaveSubTeam.setAddTime(organizationStructure.getAddTime());
        //部门名称
        responseToUmsSaveSubTeam.setDeptName(organizationStructure.getDeptName());

        //部门id
        responseToUmsSaveSubTeam.setId(organizationStructure.getId());
        //是否是组织架构
        responseToUmsSaveSubTeam.setIsOrganizationalStructure(organizationStructure.getIsOrganizationalStructure());
        //修改人
        responseToUmsSaveSubTeam.setModer(organizationStructure.getModer());
        //修改时间
        responseToUmsSaveSubTeam.setModTime(organizationStructure.getModTime());
        //上级部门id
        responseToUmsSaveSubTeam.setSuperiorDeptId(organizationStructure.getSuperiorDeptId());
        //部门状态
        responseToUmsSaveSubTeam.setTeamStatus(organizationStructure.getTeamStatus());
        //部门类型
        responseToUmsSaveSubTeam.setTeamType(organizationStructure.getTeamType());

        return responseToUmsSaveSubTeam;
    }

    @Override
    public Integer countByTeamName(Map<String, Object> map) {
        return organizationStructureMapper.countByTeamName(map);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateDepartment(OrganizationStructure organizationStructure) {

        if (null == organizationStructure.getId()) {
            return false;
        }

        String deptName = organizationStructureMapper.selectByPrimaryKey(organizationStructure.getId()).getDeptName();
        try {
            //更新 组织结构表
            organizationStructureMapper.updateByPrimaryKeySelective(organizationStructure);

            //关联修改UMS部门信息
            RespUmsEditTeam respUmsEditTeam = transOrganizationStructureToUMSEditTeam(organizationStructure, deptName);

            if (null != respUmsEditTeam && null == respUmsEditTeam.getId()) {
                logHelper.LoggerSendERROR("UMS中没有和HR同名称的部门!!");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                return false;
            } else {
                //关联修改UMS部门
                logHelper.LoggerSendINFO("开始调用UMS修改部门接口");
                UMSResponseModel umsEditSubTeamRespModel = umsFeign.editSubTeam(respUmsEditTeam);
                //调用接口成功返回正确信息
                if (null != umsEditSubTeamRespModel && "1".equals(umsEditSubTeamRespModel.getResultCode())) {
                    //接口返回正确信息
                    logHelper.LoggerSendINFO("UMS修改部门接口调用成功,并返回正确信息");
                } else {
                    //接口调用成功返回错误信息
                    logHelper.LoggerSendERROR("UMS修改部门接口调用成功,但返回错误信息");
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                    return false;
                }
            }

            //部门 id
            Long deptId = organizationStructure.getId();
            //不停用
            if (null != organizationStructure.getDeleted() && !organizationStructure.getDeleted()) {
                if (!CollectionUtils.isEmpty(organizationStructure.getDeptLeaderIds())) {
                    //假删除（deleted更新为true） 中间表
                    departmentLeaderMapper.deleteByDeptId(deptId);
                    //批量插入 部门 - 负责人 中间表
                    batchInsertDeptLeader(organizationStructure, deptId);
                }
            }

            //停用 （且 当前部门下的在职人数=0）
//            if(null!=organizationStructure.getDeleted()&&organizationStructure.getDeleted()){
//
//            }
            logHelper.LoggerSendINFO("修改部门成功!");
            return true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            logHelper.LoggerSendERROR("修改部门失败:" + e.toString());
            return false;
        }
    }

    private RespUmsEditTeam transOrganizationStructureToUMSEditTeam(OrganizationStructure organizationStructure, String deptName) {
        RespUmsEditTeam respUmsEditTeam = new RespUmsEditTeam();

        //团队id
        if (null != organizationStructure.getId()) {
            //调用UMS 通过部门名称获取 部门信息的接口
            RespUMSSelectTeamByDeptName respSelectTeamByDeptName = new RespUMSSelectTeamByDeptName();
            respSelectTeamByDeptName.setDeptName(deptName);
            UMSTeam umsTeam = umsFeign.selectTeamByDeptName(respSelectTeamByDeptName);
            if (null != umsTeam) {
                //UMS 中 有 HR 相同名称的部门
                respUmsEditTeam.setId(umsTeam.getId());
            } else {
                //没有名称相同的部门,返回 null
                return null;
            }
        }

        //是否删除
        respUmsEditTeam.setDeleted(organizationStructure.getDeleted());

        //部门负责人
        if (null != organizationStructure.getDeptOverList() && organizationStructure.getDeptOverList().size() > 0) {

            respUmsEditTeam.setDeptOver(organizationStructure.getDeptOverList().get(0).getDeptOverName());
        }
        //部门名称
        if (StringUtils.isNotBlank(organizationStructure.getDeptName())) {
            respUmsEditTeam.setDeptName(organizationStructure.getDeptName());
        }
        //是否是组织架构
        if (null != organizationStructure.getIsOrganizationalStructure()) {
            respUmsEditTeam.setIsOrganizationalStructure(organizationStructure.getIsOrganizationalStructure());
        }
        if (StringUtils.isNotBlank(organizationStructure.getModer())) {
            //修改人
            respUmsEditTeam.setModer(organizationStructure.getModer());
        }
        //添加人
        if (StringUtils.isNotBlank(organizationStructure.getAdder())) {
            respUmsEditTeam.setAdder(organizationStructure.getAdder());
        }
        //修改时间
        if (null != organizationStructure.getModTime()) {
            respUmsEditTeam.setModTime(organizationStructure.getModTime());
        }
        //上级部门id
        if (null != organizationStructure.getSuperiorDeptId()) {
            respUmsEditTeam.setSuperiorDeptId(organizationStructure.getSuperiorDeptId());
        }
        //部门状态
        if (StringUtils.isNotBlank(organizationStructure.getTeamStatus())) {
            respUmsEditTeam.setTeamStatus(organizationStructure.getTeamStatus());
        }
        //部门类型
        if (StringUtils.isNotBlank(organizationStructure.getTeamType())) {
            respUmsEditTeam.setTeamType(organizationStructure.getTeamType());
        }

        return respUmsEditTeam;
    }

    /**
     * 批量插入 部门 - 负责人 中间表
     *
     * @param
     * @param
     * @return
     */
    void batchInsertDeptLeader(OrganizationStructure organizationStructure, Long deptId) {
        List<DepartmentLeader> departmentLeaderList = new ArrayList<>();
        for (Integer leaderId : organizationStructure.getDeptLeaderIds()) {
            DepartmentLeader departmentLeader = new DepartmentLeader();
            departmentLeader.setDeptId(deptId);
            departmentLeader.setDeptLeaderId(leaderId);
            departmentLeaderList.add(departmentLeader);
        }
        //插入 部门 - 负责人 中间表
        departmentLeaderMapper.batchInsertDeptLeader(departmentLeaderList);
    }

    @Override
    public Map<String, Object> initOrganizationPage() {
        Map<String, Object> result = new HashMap<>();
        //查询 初始化 组织架构树
        result.put("organizationTree", organizationStructureMapper.initOrganization());
        //查询 初始化 下级部门列表
        result.put("organizationList", organizationStructureMapper.initSubDeptList());
        return result;
    }

    @Override
    public List<Map<String, Object>> queryDeptLeaderInfo(String leaderName) {
        return organizationStructureMapper.queryDeptLeaderInfo(leaderName);
    }

    @Override
    public Integer countDepartmentPeople(String deptName) {
        return organizationStructureMapper.countDepartmentPeople(deptName);
    }

    @Override
    public List<ResponseTeamTree> queryDeptTree() {
        return organizationStructureMapper.initOrganization();
    }

    /**
     * 查询所有部门信息
     *
     * @return
     */
    @Override
    public List<OrganizationStructure> selectAll() {
        return organizationStructureMapper.selectAll();
    }
}
