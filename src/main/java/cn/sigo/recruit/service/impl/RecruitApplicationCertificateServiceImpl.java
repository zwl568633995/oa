/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationCertificateServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:23:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitApplicationCertificateMapper;
import cn.sigo.recruit.model.RecruitApplicationCertificate;
import cn.sigo.recruit.service.RecruitApplicationCertificateService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 证书现实类
 *
 * @author zhixiang.meng
 */
@Service("recruitApplicationCertificateService")
public class RecruitApplicationCertificateServiceImpl implements RecruitApplicationCertificateService {
    
    @Autowired
    private RecruitApplicationCertificateMapper recruitApplicationCertificateMapper;

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.RecruitApplicationCertificateService#saveRecruitApplicationCertificate(cn.sigo.recruit.
     * model.RecruitApplicationCertificate)
     */
    @Override
    public boolean saveRecruitApplicationCertificate(RecruitApplicationCertificate certificate) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        certificate.setAddTime(date);
        return recruitApplicationCertificateMapper.insert(certificate) > 0;
    }

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.RecruitApplicationCertificateService#saveRecruitApplicationCertificates(java.util.List,
     * java.lang.Long)
     */
    @Override
    public boolean saveRecruitApplicationCertificates(List<RecruitApplicationCertificate> certificates,
            Integer applicationId, String adder) {
        for(RecruitApplicationCertificate certificate : certificates) {
            certificate.setApplicationId(applicationId);
            certificate.setAdder(adder);
            this.saveRecruitApplicationCertificate(certificate);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationCertificateService#queryCertificateByApplicationRegistId(java.lang.Long)
     */
    @Override
    public List<RecruitApplicationCertificate> queryCertificateByApplicationRegistId(Integer applicationId) {
        return recruitApplicationCertificateMapper.queryCertificateByApplicationRegistId(applicationId);
    }

}
