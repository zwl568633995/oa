/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationEducationServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:36:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitApplicationEducationMapper;
import cn.sigo.recruit.model.RecruitApplicationEducation;
import cn.sigo.recruit.service.RecruitApplicationEducationService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 实现
 *
 * @author zhixiang.meng
 */
@Service("recruitApplicationEducationService")
public class RecruitApplicationEducationServiceImpl implements RecruitApplicationEducationService {

    @Autowired
    private RecruitApplicationEducationMapper recruitApplicationEducationMapper;

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.RecruitApplicationEducationService#saveRecruitApplicationEducation(cn.sigo.recruit.model.
     * RecruitApplicationEducation)
     */
    @Override
    public boolean saveRecruitApplicationEducation(RecruitApplicationEducation education) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        education.setAddTime(date);
        return recruitApplicationEducationMapper.insert(education) > 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationEducationService#saveRecruitApplicationEducations(java.util.List,
     * java.lang.Long)
     */
    @Override
    public boolean saveRecruitApplicationEducations(List<RecruitApplicationEducation> educations, Integer applicationId,
            String adder) {
        for (RecruitApplicationEducation education : educations) {
            education.setApplicationId(applicationId);
            education.setAdder(adder);
            this.saveRecruitApplicationEducation(education);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationEducationService#queryEducationByApplicationId(java.lang.Long)
     */
    @Override
    public List<RecruitApplicationEducation> queryEducationByApplicationId(Integer applicationId) {
        return recruitApplicationEducationMapper.queryEducationByApplicationId(applicationId);
    }

}
