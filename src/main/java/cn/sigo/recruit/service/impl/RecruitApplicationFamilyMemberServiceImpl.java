/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationFamilyMemberServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:43:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitApplicationFamilyMemberMapper;
import cn.sigo.recruit.model.RecruitApplicationFamilyMember;
import cn.sigo.recruit.service.RecruitApplicationFamilyMemberService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service("recruitApplicationFamilyMemberService")
public class RecruitApplicationFamilyMemberServiceImpl implements RecruitApplicationFamilyMemberService {
    
    @Autowired
    private RecruitApplicationFamilyMemberMapper recruitApplicationFamilyMemberMapper;

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationFamilyMemberService#saveRecruitApplicationFamilyMember(cn.sigo.recruit.model.RecruitApplicationFamilyMember)
     */
    @Override
    public boolean saveRecruitApplicationFamilyMember(RecruitApplicationFamilyMember familyMenber) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        familyMenber.setAddTime(date);
        return recruitApplicationFamilyMemberMapper.insert(familyMenber) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationFamilyMemberService#saveRecruitApplicationFamilyMembers(java.util.List, java.lang.Long)
     */
    @Override
    public boolean saveRecruitApplicationFamilyMembers(List<RecruitApplicationFamilyMember> familyMenbers,
            Integer applicationId, String adder) {
        for(RecruitApplicationFamilyMember familyMenber : familyMenbers) {
            familyMenber.setApplicationId(applicationId);
            familyMenber.setAdder(adder);
            this.saveRecruitApplicationFamilyMember(familyMenber);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationFamilyMemberService#queryFamilyMemberByApplicationId(java.lang.Long)
     */
    @Override
    public List<RecruitApplicationFamilyMember> queryFamilyMemberByApplicationId(Integer applicationId) {
        return recruitApplicationFamilyMemberMapper.queryFamilyMemberByApplicationId(applicationId);
    }

}
