/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationRegistServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月17日 上午10:45:08
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.mapper.RecruitApplicationRegistMapper;
import cn.sigo.recruit.model.RecruitApplicationCertificate;
import cn.sigo.recruit.model.RecruitApplicationEducation;
import cn.sigo.recruit.model.RecruitApplicationFamilyMember;
import cn.sigo.recruit.model.RecruitApplicationRegist;
import cn.sigo.recruit.model.RecruitApplicationWork;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitInterviewProgress;
import cn.sigo.recruit.model.ResponseModel;
import cn.sigo.recruit.model.req.ReqApplicationRegist;
import cn.sigo.recruit.model.req.ReqRecruitApplicationRegist;
import cn.sigo.recruit.service.InterviewProcessService;
import cn.sigo.recruit.service.InterviewProgressTimeService;
import cn.sigo.recruit.service.RecruitApplicationCertificateService;
import cn.sigo.recruit.service.RecruitApplicationEducationService;
import cn.sigo.recruit.service.RecruitApplicationFamilyMemberService;
import cn.sigo.recruit.service.RecruitApplicationRegistService;
import cn.sigo.recruit.service.RecruitApplicationWorkService;
import cn.sigo.recruit.service.RecruitBaseStatisticsService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.GenerateCode;

/**
 * 实现类
 *
 * @author zhixiang.meng
 */
@Service("recruitApplicationRegistService")
public class RecruitApplicationRegistServiceImpl implements RecruitApplicationRegistService {

   private final static Logger LOGGER = LoggerFactory.getLogger(RecruitApplicationRegistService.class);

   @Autowired
   private RecruitApplicationRegistMapper recruitApplicationRegistMapper;
   @Autowired
   private RecruitInterviewManagementService recruitInterviewManagement;
   @Autowired
   private RecruitApplicationCertificateService recruitApplicationCertificateService;
   @Autowired
   private RecruitApplicationEducationService recruitApplicationEducationService;
   @Autowired
   private RecruitApplicationFamilyMemberService recruitApplicationFamilyMemberService;
   @Autowired
   private RecruitApplicationWorkService recruitApplicationWorkService;
   @Autowired
   private InterviewProcessService interviewProcessService;
   @Autowired
   private RecruitBaseStatisticsService recruitBaseStatisticsService;
   @Autowired
   private InterviewProgressTimeService interviewProgressTimeService;

   /*
    * (non-Javadoc)
    * @see cn.sigo.recruit.service.JobInfoService#queryMaxJobCode()
    */
   @Override
   public String queryMaxCode() {
      return recruitApplicationRegistMapper.queryMaxCode();
   }

   /*
    * (non-Javadoc)
    * @see cn.sigo.recruit.service.RecruitApplicationRegistService#saveRecruitApplicationRegist(cn.sigo.recruit.model.
    * RecruitApplicationRegist)
    */
   @Override
   public RecruitApplicationRegist saveRecruitApplicationRegist(RecruitApplicationRegist regist) {
      synchronized (this) {
         String maxCode = queryMaxCode();
         String nextCode = GenerateCode.queryNextMaxCode(maxCode, "YP");
         regist.setApplicationCode(nextCode);
         // 设置添加时间
         Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
         regist.setAddTime(date);
         int result = recruitApplicationRegistMapper.insert(regist);
         if (result > 0)
            return regist;
      }
      return null;
   }


   /* (non-Javadoc)
    * @see cn.sigo.recruit.service.RecruitApplicationRegistService#queryApplicationRegistByInterviewCode(java.lang.String)
    */
   @Override
   public RecruitApplicationRegist queryApplicationRegistByInterviewCode(String interviewCode) {
      return recruitApplicationRegistMapper.queryApplicationRegistByInterviewCode(interviewCode);
   }

   @Override
   public RecruitApplicationRegist queryApplicationRegistByApplicationCode(String applicationCode) {
      return recruitApplicationRegistMapper.queryApplicationRegistByApplicationCode(applicationCode);
   }

   /**
    * 根据身份证号码查询应聘登记表记录
    *
    * @param idCardNo
    * @return
    */
   @Override
   public RecruitApplicationRegist queryInfoByIdCardNo(String idCardNo) {
      return recruitApplicationRegistMapper.queryApplicationRegistByIdCardNo(idCardNo);
   }

   @Override
   public PageInfo<RecruitApplicationRegist> queryApplicationRegistrationList(ReqApplicationRegist reqApplicationRegist, String userId, String backendRole) {
      List<RecruitApplicationRegist> resultList = new ArrayList<>();
      // 判断是否为 普通用户
      if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
        && backendRole.indexOf("recruit-系统管理员") < 0) {
//            reqApplicationRegist.setCreatePeople(userId);
         return new PageInfo<>(resultList);
      }
      //创建时间的判断
      if (null != reqApplicationRegist.getStartCreateTime() && null != reqApplicationRegist.getEndCreateTime()) {
         if (10 == reqApplicationRegist.getStartCreateTime().length() && 10 == reqApplicationRegist.getEndCreateTime().length()) {
            reqApplicationRegist.setStartCreateTime(reqApplicationRegist.getStartCreateTime() + " 00:00:00");
            reqApplicationRegist.setEndCreateTime(reqApplicationRegist.getEndCreateTime() + " 23:59:59");
         }
      }
      if (StringUtils.isNotBlank(reqApplicationRegist.getCodeNumber())) {
         String codeNumber = reqApplicationRegist.getCodeNumber().toUpperCase();
         if (codeNumber.startsWith("MS")) {
            reqApplicationRegist.setInterviewCode(codeNumber);
         } else if (codeNumber.startsWith("YP")) {
            reqApplicationRegist.setApplicationCode(codeNumber);
         } else {
            reqApplicationRegist.setInterviewCode(codeNumber);
            reqApplicationRegist.setApplicationCode(codeNumber);
         }
      }
      PageHelper.startPage(reqApplicationRegist.getPageNum(), reqApplicationRegist.getPageSize());
      resultList = recruitApplicationRegistMapper.queryApplicationRegistrationList(reqApplicationRegist);
      return new PageInfo<>(resultList);
   }

   /* (non-Javadoc)
    * @see cn.sigo.recruit.service.RecruitApplicationRegistService#saveRecruitApplicationRegist(cn.sigo.recruit.model.req.ReqRecruitApplicationRegist)
    */
   @Override
   @Transactional
   public ResponseModel saveRecruitApplicationRegist(ReqRecruitApplicationRegist reqRegist, String userId) {
      ResponseModel model = new ResponseModel();
      RecruitApplicationRegist regist = reqRegist.getRegist();
      if (null != regist) {
         // 获取erp的userId
         regist.setAdder(userId);
         String interviewCode = regist.getInterviewCode();
         // 根据面试单号，查询面单单
         RecruitInterviewManagement interview = recruitInterviewManagement.queryInterviewManagentByCode(interviewCode);
         if (null != interview) {
            if (!interview.getInterviewStatus().equals("待应聘登记")) {
               model.setResultCode("0401");
               model.setResultMsg("面试单没有提交");
               return model;
            }
            regist.setJobName(interview.getApplyJobName());
            regist.setJobDept(interview.getApplyDept());
         }
         // 保存应聘登记表基本信息
         RecruitApplicationRegist saveRegist = this.saveRecruitApplicationRegist(regist);
         List<RecruitApplicationCertificate> certificates = reqRegist.getCertificates();
         // 保存证书
         if (null != certificates && !certificates.isEmpty()) {
            recruitApplicationCertificateService.saveRecruitApplicationCertificates(certificates, saveRegist.getId(), userId);
         }
         // 保存教育经历
         List<RecruitApplicationEducation> educations = reqRegist.getEducations();
         if (null != educations && !educations.isEmpty()) {
            recruitApplicationEducationService.saveRecruitApplicationEducations(educations, saveRegist.getId(), userId);
         }
         // 保存家庭成员
         List<RecruitApplicationFamilyMember> familyMenbers = reqRegist.getFamilyMembers();
         if (null != familyMenbers && !familyMenbers.isEmpty()) {
            recruitApplicationFamilyMemberService.saveRecruitApplicationFamilyMembers(familyMenbers, saveRegist.getId(), userId);
         }
         // 保存工作经历
         List<RecruitApplicationWork> works = reqRegist.getApplicationWorks();
         if (null != works && !works.isEmpty()) {
            recruitApplicationWorkService.saveRecruitApplicationWorks(works, saveRegist.getId(), userId);
         }
         //修改面试单状态
         boolean result = recruitInterviewManagement.modifyInterviewStatus(interviewCode, "待初试");
         if (!result) {
            LOGGER.error("面试单：" + interviewCode + "，状态修改失败！");
         }
         // 面试流程：同意面试
         interviewProcessService.isCommonInterview(userId, interview.getProcId(), "true");
         // 面试流程：填写应聘登记表
         String applicationCode = saveRegist.getApplicationCode();
         interviewProcessService.fillRegistration(userId, interview.getProcId(), applicationCode);
         // 更新基础信息统计表
         recruitBaseStatisticsService.updateBaseStatistics(interviewCode, "interviewer");
         // 设置应聘登记的时间
         RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
         recruitInterviewProgress.setApplyRegister(new Date());
         recruitInterviewProgress.setInterviewCode(interviewCode);
         interviewProgressTimeService.updateInterviewProgress(recruitInterviewProgress);
         model.setResultCode("0000");
         model.setResultMsg("");
      }
      return model;
   }


}
