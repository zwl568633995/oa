/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationWorkServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月18日 下午4:49:02
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitApplicationWorkMapper;
import cn.sigo.recruit.model.RecruitApplicationWork;
import cn.sigo.recruit.service.RecruitApplicationWorkService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service("recruitApplicationWorkService")
public class RecruitApplicationWorkServiceImpl implements RecruitApplicationWorkService {
    
    @Autowired
    private RecruitApplicationWorkMapper recruitApplicationWorkMapper;

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationWorkService#saveRecruitApplicationWork(cn.sigo.recruit.model.RecruitApplicationWork)
     */
    @Override
    public boolean saveRecruitApplicationWork(RecruitApplicationWork work) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        work.setAddTime(date);
        return recruitApplicationWorkMapper.insert(work) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationWorkService#saveRecruitApplicationWorks(java.util.List, java.lang.Long)
     */
    @Override
    public boolean saveRecruitApplicationWorks(List<RecruitApplicationWork> works, Integer applicationId, String adder) {
        for(RecruitApplicationWork work : works) {
            work.setApplicationId(applicationId);
            work.setAdder(adder);
            this.saveRecruitApplicationWork(work);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitApplicationWorkService#queryWorkByApplicationId(java.lang.Long)
     */
    @Override
    public List<RecruitApplicationWork> queryWorkByApplicationId(Integer applicationId) {
        return recruitApplicationWorkMapper.queryWorkByApplicationId(applicationId);
    }

}
