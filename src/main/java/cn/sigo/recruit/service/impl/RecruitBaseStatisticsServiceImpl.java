/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitBaseStatisticsServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年8月13日 下午4:21:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitBaseStatisticsMapper;
import cn.sigo.recruit.model.RecruitBaseStatistics;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.service.JobInfoService;
import cn.sigo.recruit.service.RecruitBaseStatisticsService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import cn.sigo.recruit.util.DateUtils;

/**
 * 实现类
 *
 * @author zhixiang.meng
 */
@Service("recruitBaseStatisticsService")
public class RecruitBaseStatisticsServiceImpl implements RecruitBaseStatisticsService {

    @Autowired
    private RecruitBaseStatisticsMapper recruitBaseStatisticsMapper;
    @Autowired
    private JobInfoService jobInfoService;
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitBaseStatisticsService#saveOrUpdateBaseStatistics(java.lang.String)
     */
    @Override
    public boolean saveOrUpdateBaseStatistics(String jobCode) {
        if (StringUtils.isNotBlank(jobCode)) {
            // 根据jobcode查询职位信息
            RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(jobCode);
            String jobName = jobInfo.getJobName();
            String deptName = jobInfo.getApplicationDepartment();
            Integer jobScore = jobInfo.getJobScore();
            if (null != jobInfo && StringUtils.isNotBlank(jobName)
                    && StringUtils.isNotBlank(deptName) && null != jobScore) {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put("jobName", jobName);
                param.put("deptName", deptName);
                param.put("jobScore", jobScore);
                String month = DateUtils.currentMonth();
                param.put("statisticsMonth", month);
                RecruitBaseStatistics baseStatistics = this.queryBaseStatistics(param);
                if(null != baseStatistics) {
                    param.clear();
                    param.put("id", baseStatistics.getId());
                    param.put("numberOfVacancies", 1);
                    this.updateBaseStatistics(param);
                } else {
                    RecruitBaseStatistics beans = new RecruitBaseStatistics();
                    beans.setDeptName(deptName);
                    beans.setJobScore(jobScore);
                    beans.setJobName(jobName);
                    beans.setNumberOfPeople(jobInfo.getNumberOfPeople());
                    beans.setStatisticsMonth(month);
                    beans.setNumberOfVacancies(1);
                    this.saveBaseStatistics(beans);
                }
            }
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitBaseStatisticsService#isEmpty(java.util.Map)
     */
    @Override
    public RecruitBaseStatistics queryBaseStatistics(Map<String, Object> param) {
        return recruitBaseStatisticsMapper.queryBaseStatistics(param);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitBaseStatisticsService#saveBaseStatistics(cn.sigo.recruit.model.RecruitBaseStatistics)
     */
    @Override
    public Integer saveBaseStatistics(RecruitBaseStatistics baseStatistics) {
        int result = recruitBaseStatisticsMapper.insertSelective(baseStatistics);
        if(result > 0) {
            return baseStatistics.getId();
        }
        return 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitBaseStatisticsService#updateBaseStatistics(java.lang.String, java.lang.Integer)
     */
    @Override
    public boolean updateBaseStatistics(Map<String, Object> param) {
        return recruitBaseStatisticsMapper.updateBaseStatistics(param) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitBaseStatisticsService#updateBaseStatistics(cn.sigo.recruit.model.RecruitInterviewManagement, java.lang.String)
     */
    @Override
    public boolean updateBaseStatistics(String interviewCode, String status) {
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
                .queryInterviewManagentByCode(interviewCode);
        // 根据面试单信息查询
        RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(interviewManagement.getJobCode());
        if(null != jobInfo) {
            String jobName = jobInfo.getJobName();
            String deptName = jobInfo.getApplicationDepartment();
            Integer jobScore = jobInfo.getJobScore();
            if (null != jobInfo && StringUtils.isNotBlank(jobName)
                    && StringUtils.isNotBlank(deptName) && null != jobScore) {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put("jobName", jobName);
                param.put("deptName", deptName);
                param.put("jobScore", jobScore);
                String month = DateUtils.currentMonth();
                param.put("statisticsMonth", month);
                // 根据部门名 + 职位 + 分值 查询记录是否存在 不存在新增
                RecruitBaseStatistics baseStatistics = this.queryBaseStatistics(param);
                int id = 0;
                if(null == baseStatistics) {
                    RecruitBaseStatistics beans = new RecruitBaseStatistics();
                    beans.setDeptName(deptName);
                    beans.setJobScore(jobScore);
                    beans.setJobName(jobName);
                    beans.setNumberOfPeople(jobInfo.getNumberOfPeople());
                    beans.setStatisticsMonth(month);
                    id = this.saveBaseStatistics(beans);
                } else {
                    id = baseStatistics.getId();
                }
                param.clear();
                param.put("id", id);
                // 根据状态跟新 拒绝面试
                if(status.equals("noInterviewNumber")) {
                    param.put("noInterviewNumber", "1");
                } else if(status.equals("interviewer")) {
                    // 面过中
                    param.put("interviewer", "1");
                } else if(status.equals("interviewNumber")) {
                    // 面试通过
                    param.put("interviewNumber", "1");
                } else if(status.equals("noInterview")) {
                    // 面试不通过
                    param.put("noInterview", "1");
                } else if(status.equals("numberOfOffer")) {
                    // 发送offer
                    param.put("numberOfOffer", "1");
                } else if(status.equals("numberOfNoOffer")) {
                    // offer审核拒绝
                    param.put("numberOfOffer", "1");
                } else if(status.equals("numberOfRecruits")) {
                    // 已入职
                    param.put("numberOfRecruits", "1");
                } else if(status.equals("noOfferNumber")) {
                    param.put("noOfferNumber", "1");
                }
                this.updateBaseStatistics(param);
            }
        }
        
        return true;
    }

    @Override
    public RecruitBaseStatistics dataOverview(String yearMonth) {
        return recruitBaseStatisticsMapper.dataOverview(yearMonth);
    }

    @Override
    public List<RecruitBaseStatistics> departmentRecruitInfo(String yearMonth) {
        return recruitBaseStatisticsMapper.departmentRecruitInfo(yearMonth);
    }

    @Override
    public List<RecruitBaseStatistics> positionRecruitInfo(String yearMonth, String department) {
        Map<String,Object> param = new HashMap<>();
        param.put("yearMonth",yearMonth);
        param.put("department",department);
        return recruitBaseStatisticsMapper.positionRecruitInfo(param);
    }


}
