package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.mapper.*;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegister;
import cn.sigo.recruit.model.req.ReqRecruitEntryRegisterPageQuery;
import cn.sigo.recruit.service.*;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.GenerateCode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service("recruitEntryRegisterService")
public class RecruitEntryRegisterServiceImpl implements RecruitEntryRegisterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecruitEntryRegisterServiceImpl.class);

    @Autowired
    private RecruitEntryHealthyMapper recruitEntryHealthyMapper;
    @Autowired
    private RecruitEntryRegisterBaseInfoMapper recruitEntryRegisterBaseInfoMapper;
    @Autowired
    private RecruitIncumbencyCertificateMapper recruitIncumbencyCertificateMapper;
    @Autowired
    private RecruitIncumbencyEducationMapper recruitIncumbencyEducationMapper;
    @Autowired
    private RecruitIncumbencyFamilyMemberMapper recruitIncumbencyFamilyMemberMapper;
    @Autowired
    private RecruitIncumbencyWorkMapper recruitIncumbencyWorkMapper;

    @Autowired
    private RecruitBaseStatisticsService recruitBaseStatisticsService;

    @Autowired
    private RecruitStatisticalTransformationService recruitStatisticalTransformationService;

    @Autowired
    private RecruitInterviewManagementResultService recruitInterviewManagementResultService;
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;
    @Autowired
    private InterviewProgressTimeService interviewProgressTimeService;


    @Override
    public RecruitEntryRegisterBaseInfo saveRecruitEntryRegisterBaseInfo(RecruitEntryRegisterBaseInfo recruitEntryRegisterBaseInfo) {
        synchronized (this) {
            String maxCode = queryMaxCode();
            String nextCode = GenerateCode.queryNextMaxCode(maxCode,"RZ");
            recruitEntryRegisterBaseInfo.setEntryCode(nextCode);
            // 设置添加时间
            Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
            recruitEntryRegisterBaseInfo.setAddTime(date);
            int result = recruitEntryRegisterBaseInfoMapper.insert(recruitEntryRegisterBaseInfo);
            if(result>0)
                return recruitEntryRegisterBaseInfo;
            return null;
        }
    }

    @Override
    public Boolean saveRecruitEntryHealthy(RecruitEntryHealthy recruitEntryHealthy, Long applicationId, String adder) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitEntryHealthy.setAddTime(date);
        recruitEntryHealthy.setEntryRegisterId(applicationId.intValue());
        recruitEntryHealthy.setAdder(adder);
        return recruitEntryHealthyMapper.insert(recruitEntryHealthy)>0;
    }

    public Boolean saveRecruitIncumbencyCertificate(RecruitIncumbencyCertificate recruitIncumbencyCertificate) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitIncumbencyCertificate.setAddTime(date);
        return recruitIncumbencyCertificateMapper.insert(recruitIncumbencyCertificate)>0;
    }

    @Override
    public Boolean saveRecruitIncumbencyCertificates(List<RecruitIncumbencyCertificate> recruitIncumbencyCertificates, Long applicationId, String adder) {
        for(RecruitIncumbencyCertificate  item:recruitIncumbencyCertificates){
            item.setEntryRegisterId(applicationId.intValue());
            item.setAdder(adder);
            this.saveRecruitIncumbencyCertificate(item);
        }
        return true;
    }

    public Boolean saveRecruitIncumbencyEducation(RecruitIncumbencyEducation recruitIncumbencyEducation) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitIncumbencyEducation.setAddTime(date);
        return recruitIncumbencyEducationMapper.insert(recruitIncumbencyEducation)>0;
    }
    @Override
    public Boolean saveRecruitIncumbencyEducations(List<RecruitIncumbencyEducation> recruitIncumbencyEducations, Long applicationId, String adder) {
        for(RecruitIncumbencyEducation  item:recruitIncumbencyEducations){
            item.setEntryRegisterId(applicationId.intValue());
            item.setAdder(adder);
            this.saveRecruitIncumbencyEducation(item);
        }
        return true;
    }

    public Boolean saveRecruitIncumbencyFamilyMember(RecruitIncumbencyFamilyMember recruitIncumbencyFamilyMember) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitIncumbencyFamilyMember.setAddTime(date);
        return recruitIncumbencyFamilyMemberMapper.insert(recruitIncumbencyFamilyMember)>0;
    }

    @Override
    public Boolean saveRecruitIncumbencyFamilyMembers(List<RecruitIncumbencyFamilyMember> recruitIncumbencyFamilyMembers, Long applicationId, String adder) {
        for(RecruitIncumbencyFamilyMember  item:recruitIncumbencyFamilyMembers){
            item.setEntryRegisterId(applicationId.intValue());
            item.setAdder(adder);
            this.saveRecruitIncumbencyFamilyMember(item);
        }
        return true;
    }

    public Boolean saveRecruitIncumbencyWork(RecruitIncumbencyWork recruitIncumbencyWork) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitIncumbencyWork.setAddTime(date);
        return recruitIncumbencyWorkMapper.insert(recruitIncumbencyWork)>0;
    }

    @Override
    public Boolean saveRecruitIncumbencyWorks(List<RecruitIncumbencyWork> recruitIncumbencyWorks, Long applicationId, String adder) {
        for(RecruitIncumbencyWork  item:recruitIncumbencyWorks){
            item.setEntryRegisterId(applicationId.intValue());
            item.setAdder(adder);
            this.saveRecruitIncumbencyWork(item);
        }
        return true;
    }

    @Override
    public ReqRecruitEntryRegister queryRecruitEntryRegister(String entryCode) {
        ReqRecruitEntryRegister result = new ReqRecruitEntryRegister();
        RecruitEntryRegisterBaseInfo baseInfo = recruitEntryRegisterBaseInfoMapper.findBaseInfoByCode(entryCode);
        Long registerId = baseInfo.getId().longValue();
        RecruitEntryHealthy healthy = recruitEntryHealthyMapper.findHealthInfoByRegisterId(registerId);
        List<RecruitIncumbencyCertificate> certificates = recruitIncumbencyCertificateMapper.findCertificateInfoByRegisterId(registerId);
        List<RecruitIncumbencyEducation> educations = recruitIncumbencyEducationMapper.findEducationInfoByRegisterId(registerId);
        List<RecruitIncumbencyFamilyMember> familyMembers = recruitIncumbencyFamilyMemberMapper.findFamilyInfoByRegisterId(registerId);
        List<RecruitIncumbencyWork> works = recruitIncumbencyWorkMapper.findWorkInfoByRegisterId(registerId);
        result.setBaseInfo(baseInfo);
        result.setHealthy(healthy);
        result.setCertificates(certificates);
        result.setEducations(educations);
        result.setFamilyMembers(familyMembers);
        result.setWorks(works);
        return result;
    }

    @Override
    public PageInfo<ReqRecruitEntryRegisterPageQuery> pageQueryRecruitEntryRegister(ReqRecruitEntryRegisterPageQuery reqRecruitEntryRegisterPageQuery, String userId, String backendRole) {
        List<ReqRecruitEntryRegisterPageQuery> result = new ArrayList<>();
        // 判断是否为 普通用户
        if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
                && backendRole.indexOf("recruit-系统管理员") < 0) {
            return new PageInfo<>(result);
        }

        //创建时间的判断
        if(null!=reqRecruitEntryRegisterPageQuery.getStartCreateTime()&&null!=reqRecruitEntryRegisterPageQuery.getEndCreateTime()){
            if(10==reqRecruitEntryRegisterPageQuery.getStartCreateTime().length()&&10==reqRecruitEntryRegisterPageQuery.getEndCreateTime().length()){
                reqRecruitEntryRegisterPageQuery.setStartCreateTime(reqRecruitEntryRegisterPageQuery.getStartCreateTime()+" 00:00:00");
                reqRecruitEntryRegisterPageQuery.setEndCreateTime(reqRecruitEntryRegisterPageQuery.getEndCreateTime()+" 23:59:59");
            }
        }

        if (StringUtils.isNotBlank(reqRecruitEntryRegisterPageQuery.getCodeNumber())) {
            String codeNumber = reqRecruitEntryRegisterPageQuery.getCodeNumber().toUpperCase();
            if (codeNumber.startsWith("MS")) {
                reqRecruitEntryRegisterPageQuery.setInterviewCode(codeNumber);
            }
            if (codeNumber.startsWith("YP")) {
                reqRecruitEntryRegisterPageQuery.setApplicationCode(codeNumber);
            }
            if (codeNumber.startsWith("RZ")) {
                reqRecruitEntryRegisterPageQuery.setEntryCode(codeNumber);
            }

        }
        PageHelper.startPage(reqRecruitEntryRegisterPageQuery.getPageNum(), reqRecruitEntryRegisterPageQuery.getPageSize());
        result = recruitEntryRegisterBaseInfoMapper.pageQueryRecruitEntryRegister(reqRecruitEntryRegisterPageQuery);
        return new PageInfo<ReqRecruitEntryRegisterPageQuery>(result);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String,Object> saveRecruitEntryRegister(ReqRecruitEntryRegister reqRecruitEntry, String adder) {
        //返回结果map
        Map<String,Object> resultMap = new HashMap<>();
        try {
            RecruitEntryRegisterBaseInfo baseInfo = reqRecruitEntry.getBaseInfo();
            if(null==baseInfo){
                resultMap.put("resultCode","0000");
                resultMap.put("result","SUCCESS");
                resultMap.put("msg","入职基本信息为空");
                return  resultMap;
            }
            baseInfo.setAdder(adder);
            RecruitEntryRegisterBaseInfo saveBaseResult = this.saveRecruitEntryRegisterBaseInfo(baseInfo);
            Long baseResult =  saveBaseResult.getId().longValue();
            RecruitEntryHealthy recruitEntryHealthy = reqRecruitEntry.getHealthy();
            List<RecruitIncumbencyCertificate> certificates = reqRecruitEntry.getCertificates();
            List<RecruitIncumbencyEducation> educations = reqRecruitEntry.getEducations();
            List<RecruitIncumbencyFamilyMember> familyMembers = reqRecruitEntry.getFamilyMembers();
            List<RecruitIncumbencyWork> works = reqRecruitEntry.getWorks();
            if(null!=recruitEntryHealthy){
                this.saveRecruitEntryHealthy(recruitEntryHealthy,baseResult,adder);
            }
            if(!CollectionUtils.isEmpty(certificates)){
                this.saveRecruitIncumbencyCertificates(certificates,baseResult,adder);
            }
            if(!CollectionUtils.isEmpty(educations)){
                this.saveRecruitIncumbencyEducations(educations,baseResult,adder);
            }
            if(!CollectionUtils.isEmpty(familyMembers)){
                this.saveRecruitIncumbencyFamilyMembers(familyMembers,baseResult,adder);
            }
            if(!CollectionUtils.isEmpty(works)){
                this.saveRecruitIncumbencyWorks(works,baseResult,adder);
            }
            //更新职位表 已入职人数(如果已入职人数等于需求人数，职位状态 更新为 ‘招聘完成’)
            recruitEntryRegisterBaseInfoMapper.updateJobInfoByInterviewCode(baseInfo.getInterviewCode());

            //更新 面试单
            recruitInterviewManagementResultService.employeeEntry(baseInfo.getInterviewCode(),adder,saveBaseResult.getEntryCode());
            //更新 面试单  状态 为‘已入职’
            recruitInterviewManagementService.modifyInterviewStatus(baseInfo.getInterviewCode(),"已入职");


            //更新面试单 进度（入职时间）
            RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
            recruitInterviewProgress.setInterviewCode(baseInfo.getInterviewCode());
            recruitInterviewProgress.setEntry(new Date());
            interviewProgressTimeService.updateInterviewProgress(recruitInterviewProgress);

            //更新两张统计结果表 已入职人数字段（面试单流程更改后，，，统计需要修改）
//            recruitBaseStatisticsService.updateBaseStatistics(baseInfo.getInterviewCode(),"numberOfRecruits");
//            recruitStatisticalTransformationService.updateBaseStatisticalTransformation(baseInfo.getInterviewCode(),"numberOfRecruits");

            resultMap.put("resultCode","0000");
            resultMap.put("result","SUCCESS");
            resultMap.put("msg","SUCCESS");
            return resultMap;

        }catch (Exception e){
            LOGGER.info(e.getMessage());
            resultMap.put("resultCode","0044");
            resultMap.put("result","ERROR");
            resultMap.put("msg",e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            return resultMap;
        }
    }

    @Override
    public Boolean isExistInEntryRegisterBase(Map<String, Object> map) {
        return recruitEntryRegisterBaseInfoMapper.isExistInEntryRegisterBase(map)>0;
    }


    public String queryMaxCode() {
        return recruitEntryRegisterBaseInfoMapper.queryMaxCode();
    }

}
