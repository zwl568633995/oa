/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InterviewManagementResultServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年7月16日 下午5:06:49
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.mapper.RecruitInterviewManagementResultMapper;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.response.ReceiveAccount;
import cn.sigo.recruit.model.response.SendNotification;
import cn.sigo.recruit.model.response.SendNotificationResult;
import cn.sigo.recruit.service.*;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.LogHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service("recruitInterviewManagementResultService")
public class RecruitInterviewManagementResultServiceImpl implements RecruitInterviewManagementResultService {

    @Autowired
    private RecruitInterviewManagementResultMapper recruitInterviewManagementResultMapper;
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;
    @Autowired
    private InterviewProcessService interviewProcessService;
    @Autowired
    private RecruitBaseStatisticsService recruitBaseStatisticsService;
    @Autowired
    private RecruitStatisticalTransformationService recruitStatisticalTransformationService;
    @Autowired
    private InterviewProgressTimeService interviewProgressTimeService;
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private LogHelper logHelper;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.InterviewManagementResultService#queryResultForManagementId(int)
     */
    @Override
    public RecruitInterviewManagementResult queryResultForManagementId(Integer managementId) {
        return recruitInterviewManagementResultMapper.queryResultForManagementId(managementId);
    }

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.RecruitInterviewManagementResultService#saveInterviewManagementResult(cn.sigo.recruit.
     * model.RecruitInterviewManagementResult)
     */
    @Override
    public boolean saveInterviewManagementResult(RecruitInterviewManagementResult interviewResult) {
        // 设置添加时间
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        interviewResult.setAddTime(date);
        return recruitInterviewManagementResultMapper.insert(interviewResult) > 0;
    }

    /*
     * (non-Javadoc)
     * @see
     * cn.sigo.recruit.service.RecruitInterviewManagementResultService#modifyInterviewManagementResult(cn.sigo.recruit.
     * model.RecruitInterviewManagementResult)
     */
    @Override
    public boolean modifyInterviewManagementResult(RecruitInterviewManagementResult interviewResult) {
        return recruitInterviewManagementResultMapper.updateByPrimaryKeySelective(interviewResult) > 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#interviewReview(java.lang.String,
     * cn.sigo.recruit.model.RecruitInterviewManagementResult)
     */
    @Transactional
    @Override
    public ResponseModel interviewReview(String interviewCode, RecruitInterviewManagementResult interviewResult) {
        ResponseModel model = new ResponseModel();
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            String statisticsStatus = "";
            // 当前面试单的状态
            String queryInterviewStatus = interviewManagement.getInterviewStatus();
            if (queryInterviewStatus.equals("面试中") || queryInterviewStatus.equals("待定")) {
                String userId = interviewResult.getAdder();
                // 判断是否可以修改面试状态
                boolean statusFlag = true;
                // 判断初试
                if (StringUtils.isNotBlank(interviewManagement.getFirstInterviewer())) {
                    if (interviewManagement.getFirstInterviewer().indexOf(userId) < 0) {
                        statusFlag = false;
                    }
                }
                // 判断复试
                if (!statusFlag && StringUtils.isNotBlank(interviewManagement.getSecondInterviewer())) {
                    if (interviewManagement.getSecondInterviewer().indexOf(userId) < 0) {
                        statusFlag = false;
                    } else {
                        statusFlag = true;
                    }
                }
                if (!statusFlag) {
                    model.setResultCode("0307");
                    model.setResultMsg("对不起，您没有权限操作该面试人员");
                    return model;
                }
                String status = "";
                if (interviewResult.getOperation().equals("待定")) {
                    status = "待定";
                } else if (interviewResult.getOperation().equals("面试通过")) {
                    status = "offer待审核";
                    statisticsStatus = "interviewNumber";
                    // 更新转化率表的数量
                    recruitStatisticalTransformationService.updateBaseStatisticalTransformation(interviewCode,
                        statisticsStatus);
                } else if (interviewResult.getOperation().equals("面试不通过")) {
                    status = "面试不通过";
                    statisticsStatus = "noInterview";
                }
                // 面试流程,是否需要走下一个工作流的流程
                if (!status.equals("待定")) {
                    String isInterviewPassed = "";
                    if (status.equals("offer待审核")) {
                        isInterviewPassed = "true";
                    } else {
                        isInterviewPassed = "false";
                    }
                    boolean procResult = interviewProcessService.procInterviewReview(userId,
                        interviewManagement.getProcId(), isInterviewPassed, interviewResult.getOperation());
                    if (!procResult) {
                        model.setResultCode("0307");
                        model.setResultMsg("对不起，您没有权限操作该面试人员");
                        return model;
                    }
                }
                // 查询db 是否已经存在该面试单的结果
                RecruitInterviewManagementResult queryDbInterviewResult = this
                    .queryResultForManagementId(interviewManagement.getId());
                // 存在面试结果 -- 保存面试结果
                if (null != queryDbInterviewResult) {
                    interviewResult.setId(queryDbInterviewResult.getId());
                    interviewResult.setAdder("");
                    this.modifyInterviewManagementResult(interviewResult);
                } else {
                    interviewResult.setModer("");
                    interviewResult.setInterviewManagementId(interviewManagement.getId());
                    this.saveInterviewManagementResult(interviewResult);
                }
                // 修改面试单的状态
                boolean result = recruitInterviewManagementService.modifyInterviewStatus(interviewCode, status);
                if (result) {
                    model.setResultCode("0000");
                    model.setResultMsg("");
                } else {
                    model.setResultCode("0002");
                    model.setResultMsg("异常！数据入库失败");
                }
            } else {
                model.setResultCode("0306");
                model.setResultMsg("该人选不在面试中，请确认面试人员！");
            }
            if (StringUtils.isNotBlank(statisticsStatus)) {
                // 更新基础统计表
                recruitBaseStatisticsService.updateBaseStatistics(interviewCode, statisticsStatus);
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#offerAudit(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Transactional
    @Override
    public ResponseModel offerAudit(String interviewCode, String salary, String offerOpinion, String userId,
                                    String isInterviewPassed) {
        ResponseModel model = new ResponseModel();
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            String statisticsStatus = "";
            String resultOffer = "true";
            if (StringUtils.isNotBlank(isInterviewPassed) && isInterviewPassed.equals("拒绝发offer")) {
                resultOffer = "false";
            }
            if (interviewManagement.getOfferAudit().indexOf(userId) < 0) {
                model.setResultCode("0307");
                model.setResultMsg("对不起，您没有权限操作该面试人员");
                return model;
            }
            // 携带参数 到 监听器  offer意见 , offer工资
            boolean result = interviewProcessService.offerAudit(interviewManagement.getProcId(), userId, resultOffer, offerOpinion, salary);
            if (result) {
                // TODO保存数据库 offer审核结果
                // 查询db 是否已经存在该面试单的结果
                RecruitInterviewManagementResult queryDbInterviewResult = this
                    .queryResultForManagementId(interviewManagement.getId());
                queryDbInterviewResult.setOfferOpinion(offerOpinion);
                queryDbInterviewResult.setOfferSalary(salary);
                // 保存offer审核意见和薪资
                result = this.modifyInterviewManagementResult(queryDbInterviewResult);
                if (result) {
                    // 修改面试单状态
                    if (isInterviewPassed.equals("拒绝发offer")) {
                        result = recruitInterviewManagementService.modifyInterviewStatus(interviewCode, "offer审核拒绝");
                        statisticsStatus = "numberOfNoOffer";
                    } else {
                        result = recruitInterviewManagementService.modifyInterviewStatus(interviewCode, "待入职");
                        statisticsStatus = "numberOfOffer";
                        // 更新转化率表的数量
                        recruitStatisticalTransformationService.updateBaseStatisticalTransformation(interviewCode,
                            statisticsStatus);
                    }
                    // 修改offer审核时间
                    recruitInterviewManagementService.modifyOfferAuitTime(interviewCode);
                    // 设置offer审核完成时间
                    RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
                    recruitInterviewProgress.setOfferAudit(new Date());
                    recruitInterviewProgress.setInterviewCode(interviewCode);
                    interviewProgressTimeService.updateInterviewProgress(recruitInterviewProgress);
                    if (result) {
                        model.setResultCode("0000");
                        model.setResultMsg("");
                    } else {
                        model.setResultCode("0002");
                        model.setResultMsg("异常！数据入库失败");
                    }
                    // 更新基础统计表
                    recruitBaseStatisticsService.updateBaseStatistics(interviewCode, statisticsStatus);
                } else {
                    model.setResultCode("0002");
                    model.setResultMsg("异常！数据入库失败");
                }

            } else {
                model.setResultCode("0308");
                model.setResultMsg("您没有权限审核该面试的offer！");
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }


    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#isSendOffer(java.lang.String,
     * java.lang.String)
     */
    @Override
    public ResponseModel isSendOffer(String interviewCode, String isSendOffer, String userId) {
        ResponseModel model = new ResponseModel();
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            String resultOffer = "true";
            if (isSendOffer.equals("拒绝offer")) {
                resultOffer = "false";
                // 拒绝offer更新基本信息统计
                recruitBaseStatisticsService.updateBaseStatistics(interviewCode, "noOfferNumber");
                // 更新面试单
                recruitInterviewManagementService.modifyInterviewStatus(interviewCode, "已拒offer");
            }
            // 更新activiti流程
            boolean result = interviewProcessService.sendOffer(interviewManagement.getProcId(), resultOffer, userId, interviewCode);
            if (result) {
                model.setResultCode("0000");
                model.setResultMsg("");
            } else {
                model.setResultCode("0002");
                model.setResultMsg("异常！数据入库失败");
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#employeeEntry(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public boolean employeeEntry(String interviewCode, String userId, String entryCode) {
        // offer更新activiti任务节点
        ResponseModel model = this.isSendOffer(interviewCode, "已入职", userId);
        // 办理入职流程，这里仅更新activiti流程
        if (model.getResultCode().equals("0000")) {
            RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
                .queryInterviewManagentByCode(interviewCode);
            interviewProcessService.employeeEntry(interviewManagement.getProcId(), entryCode, userId);
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#setInterviewer(java.lang.String)
     */
    @Override
    public void setInterviewer(String interviewCode) {
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            Set<String> interviewers = new HashSet<String>();
            // 初试
            if (StringUtils.isNotBlank(interviewManagement.getFirstInterviewer())) {
                if (interviewManagement.getFirstInterviewer().indexOf(";") > 0) {
                    String[] strs = interviewManagement.getFirstInterviewer().split(";");
                    for (String str : strs) {
                        interviewers.add(str.split(":")[0]);
                    }
                } else {
                    interviewers.add(interviewManagement.getFirstInterviewer().split(":")[0]);
                }
            }
            // 复试
            if (StringUtils.isNotBlank(interviewManagement.getSecondInterviewer())) {
                if (interviewManagement.getSecondInterviewer().indexOf(";") > 0) {
                    String[] strs = interviewManagement.getSecondInterviewer().split(";");
                    for (String str : strs) {
                        interviewers.add(str.split(":")[0]);
                    }
                } else {
                    interviewers.add(interviewManagement.getSecondInterviewer().split(":")[0]);
                }
            }
            interviewProcessService.setInterviewers(interviewers, interviewManagement.getProcId());
        }
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#interviewFirstReview(java.lang.String, cn.sigo.recruit.model.RecruitInterviewManagementResult)
     */
    @Transactional
    @Override
    public ResponseModel interviewFirstReview(String interviewCode, RecruitInterviewManagementResult interviewResult) {
        ResponseModel model = new ResponseModel();
        // 查询面试单
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            String userId = interviewResult.getAdder();
            // 当前面试单的状态
            String queryInterviewStatus = interviewManagement.getInterviewStatus();
            // 判断面试单的状态是否是 待初试 和 初试待定
            if (queryInterviewStatus.equals("待初试") || queryInterviewStatus.equals("初试待定")) {
                // 判断人员是否可以操作面试单
                if (StringUtils.isNotBlank(interviewManagement.getFirstInterviewer())) {
                    if (interviewManagement.getFirstInterviewer().indexOf(userId) < 0) {
                        model.setResultCode("0307");
                        model.setResultMsg("对不起，您没有权限操作该面试人员");
                        return model;
                    }
                }
                // 设置面试单状态 以及 工作流的状态
                String saveStatus = "";
                // 工作流状态
                String activitiStatus = "";
                // 复试人
                String secondInterviewer = "";
                if (interviewResult.getOperation().equals("初试待定")) {
                    saveStatus = "初试待定";
                }
                if (interviewResult.getOperation().equals("初试不通过")) {
                    saveStatus = "初试不通过";
                    activitiStatus = "false";
                }
                if (interviewResult.getOperation().equals("初试通过")) {
                    // 初试通过，判断是否需要复试
                    if (StringUtils.isNotBlank(interviewResult.getSecondInterviewer())) {
                        secondInterviewer = interviewResult.getSecondInterviewer();
                        saveStatus = "待安排复试";
                        activitiStatus = "second";
                    } else {
                        // 不需要复试
                        saveStatus = "offer待审核";
                        activitiStatus = "true";
                        // 设置面试完成时间
                        RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
                        recruitInterviewProgress.setInterview(new Date());
                        recruitInterviewProgress.setInterviewCode(interviewCode);
                        interviewProgressTimeService.updateInterviewProgress(recruitInterviewProgress);
                    }
                }
                // 修改面试单状态
                recruitInterviewManagementService.modifyInterviewStatus(interviewCode, saveStatus, secondInterviewer);
                // 保存面试结果
                RecruitInterviewManagementResult queryDbInterviewResult = this
                    .queryResultForManagementId(interviewManagement.getId());
                if (null != queryDbInterviewResult) {
                    interviewResult.setAdder("");
                    interviewResult.setId(queryDbInterviewResult.getId());
                    this.modifyInterviewManagementResult(interviewResult);
                } else {
                    interviewResult.setModer("");
                    interviewResult.setInterviewManagementId(interviewManagement.getId());
                    this.saveInterviewManagementResult(interviewResult);
                }
                //设置activiti工作流状态
                if (StringUtils.isNotBlank(activitiStatus)) {
                    interviewProcessService.procInterviewReview(userId,
                        interviewManagement.getProcId(), activitiStatus, interviewResult.getOperation());
                }

                //页面操作 初试待定时, 发送消息通知
                if ("初试待定".equals(interviewResult.getOperation())) {
                    //发送初试待定通知
                    Notification notification = notificationMapper.selectByNode("初试待定");

                    Map<String, Object> map = transFirstInterview(interviewManagement.getFirstInterviewer());

                    // 姓名 部门 职位 来源 初试面试官 初试时间 初试反馈
                    Object[] params = {interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                        interviewManagement.getResumeSource(), (String) map.get("name"), interviewManagement.getFirstTime().substring(0, 16), interviewResult.getFirstRemarks(),
                        MessageFormat.format(notification.getUrl(), interviewManagement.getInterviewCode())};

                    doSengNotification(notification, params, (List<ReceiveAccount>) map.get("receiveAccountList"));

                }

                model.setResultCode("0000");
                model.setResultMsg("");
            } else {
                model.setResultCode("0306");
                model.setResultMsg("该人选不在初试中，请确认初试人员！");
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }


    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#interviewSecondReview(java.lang.String, cn.sigo.recruit.model.RecruitInterviewManagementResult)
     */
    @Transactional
    @Override
    public ResponseModel interviewSecondReview(String interviewCode, RecruitInterviewManagementResult interviewResult) {
        ResponseModel model = new ResponseModel();
        // 查询面试单
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            // 当前面试单的状态
            String queryInterviewStatus = interviewManagement.getInterviewStatus();
            // 判断面试单的状态是否是 待复试 和 复试待定
            if (queryInterviewStatus.equals("待复试") || queryInterviewStatus.equals("复试待定")) {
                String userId = interviewResult.getAdder();
                // 判断人员是否可以操作面试单
                if (StringUtils.isNotBlank(interviewManagement.getSecondInterviewer())) {
                    if (interviewManagement.getSecondInterviewer().indexOf(userId) < 0) {
                        model.setResultCode("0307");
                        model.setResultMsg("对不起，您没有权限操作该面试人员");
                        return model;
                    }
                }
                if (queryInterviewStatus.equals("待复试")) {
                    // 将工作流走到复试流程
                    interviewProcessService.isSecondCommonInterview(userId, interviewManagement.getProcId(), "true", interviewResult.getOperation());
                }
                // 设置面试单状态 以及 工作流的状态
                String saveStatus = "";
                // 工作流状态
                String activitiStatus = "";
                if (interviewResult.getOperation().equals("复试待定")) {
                    saveStatus = "复试待定";
                }
                if (interviewResult.getOperation().equals("复试不通过")) {
                    saveStatus = "复试不通过";
                    activitiStatus = "false";
                }
                if (interviewResult.getOperation().equals("复试通过")) {
                    saveStatus = "offer待审核";
                    activitiStatus = "true";
                }
                // 保存面试结果
                RecruitInterviewManagementResult queryDbInterviewResult = this
                    .queryResultForManagementId(interviewManagement.getId());
                if (null != queryDbInterviewResult) {
                    interviewResult.setId(queryDbInterviewResult.getId());
                    interviewResult.setAdder("");
                    this.modifyInterviewManagementResult(interviewResult);
                }

                // 修改面试单状态
                recruitInterviewManagementService.modifyInterviewStatus(interviewCode, saveStatus);

                //设置activiti工作流状态
                if (StringUtils.isNotBlank(activitiStatus)) {
                    interviewProcessService.procSecondInterviewReview(userId,
                        interviewManagement.getProcId(), activitiStatus);
                    // 设置面试完成时间
                    RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
                    recruitInterviewProgress.setInterview(new Date());
                    recruitInterviewProgress.setInterviewCode(interviewCode);
                    interviewProgressTimeService.updateInterviewProgress(recruitInterviewProgress);
                }
                model.setResultCode("0000");
                model.setResultMsg("");
            } else {
                model.setResultCode("0306");
                model.setResultMsg("该人选不在复试中，请确认复试人员！");
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementResultService#refusingSencondInterview(java.lang.String)
     */
    @Transactional
    @Override
    public ResponseModel refusingSencondInterview(String interviewCode, String userId) {
        ResponseModel model = new ResponseModel();
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
            .queryInterviewManagentByCode(interviewCode);
        if (null != interviewManagement) {
            if (interviewManagement.getInterviewStatus().equals("待安排复试") ||
                interviewManagement.getInterviewStatus().equals("待复试")) {
                // 面试流程:拒绝面试
                interviewProcessService.isSecondCommonInterview(userId, interviewManagement.getProcId(), "false", "复试爽约");
                // 修改面试单记录
                recruitInterviewManagementService.modifyInterviewStatus(interviewCode, "复试爽约");

                //待复试的时候,面试人爽约复试,不来了
                if ("待复试".equals(interviewManagement.getInterviewStatus())) {
                    RecruitInterviewManagementResult interviewManagementResult = recruitInterviewManagementResultMapper.queryResultForManagementId(interviewManagement.getId());
                    Notification notification = notificationMapper.selectByNode("复试爽约");

                    Map<String, Object> firstMap = transFirstInterview(interviewManagement.getFirstInterviewer());
                    Map<String, Object> secondMap = transFirstInterview(interviewManagement.getSecondInterviewer());

                    // 姓名 部门 职位 来源 初试面试官 初试时间 初试反馈 复试面试官 复试时间 url
                    Object[] params = {
                        interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getResumeSource(),
                        firstMap.get("name"), interviewManagement.getFirstTime().substring(0, 16), interviewManagementResult.getFirstRemarks(),
                        secondMap.get("name"), interviewManagement.getSecondTime().substring(0, 16), MessageFormat.format(notification.getUrl(), interviewCode)
                    };

                    doSengNotification(notification,params, (List<ReceiveAccount>) secondMap.get("receiveAccountList"));
                }


                // 更新统计表的基础信息
                recruitBaseStatisticsService.updateBaseStatistics(interviewCode, "noInterviewNumber");
                model.setResultCode("0000");
                model.setResultMsg("");
            } else {
                model.setResultCode("0306");
                model.setResultMsg("面试单状态不正确，请确认！");
            }
        } else {
            model.setResultCode("0305");
            model.setResultMsg("面试单号未能查询到面试信息！");
        }
        return model;
    }


    /**
     * 根据传入的 面试官字符串 : wuyicheng:武宜城;xudou:徐豆  获取 中文名称 和 邮件收件人
     *
     * @param firstInterview
     * @return
     */
    public Map<String, Object> transFirstInterview(String firstInterview) {
        Map<String, Object> tansMap = new HashMap<>();

        String firstName = "";
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();
        String[] split = firstInterview.split(";");
        for (String name : split) {
            firstName = "," + name.split(":")[1];

            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(name.split(":")[0] + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(name.split(":")[1]);
            receiveAccountList.add(receiveAccount);
        }

        tansMap.put("name", firstName.substring(1));
        tansMap.put("receiveAccountList", receiveAccountList);

        return tansMap;
    }

    /**
     * 发送消息
     *
     * @return
     */
    public Map<String, Object> doSengNotification(Notification notification, Object[] params, List<ReceiveAccount> receiveAccountList) {
        Map<String, Object> result = new HashMap<>();
        String content = MessageFormat.format(notification.getContent(), params);

        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgContent(content);
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setReceiveAccountList(receiveAccountList);

        SendNotificationResult sendNotificationResult = sendNotificationFeign.messageNotify(sendNotification);

        if (null != sendNotificationResult && sendNotificationResult.getCode().equals("success")) {
            //消息发送成功
            logHelper.LoggerSendINFO("sendNotification success");
            result.put("code", "success");
        } else {
            //消息发送失败
            logHelper.LoggerSendERROR("sendNotification error : 调用 sendNotification messageNotify 接口失败" + sendNotificationResult.getMessage());
            result.put("code", sendNotificationResult.getCode());
            result.put("message", sendNotificationResult.getMessage());
        }

        return result;
    }
}
