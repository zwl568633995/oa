package cn.sigo.recruit.service.impl;

import java.text.MessageFormat;
import java.util.*;

import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.mapper.RecruitInterviewManagementResultMapper;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.util.LogHelper;
import com.sun.javafx.collections.MappingChange;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.InitializationBean;
import cn.sigo.recruit.mapper.RecruitInterviewManagementMapper;
import cn.sigo.recruit.model.req.ReqInterviewManagementModel;
import cn.sigo.recruit.model.req.ReqModifyInterviewTime;
import cn.sigo.recruit.service.InterviewProcessService;
import cn.sigo.recruit.service.InterviewProgressTimeService;
import cn.sigo.recruit.service.JobInfoService;
import cn.sigo.recruit.service.RecruitBaseStatisticsService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import cn.sigo.recruit.service.RecruitStatisticalTransformationService;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.GenerateCode;

@Service("recruitInterviewManagementService")
public class RecruitInterviewManagementServiceImpl implements RecruitInterviewManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecruitInterviewManagementService.class);

    @Autowired
    private RecruitInterviewManagementMapper recruitInterviewManagementMapper;
    @Autowired
    private RecruitInterviewManagementResultMapper interviewManagementResultMapper;
    @Autowired
    private RecruitBaseStatisticsService recruitBaseStatisticsService;
    @Autowired
    private RecruitStatisticalTransformationService recruitStatisticalTransformationService;
    @Autowired
    private InterviewProcessService interviewProcessService;
    @Autowired
    private JobInfoService jobInfoService;
    @Autowired
    private InterviewProgressTimeService interviewProgressTimeService;
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private LogHelper logHelper;

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#queryMaxCode()
     */
    @Override
    public String queryMaxCode() {
        return recruitInterviewManagementMapper.queryMaxCode();
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#queryByPhone(java.lang.String, java.lang.String)
     */
    @Override
    public RecruitInterviewManagement queryByPhone(String phone, String code) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("phoneNumber", phone);
        if (StringUtils.isNotBlank(code)) {
            param.put("interviewcode", code);
        }
        return recruitInterviewManagementMapper.queryByPhoneIsNotCode(param);
    }

    @Override
    public RecruitInterviewManagement queryInterviewManagentByPhone(String phone) {
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementMapper.queryInterviewManagentByPhone(phone);

        /**
         * 发送消息通知 : 初试准备通知
         */
        if (null != interviewManagement) {
            //查找成功,发送消息
            Notification notification = notificationMapper.selectByNode("候选人开始应聘登记");
            //解析面试官,获取名称和收件人
            Map<String, Object> map = transFirstInterview(interviewManagement.getFirstInterviewer());

            //姓名 部门 职位 来源 初试面试官 初试时间 url
            Object[] params = {
                interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                interviewManagement.getResumeSource(), (String) map.get("name"), interviewManagement.getFirstTime().substring(0,16),
                MessageFormat.format(notification.getUrl(), interviewManagement.getInterviewCode())};
            //发送消息
            doSengNotification(notification, params, (List<ReceiveAccount>) map.get("receiveAccountList"));
        }

        return interviewManagement;
    }

    /**
     * 根据传入的 面试官字符串 : wuyicheng:武宜城;xudou:徐豆  获取 中文名称 和 邮件收件人
     *
     * @param firstInterview
     * @return
     */
    public Map<String, Object> transFirstInterview(String firstInterview) {
        Map<String, Object> tansMap = new HashMap<>();

        String firstName = "";
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();
        String[] split = firstInterview.split(";");
        for (String name : split) {
            firstName = "," + name.split(":")[1];

            ReceiveAccount receiveAccount = new ReceiveAccount();
            receiveAccount.setReceiveMsgAccount(name.split(":")[0] + "@sigo.cn");
            receiveAccount.setReceiveMsgAccountName(name.split(":")[1]);
            receiveAccountList.add(receiveAccount);
        }

        tansMap.put("name", firstName.substring(1));
        tansMap.put("receiveAccountList", receiveAccountList);

        return tansMap;
    }


    /**
     * 发送消息
     *
     * @return
     */
    public Map<String, Object> doSengNotification(Notification notification, Object[] params, List<ReceiveAccount> receiveAccountList) {
        Map<String, Object> result = new HashMap<>();
        String content = MessageFormat.format(notification.getContent(), params);

        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgContent(content);
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setReceiveAccountList(receiveAccountList);

        SendNotificationResult sendNotificationResult = sendNotificationFeign.messageNotify(sendNotification);

        if (null != sendNotificationResult && sendNotificationResult.getCode().equals("success")) {
            //消息发送成功
            logHelper.LoggerSendINFO("sendNotification success");
            result.put("code", "success");
        } else {
            //消息发送失败
            logHelper.LoggerSendERROR("sendNotification error : 调用 sendNotification messageNotify 接口失败" + sendNotificationResult.getMessage());
            result.put("code", sendNotificationResult.getCode());
            result.put("message", sendNotificationResult.getMessage());
        }

        return result;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#saveRecruitInterview(cn.sigo.recruit.model.
     * RecruitInterviewManagement)
     */
    @Override
    public RecruitInterviewManagement saveRecruitInterview(RecruitInterviewManagement recruitInterview) {
        synchronized (this) {
            String maxRecuitCode = this.queryMaxCode();
            String nextRecuitCode = GenerateCode.queryNextMaxCode(maxRecuitCode, "MS");
            recruitInterview.setInterviewCode(nextRecuitCode);
            // 设置添加时间
            Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
            recruitInterview.setAddTime(date);
            int result = recruitInterviewManagementMapper.insertSelective(recruitInterview);
            if (result > 0) {
                return recruitInterview;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#modifyJobInfo(cn.sigo.recruit.model.
     * RecruitInterviewManagement)
     */
    @Override
    public boolean modifyRecruitInterview(RecruitInterviewManagement recruitInterview) {
        Date date = DateUtils.nowByDate(DateUtils.YYYY_MM_DD_HH_MM_SS);
        recruitInterview.setAddTime(null);
        recruitInterview.setModTime(date);
        return recruitInterviewManagementMapper.updateByPrimaryKeySelective(recruitInterview) > 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#queryInterviews()
     */
    @Override
    public List<RecruitInterviewPeople> queryInterviews() {
        return recruitInterviewManagementMapper.queryInterviews("待应聘登记");
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#queryInterviewManagentByCode(java.lang.String)
     */
    @Override
    public RecruitInterviewManagement queryInterviewManagentByCode(String interviewCode) {
        return recruitInterviewManagementMapper.queryInterviewManagentByCode(interviewCode);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#queryInterviewManagents(cn.sigo.recruit.model.req.
     * ReqInterviewManagementModel)
     */
    @Override
    public PageInfo<RecruitInterviewManagement> queryInterviewManagents(ReqInterviewManagementModel reqModel,
                                                                        String userId, String backendRole) {
        if (StringUtils.isNotBlank(reqModel.getCodeNumber())) {
            String codeNumber = reqModel.getCodeNumber().toUpperCase();
            if (codeNumber.startsWith("MS")) {
                reqModel.setInterviewCode(codeNumber);
            } else if (codeNumber.startsWith("ZP")) {
                reqModel.setJobCode(codeNumber);
            } else {
                reqModel.setInterviewCode(codeNumber);
                reqModel.setJobCode(codeNumber);
            }
        }
        // 当前系统登录用户 和 查询的申请人不一致，且用户是普通员工角色 这个时候，是不需要查询数据库的
        if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
            && backendRole.indexOf("recruit-系统管理员") < 0) {
            reqModel.setUserId(userId);
        }
        // 面试管理：支持按初试时间、复试时间排序  (默认时间 为 add_time desc)
        String orderBy = null;
        if (StringUtils.isNotBlank(reqModel.getField()) && StringUtils.isNotBlank(reqModel.getSort())) {
            orderBy = reqModel.getField() + " " + reqModel.getSort();
        }
        // 保持默认值
        if (StringUtils.isBlank(reqModel.getField()) || StringUtils.isBlank(reqModel.getSort())) {
            orderBy = "add_time DESC";
        }
        PageHelper.startPage(reqModel.getPageNum(), reqModel.getPageSize(), orderBy);
        List<RecruitInterviewManagement> interviewManagements = recruitInterviewManagementMapper
            .queryInterviewManagents(reqModel);
        return new PageInfo<RecruitInterviewManagement>(interviewManagements);
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#modifyInterviewStatus(java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean modifyInterviewStatus(String interviewCode, String status) {
        if (StringUtils.isNotBlank(status) && StringUtils.isNotBlank(interviewCode)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("interviewCode", interviewCode);
            param.put("status", status);

            Boolean modResult = recruitInterviewManagementMapper.modifyInterviewStatusByCode(param) > 0;

            //状态修改 成功后 发送消息
            if (modResult) {

                RecruitInterviewManagement interviewManagement = recruitInterviewManagementMapper.queryInterviewManagentByCode(interviewCode);
                RecruitInterviewManagementResult interviewManagementResult = interviewManagementResultMapper.queryResultForManagementId(interviewManagement.getId());

                String firstName = "";
                String[] split = interviewManagement.getFirstInterviewer().split(";");
                for (String name : split) {
                    firstName = "," + name.split(":")[1];
                }
                firstName = firstName.substring(1);

                //收件人
                List<ReceiveAccount> receiveAccountList = new ArrayList<>();
                UMSRole umsRole = new UMSRole();
                umsRole.setRoleName("recruit-人事员工");
                UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
                if (null != umsNewResponseModel && umsNewResponseModel.getCode().equals("success")) {
                    //调用 UMS 根据角色名查询用户成功
                    List<Map<String, String>> umsUserList = (List<Map<String, String>>) umsNewResponseModel.getData();
                    if (!umsUserList.isEmpty()) {
                        //转换成功
                        for (Map<String, String> umsUser : umsUserList) {
                            ReceiveAccount receiveAccount = new ReceiveAccount();
                            receiveAccount.setReceiveMsgAccount(umsUser.get("userName") + "@sigo.cn");
                            receiveAccount.setReceiveMsgAccountName(umsUser.get("reallyName"));
                            receiveAccountList.add(receiveAccount);
                        }
                    }
                } else {
                    //失败 , 调用UMS 根据角色名查询用户失败
                    logHelper.LoggerSendERROR("修改状态初试待定,复试待定时, 调用 UMS 通过角色名 获取 用户接口失败,没有获取到收件人");
                }
                //面试 状态里 修改为 初试待定 发送消息通知
                if ("初试待定".equals(status) || "复试待定".equals(status)) {

                    Notification notification = null;
                    String content = "";
                    String url = "";
                    Object[] params = null;

                    if ("初试待定".equals(status)) {
                        notification = notificationMapper.selectByNode("初试待定");
                        url = MessageFormat.format(notification.getUrl(), interviewCode);
                        content = notification.getContent();
                        //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.url
                        params = new Object[]{
                            interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                            interviewManagement.getResumeSource(), firstName, interviewManagement.getFirstTime(),
                            interviewManagementResult.getFirstFeedback(), url};
                        content = MessageFormat.format(content, params);


                    } else if ("复试待定".equals(status)) {
                        String secondInterviewer = interviewManagement.getSecondInterviewer();
                        String secondName = "";
                        String[] secondSplit = secondInterviewer.split(";");
                        for (String name : secondSplit) {
                            secondName = "," + name.split(":")[1];
                        }
                        secondName = secondName.substring(1);
                        notification = notificationMapper.selectByNode("复试待定");
                        url = MessageFormat.format(notification.getUrl(), interviewCode);

                        //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.复试时间.9.复试反馈.url
                        params = new Object[]{
                            interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                            interviewManagement.getResumeSource(), firstName, interviewManagement.getFirstTime().substring(0,16),
                            interviewManagementResult.getFirstRemarks(), secondName, interviewManagement.getSecondTime().substring(0,16),
                            interviewManagementResult.getSecondRemarks(), url};
                        content = MessageFormat.format(notification.getContent(), params);

                    }

                    //发送邮件需要的参数
                    SendNotification sendNotification = new SendNotification();
                    sendNotification.setMsgContent(content);
                    sendNotification.setMsgTitle(notification.getTitle());
                    sendNotification.setReceiveAccountList(receiveAccountList);
                    SendNotificationResult sendNotificationResult = sendNotificationFeign.messageNotify(sendNotification);

                    if (null != sendNotificationResult && sendNotificationResult.getCode().equals("success")) {
                        //消息发送成功
                        logHelper.LoggerSendINFO("初试待定状态 发消息成功");
                    } else {
                        //消息发送失败
                        logHelper.LoggerSendERROR("初试待定状态 发消息失败");
                    }
                }
            }

            return modResult;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#modifyInterviewStatus(java.lang.String,
     * java.lang.String)
     */
    @Override
    public boolean modifyInterviewStatus(String interviewCode, String status, String secondInterviewer) {
        if (StringUtils.isNotBlank(status) && StringUtils.isNotBlank(interviewCode)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("interviewCode", interviewCode);
            param.put("status", status);
            if (StringUtils.isNotBlank(secondInterviewer)) {
                param.put("secondInterviewer", secondInterviewer);
            }
            return recruitInterviewManagementMapper.modifyInterviewStatusByCode(param) > 0;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#modifyInterviewProcId(java.lang.String,
     * java.lang.Long)
     */
    @Override
    public boolean modifyInterviewProcId(String procId, Integer interviewId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("procId", procId);
        param.put("interviewId", interviewId);
        return recruitInterviewManagementMapper.modifyInterviewProcId(param) > 0;
    }

    /*
     * (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#saveInterviewManagement(cn.sigo.recruit.model.
     * RecruitInterviewManagement)
     */
    @Override
    @Transactional
    public ResponseModel saveInterviewManagement(RecruitInterviewManagement recruit, String interviewerCode) {
        ResponseModel model = new ResponseModel();
        if (StringUtils.isBlank(recruit.getJobCode())) {
            model.setResultCode("0306");
            model.setResultMsg("请去招聘申请管理申请该职位的招聘");
            model.setData(null);
            return model;
        }
        if (StringUtils.isNotBlank(recruit.getJobCode())) {
            RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(recruit.getJobCode());
            if (null != jobInfo) {
                // 不能创建面试单
                if (jobInfo.getJobStatus().equals("招聘完成") || jobInfo.getJobStatus().equals("招聘关闭") || jobInfo.getJobStatus().equals("招聘暂停")
                    || jobInfo.getJobStatus().equals("审核拒绝")) {
                    model.setResultCode("0301");
                    model.setResultMsg("当前职位不存在有效招聘申请单，不可创建");
                    model.setData(null);
                    return model;
                }
            } else {
                model.setResultCode("0306");
                model.setResultMsg("请去招聘申请管理申请该职位的招聘");
                model.setData(null);
                return model;
            }
        }
        if (StringUtils.isNotBlank(recruit.getApplyPhoneNumber())) {
            // 确认面人人员的状态
            if (recruit.getInterviewStatus().equals("保存")) {
                recruit.setInterviewStatus("已创建");
            } else if (recruit.getInterviewStatus().equals("提交")) {
                recruit.setInterviewStatus("待应聘登记");
                recruit.setActivitiProcdId(InitializationBean.activitiMap.get("面试流程").toString());
            } else {
                recruit.setInterviewStatus("已作废");
            }
            if (StringUtils.isBlank(interviewerCode) || interviewerCode.equals("null")) {
                RecruitInterviewManagement queryRecruit = this.queryByPhone(recruit.getApplyPhoneNumber(), null);
                if (null != queryRecruit) {
                    model.setResultCode("0303");
                    model.setResultMsg("当前求职者已存在相关面试单，不可重复创建");
                    return model;
                } else {
                    // 获取erp的userId
                    RecruitInterviewManagement result = this.saveRecruitInterview(recruit);
                    if (null != result) {
                        recruit.setId(result.getId());
                        recruit.setInterviewCode(result.getInterviewCode());
                        model.setResultCode("0000");
                        model.setResultMsg("");
                    } else {
                        model.setResultCode("0002");
                        model.setResultMsg("异常！数据入库失败");
                    }
                }
            } else {
                // 修改
                RecruitInterviewManagement queryRecruit = this.queryByPhone(recruit.getApplyPhoneNumber(),
                    interviewerCode);
                if (null != queryRecruit) {
                    model.setResultCode("0303");
                    model.setResultMsg("当前求职者已存在相关面试单，不可重复创建");
                    return model;
                } else {
                    RecruitInterviewManagement qRecruit = this.queryInterviewManagentByCode(interviewerCode);
                    if (null != qRecruit) {
                        recruit.setId(qRecruit.getId());
                    }
                    recruit.setInterviewCode(interviewerCode);
                    boolean result = this.modifyRecruitInterview(recruit);
                    if (result) {
                        model.setResultCode("0000");
                        model.setResultMsg("");
                    } else {
                        model.setResultCode("0002");
                        model.setResultMsg("异常！数据入库失败");
                        return model;
                    }
                }
            }
            // 启动面试流程
            if (recruit.getInterviewStatus().equals("待应聘登记")) {
                // 保存或更新统计基本信息表
                recruitBaseStatisticsService.saveOrUpdateBaseStatistics(recruit.getJobCode());
                // 保存活更新转化统计表
                recruitStatisticalTransformationService.saveOrUpdateStatisticalTransformation(recruit.getJobCode());
                // 启动面试流程
                String procId = interviewProcessService.startInterviewProcess(recruit);
                // 更新 将流程启动id 更新到面试管理表里面
                boolean result = this.modifyInterviewProcId(procId, recruit.getId());
                // 新增记录
                RecruitInterviewProgress recruitInterviewProgress = new RecruitInterviewProgress();
                recruitInterviewProgress.setFirstInterview(DateUtils.stringToDate(recruit.getFirstTime()));
                recruitInterviewProgress.setInterviewCode(recruit.getInterviewCode());
                interviewProgressTimeService.insertInterviewProgress(recruitInterviewProgress);
                if (result) {
                    model.setResultCode("0000");
                    model.setResultMsg("");
                } else {
                    LOGGER.error("面试管理id：" + recruit.getId() + "，启动面试流程实例id为：" + procId + "。保存数据库异常。。。。");
                    model.setResultCode("0002");
                    model.setResultMsg("异常！数据入库失败");
                }
            }
        } else {
            model.setResultCode("0302");
            model.setResultMsg("手机号码不能为空");
        }
        return model;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitInterviewManagementService#modifyOfferAuitTime(java.lang.String)
     */
    @Override
    public boolean modifyOfferAuitTime(String interviewerCode) {
        return recruitInterviewManagementMapper.modifyOfferAuitTime(interviewerCode) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean modifySecondInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime) {
        try {
            boolean result = recruitInterviewManagementMapper.modifySecondInterviewTime(reqModifyInterviewTime) > 0;

            RecruitInterviewManagement interviewManagement = recruitInterviewManagementMapper.queryInterviewManagentByCode(reqModifyInterviewTime.getInterviewCode());

            RecruitInterviewManagementResult interviewManagementResult = interviewManagementResultMapper.queryResultForManagementId(interviewManagement.getId());

            //待安排复试状态,复试时间安排完成,发送消息
            Notification notification = notificationMapper.selectByNode("复试时间安排完成");

            Map<String, Object> firstInterviewmap = transFirstInterview(interviewManagement.getFirstInterviewer());
            Map<String, Object> secondInterviewmap = transFirstInterview(interviewManagement.getSecondInterviewer());

            //姓名  部门  职位  来源 初试面试官  初试时间 初试反馈 复试面试官  复试时间 url
            Object[] params = {
                interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                interviewManagement.getResumeSource(), firstInterviewmap.get("name"), interviewManagement.getFirstTime().substring(0,16),
                interviewManagementResult.getFirstRemarks(), secondInterviewmap.get("name"), reqModifyInterviewTime.getSecondTime().substring(0,16),
                MessageFormat.format(notification.getUrl(), reqModifyInterviewTime.getInterviewCode())
            };

            List<ReceiveAccount> receiveAccountList = (List<ReceiveAccount>) secondInterviewmap.get("receiveAccountList");


            doSengNotification(notification, params, receiveAccountList);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            return false;
        }
    }

    @Override
    public Boolean updateInterviewTime(ReqModifyInterviewTime reqModifyInterviewTime) {
        try {
            RecruitInterviewManagement interviewManagement = recruitInterviewManagementMapper.queryInterviewManagentByCode(reqModifyInterviewTime.getInterviewCode());
            String oldFirstTime = interviewManagement.getFirstTime();
            String oldSecondTime = interviewManagement.getSecondTime();
            RecruitInterviewManagementResult interviewManagementResult = interviewManagementResultMapper.queryResultForManagementId(interviewManagement.getId());

            Boolean result = recruitInterviewManagementMapper.updateInterviewTime(reqModifyInterviewTime) > 0;

            if (result) {
                String url = "";
                String content = "";
                Object[] params = null;
                Notification notification = null;
                List<ReceiveAccount> receiveAccountList = new ArrayList<>();

                String firstName = "";
                String[] split = interviewManagement.getFirstInterviewer().split(";");
                for (String name : split) {
                    firstName = "," + name.split(":")[1];
                    ReceiveAccount receiveAccount = new ReceiveAccount();
                    receiveAccount.setReceiveMsgAccount(name.split(":")[0] + "@sigo.cn");
                    receiveAccount.setReceiveMsgAccountName(name.split(":")[1]);
                    receiveAccountList.add(receiveAccount);
                }
                firstName = firstName.substring(1);

                if (StringUtils.isNotBlank(reqModifyInterviewTime.getFirstTime())) {
                    //修改初试时间, 发送初试时间更改通知
                    notification = notificationMapper.selectByNode("更改初试时间");

                    url = MessageFormat.format(notification.getUrl(), reqModifyInterviewTime.getInterviewCode());

                    //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.原初试时间.6.现初试时间.7.url
                    params = new Object[]{
                        interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                        interviewManagement.getResumeSource(), firstName, oldFirstTime.substring(0, 16),
                        reqModifyInterviewTime.getFirstTime().substring(0, 16), url};
                    content = MessageFormat.format(notification.getContent(), params);


                } else if (StringUtils.isNotBlank(reqModifyInterviewTime.getSecondTime())) {
                    //修改复试时间, 发送复试时间更改通知
                    notification = notificationMapper.selectByNode("更改复试时间");

                    url = MessageFormat.format(notification.getUrl(), reqModifyInterviewTime.getInterviewCode());

                    String secondInterviewer = interviewManagement.getSecondInterviewer();
                    String secondName = "无";
                    String[] secondSplit = secondInterviewer.split(";");
                    receiveAccountList.clear();
                    for (String secondSplitName : secondSplit) {
                        secondName = "," + secondSplitName.split(":")[1];
                        ReceiveAccount receiveAccount = new ReceiveAccount();
                        receiveAccount.setReceiveMsgAccountName(secondSplitName.split(":")[1]);
                        receiveAccount.setReceiveMsgAccount(secondSplitName.split(":")[0] + "@sigo.cn");
                        receiveAccountList.add(receiveAccount);
                    }
                    secondName = secondName.substring(1);
                    //填充占位符 参数 0: 应聘人姓名,1.应聘人部门.2.职位.3.来源,4.初试面试官.5.初试时间.6.初试反馈.7.复试面试官.8.原复试时间.9.现复试时间.10.url
                    params = new Object[]{
                        interviewManagement.getApplyName(), interviewManagement.getApplyDept(), interviewManagement.getApplyJobName(),
                        interviewManagement.getResumeSource(), firstName, interviewManagement.getFirstTime().substring(0, 16),
                        interviewManagementResult.getFirstRemarks(), secondName, oldSecondTime.substring(0, 16),
                        reqModifyInterviewTime.getSecondTime().substring(0, 16), url};
                    content = MessageFormat.format(notification.getContent(), params);

                }

                SendNotification sendNotification = new SendNotification();
                sendNotification.setMsgContent(content);
                sendNotification.setMsgTitle(notification.getTitle());
                sendNotification.setReceiveAccountList(receiveAccountList);

                SendNotificationResult sendNotificationResult = sendNotificationFeign.messageNotify(sendNotification);
                if (null != sendNotificationResult && sendNotificationResult.getCode().equals("success")) {
                    //消息发送成功
                    logHelper.LoggerSendINFO(notification.getTitle() + "消息发送成功");
                } else {
                    //消息发送失败
                    logHelper.LoggerSendERROR(notification.getTitle() + "消息发送失败");
                }

            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            return false;
        }
    }

}
