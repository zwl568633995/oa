/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: StatisticalTransformationServiceImpl.java
 * Author:   zhixiang.meng
 * Date:     2018年8月14日 上午9:29:31
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.sigo.recruit.model.ResStatisticalTransformation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sigo.recruit.mapper.RecruitStatisticalTransformationMapper;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.RecruitStatisticalTransformation;
import cn.sigo.recruit.service.JobInfoService;
import cn.sigo.recruit.service.RecruitInterviewManagementService;
import cn.sigo.recruit.service.RecruitStatisticalTransformationService;
import cn.sigo.recruit.util.DateUtils;

/**
 *
 * @author zhixiang.meng
 */
@Service("statisticalTransformationService")
public class RecruitStatisticalTransformationServiceImpl implements RecruitStatisticalTransformationService {
    
    @Autowired
    private RecruitStatisticalTransformationMapper recruitStatisticalTransformationMapper;
    @Autowired
    private JobInfoService jobInfoService;
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;
    

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.StatisticalTransformationService#saveOrUpdateStatisticalTransformation(java.lang.String)
     */
    @Override
    public boolean saveOrUpdateStatisticalTransformation(String jobCode) {
        if(StringUtils.isNotBlank(jobCode)) {
            // 根据jobcode查询职位信息
            RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(jobCode);
            String jobName = jobInfo.getJobName();
            String deptName = jobInfo.getApplicationDepartment();
            Integer jobScore = jobInfo.getJobScore();
            if(null != jobInfo && StringUtils.isNotBlank(jobName)
                    && StringUtils.isNotBlank(deptName) && null != jobScore) {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put("jobName", jobName);
                param.put("deptName", deptName);
                param.put("jobScore", jobScore);
                String month = DateUtils.currentMonth();
                param.put("statisticsMonth", month);
                RecruitStatisticalTransformation statisticalTransformation = this.queryStatisticalTransformation(param);
                if(null != statisticalTransformation) {
                    param.clear();
                    param.put("id", statisticalTransformation.getId());
                    param.put("numberOfVacancies", "1");
                    this.updateBaseStatisticalTransformation(param);
                } else {
                    RecruitStatisticalTransformation beans = new RecruitStatisticalTransformation();
                    beans.setDeptName(deptName);
                    beans.setJobScore(jobScore);
                    beans.setJobName(jobName);
                    beans.setStatisticsMonth(month);
                    beans.setNumberOfVacancies(1);
                    this.saveBaseStatisticalTransformation(beans);
                }
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.StatisticalTransformationService#queryBaseStatistics(java.util.Map)
     */
    @Override
    public RecruitStatisticalTransformation queryStatisticalTransformation(Map<String, Object> param) {
        return recruitStatisticalTransformationMapper.queryStatisticalTransformation(param);
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.StatisticalTransformationService#saveBaseStatisticalTransformation(cn.sigo.recruit.model.RecruitStatisticalTransformation)
     */
    @Override
    public boolean saveBaseStatisticalTransformation(RecruitStatisticalTransformation statisticalTransformation) {
        return recruitStatisticalTransformationMapper.insertSelective(statisticalTransformation) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitStatisticalTransformationService#updateBaseStatisticalTransformation(java.util.Map)
     */
    @Override
    public boolean updateBaseStatisticalTransformation(Map<String, Object> param) {
        return recruitStatisticalTransformationMapper.updateBaseStatisticalTransformation(param) > 0;
    }

    /* (non-Javadoc)
     * @see cn.sigo.recruit.service.RecruitStatisticalTransformationService#updateBaseStatisticalTransformation(java.lang.String, java.lang.String)
     */
    @Override
    public boolean updateBaseStatisticalTransformation(String interviewCode, String status) {
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService
                .queryInterviewManagentByCode(interviewCode);
        Date date = interviewManagement.getAddTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        String month = format.format(date);
        // 根据面试单信息查询
        RecruitJobInfo jobInfo = jobInfoService.queryByJobCode(interviewManagement.getJobCode());
        
        if(null != jobInfo && StringUtils.isNotBlank(jobInfo.getJobName())
                && StringUtils.isNotBlank(jobInfo.getApplicationDepartment()) && null != jobInfo.getJobScore()) {
            String jobName = jobInfo.getJobName();
            String deptName = jobInfo.getApplicationDepartment();
            Integer jobScore = jobInfo.getJobScore();
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("jobName", jobName);
            param.put("deptName", deptName);
            param.put("jobScore", jobScore);
            param.put("statisticsMonth", month);
            RecruitStatisticalTransformation statisticalTransformation = this.queryStatisticalTransformation(param);
            if(null != statisticalTransformation) {
                param.clear();
                param.put("id", statisticalTransformation.getId());
                if(status.equals("interviewNumber")) {
                    // 面试通过
                    param.put("interviewNumber", "1");
                } else if(status.equals("numberOfOffer")) {
                    // 发送offer
                    param.put("numberOfOffer", "1");
                } else if(status.equals("numberOfRecruits")) {
                    // 已入职
                    param.put("numberOfRecruits", "1");
                }
                this.updateBaseStatisticalTransformation(param);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ResStatisticalTransformation> departmentRecruitTransformation(String yearMonth) {
        return recruitStatisticalTransformationMapper.departmentRecruitTransformation(yearMonth);
    }

    @Override
    public List<ResStatisticalTransformation> positionRecruitTransformation(String yearMonth, String department) {
        Map<String,Object> param = new HashMap<>();
        param.put("yearMonth",yearMonth);
        param.put("department",department);
        return recruitStatisticalTransformationMapper.positionRecruitTransformation(param);
    }
}
