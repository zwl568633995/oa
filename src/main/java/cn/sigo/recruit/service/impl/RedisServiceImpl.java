package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.service.RedisService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.instanceOf;

@Service
public class RedisServiceImpl implements RedisService {

	@Autowired
	private RedisTemplate redisTemplate;

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#set(java.lang.String, java.lang.Object)
	 */
	@Override
	public boolean set(final String key, Object value) {
		boolean result = false;
		try {
			ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
			if (isBasisType(value)) {
				operations.set(key, value);
			} else {
				operations.set(key, JSONObject.fromObject(value).toString());
			}
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#set(java.lang.String, java.lang.Object, java.lang.Long)
	 */
	@Override
	public boolean set(final String key, Object value, Long expireTime) {
		boolean result = false;
		try {
			ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
			if (isBasisType(value)) {
				operations.set(key, value);
			} else {
				operations.set(key, JSONObject.fromObject(value).toString());
			}
			redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#remove(java.lang.String[])
	 */
	@Override
	public void remove(final String... keys) {
		for (String key : keys) {
			remove(key);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#removePattern(java.lang.String)
	 */
	@Override
	public void removePattern(final String pattern) {
		Set<Serializable> keys = redisTemplate.keys(pattern);
		if (keys.size() > 0)
			redisTemplate.delete(keys);
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#remove(java.lang.String)
	 */
	@Override
	public void remove(final String key) {
		if (exists(key)) {
			redisTemplate.delete(key);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#exists(java.lang.String)
	 */
	@Override
	public boolean exists(final String key) {
        Boolean aBoolean = redisTemplate.hasKey(key);
        return aBoolean;
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#get(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object get(final String key, Class clazz) {
		Object result = null;
		ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
		if(isBasisType(clazz)) {
			return operations.get(key);
		} else {
//		    Object ookey = key;
//            Object o = operations.get(ookey);
            result = operations.get(key);
			JSONObject obj = new JSONObject().fromObject(result);
			return (Object) JSONObject.toBean(obj, clazz);
		}
	}

	@Override
	public void hmSet(String key, Object hashKey, Object value) {
		HashOperations<String, Object, Object> hash = redisTemplate.opsForHash();
		hash.put(key, hashKey, value);
	}

	@Override
	public Object hmGet(String key, Object hashKey) {
		HashOperations<String, Object, Object> hash = redisTemplate.opsForHash();
		return hash.get(key, hashKey);
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#push(java.lang.String, java.util.List)
	 */
	@Override
	public void push(String k, List<Object> objs) {
		ListOperations<String, Object> list = redisTemplate.opsForList();
		if(isBasisType(objs.get(0))) {
			for (Object obj : objs) {
				list.rightPush(k, obj);
			}
		} else {
			for (Object obj : objs) {
				list.rightPush(k, JSONObject.fromObject(obj).toString());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.demo.service.RedisService#range(java.lang.String, long, long, java.lang.Class)
	 */
	@Override
	public List<Object> range(String k, long l, long l1, Class clazz) {
		ListOperations<String, Object> list = redisTemplate.opsForList();
		List<Object> objs = list.range(k, l, l1);
		List<Object> resultObjs = new ArrayList<Object>();
		if (null != objs && !objs.isEmpty()) {
			if(isBasisType(clazz)) {
				for (Object obj : objs) {
					resultObjs.add(obj);
				}
			} else {
				for (Object obj : objs) {
					JSONObject jsonObj = new JSONObject().fromObject(obj);
					Object resultObj = (Object) JSONObject.toBean(jsonObj, clazz);
					resultObjs.add(resultObj);
				}
			}
		}
		return resultObjs;
	}

	@Override
	public void add(String key, Object value) {
		SetOperations<String, Object> set = redisTemplate.opsForSet();
		set.add(key, value);
	}

	@Override
	public Set<Object> setMembers(String key) {
		SetOperations<String, Object> set = redisTemplate.opsForSet();
		return set.members(key);
	}

	@Override
	public void zAdd(String key, Object value, double scoure) {
		ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
		zset.add(key, value, scoure);
	}

	@Override
	public Set<Object> rangeByScore(String key, double scoure, double scoure1) {
		ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
		return zset.rangeByScore(key, scoure, scoure1);
	}

	/**
	 * 判断传值的object 是否是基本类型 或 String 类型
	 * 
	 * @param obj
	 * @return true：是(八种基本类型 或 String);false:不是(八种基本类型 或 String)
	 */
	private boolean isBasisType(Object obj) {
		return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Character)
				|| (obj instanceof Integer) || (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Long)
				|| (obj instanceof Float) || (obj instanceof Double);
	}

	/**
	 * 判断传值的class 是否是基本类型 或 String 类型
	 * 
	 * @param clazz
	 * @return true：是(八种基本类型 或 String);false:不是(八种基本类型 或 String)
	 */
	private boolean isBasisType(Class clazz) {
		return (instanceOf(clazz).toString().indexOf("String") > 0) || (instanceOf(clazz).toString().indexOf("Boolean") > 0)
				|| (instanceOf(clazz).toString().indexOf("Character") > 0)
				|| (instanceOf(clazz).toString().indexOf("Integer") > 0)
				|| (instanceOf(clazz).toString().indexOf("Byte") > 0)
				|| (instanceOf(clazz).toString().indexOf("Short") > 0)
				|| (instanceOf(clazz).toString().indexOf("Long") > 0)
				|| (instanceOf(clazz).toString().indexOf("Float") > 0 || (instanceOf(clazz).toString().indexOf("Double") > 0));
	}
}
