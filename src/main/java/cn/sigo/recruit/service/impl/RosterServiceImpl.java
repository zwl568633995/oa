package cn.sigo.recruit.service.impl;

import cn.sigo.recruit.InitializationBean;
import cn.sigo.recruit.config.UrlUmsConfig;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.*;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.req.ReqRosterEmployee;
import cn.sigo.recruit.model.req.ReqRosterPageQuery;
import cn.sigo.recruit.model.response.RespUMSUpdateUser;
import cn.sigo.recruit.model.response.ResponseToUmsSaveUser;
import cn.sigo.recruit.model.response.UMSResponseModel;
import cn.sigo.recruit.model.response.UMSUser;
import cn.sigo.recruit.service.RosterService;
import cn.sigo.recruit.util.DateUtils;
import cn.sigo.recruit.util.HttpClientUtils;
import cn.sigo.recruit.util.LogHelper;
import cn.sigo.recruit.util.PinyinUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Auther: DELL
 * @Date: 2018/8/6 09:29
 * @Description: 花名册
 */
@Service("rosterService")
public class RosterServiceImpl implements RosterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RosterServiceImpl.class);

    @Autowired
    private RecruitIncumbencyUserBaseInfoMapper recruitIncumbencyUserBaseInfoMapper;
    @Autowired
    private RecruitIncumbencyHistoryMapper recruitIncumbencyHistoryMapper;
    @Autowired
    private RecruitIncumbencyCertificateMapper recruitIncumbencyCertificateMapper;
    @Autowired
    private RecruitIncumbencyContractMapper recruitIncumbencyContractMapper;
    @Autowired
    private RecruitIncumbencyEducationMapper recruitIncumbencyEducationMapper;
    @Autowired
    private RecruitIncumbencyEnclosureMapper recruitIncumbencyEnclosureMapper;
    @Autowired
    private RecruitIncumbencyFamilyMemberMapper recruitIncumbencyFamilyMemberMapper;
    @Autowired
    private RecruitIncumbencyInfoMapper recruitIncumbencyInfoMapper;
    @Autowired
    private RecruitIncumbencySecurityCardMapper recruitIncumbencySecurityCardMapper;
    @Autowired
    private RecruitIncumbencyWorkMapper recruitIncumbencyWorkMapper;
    @Autowired
    private UrlUmsConfig urlUmsConfig;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private UMSFeign umsFeign;

    @Override
    public PageInfo<ResponseRosterPageQuery> pageQueryRosterList(ReqRosterPageQuery reqRosterPageQuery, String userName, String backendRole) {
        List<ResponseRosterPageQuery> result = new ArrayList<>();
        // 判断是否为 普通用户
        if (backendRole.indexOf("recruit-普通员工") >= 0 && backendRole.indexOf("recruit-人事员工") < 0
            && backendRole.indexOf("recruit-系统管理员") < 0) {
            reqRosterPageQuery.setCreatePerson(userName);
            if (null != reqRosterPageQuery.getEntryRegisterName()) {
                if (!userName.equals(reqRosterPageQuery.getEntryRegisterName())) {
                    return new PageInfo<>(result);
                }
            }
        }

        //创建时间的判断
        if (null != reqRosterPageQuery.getStartCreateTime() && null != reqRosterPageQuery.getEndCreateTime()) {
            if (10 == reqRosterPageQuery.getStartCreateTime().length() && 10 == reqRosterPageQuery.getEndCreateTime().length()) {
                reqRosterPageQuery.setStartCreateTime(reqRosterPageQuery.getStartCreateTime() + " 00:00:00");
                reqRosterPageQuery.setEndCreateTime(reqRosterPageQuery.getEndCreateTime() + " 23:59:59");
            }
        }
        //入职时间的判断
        if (null != reqRosterPageQuery.getTimeOfEntryStart() && null != reqRosterPageQuery.getTimeOfEntryEnd()) {
            if (10 == reqRosterPageQuery.getTimeOfEntryStart().length() && 10 == reqRosterPageQuery.getTimeOfEntryEnd().length()) {
                reqRosterPageQuery.setTimeOfEntryStart(reqRosterPageQuery.getTimeOfEntryStart() + " 00:00:00");
                reqRosterPageQuery.setTimeOfEntryEnd(reqRosterPageQuery.getTimeOfEntryEnd() + " 23:59:59");
            }
        }


        Boolean contractWarning = reqRosterPageQuery.getContractWarning();
        Integer warnDay = Integer.parseInt((String) InitializationBean.initMap.get("contract.warn.time"));
        String contractWarningTime = DateUtils.plusDay(warnDay);
        if (null != contractWarning && contractWarning) {
            reqRosterPageQuery.setContractWarningTime(contractWarningTime);
        }
        //合同预警 、在司工龄 还未处理
        PageHelper.startPage(reqRosterPageQuery.getPageNum(), reqRosterPageQuery.getPageSize());
        result = recruitIncumbencyUserBaseInfoMapper.pageQueryRosterList(reqRosterPageQuery);
        for (ResponseRosterPageQuery item : result) {
            //处理 在司工龄
            String workYear = DateUtils.getTime(new Date(), item.getTimeOfEntry());
            item.setWorkYear(workYear);
            //处理 合同预警
            Date contractDateEnd = item.getContractDateEnd();
            Date warnTime = null;
            if (null != contractDateEnd) {
                try {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    warnTime = format.parse(contractWarningTime);
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                }
                if (contractDateEnd.before(warnTime)) {
                    Long days = DateUtils.getDifferenceDay(warnTime, contractDateEnd);
                    item.setContractWarning(days.intValue());
                }
            }

        }
        return new PageInfo<ResponseRosterPageQuery>(result);
    }

    @Override
    public Integer saveIncumbencyHistory(RecruitIncumbencyHistory recruitIncumbencyHistory) {
        return recruitIncumbencyHistoryMapper.insert(recruitIncumbencyHistory);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> saveRosterEmployee(ReqRosterEmployee reqRosterEmployee, String adder) {
        //返回结果map
        Map<String, Object> resultMap = new HashMap<>();
        try {
            RecruitIncumbencyUserBaseInfo baseInfo = reqRosterEmployee.getUserBaseInfo();
            if (null == baseInfo) {
                errorMsg(resultMap, "关键信息为空");
                logHelper.LoggerSendERROR("关键信息为空");
                return resultMap;
            }
            List<RecruitIncumbencyEducation> educations = reqRosterEmployee.getEducations();
            List<RecruitIncumbencyCertificate> certificates = reqRosterEmployee.getCertificates();
            List<RecruitIncumbencyFamilyMember> familyMembers = reqRosterEmployee.getFamilyMembers();
            List<RecruitIncumbencyWork> works = reqRosterEmployee.getWorks();
            List<RecruitIncumbencyEnclosure> attach = reqRosterEmployee.getAttach();
            RecruitIncumbencyInfo info = reqRosterEmployee.getInfo();
            RecruitIncumbencyContract contract = reqRosterEmployee.getContract();
            RecruitIncumbencySecurityCard card = reqRosterEmployee.getCard();
            //执行插入
            baseInfo.setAdder(adder);
            //主表增加合同到期时间
            String contractDateEnd = contract.getContractDateEnd();
            //baseInfo.setContractDateEnd(contractDateEnd);
            //插入基本信息
            RecruitIncumbencyUserBaseInfo baseInfoResult = this.saveUserBaseInfo(baseInfo);
            Integer incumbencyId = baseInfoResult.getId();
            info.setIncumbencyId(incumbencyId);
            contract.setIncumbencyId(incumbencyId);
            card.setIncumbencyId(incumbencyId);
            //教育经历
            if (!CollectionUtils.isEmpty(educations)) {
                this.saveEducations(educations, adder, incumbencyId);
            }

            if (!CollectionUtils.isEmpty(certificates)) {
                this.saveCertificates(certificates, adder, incumbencyId);
            }
            //家庭成员
            if (!CollectionUtils.isEmpty(familyMembers)) {
                this.saveFamilyMembers(familyMembers, adder, incumbencyId);
            }
            //工作经历
            if (!CollectionUtils.isEmpty(works)) {
                this.saveWorks(works, adder, incumbencyId);
            }

            if (!CollectionUtils.isEmpty(attach)) {
                this.saveAttach(attach, adder, incumbencyId);
            }
            recruitIncumbencyInfoMapper.insert(info);
            recruitIncumbencyContractMapper.insert(contract);
            recruitIncumbencySecurityCardMapper.insert(card);

            //提取花名册中的数据,添加UMS中用户
            ResponseToUmsSaveUser responseToUmsSaveUser = transReqRosterEmployeeToUMSSaveUser(reqRosterEmployee, adder);
            UMSResponseModel umsResponseModel = umsFeign.saveUser(responseToUmsSaveUser);

            if (null == umsResponseModel && "-1".equals(umsResponseModel.getResultCode())) {
                errorMsg(resultMap, "关联添加UMS用户失败!!");
                logHelper.LoggerSendERROR("关联添加UMS用户失败!!");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                return resultMap;
            }

            //返回成功信息
            successMsg(resultMap);
            return resultMap;

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            LOGGER.info(e.getMessage());
            logHelper.LoggerSendERROR(e.getMessage());
            errorMsg(resultMap, e.getMessage());
            return resultMap;
        }
    }

    /**
     * 提取花名册中的信息封装给UMS中saveUser接口的参数
     *
     * @param reqRosterEmployee
     * @param adder
     * @return
     */
    private ResponseToUmsSaveUser transReqRosterEmployeeToUMSSaveUser(ReqRosterEmployee reqRosterEmployee, String adder) {
        ResponseToUmsSaveUser responseToUmsSaveUser = new ResponseToUmsSaveUser();
        //基本信息
        RecruitIncumbencyUserBaseInfo userBaseInfo = reqRosterEmployee.getUserBaseInfo();

        //花名册id
        if (null != userBaseInfo.getId()) {
            responseToUmsSaveUser.setRosterId(userBaseInfo.getId());
        }

        //添加人
        responseToUmsSaveUser.setAdder(adder);
        //邮箱
        responseToUmsSaveUser.setEmail(userBaseInfo.getCompanyEmail());
        //默认密码
        responseToUmsSaveUser.setPassword("sigo@123");
        //用户类型
        responseToUmsSaveUser.setUserType("普通用户");
        //手机号码
        responseToUmsSaveUser.setPhone(userBaseInfo.getPhoneNumber());
        //真实姓名
        responseToUmsSaveUser.setReallyName(userBaseInfo.getEntryRegisterName());

        //在职的部门id
        if (reqRosterEmployee.getInfo() != null) {
            List<Long> teamIds = new ArrayList<>();
            teamIds.add(reqRosterEmployee.getInfo().getDeptId());
            responseToUmsSaveUser.setTeamIds(teamIds);
        }
        //职位
        responseToUmsSaveUser.setPosition(reqRosterEmployee.getInfo().getJobName());

        //把真实姓名转换成拼音用作用户名
        responseToUmsSaveUser.setUserName(PinyinUtil.getPingYin(userBaseInfo.getEntryRegisterName()));

        return responseToUmsSaveUser;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> updateRosterEmployee(ReqRosterEmployee reqRosterEmployee, String adder) {
        //返回结果map
        Map<String, Object> resultMap = new HashMap<>();
        try {
            RecruitIncumbencyUserBaseInfo baseInfo = reqRosterEmployee.getUserBaseInfo();
            if (null == baseInfo || null == baseInfo.getId()) {
                errorMsg(resultMap, "关键信息为空");
                return resultMap;
            }
            List<RecruitIncumbencyEducation> educations = reqRosterEmployee.getEducations();
            List<RecruitIncumbencyCertificate> certificates = reqRosterEmployee.getCertificates();
            List<RecruitIncumbencyFamilyMember> familyMembers = reqRosterEmployee.getFamilyMembers();
            List<RecruitIncumbencyWork> works = reqRosterEmployee.getWorks();
            List<RecruitIncumbencyEnclosure> attach = reqRosterEmployee.getAttach();
            RecruitIncumbencyInfo info = reqRosterEmployee.getInfo();
            RecruitIncumbencyContract contract = reqRosterEmployee.getContract();
            RecruitIncumbencySecurityCard card = reqRosterEmployee.getCard();

            baseInfo.setAdder(adder);
            //baseInfo.setModTime(new Date());
            //baseInfo.setContractDateEnd(contract.getContractDateEnd());
            //更新在职信息基本表
            int i = recruitIncumbencyUserBaseInfoMapper.updateByPrimaryKeySelective(baseInfo);

            if (1 != i) {
                //更新在职信息基本表失败
                logHelper.LoggerSendERROR("更新在职信息基本表失败");
                errorMsg(resultMap, "更新在职信息基本表失败");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                return resultMap;
            } else {
                //关联修改UMS用户
                RespUMSUpdateUser respUMSUpdateUser = transReqRosterEmployeeToUMSUpdateUser(reqRosterEmployee);
                //HR用户关联UMS用户,通过花名册id 匹配 UMS 用户
                logHelper.LoggerSendINFO("调用UMS通过花名册id查找用户接口");
                UMSUser umsUser = umsFeign.selectUserByRosterId(reqRosterEmployee.getUserBaseInfo().getId());

                if (null != umsUser && null != umsUser.getId()) {
                    logHelper.LoggerSendINFO("调用UMS通过花名册id查找用户接口成功");
                    respUMSUpdateUser.setId(umsUser.getId());

                    logHelper.LoggerSendINFO("调用UMS修改用户接口");
                    UMSResponseModel umsUpdateUserResponse = umsFeign.updateUser(respUMSUpdateUser);

                    if (null != umsUpdateUserResponse && umsUpdateUserResponse.getResultCode().equals("1")) {
                        logHelper.LoggerSendINFO("调用UMS修改用户接口成功");
                    } else {
                        logHelper.LoggerSendERROR("调用UMS修改用户接口失败!!!");
                        errorMsg(resultMap, "调用UMS修改用户接口失败!!!");
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                        return resultMap;
                    }

                } else {
                    logHelper.LoggerSendERROR("调用UMS通过花名册id查找用户接口失败!!!");
                    errorMsg(resultMap, "调用UMS通过花名册id查找用户接口失败!!!");
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
                    return resultMap;
                }
            }


            //更新在职信息表
            if (null != info && null != info.getId()) {
                info.setAdder(adder);
                //info.setModTime(new Date());
                recruitIncumbencyInfoMapper.updateByPrimaryKeySelective(info);
            }

            //更新在职合同
            if (null != contract && null != contract.getId()) {
                contract.setAdder(adder);
                contract.setModTime(new Date());
                recruitIncumbencyContractMapper.updateByPrimaryKeySelective(contract);
            }

            //更新在职社保卡银行卡
            if (null != card && null != card.getId()) {
                card.setAdder(adder);
                card.setModTime(new Date());
                recruitIncumbencySecurityCardMapper.updateByPrimaryKeySelective(card);
            }

            //更新教育信息
            if (!CollectionUtils.isEmpty(educations)) {
                for (RecruitIncumbencyEducation item : educations) {
                    if (null != item && null != item.getId()) {
                        item.setAdder(adder);
                        item.setModTime(new Date());
                        recruitIncumbencyEducationMapper.updateByPrimaryKeySelective(item);
                    }
                }
            }

            //更新证书信息
            if (!CollectionUtils.isEmpty(certificates)) {
                for (RecruitIncumbencyCertificate item : certificates) {
                    if (null != item && null != item.getId()) {
                        item.setAdder(adder);
                        item.setModTime(new Date());
                        recruitIncumbencyCertificateMapper.updateByPrimaryKeySelective(item);
                    }
                }
            }

            //更新家庭成员信息
            if (!CollectionUtils.isEmpty(familyMembers)) {
                for (RecruitIncumbencyFamilyMember item : familyMembers) {
                    if (null != item && null != item.getId()) {
                        item.setAdder(adder);
                        item.setModTime(new Date());
                        recruitIncumbencyFamilyMemberMapper.updateByPrimaryKeySelective(item);
                    }
                }
            }

            //更新工作履历信息
            if (!CollectionUtils.isEmpty(works)) {
                for (RecruitIncumbencyWork item : works) {
                    if (null != item && null != item.getId()) {
                        item.setAdder(adder);
                        item.setModTime(new Date());
                        recruitIncumbencyWorkMapper.updateByPrimaryKeySelective(item);
                    }
                }
            }

            //更新附件信息
            if (!CollectionUtils.isEmpty(attach)) {
                for (RecruitIncumbencyEnclosure item : attach) {
                    if (null != item && null != item.getId()) {
                        item.setAdder(adder);
                        item.setModTime(new Date());
                        recruitIncumbencyEnclosureMapper.updateByPrimaryKeySelective(item);
                    }
                }
            }
            //返回成功信息
            successMsg(resultMap);
            return resultMap;

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//异常手动回滚
            LOGGER.info(e.getMessage());
            logHelper.LoggerSendERROR("HR修改用户失败:" + e.getMessage());
            errorMsg(resultMap, e.getMessage());
            return resultMap;
        }
    }

    private RespUMSUpdateUser transReqRosterEmployeeToUMSUpdateUser(ReqRosterEmployee reqRosterEmployee) {
        RespUMSUpdateUser respUpdateUser = new RespUMSUpdateUser();

        //是否删除
        respUpdateUser.setDeleted(false);

        //邮箱
        if (StringUtils.isNotBlank(reqRosterEmployee.getUserBaseInfo().getCompanyEmail())) {
            respUpdateUser.setEmail(reqRosterEmployee.getUserBaseInfo().getEmail());
        }

        //手机号码
        if (StringUtils.isNotBlank(reqRosterEmployee.getUserBaseInfo().getPhoneNumber())) {
            respUpdateUser.setPhone(reqRosterEmployee.getUserBaseInfo().getPhoneNumber());
        }

        //真实姓名
        if (StringUtils.isNotBlank(reqRosterEmployee.getUserBaseInfo().getEntryRegisterName())) {
            respUpdateUser.setReallyName(reqRosterEmployee.getUserBaseInfo().getEntryRegisterName());
        }

        //团队id集合
        if (null != reqRosterEmployee.getInfo() && null != reqRosterEmployee.getInfo().getDeptId()) {
            List<Long> teamIds = new ArrayList<>();
            teamIds.add(reqRosterEmployee.getInfo().getDeptId());
            respUpdateUser.setTeamIds(teamIds);
        }

        return respUpdateUser;
    }

    public Map<String, Object> errorMsg(Map<String, Object> resultMap, String msg) {
        resultMap.put("resultCode", "0004");
        resultMap.put("result", "ERROR");
        resultMap.put("msg", msg);
        return resultMap;
    }

    public Map<String, Object> successMsg(Map<String, Object> resultMap) {
        resultMap.put("resultCode", "0000");
        resultMap.put("result", "SUCCESS");
        resultMap.put("msg", "SUCCESS");
        return resultMap;
    }

    @Override
    public Map<String, Object> queryEmployeeInfo(String incumbencyId) {
        Map<String, Object> result = new HashMap<>();
        result.put("userBaseInfo", recruitIncumbencyUserBaseInfoMapper.selectByIncumbencyId(incumbencyId));
        result.put("certificates", recruitIncumbencyCertificateMapper.selectByIncumbencyId(incumbencyId));
        result.put("educations", recruitIncumbencyEducationMapper.selectByIncumbencyId(incumbencyId));
        result.put("familyMembers", recruitIncumbencyFamilyMemberMapper.selectByIncumbencyId(incumbencyId));
        result.put("works", recruitIncumbencyWorkMapper.selectByIncumbencyId(incumbencyId));
        result.put("info", recruitIncumbencyInfoMapper.selectByIncumbencyId(incumbencyId));
        result.put("contract", recruitIncumbencyContractMapper.selectByIncumbencyId(incumbencyId));
        result.put("card", recruitIncumbencySecurityCardMapper.selectByIncumbencyId(incumbencyId));
        result.put("attach", recruitIncumbencyEnclosureMapper.selectByIncumbencyId(incumbencyId));
        result.put("history", recruitIncumbencyHistoryMapper.selectByIncumbencyId(incumbencyId));
        return result;
    }

    @Override
    public List<ExcelRosterList> exportRosterList(ReqRosterPageQuery reqRosterPageQuery) {
        Boolean contractWarning = reqRosterPageQuery.getContractWarning();
        Integer warnDay = Integer.parseInt((String) InitializationBean.initMap.get("contract.warn.time"));
        if (null != contractWarning && contractWarning) {
            reqRosterPageQuery.setContractWarningTime(DateUtils.plusDay(warnDay));
        }
        return recruitIncumbencyUserBaseInfoMapper.exportRosterList(reqRosterPageQuery);
    }

    public RecruitIncumbencyUserBaseInfo saveUserBaseInfo(RecruitIncumbencyUserBaseInfo baseInfo) {
        int result = recruitIncumbencyUserBaseInfoMapper.insertSelective(baseInfo);
        if (result > 0) {
            return baseInfo;
        }
        return null;
    }

    public void saveCertificates(List<RecruitIncumbencyCertificate> certificates, String adder, Integer incumbencyId) {
        for (RecruitIncumbencyCertificate item : certificates) {
            item.setAdder(adder);
            item.setIncumbencyId(incumbencyId);
        }
        recruitIncumbencyCertificateMapper.saveBatchRecruitIncumbencyCertificate(certificates);
    }

    public void saveEducations(List<RecruitIncumbencyEducation> educations, String adder, Integer incumbencyId) {
        for (RecruitIncumbencyEducation item : educations) {
            item.setAdder(adder);
            item.setIncumbencyId(incumbencyId);
        }
        recruitIncumbencyEducationMapper.saveBatchRecruitIncumbencyEducation(educations);
    }

    public void saveFamilyMembers(List<RecruitIncumbencyFamilyMember> familyMembers, String adder, Integer incumbencyId) {
        for (RecruitIncumbencyFamilyMember item : familyMembers) {
            item.setAdder(adder);
            item.setIncumbencyId(incumbencyId);
        }
        recruitIncumbencyFamilyMemberMapper.saveBatchRecruitIncumbencyFamilyMember(familyMembers);
    }

    public void saveWorks(List<RecruitIncumbencyWork> works, String adder, Integer incumbencyId) {
        for (RecruitIncumbencyWork item : works) {
            item.setAdder(adder);
            item.setIncumbencyId(incumbencyId);
        }
        recruitIncumbencyWorkMapper.saveBatchRecruitIncumbencyWork(works);
    }

    public void saveAttach(List<RecruitIncumbencyEnclosure> attach, String adder, Integer incumbencyId) {
        for (RecruitIncumbencyEnclosure item : attach) {
            item.setAdder(adder);
            item.setIncumbencyId(incumbencyId);
        }
        recruitIncumbencyEnclosureMapper.saveBatchRecruitIncumbencyEnclosure(attach);
    }

    /**
     * 通过真实姓名查找花名册用户
     *
     * @param reallyName
     * @return
     */
    @Override
    public RecruitIncumbencyUserBaseInfo selectRosterByReallyName(String reallyName) {

        List<RecruitIncumbencyUserBaseInfo> userBaseInfos = recruitIncumbencyUserBaseInfoMapper.selectRosterByReallyName(reallyName);

        if (null != userBaseInfos && userBaseInfos.size() > 0) {
            return userBaseInfos.get(0);
        } else {
            return null;
        }
    }
}
