package cn.sigo.recruit.util;


import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

/**
 * @Auther: DELL
 * @Date: 2018/8/7 12:53
 * @Description:
 */
public class CommonUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);
    /**
     * 返回传入时间的第二天凌晨
     *
     * @param date
     * @return
     */
    public static Date getNextDayWithoutHour(Date date) {
        return DateUtils.truncate(DateUtils.addDays(date, 1), Calendar.DATE);//第二天凌晨
    }

    /**
     * 取得下载文件名
     *
     * @param fileName
     * @param req
     * @return
     * @throws Exception
     */
    public static String getFileName(String fileName, HttpServletRequest req) throws Exception {
        String agent = getBrowserName(req.getHeader("User-Agent").toLowerCase());
        if (agent.equals("Firefox")) {
            return new String(fileName.getBytes("utf-8"), "iso-8859-1");
        } else {
            return java.net.URLEncoder.encode(fileName, "UTF-8");
        }
    }

    /**
     * 取得浏览器类型
     *
     * @param agent
     * @return
     */
    private static String getBrowserName(String agent) {
        if (agent.indexOf("firefox") > 0) {
            return "Firefox";
        } else {
            return "Others";
        }
    }


//    public static void download(String path, HttpServletResponse response) {
//        try {
//            File file = new File(path);
//            response.reset();
//            response.addHeader("content-disposition", "attachment;filename=" +
//                    new String(file.getName().replaceAll(" ", "").getBytes("gb2312"), "iso8859-1"));
//            response.addHeader("Content-Length", "" + file.length());
//            response.setContentType("application/octet-stream");
//            OutputStream out = new BufferedOutputStream(response.getOutputStream());
//            FileUtils.copyFile(file, response.getOutputStream());
//            out.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//            LOGGER.error("下载文件失败", e);
//        }
//    }
}

