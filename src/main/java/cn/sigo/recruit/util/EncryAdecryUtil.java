/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: EncryAdecryUtil.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午4:58:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 加密 解密
 *
 * @author zhixiang.meng
 */
public class EncryAdecryUtil {
    /**
     * 
     * md5加密
     *
     * @param content：加密的字符串
     * @return
     */
    public static String md5Password(String content) {
        try {
            // 得到一个信息摘要器
            MessageDigest digest = MessageDigest.getInstance("md5");
            byte[] result = digest.digest(content.getBytes());
            StringBuffer buffer = new StringBuffer();
            // 把每一个byte 做一个与运算 0xff;
            for (byte b : result) {
                // 与运算
                int number = b & 0xff;// 加盐
                String str = Integer.toHexString(number);
                if (str.length() == 1) {
                    buffer.append("0");
                }
                buffer.append(str);
            }
            // 标准的md5加密后的结果
            return buffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
}
