package cn.sigo.recruit.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 产生code
 */
public class GenerateCode {

    /**
     *
     * 获取下一个Code
     *
     * @param maxCode:当前系统最大的编码
     * @return
     */
    public static String queryNextMaxCode(String maxCode,String type) {
        StringBuilder jobCode = new StringBuilder(type);
        String lastCode = "";
        Date nowDate = new Date();
        String format = DateUtils.dateToString(nowDate, DateUtils.YYYY_MM_DD);
        // jobCode 不为空
        if (StringUtils.isNotBlank(maxCode)) {
            String tempStr = maxCode.substring(2, 10);
            // 相同
            if (tempStr.equals(format)) {
                // 获取当前最后两位的code
                String tempLastCode = maxCode.substring(10, 12);
                // 转化成整型
                int in = Integer.parseInt(tempLastCode);
                if (in < 9) {
                    lastCode = "0" + (in + 1);
                } else {
                    lastCode = (in + 1) + "";
                }
            } else {
                lastCode = "01";
            }
        } else {
            lastCode = "01";
        }
        jobCode.append(format);
        jobCode.append(lastCode);
        return jobCode.toString();
    }
}
