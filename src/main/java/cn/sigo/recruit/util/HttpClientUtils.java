/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitUtils.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 下午2:29:34
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.util;

import cn.sigo.recruit.InitializationBean;
import cn.sigo.recruit.model.*;
import cn.sigo.recruit.model.response.ResponseToUmsSaveSubTeam;
import cn.sigo.recruit.model.response.ResponseToUmsSaveUser;
import com.alibaba.fastjson.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * httpclient 工具类
 *
 * @author zhixiang.meng
 */
public class HttpClientUtils {

   private final static Logger LOGGER = LoggerFactory.getLogger(HttpClientUtils.class);

   /**
    * 发起http请求
    *
    * @param jsonObject
    * @param uri
    * @return
    */
   public static String doPost(String jsonObject, String uri) {
      String postUrl = (String) InitializationBean.initMap.get("user.login.url");
      DefaultHttpClient httpclient = null;
      try {
         if (StringUtils.isNotBlank(postUrl)) {
            httpclient = new DefaultHttpClient();
            HttpPost httpost = new HttpPost(postUrl + uri);
            // 设置请求头部
            httpost.setHeader("User-Agent",
              "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0");
            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
            formparams.add(new BasicNameValuePair("from", jsonObject));
            UrlEncodedFormEntity urlEntity = new UrlEncodedFormEntity(formparams, "UTF-8");
            httpost.setEntity(urlEntity);
            HttpResponse response = httpclient.execute(httpost);
            HttpEntity entity = response.getEntity();
            String respStr = EntityUtils.toString(entity, "UTF-8");
            if (StringUtils.isNotBlank(respStr)) {
               return respStr;
            } else {
               return null;
            }
         }
      } catch (Exception e) {
         LOGGER.error("call erp login error..." + e.getMessage());
         return null;
      } finally {
         httpclient.close();
      }
      return null;
   }

   /**
    * 发送http请求调用UMS里面的接口
    *
    * @param
    * @param
    * @return
    */
   public static String doPost2UMSOrAD(String jsonString, String url) {
      HttpClient client = HttpClients.createDefault();
//      String url = "http://localhost:8081/user/add";
      HttpPost post = new HttpPost(url);
      String responseStr = null;
      try {
         StringEntity strEntity = new StringEntity(jsonString, "UTF-8");
         strEntity.setContentEncoding("UTF-8");
         strEntity.setContentType("application/json");
         post.setEntity(strEntity);
         post.addHeader("content-type", "application/json");
         HttpResponse res = client.execute(post);
         if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            // 返回json格式：
            responseStr = EntityUtils.toString(res.getEntity(), "UTF-8");
            System.out.println(responseStr);
         }
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
      return responseStr;
   }

   /**
    * 调用UMS接口保存用户
    *
    * @param responseToUmsSaveUser
    * @return
    */
   public static String getUMSSaveUser(ResponseToUmsSaveUser responseToUmsSaveUser, String url) {
      //将要发送的数据转换成JSONObject对象
      String jsonString = JSON.toJSONString(responseToUmsSaveUser);

      //发送post请求,接收返回参数
      String responseStr = doPost2UMSOrAD(jsonString, url);

      System.out.println("向UMS添加用户传递参数:" + jsonString);
      return responseStr;
   }

   /**
    * 调用UMS中保存子团队接口
    *
    * @param responseToUmsSaveSubTeam
    * @return
    */
   public static String getUmsSaveSubTeam(ResponseToUmsSaveSubTeam responseToUmsSaveSubTeam, String url) {
      String jsonString = JSON.toJSONString(responseToUmsSaveSubTeam);
      System.out.println("向UMS添加子团队传递参数:" + jsonString);
      return doPost2UMSOrAD(jsonString, url);
   }

   /**
    * 获取erp token
    *
    * @return
    */
   public static String getErpTocken() {
      String appkey = (String) InitializationBean.initMap.get("user.login.appkey");
      String appsecret = (String) InitializationBean.initMap.get("user.login.appsecret");
      if (StringUtils.isNotBlank(appkey) && StringUtils.isNotBlank(appsecret)) {
         RequestToken token = new RequestToken();
         token.setAppkey(appkey);
         token.setAppsecret(appsecret);
         token.setAction("gettoken");
         String responseStr = doPost(JSONObject.fromObject(token).toString(), "/ums/token/");
         if (StringUtils.isNotBlank(responseStr)) {
            JSONObject obj = new JSONObject().fromObject(responseStr);
            ResponseErpDate date = (ResponseErpDate) JSONObject.toBean(obj, ResponseErpDate.class);
            if (date.getError_code() == 0) {
               return date.getData().toString();
            }
         }
      }
      return null;
   }

   /**
    * 调用erp用户登录接口
    *
    * @param user
    * @param token：系统token
    * @return
    */
   public static ResponseErpUserInfo getErpUserInfo(User user, String token) {
      String appkey = (String) InitializationBean.initMap.get("user.login.appkey");
      if (StringUtils.isNotBlank(appkey)) {
         RequestErpLoginInfo bean = new RequestErpLoginInfo();
         bean.setAppkey(appkey);
         bean.setAccesstoken(token);
         bean.setAction("GetUserToken");
         bean.setData(user);
         String beanJson = JSONObject.fromObject(bean).toString();
         String respStr = doPost(beanJson, "/ums/user");
         if (StringUtils.isNotBlank(respStr)) {
            ResponseErpDate date = jsonToObject(respStr);
            if (date.getError_code() == 0) {
               return reponDataToUserInfo(date.getData());
            }
         }
      }
      return null;
   }

   /**
    * 调用erp接口，根据传入的用户名查询
    *
    * @param likeName：需要查询的用户名
    * @param token：系统token
    * @return
    */
   public static String getLikeForERP(String likeName, String token) {
      String appkey = (String) InitializationBean.initMap.get("user.login.appkey");
      if (StringUtils.isNotBlank(appkey) && StringUtils.isNotBlank(likeName) && StringUtils.isNotBlank(token)) {
         Map<String, String> map = new HashMap<String, String>();
         if (likeName.matches("[a-zA-Z]+")) {
            map.put("UserName", likeName);
            map.put("RealName", "");
         } else {
            map.put("UserName", "");
            map.put("RealName", likeName);
         }
         RequestErpLoginInfo bean = new RequestErpLoginInfo();
         bean.setAccesstoken(token);
         bean.setAction("GetUserList");
         bean.setAppkey(appkey);
         bean.setData(com.alibaba.fastjson.JSONObject.toJSON(map));
         return doPost(JSONObject.fromObject(bean).toString(), "/ums/user");

      }
      return "";
   }

   /**
    * 查询erp的地址信息
    *
    * @param province：省名称
    * @param city：市名称
    * @param token：
    * @return
    */
   public static String getAddressInfo(String province, String city, String token) {
      String appkey = (String) InitializationBean.initMap.get("user.login.appkey");
      String appsecret = (String) InitializationBean.initMap.get("user.login.appsecret");
      if (StringUtils.isNotBlank(appkey) && StringUtils.isNotBlank(token) && StringUtils.isNotBlank(appsecret)) {
         // 封装请求data数据
         Map<String, String> dataMap = new HashMap<String, String>();
         if (StringUtils.isBlank(city) && StringUtils.isBlank(province)) {
            dataMap.put("Type", "Province");
            dataMap.put("OriginProvince", "");
            dataMap.put("OriginCity", "");
         } else if (StringUtils.isBlank(city) && StringUtils.isNotBlank(province)) {
            dataMap.put("Type", "GetCity");
            dataMap.put("OriginProvince", province);
            dataMap.put("OriginCity", "");
         } else {
            dataMap.put("Type", "GetRegion");
            dataMap.put("OriginProvince", province);
            dataMap.put("OriginCity", city);
         }
         Map<String, Object> requestMap = new HashMap<String, Object>();
         requestMap.put("AccessToken", token);
         requestMap.put("AppKey", appkey);
         requestMap.put("AppSecret", appsecret);
         requestMap.put("Action", "getpca");
         requestMap.put("Data", dataMap);
         return doPost(com.alibaba.fastjson.JSONObject.toJSON(requestMap).toString(), "/oms/Order");
      }
      return "";
   }

   /**
    * 将返回的json字符串转化成对象
    *
    * @param jsonString
    * @return
    */
   private static ResponseErpDate jsonToObject(String jsonString) {
      ResponseErpDate date = new ResponseErpDate();
      JSONObject jsonObj = new JSONObject().fromObject(jsonString);
      date.setError_code(jsonObj.getInt("error_code"));
      if (date.getError_code() != 0 && StringUtils.isNotBlank((String) jsonObj.get("error_message"))) {
         date.setError_message((String) jsonObj.get("error_message"));
      } else {
         if (jsonObj.get("data") instanceof JSONObject) {
            date.setData(jsonObj.get("data"));
         } else {
            date.setData((String) jsonObj.get("data"));
         }
      }
      return date;
   }

   /**
    * erp返回data字符串转化为数组
    *
    * @param jsonString
    * @return
    */
   private static ResponseErpUserInfo reponDataToUserInfo(Object jsonString) {
      ResponseErpUserInfo userInfo = new ResponseErpUserInfo();
      JSONObject jsonObj = (JSONObject) jsonString;
//        JSONObject jsonObj = new JSONObject().fromObject(jsonString);
      userInfo.setUserId(jsonObj.getInt("UserID"));
      if (StringUtils.isNotBlank((String) jsonObj.get("UserName"))) {
         userInfo.setUserName((String) jsonObj.get("UserName"));
      }
      if (!(jsonObj.get("UserPassword") instanceof JSONNull)) {
         userInfo.setUserPassword((String) jsonObj.get("UserPassword"));
      }
      if (!(jsonObj.get("RealName") instanceof JSONNull)) {
         userInfo.setRealName((String) jsonObj.get("RealName"));
      }
      if (!(jsonObj.get("AddTime") instanceof JSONNull)) {
         userInfo.setAddTime((String) jsonObj.get("AddTime"));
      }
      if (!(jsonObj.get("Status") instanceof JSONNull)) {
         userInfo.setStatus((String) jsonObj.get("Status"));
      }
      if (!(jsonObj.get("UserToken") instanceof JSONNull)) {
         userInfo.setUserToken((String) jsonObj.get("UserToken"));
      }
      if (!(jsonObj.get("Permission") instanceof JSONNull)) {
         userInfo.setPermission((String) jsonObj.get("Permission"));
      }
      if (!(jsonObj.get("DataPermission") instanceof JSONNull)) {
         userInfo.setDataPermission((String) jsonObj.get("DataPermission"));
      }
      if (!(jsonObj.get("BackendRole") instanceof JSONNull)) {
         JSONArray backendRoles = jsonObj.getJSONArray("BackendRole");
         if (null != backendRoles) {
            List<ErpRole> roles = new ArrayList<ErpRole>();
            for (int index = 0; index < backendRoles.size(); index++) {
               JSONObject obj = backendRoles.getJSONObject(index);
               ErpRole role = new ErpRole();
               role.setRoleId(obj.getInt("RoleID"));
               if (!(obj.get("RoleName") instanceof JSONNull)) {
                  role.setRoleName((String) obj.get("RoleName"));
               }
               roles.add(role);
            }
            userInfo.setRackendRole(roles);
         }
      }
      return userInfo;
   }
}
