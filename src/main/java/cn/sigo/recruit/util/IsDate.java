/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: IsDate.java
 * Author:   zhixiang.meng
 * Date:     2018年9月14日 下午2:15:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

/**
 * 校验日期格式 
 *
 * @author zhixiang.meng
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = IsDateValidator.class)
public @interface IsDate {
    // 错误码
    String code() default "0004";
    //错误信息
    String message() default "日期格式不正确";
}
