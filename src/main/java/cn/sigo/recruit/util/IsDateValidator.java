/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: IsDateValidator.java
 * Author:   zhixiang.meng
 * Date:     2018年9月14日 下午3:11:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package cn.sigo.recruit.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class IsDateValidator implements ConstraintValidator<IsDate, String> {
    
    @Override
    public void initialize(IsDate constraintAnnotation) {
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try{
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            format.setLenient(false);
            Date date = format.parse(value);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
