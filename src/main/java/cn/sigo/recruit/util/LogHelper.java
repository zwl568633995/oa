package cn.sigo.recruit.util;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class LogHelper {
    /**
     * 交换机：exchange.sigo.log
     * 关键字：INFO、ERROR、DEBUG
     */
    private static String routingKey_INFO = "info";

    private static String routingKey_ERROR = "error";

    private static String routingKey_DEBUG = "debug";

    private static String exchange = "exchange.sigo.log";

    /**
     * IP 地址
     */
    private static String IP;

    static {
        try {
            IP = InetAddress.getLocalHost().toString().substring(16);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private AmqpTemplate rabbitTempalte;

    /**
     * 发送INFO级别日志
     *
     * @param message
     */
    public void LoggerSendINFO(String message) {
        message = this.transDate(new Date()) + "  " + IP + "  " + routingKey_INFO + "  " + message;
        rabbitTempalte.convertAndSend(exchange, routingKey_INFO, message);
    }

    /**
     * 发送ERROR级别日志
     *
     * @param message
     */
    public void LoggerSendERROR(String message) {
        message = this.transDate(new Date()) + "  " + IP + "  " + routingKey_ERROR + "  " + message;
        rabbitTempalte.convertAndSend(exchange, routingKey_ERROR, message);
    }

    /**
     * 发送DEBUG级别日志
     *
     * @param message
     */
    public void LoggerSendDEBUG(String message) {
        message = this.transDate(new Date()) + "  " + IP + "  " + routingKey_DEBUG + "  " + message;
        rabbitTempalte.convertAndSend(exchange, routingKey_DEBUG, message);
    }

    private String transDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
