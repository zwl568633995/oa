package cn.sigo.recruit.util;

import java.text.MessageFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MessageFormatUtil {

    public static void main(String[] args) {
        String pattern1 = "{0},你好!你于 {1} 存入 {2}元";
        String pattern2 = "At {1,time,short} on {1,date,long},{0} paid {2,number,currency}";
        String pattern3 = "【HR通知】您有待审批的招聘申请单，请及时审批,申请部门:{0},申请招聘职位:{1},职位评分:{7},申请原因:{3},需求人数:{4},申请人:{5}.<a href=\"{6}\"></a>点此进行审批.\n";

        //1.0E3 科学计数法 即 1.0*10^3 E3表示10的三次方
        Object[] params = {"hoonick",new GregorianCalendar().getTime(),1.0E3};


        String deptName = "技术部";
        Object[] params3 = {deptName, "java开发工程师", "99", "基础知识扎实，经验丰富。", "5", "武宜城", "https://www.baidu.com","50"};

        //使用默认的本地化对象格式化字符串
        String format = MessageFormat.format(pattern1, params);
        System.out.println(format);

        //使用指定的本地化对象格式化字符串
        MessageFormat mf = new MessageFormat(pattern2, Locale.US);
        String format2 = mf.format(params);
        System.out.println(format2);

        // 使用指定的本地化对象格式化字符串
        /**
         * 静态方法
         */
        String format3 = MessageFormat.format(pattern3, params3);

        System.out.println(format3);
    }
}
