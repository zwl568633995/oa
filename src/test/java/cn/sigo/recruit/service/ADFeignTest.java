package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.ADFeign;
import cn.sigo.recruit.model.response.ADResult;
import cn.sigo.recruit.model.response.ADUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class ADFeignTest {
    @Autowired
    private ADFeign adFeign;

    @Test
    public void adAddUser() {
        ADUser adUser = new ADUser();
        //高磊
        /**
         * 高磊    男     15375511506      gaolei@sigo.cn     技术部>官网商城
         * 朱唯一    女     15861807173    zhuweiyi@sigo.cn     运营中心>网站运营部
         * 陈利     女      15951920418     chenli@sigo.cn      客服部>天猫服务组
         */
        //职位
        adUser.setPosition("开发");
        adUser.setGivenName("chen");
        adUser.setEmail( "chenli@sigo.cn");
        adUser.setPassword("sigo@123");
        adUser.setRealName("陈利");
        adUser.setSurname("li");
        adUser.setUserName("chenli");
        adUser.setDepartment("官网运营");
        adUser.setDeptPath("ou=官网运营,ou=运营");
        adUser.setMobile("15951920418");

        ADResult adResult = adFeign.addUser(adUser);
        System.out.println(adResult);
    }
}
