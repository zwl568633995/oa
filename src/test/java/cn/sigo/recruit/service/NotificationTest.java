package cn.sigo.recruit.service;


import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.SendNotificationFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.NotificationMapper;
import cn.sigo.recruit.model.Notification;
import cn.sigo.recruit.model.response.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class NotificationTest {
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private SendNotificationFeign sendNotificationFeign;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private RedisService redisService;

    @Test
    public void sendTest() {
        Notification notification = notificationMapper.selectByPrimaryKey(1);
        String content = notification.getContent();

        Object[] params = {"技术部", "java开发工程师", "99", "基础知识扎实，经验丰富。", "5", "武宜城", "https://www.baidu.com"};

        //发送的内容
        String format = MessageFormat.format(content, params);

        //收件人集合
        List<ReceiveAccount> receiveAccountList = new ArrayList<>();
        //收件人
        ReceiveAccount receiveAccount = new ReceiveAccount();
        receiveAccount.setReceiveMsgAccount("xudou@sigo.cn");
        receiveAccount.setReceiveMsgAccountName("视客-徐豆");
        receiveAccountList.add(receiveAccount);

        //调用消息发送服务需要的参数
        SendNotification sendNotification = new SendNotification();
        sendNotification.setMsgTitle(notification.getTitle());
        sendNotification.setMsgContent(format);
        sendNotification.setReceiveAccountList(receiveAccountList);

        SendNotificationResult adResult = sendNotificationFeign.messageNotify(sendNotification);

        System.out.println(adResult);
    }

    /**
     * 修改模板 : 为 ：
     */
    @Test
    public void modNotification() {
        for (Integer i = 2; i <= 26; i++) {
            if (i == 25 ){
                continue;
            }
            Notification notification = notificationMapper.selectByPrimaryKey(i);
            String content = notification.getContent();
            String replace = content.replace(":", "：");
            System.out.println(content);
            System.out.println(replace);
        }
    }

    @Test
    public void ymlTest() {
        String url = "${url.notification}";
        System.out.println(url);

        ReceiveAccount receiveAccount = new ReceiveAccount();
        System.out.println(receiveAccount.getReceiveMsgAccount());
    }

    @Test
    public void UMSFeifn() {
        UMSRole umsRole = new UMSRole();
        umsRole.setRoleName("recruit-人事员工");
        UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
        System.out.println(umsNewResponseModel.toString());
    }

    @Test
    public void redisKeyTest() {

        boolean department = redisService.exists("department");

        System.out.println(department);
    }
}
