package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.ADFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.OrganizationStructureMapper;
import cn.sigo.recruit.mapper.RecruitIncumbencyInfoMapper;
import cn.sigo.recruit.mapper.RecruitIncumbencyUserBaseInfoMapper;
import cn.sigo.recruit.model.OrganizationStructure;
import cn.sigo.recruit.model.RecruitIncumbencyInfo;
import cn.sigo.recruit.model.RecruitIncumbencyUserBaseInfo;
import cn.sigo.recruit.model.response.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据初始化
 */

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class OrganizationServiceTest {

    @Autowired
    private OrganizationService organizationService;
    @Autowired
    private UMSFeign umsFeign;
    @Autowired
    private RosterService rosterService;
    @Autowired
    private RecruitIncumbencyInfoMapper recruitIncumbencyInfoMapper;
    @Autowired
    private RecruitIncumbencyUserBaseInfoMapper userBaseInfoMapper;

    @Test
    public void adduserTest() {
        ResponseToUmsSaveUser responseToUmsSaveUser = new ResponseToUmsSaveUser();
        responseToUmsSaveUser.setUserType("普通用户");
        responseToUmsSaveUser.setPosition("测试");
        responseToUmsSaveUser.setRosterId(248);
        List<Long> teamIds = new ArrayList<>();
        teamIds.add(48L);
        responseToUmsSaveUser.setTeamIds(teamIds);
        responseToUmsSaveUser.setUserName("test1123-4");
        responseToUmsSaveUser.setReallyName("何炅");
        responseToUmsSaveUser.setPhone("13112345678");
        responseToUmsSaveUser.setPassword("123456");
        responseToUmsSaveUser.setEmail("test@123");

        UMSResponseModel umsResponseModel = umsFeign.saveUser(responseToUmsSaveUser);
        System.out.println(umsResponseModel.toString());
    }


    /**
     * 同步HR 部门到UMS  成功
     */
    @Test
    public void selectAll() {
        List<OrganizationStructure> organizationStructures = organizationService.selectAll();
        for (OrganizationStructure organizationStructure : organizationStructures) {
            RespUMSAddHRTeamToUMS respUMSAddHRTeamToUMS = transOrganizationStructureToUMSTeam(organizationStructure);

            UMSResponseModel umsResponseModel = umsFeign.addSubTeam(respUMSAddHRTeamToUMS);
            if (StringUtils.isNotBlank(umsResponseModel.getResultCode()) && umsResponseModel.getResultCode().equals("1")) {
                System.out.println(organizationStructure.getId() + "插入成功");
            } else {
                System.out.println(organizationStructure.getId() + "插入失败");
            }
        }
    }

    public RespUMSAddHRTeamToUMS transOrganizationStructureToUMSTeam(OrganizationStructure organizationStructure) {
        RespUMSAddHRTeamToUMS respUMSAddHRTeamToUMS = new RespUMSAddHRTeamToUMS();

        respUMSAddHRTeamToUMS.setDeptName(organizationStructure.getDeptName());
        //部门负责人
        //umsTeam.setDeptOver(organizationStructure);
        respUMSAddHRTeamToUMS.setId(organizationStructure.getId());
        respUMSAddHRTeamToUMS.setTeamType(organizationStructure.getTeamType());
        respUMSAddHRTeamToUMS.setSuperiorDeptId(organizationStructure.getSuperiorDeptId());
        respUMSAddHRTeamToUMS.setIsOrganizationalStructure(organizationStructure.getIsOrganizationalStructure());

//     respUMSAddHRTeamToUMS.setTeamStatus(organizationStructure.getTeamStatus());
//     respUMSAddHRTeamToUMS.setAdder(organizationStructure.getAdder());
//     respUMSAddHRTeamToUMS.setAddTime(organizationStructure.getAddTime());
//     respUMSAddHRTeamToUMS.setModer(organizationStructure.getModer());
//     respUMSAddHRTeamToUMS.setModTime(organizationStructure.getModTime());

        return respUMSAddHRTeamToUMS;
    }

    @Test
    public void selectByReallyName() {

        RecruitIncumbencyUserBaseInfo recruitIncumbencyUserBaseInfo = rosterService.selectRosterByReallyName("刘海勇");
        System.out.println(recruitIncumbencyUserBaseInfo.toString());

    }

    /**
     * HR通过花名册ID，将花名册对应的部门初始化至UMS，记录UMS用户对应的团队,添加用户团队关系,
     * <p>
     * 初始化 用户和团队的关系
     */
    @Test
    public void authUserTeamTest() {
        List<Integer> errorRosterId = new ArrayList();

        List<RecruitIncumbencyInfo> recruitIncumbencyInfos = recruitIncumbencyInfoMapper.selectAll();
        //获取所有的花名册用户在职信息,得到花名册id,部门id

        for (RecruitIncumbencyInfo recruitIncumbencyInfo : recruitIncumbencyInfos) {
            //调用UMS 系统的通过花名册id查找用户的接口获取用户,得到用户id
            UMSUser umsUser = umsFeign.selectUserByRosterId(recruitIncumbencyInfo.getIncumbencyId());
            if (null == umsUser) {
                //花名册id 没有对应的 UMS用户
                errorRosterId.add(recruitIncumbencyInfo.getIncumbencyId());
                continue;
            }

            //部门id为空
            if (null == recruitIncumbencyInfo.getDeptId()) {
                //花名册id对应的用户没有部门id
                errorRosterId.add(recruitIncumbencyInfo.getIncumbencyId());
                continue;
            }

            //调用UMS 系统中设置 用户团队关系的接口,保存用户团队关系
            RespUMSSetUserTeam respUMSSetUserTeam = new RespUMSSetUserTeam();
            respUMSSetUserTeam.setUserId(umsUser.getId().intValue());

            List<Integer> teamIds = new ArrayList<>();
            teamIds.add(recruitIncumbencyInfo.getDeptId().intValue());
            respUMSSetUserTeam.setTeamIds(teamIds);
            UMSResponseModel umsResponseModel = umsFeign.setUserTeam(respUMSSetUserTeam);

            if (null != umsResponseModel && umsResponseModel.getResultCode().equals("1")) {
                //成功
                System.out.println("花名册id:" + recruitIncumbencyInfo.getIncumbencyId() + "对应的用户,添加用户团队成功");
            } else {
                //失败
                System.out.println("花名册id:" + recruitIncumbencyInfo.getIncumbencyId() + "对应的用户,添加用户团队失败");
            }

        }

        System.out.println("花名册id匹配不到UMS用户的id" + errorRosterId.toString());
    }

    /**
     * 绑定用户 花名册id
     * UMS用户与花名册用户匹配（姓名一致且只有一个的，系统自动匹配花名册ID，剩余人工匹配处理）；
     */
    @Test
    public void userRosterIdTest() {
        Map<String, Object> error = new HashMap<>();

        List<RecruitIncumbencyUserBaseInfo> userBaseInfoList = userBaseInfoMapper.selectAll();

        for (RecruitIncumbencyUserBaseInfo userBaseInfo : userBaseInfoList) {
            String reallyName = userBaseInfo.getEntryRegisterName();

            RespUMSSelectByReallyName respUMSSelectByReallyName = new RespUMSSelectByReallyName();
            respUMSSelectByReallyName.setReallyName(reallyName);

            //调用UMS通过真实姓名查找用户的接口,匹配 HR 中的用户,设置 rosterId
            UMSResponseModel umsResponseModel = umsFeign.selectByReallyName(respUMSSelectByReallyName);
            if (null != umsResponseModel && umsResponseModel.getResultCode().equals("1")) {
                //成功 调用 UMS 更新用户接口

            } else {
                //失败

            }

        }

        //System.out.println(userBaseInfoList.size());

    }

    /**
     * AD添加部门
     */
    @Autowired
    private OrganizationStructureMapper organizationStructureMapper;
    @Autowired
    private ADFeign adFeign;

    @Test
    public void adAddDept() {
        Long deptId = null;
        for (Long id = 19L; id <= 42; id++ ){
            deptId = id;
        }

        OrganizationStructure organizationStructure = organizationStructureMapper.selectByPrimaryKey(deptId);

        //AD添加部门所需参数
        RespADAddDept adAddDept = new RespADAddDept();
        //部门名称
        adAddDept.setName(organizationStructure.getDeptName());

        //部门所属层级
        String path="";

        while (true){
            //上级部门id为0,本级部门为最上级,没有所属层级
            if (0 == organizationStructure.getSuperiorDeptId()){
                break;
            }
            //获取上级部门
            organizationStructure =  organizationStructureMapper.selectByPrimaryKey(organizationStructure.getSuperiorDeptId());
            path = ",ou=" + organizationStructure.getDeptName();
        }
        if (path.length() > 0){
            path = path.substring(1);
        }

        adAddDept.setPath(path);

        ADResult adResult = adFeign.deptAdd(adAddDept);

        System.out.println(adResult);
    }

}