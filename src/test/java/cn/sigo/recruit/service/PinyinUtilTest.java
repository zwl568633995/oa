package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.ADFeign;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.model.response.*;
import cn.sigo.recruit.util.PinyinUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class PinyinUtilTest {
   @Autowired
   private UMSFeign umsFeign;

   @Test
   public void PinyinTest() {

      String userName = "惠楠楠";
      String pingYin = PinyinUtil.getPingYin(userName);
      String firstSpell = PinyinUtil.getFirstSpell(userName);
      String fullSpell = PinyinUtil.getFullSpell(userName);
      String substring = userName.substring(0, 1);
      String substring1 = userName.substring(1);
      System.out.println(substring);
      System.out.println(substring1);
      int length = userName.length();
      System.out.println(length);
      System.out.println(fullSpell);
      System.out.println(firstSpell);
      System.out.println(pingYin);
   }

   @Test
   public void FeignTest() {
      RespUmsEditTeam respUmsEditTeam = new RespUmsEditTeam();
      respUmsEditTeam.setId(49L);
      respUmsEditTeam.setSuperiorDeptId(1L);
      respUmsEditTeam.setTeamType("测试数据");
      respUmsEditTeam.setTeamStatus("测试数据");

      UMSResponseModel umsResponseModel = umsFeign.editSubTeam(respUmsEditTeam);
      System.out.println("UMS修改部门接口返回:" + umsResponseModel.toString());
   }

   @Test
   public void FeignTest2() {

      RespUMSSelectTeamByDeptName respSelectTeamByDeptName = new RespUMSSelectTeamByDeptName();
      respSelectTeamByDeptName.setDeptName("测试数据1115");
      UMSTeam umsTeam = umsFeign.selectTeamByDeptName(respSelectTeamByDeptName);
      System.out.println(umsTeam.toString());
   }

   @Autowired
   private ADFeign adFeign;

   /**
    * 添加部门测试
    */
   @Test
   public void FeignTest3() {
      RespADAddDept respADAddDept = new RespADAddDept();
      respADAddDept.setName("JAVA");
      respADAddDept.setPath("运营,官网运营");

      ADResult adResult = adFeign.deptAdd(respADAddDept);
      System.out.println(adResult);
   }


   @Test
   public void FeignTest4() {
      UMSUser umsUser = umsFeign.selectUserByRosterId(16);
      System.out.println(umsUser);

   }

   @Test
   public void stringLengthTest(){
      String s = "";
      System.out.println(s.length());
      s.substring(1);
   }
}
