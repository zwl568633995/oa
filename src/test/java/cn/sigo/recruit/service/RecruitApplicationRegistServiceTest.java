package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitApplicationRegist;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class RecruitApplicationRegistServiceTest {

   @Autowired
   private RecruitApplicationRegistService recruitApplicationRegistService;

   @Test
   public void queryInfoByIdCardNo() {
      RecruitApplicationRegist recruitApplicationRegist = recruitApplicationRegistService.queryInfoByIdCardNo("3212451812121296");
      System.out.println(recruitApplicationRegist.getId());

   }
}