package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class RecruitInterviewManagementServiceTest {
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;

    @Test
    public void queryInterviewManagentByPhone() {
        // RecruitInterviewManagement recruitInterviewManagement = recruitInterviewManagementService.queryInterviewManagentByPhone("13625611256");
        RecruitInterviewManagement interviewManagement = recruitInterviewManagementService.queryInterviewManagentByCode("MS2018121001");

        System.out.println(interviewManagement.getFirstTime().length());
        System.out.println(interviewManagement.getFirstTime().substring(0,16));
    }
}