package cn.sigo.recruit.service;


import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.mapper.RecruitIncumbencyInfoMapper;
import cn.sigo.recruit.mapper.RecruitIncumbencyUserBaseInfoMapper;
import cn.sigo.recruit.model.RecruitIncumbencyInfo;
import cn.sigo.recruit.model.RecruitIncumbencyUserBaseInfo;
import cn.sigo.recruit.model.response.ResponseToUmsSaveUser;
import cn.sigo.recruit.model.response.UMSResponseModel;
import cn.sigo.recruit.util.PinyinUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class RosterTest {
    @Autowired
    private RecruitIncumbencyUserBaseInfoMapper userBaseInfoMapper;
    @Autowired
    private RecruitIncumbencyInfoMapper infoMapper;
    @Autowired
    private UMSFeign umsFeign;

    /**
     * 新员工入职,添加用户
     */
    @Test
    public void addRosterTest() {

        //用户基本信息
        RecruitIncumbencyUserBaseInfo userBaseInfo = new RecruitIncumbencyUserBaseInfo();
        userBaseInfo.setEntryRegisterName("胡玲");
        userBaseInfo.setCompanyEmail("huling@sigo.cn");
        userBaseInfo.setPhoneNumber("15555195657");
        userBaseInfo.setSex("女");

        //插入数据
        Integer i = userBaseInfoMapper.insertSelective(userBaseInfo);

        if (i == null || i != 1){
            System.out.println("添加基本信息失败");
            //return false;

        }

        //员工在职信息
        RecruitIncumbencyInfo info = new RecruitIncumbencyInfo();
        info.setIncumbencyId(userBaseInfo.getId());
        //部门Id 看数据库
        info.setDeptId(29L);
        info.setDeptName("天猫服务组");
        info.setJobName("客服");

        Integer infoResult = infoMapper.insertSelective(info);


        if (infoResult == null || infoResult != 1){
            System.out.println("添加在职信息失败");
            //return false;
        }

        //HR 添加成功 向UMS 添加用户
        ResponseToUmsSaveUser umsSaveUser = new ResponseToUmsSaveUser();
        umsSaveUser.setEmail(userBaseInfo.getCompanyEmail());
        umsSaveUser.setPassword("sigo@123");
        umsSaveUser.setPhone(userBaseInfo.getPhoneNumber());
        umsSaveUser.setReallyName(userBaseInfo.getEntryRegisterName());
        umsSaveUser.setUserName(PinyinUtil.getPingYin(userBaseInfo.getEntryRegisterName()));
        List<Long> teamIds = new ArrayList<>();
        teamIds.add(info.getDeptId());
        umsSaveUser.setTeamIds(teamIds);
        umsSaveUser.setRosterId(userBaseInfo.getId());
        umsSaveUser.setPosition(info.getJobName());
        umsSaveUser.setUserType("普通用户");
        umsSaveUser.setDeleted(false);
        umsSaveUser.setStatus("0");

        UMSResponseModel umsResponseModel = umsFeign.saveUser(umsSaveUser);

        if (null != umsResponseModel && umsResponseModel.getResultCode().equals("1")){
            System.out.println("添加用户成功");
            //return  true;
        }

        System.out.println("添加用户失败");
        //return  false;
    }
}
