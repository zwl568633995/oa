package cn.sigo.recruit.service;


import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.model.response.UMSNewResponseModel;
import cn.sigo.recruit.model.response.UMSRole;
import cn.sigo.recruit.model.response.UMSUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class UMSFeignTest {

    @Autowired
    private UMSFeign umsFeign;

    @Test
    public void selectByRoleName() {
        UMSRole umsRole = new UMSRole();
        umsRole.setRoleName("recruit-人事员工");

        UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);
        List<Map<String,String>> umsUserList = (List<Map<String, String>>) umsNewResponseModel.getData();

        System.out.println(umsUserList);

        for (Map<String,String> umsUser : umsUserList) {
            System.out.println(umsUser.get("userName"));
            System.out.println(umsUser.get("reallyName"));
        }
    }
}
