package cn.sigo.recruit.service;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.feign.UMSFeign;
import cn.sigo.recruit.model.response.UMSNewResponseModel;
import cn.sigo.recruit.model.response.UMSRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
public class test1 {
    @Autowired
    private UMSFeign umsFeign;

    @Test
    public void a(){
        UMSRole umsRole = new UMSRole();
        umsRole.setRoleName("recruit-人事员工");
        UMSNewResponseModel umsNewResponseModel = umsFeign.selectByRoleName(umsRole);

        List data = (List) umsNewResponseModel.getData();
        System.out.println(data);
    }
}
