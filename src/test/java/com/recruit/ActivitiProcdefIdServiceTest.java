/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiProcdefIdServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月10日 下午4:39:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.ActivitiProcdefId;
import cn.sigo.recruit.service.ActivitiProcdefIdService;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class ActivitiProcdefIdServiceTest {
    
    @Autowired
    private ActivitiProcdefIdService activitiProcdefIdService;

    @Test
    public void test() {
        ActivitiProcdefId procdef = new ActivitiProcdefId();
        procdef.setProcdefId("1");
        procdef.setProcdefKey("test1");
        procdef.setProcdefName("测试");
        activitiProcdefIdService.savenActivitiProcdefId(procdef);
    }
}
