/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: ActivitiUserServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月3日 上午11:00:32
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class ActivitiUserServiceTest {
    
    @Autowired
    private ProcessEngine engine ;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    
//    @Test
    public void test() {
        assertNotNull(engine);
        assertNotNull(identityService);
        User user = identityService.newUser("mengzx");
        user.setFirstName("孟志祥");
        user.setEmail("xyr-2732695@163.com");
        identityService.saveUser(user);
        
        User u = identityService.createUserQuery().list().get(0);
        assertNotNull(u);
        System.out.println(u.getId() + " - " + u.getFirstName());
    }
    
    @Test
    public void assignee() {
        repositoryService.createDeployment().addClasspathResource("processes/interviewProcess.bpmn").deploy();
        // 验证已部署的流程定义
        List<ProcessDefinition>  pdes = repositoryService.createProcessDefinitionQuery().list();
//        assertEquals("jobAudit", pdes.get(0).getKey());
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("interviewCode", "0003");

        ProcessInstance processInstance =  runtimeService.startProcessInstanceByKey("interviewProcess", map);
        List<Task> tasks = this.getTaskByDeploymentId(processInstance.getDeploymentId(), "interviewProcess",
                processInstance.getId(), (String) map.get("interviewCode"));
        for (Task task : tasks) {
            this.finshTask(task.getId());
        }
    }

    public boolean finshTask(String taskId) {
        taskService.complete(taskId);
        System.out.println("finshtask");
        return true;
    }

    public List<Task> getTaskByDeploymentId(String deploymentId, String processDefinitionKey, String executionId,
                                            String loginName) {
        List<Task> list = taskService.createTaskQuery().processDefinitionKey(processDefinitionKey)
                .deploymentId(deploymentId).taskAssignee(loginName).executionId(executionId).list();
        // 获取当前人的
        return list;
    }
}
