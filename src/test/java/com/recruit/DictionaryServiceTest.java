/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: DictionaryServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年6月27日 下午3:21:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.Dictionary;
import cn.sigo.recruit.service.DictionaryService;

/**
 * 测试
 *
 * @author zhixiang.meng
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class DictionaryServiceTest {

    @Autowired
    private DictionaryService dictionaryService;
    
    @Test
    public void queryDictionaryType() {
        List<String> types = dictionaryService.queryDictionaryType();
        assertEquals(8, types.size());
    }
    
    @Test
    public void queryDictionaryByType() {
        List<Dictionary> dictionarys = dictionaryService.queryDictionaryByType("department");
        assertEquals(14, dictionarys.size());
    }
    
}
