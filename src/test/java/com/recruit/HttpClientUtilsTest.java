/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: HttpClentTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月2日 上午11:22:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.ResponseErpUserInfo;
import cn.sigo.recruit.model.User;
import cn.sigo.recruit.util.EncryAdecryUtil;
import cn.sigo.recruit.util.HttpClientUtils;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class HttpClientUtilsTest {
    
//    @Test
    public void test() throws Exception {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost httpost = new HttpPost("http://erpapi.t.vsigo.cn/ums/user");
        httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0");
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        String jsonStri = "{\r\n" + 
                "  \"appkey\": \"sigo\",\r\n" + 
                "  \"accesstoken\": \"610DF5AE6DD2F4DE57667819F51AF2F4\",\r\n" + 
                "  \"action\": \"GetUserToken\",\r\n" + 
                "  \"data\": {\r\n" + 
                "    \"username\": \"mengzhixiang\",\r\n" + 
                "    \"userpassword\": \"15d5443d84b5291f6f459118c23e52ae\"\r\n" + 
                "  }\r\n" + 
                "}";
        formparams.add(new BasicNameValuePair("from",jsonStri));
        UrlEncodedFormEntity urlEntity =  new UrlEncodedFormEntity(formparams, "UTF-8");
        httpost.setEntity(urlEntity);
        HttpResponse response = httpclient.execute(httpost);
        HttpEntity entity = response.getEntity();
        System.out.println(EntityUtils.toString(entity,"UTF-8"));
    }
    
    @Test
    public void login() throws Exception{
        User user = new User();
        user.setUsername("mengzhixiang");
        user.setUserpassword(EncryAdecryUtil.md5Password("sigo@123"));
        String token = HttpClientUtils.getErpTocken();
        ResponseErpUserInfo info = HttpClientUtils.getErpUserInfo(user, token);
        System.out.println(info.getUserId() + "--" + info.getUserName());
    }
}
