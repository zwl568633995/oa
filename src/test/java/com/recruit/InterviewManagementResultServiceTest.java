/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: InterviewManagementResultServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月16日 下午5:13:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitInterviewManagementResult;
import cn.sigo.recruit.service.RecruitInterviewManagementResultService;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class InterviewManagementResultServiceTest {
    
    @Autowired
    private RecruitInterviewManagementResultService interviewManagementResultService;
    
    @Test
    public void testqueryResultForManagementId() {
        RecruitInterviewManagementResult result = interviewManagementResultService.queryResultForManagementId(123);
        assertNull(result);
    }

}
