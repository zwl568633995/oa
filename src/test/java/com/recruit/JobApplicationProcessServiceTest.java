/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobApplicationProcessServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月16日 上午10:11:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import java.util.List;

import org.activiti.engine.FormService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.service.JobApplicationProcessService;
import cn.sigo.recruit.service.JobInfoService;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class JobApplicationProcessServiceTest {

    @Autowired
    private JobApplicationProcessService jobApplicationProcessService;
    @Autowired
    private JobInfoService jobInfoService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private FormService formService;

    //@Test
    public void startJobActivitiTest() {
        RecruitJobInfo jobInfo = new RecruitJobInfo();
        jobInfo.setActivitiProcdId("123456");
        jobInfo.setNumberOfPeople(3);
        jobInfo.setJobName("java开发工程师_0716");
        jobInfo.setSex("男");
        jobInfo.setApplicationDepartment("技术部");
        jobInfo.setAuditor("wuyicheng");
        jobInfo.setApplyPeople("mengzhixiang");
        jobInfo.setActivitiProcdId("applicationForRecruitmentId:75:132506");
        jobInfo.setJobStatus("待审核");
//        Long jobId = jobInfoService.saveJobInfo(jobInfo);
//        assertNotNull(jobId);
       // jobApplicationProcessService.startJobActiviti("24", jobInfo.getNumberOfPeople().toString(),
              //  jobInfo.getActivitiProcdId(), jobInfo.getAuditor(), jobInfo.getApplyPeople());
    }
    
    @Test
    public void queryTask() {
        String procId = "7505";
       List<Task> tasks = taskService.createTaskQuery().processInstanceId(procId).taskCandidateOrAssigned("wuyicheng").list();
       for(Task task : tasks) {
           System.out.println(task.getId() + "--" + task.getName());
           TaskFormData taskFormData = formService.getTaskFormData(task.getId());
           System.out.println(taskFormData.toString());
       }
       
    }

}
