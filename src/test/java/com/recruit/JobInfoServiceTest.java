/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: JobInfoServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月5日 下午6:13:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitJobInfo;
import cn.sigo.recruit.model.req.ReqJobInfoModel;
import cn.sigo.recruit.service.JobInfoService;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class JobInfoServiceTest {
    
    @Autowired
    private JobInfoService jobInfoService;
    
//    @Test
    public void queryMaxJobCodeTest() {
        String jobCode = jobInfoService.queryMaxJobCode();
        assertNull(jobCode);
    }
    
    @Test
    public void queryJobInfoList() {
        ReqJobInfoModel model = new ReqJobInfoModel();
        List<String> strs = new ArrayList<String>();
        strs.add("已创建");
        strs.add("招聘中");
        model.setJobStatuses(strs);
        PageInfo<RecruitJobInfo> jobInfos = jobInfoService.queryJobInfoList(model);
        System.out.println(jobInfos.getSize() +  " ------------------------------------------ ");
    }
    
    @Test
    public void saveJobInfo() throws InterruptedException {
        RecruitJobInfo jobInfo = new RecruitJobInfo();
        jobInfo.setActivitiProcdId("123456");
        jobInfo.setJobName("java开发工程师");
        jobInfo.setSex("男");
        jobInfo.setApplicationDepartment("技术部");
        long result = jobInfoService.saveJobInfo(jobInfo);
        assertNotNull(result);
        RecruitJobInfo bean = jobInfoService.queryByPrimaryKey(result);
        bean.setAdder("mengzx");
        bean.setAuditor("wuyicheng");
        bean.setId(result);
        jobInfoService.modifyJobInfo(bean);
        String jobStatus = "待审核";
        jobInfoService.modifyJobInfoByJobId(jobStatus, result);
    }
    
//    @Test
    public void isExist() {
        String deptName = "技术部";
        String jobName = "java开发工程师";
        assertFalse(jobInfoService.isExist(deptName, jobName, 5L));
    }
    
}
