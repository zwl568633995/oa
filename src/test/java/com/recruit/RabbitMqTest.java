package com.recruit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.config.rabbitMq.MessageEntity;
import cn.sigo.recruit.config.rabbitMq.consumer.MessageConsumer;
import cn.sigo.recruit.config.rabbitMq.provider.MessageProvider;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class RabbitMqTest {
	
	@Autowired
	private MessageProvider messageProvider;
	@Autowired
	private MessageConsumer messageConsumer;
	
	@Test
	public void testSend() throws InterruptedException {
		MessageEntity entity = new MessageEntity();
		entity.setContent("rabbit mq test send message...");
		messageProvider.sendMessage(entity);
//		for(int index=0;index<1000;index++) {
//			Thread.sleep(10000);
//			entity.setContent("rabbit mq test send message..." + index);
//			messageProvider.sendMessage(entity);
//		}
	}
	
	@Test
	public void testGetMessage() {
		MessageEntity entity = new MessageEntity();
		messageConsumer.handler(entity);
	}

}
