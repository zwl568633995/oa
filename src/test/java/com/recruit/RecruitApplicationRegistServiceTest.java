/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitApplicationRegistServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月17日 上午11:33:02
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitApplicationCertificate;
import cn.sigo.recruit.model.RecruitApplicationRegist;
import cn.sigo.recruit.service.RecruitApplicationCertificateService;
import cn.sigo.recruit.service.RecruitApplicationRegistService;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class RecruitApplicationRegistServiceTest {

    @Autowired
    private RecruitApplicationRegistService recruitApplicationRegistService;
    @Autowired
    private RecruitApplicationCertificateService recruitApplicationCertificateService;
    
    @Test
    public void queryMaxCode() {
        String str = recruitApplicationRegistService.queryMaxCode();
        assertNotNull(str);
    }
    
//    @Test
    public void saveRecruitApplicationRegist() {
        RecruitApplicationRegist regist = new RecruitApplicationRegist();
        Integer id = recruitApplicationRegistService.saveRecruitApplicationRegist(regist).getId();
        System.out.println(id + "-----------------------------------------");
    }
    
    @Test
    public void queryCertificateByApplicationRegistId() {
        List<RecruitApplicationCertificate> certificates = recruitApplicationCertificateService.queryCertificateByApplicationRegistId(1);
        for(RecruitApplicationCertificate certificate : certificates) {
            System.out.print(certificate.getId() + "--" + certificate.getCertificateName());
            System.out.println();
        }
    }
    
}
