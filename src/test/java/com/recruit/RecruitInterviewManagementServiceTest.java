/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: RecruitInterviewManagementServiceTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月13日 下午5:57:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import cn.sigo.recruit.model.RecruitInterviewPeople;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.RecruitInterviewManagement;
import cn.sigo.recruit.model.req.ReqInterviewManagementModel;
import cn.sigo.recruit.service.RecruitInterviewManagementService;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class RecruitInterviewManagementServiceTest {
    
    @Autowired
    private RecruitInterviewManagementService recruitInterviewManagementService;
    
//    @Test
    public void queryByPhone() {
        String phone = "13000000000";
        RecruitInterviewManagement managent = recruitInterviewManagementService.queryByPhone(phone, "");
        assertNull(managent);
    }
    
//    @Test
    public void queryBeInterviewed() {
        List<RecruitInterviewPeople> lists = recruitInterviewManagementService.queryInterviews();
        assertEquals(4, lists.size());
    }
//    @Test
    public void queryInterviewManagents() {
        ReqInterviewManagementModel model = new ReqInterviewManagementModel();
        model.setCodeNumber("ms123456789");
        PageInfo<RecruitInterviewManagement> date = recruitInterviewManagementService.queryInterviewManagents(model, "mengzhixiang", "recruit-系统管理员");
        assertEquals(date.getTotal(), 0);
    }
    
    @Test
    public void modifyInterviewProcId() {
        boolean result = recruitInterviewManagementService.modifyInterviewProcId("112345678", 4);
        assertTrue(result);
    }

}
