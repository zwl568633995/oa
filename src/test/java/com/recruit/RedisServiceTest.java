package com.recruit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.service.RedisService;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class RedisServiceTest {
    @Autowired
    private RedisService redisService;

//    @Test
    public void remove() {
        redisService.remove("test:set");
    }

    @Test
    public void exists() {
        System.out.println(redisService.exists("list4"));
    }

}
