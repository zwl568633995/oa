package com.recruit;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;
import cn.sigo.recruit.model.User;
import cn.sigo.recruit.service.RedisService;

@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class TestRedis {

	@Autowired
	private RedisService redisService;

//    @Test
    public void setOrGet(){
    	redisService.remove("user");
//    	if(!redisService.exists("user")) {
//    		User u = (User) redisService.get("user", User.class);
//          User user = new User();
//        	user.setUserId(1);
//        	user.setPassword("123456");
//        	user.setUserName("test redis123");
//        	redisService.set("user", user);	
//        	u = (User) redisService.get("user", User.class);
//        	if(null != u) {
//        		System.out.println(111 + u.getUserName());
//        	}
//    	}
    	redisService.set("user", 123456);
    	redisService.get("user", Integer.class);
    	assertEquals(123456, redisService.get("user", Integer.class));
    	
    	redisService.remove("user");
		User u = (User) redisService.get("user", User.class);
		User user = new User();
		redisService.set("user", user);
		u = (User) redisService.get("user", User.class);
		if (null != u) {
			System.out.println(111 + u.getUsername());
		}
    }

    @Test
    public void pushOrRange() {
    	redisService.remove("strList");
    	if(!redisService.exists("strList")) {
        	List users = new ArrayList<User>();
        	User u = new User();
        	u.setUsername("111");
        	User u1 = new User();
        	u1.setUsername("222");
        	User u2 = new User();
        	u2.setUsername("333");
        	users.add(u);
        	users.add(u1);
        	users.add(u2);
        	redisService.push("strList", users);
    	}
    	List<Object> objs = redisService.range("strList", 0, -1, User.class);
    	for(Object obj : objs) {
    		User user = (User)obj;
    		System.out.println(user.getUsername());
    	}
    	redisService.remove("strList");
    	List strs = new ArrayList<String>();
    	strs.add("111");
    	strs.add("222");
    	strs.add("333");
    	strs.add("444");
    	redisService.push("strList", strs);
    	objs = redisService.range("strList", 0, -1, String.class);
    	for(Object obj : objs) {
    		System.out.println((String) obj);
    	}
    }
}
