/*
 * Copyright (C), 2002-2018, 南京视客网络科技有限公司
 * FileName: WorkTest.java
 * Author:   zhixiang.meng
 * Date:     2018年7月12日 上午10:19:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.recruit;

import java.util.List;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import cn.sigo.recruit.RecruitApplication;

/**
 * 〈一句话功能简述〉<br> 
 * 〈功能详细描述〉
 *
 * @author zhixiang.meng
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootTest(classes = RecruitApplication.class)
@RunWith(SpringRunner.class)
@Component
public class WorkBpmnTest {
    
    @Autowired
    private ProcessEngine engine ;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    
    
    @Test
    public void yesWork() {
        List<Task> tasks = taskService.createTaskQuery().taskCandidateUser("wuyicheng").list();
        if(!tasks.isEmpty()) {
            System.out.println(tasks.size());
        }
    }

}
